<?php

namespace App\Console\Commands;

use App\Models\File;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FilesCleanDB extends Command
{
    protected $signature = "files:clean:db
                            {--pretend : Do not actually delete anything}";

    protected $description = "Delete DB entries and stored files for unreferenced (tmp) files and images";

    private $pretend = false;

    public function handle(): void
    {
        if ($this->option("pretend")) {
            $this->pretend = true;
        }

        $fileQuery = File::where(
            "updated_at",
            "<",
            Carbon::now()->subDay()
        )->Where(
            fn($q) => $q
                ->whereDoesntHave("fileable")
                ->orWhereNull("fileable_type")
                ->orWhereNull("field")
                ->orWhere("field", "null")
        );

        $this->deleteDbEntries($fileQuery, "file");

        $imageQuery = Image::where(
            "updated_at",
            "<",
            Carbon::now()->subDay()
        )->Where(
            fn($q) => $q
                ->whereDoesntHave("imageable")
                ->orWhereNull("imageable_type")
                ->orWhereNull("field")
                ->orWhere("field", "null")
        );
        $this->deleteDbEntries($imageQuery, "image");
    }

    private function deleteDbEntries($query, $type): void
    {
        \Log::info(
            "Start deleting unreferenced db $type entries and stored file. Pretend: $this->pretend"
        );
        $count = $query->count();

        if ($this->pretend) {
            \Log::info("Would delete $count $type");
        } else {
            \Log::info("Deleting $count db $type entries.");

            $successCount = 0;
            $failureCount = 0;
            foreach ($query->cursor() as $entry) {
                try {
                    $entry->delete();
                    $successCount++;
                } catch (\Throwable $e) {
                    \Log::error(
                        "Could not delete $type: $entry->id $entry->path/$entry->filename"
                    );
                    $failureCount++;
                }
            }

            \Log::info(
                "Deleted $successCount $type entries. Failed deleting $failureCount $type entries."
            );
        }
    }
}
