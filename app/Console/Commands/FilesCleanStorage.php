<?php

namespace App\Console\Commands;

use App\Helpers\Path;
use App\Helpers\StorageHelper;
use App\Models\File;
use App\Models\Image;
use Illuminate\Console\Command;

class FilesCleanStorage extends Command
{
    protected $signature = "files:clean:storage
                            {--checkfiles : Check files in leaf directories}
                            {--pretend : Do not actually delete anything}";

    protected $description = "Delete stored files not referenced in any db entry";

    private bool $pretend = false;
    private bool $checkFiles = false;

    private array $directoriesToClean = ["/files", "/images", "/storage"];

    private array $directoriesToKeep = ["/storage/exports"];

    public function handle(): void
    {
        if ($this->option("pretend")) {
            $this->pretend = true;
        }

        if ($this->option("checkfiles")) {
            $this->checkFiles = true;
        }

        \Log::info("Start cleaning filesystem. Pretend:$this->pretend");

        foreach ($this->directoriesToClean as $directory) {
            \Log::info("Cleaning $directory");
            [$deletedDirectories, $total, $deletedFiles] = $this->clean(
                $directory
            );
            if ($this->pretend) {
                \Log::info(
                    "Would have deleted $deletedDirectories leaf directories out of $total and $deletedFiles files in $directory"
                );
            } else {
                \Log::info(
                    "Deleted $deletedDirectories leaf directories out of $total and $deletedFiles files in $directory"
                );
            }
        }
    }

    /**
     * Goes through every leaf directory in input $directory and deletes it if not referenced
     * in database.
     *
     * @return array [number of deleted directories, total of leaf directories]
     */
    private function clean(string $directory): array
    {
        $deletedDirectories = 0;
        $leafDirectories = 0;
        $deletedFiles = 0;

        $subdirs = \Storage::directories($directory);

        // only clean leaf directories
        if (empty($subdirs)) {
            if (
                !Image::where("path", "=", $directory)->exists() &&
                !File::where("path", "=", $directory)->exists()
            ) {
                if (!$this->pretend) {
                    $deleted = \Storage::deleteDirectory($directory);
                    if (!$deleted) {
                        \Log::warning("Failed deleting $directory");
                    }
                }
                return [1, 1, 0];
            } elseif ($this->checkFiles && starts_with($directory, "files")) {
                $files = \Storage::files($directory);
                foreach ($files as $file) {
                    if (
                        !File::where("path", "=", $directory)
                            ->where("filename", "=", basename($file))
                            ->exists()
                    ) {
                        if (!$this->pretend) {
                            $deleted = \Storage::delete($file);
                            if (!$deleted) {
                                \Log::warning("Failed deleting $file");
                            }
                        }
                        $deletedFiles++;
                    }
                }
            }

            return [0, 1, $deletedFiles];
        }

        foreach ($subdirs as $subdir) {
            if (in_array(Path::asAbsolute($subdir), $this->directoriesToKeep)) {
                \Log::info("Skipping $subdir");
                continue;
            }

            [
                $newDeletedDirectories,
                $newLeafDirectories,
                $newDeletedFiles,
            ] = $this->clean($subdir);
            $deletedDirectories += $newDeletedDirectories;
            $leafDirectories += $newLeafDirectories;
            $deletedFiles += $newDeletedFiles;
        }

        return [$deletedDirectories, $leafDirectories, $deletedFiles];
    }
}
