<?php

namespace App\Console\Commands;

use App\Helpers\StorageHelper;
use App\Models\File;
use App\Models\Image;
use Illuminate\Console\Command;

class FilesListMissing extends Command
{
    protected $signature = "files:list:missing
                            {--delete : delete db entries for missing files and images}";

    protected $description = "Lists missing files and images in the DB";
    private $verbose = false;
    private $delete = false;

    public function handle(): void
    {
        if ($this->option("verbose")) {
            $this->verbose = true;
        }
        if ($this->option("delete")) {
            $this->delete = true;
        }

        \Log::info("Start listing missing files. Delete: $this->delete");

        $missingFiles = $this->listMissingFiles(File::cursor(), "file");
        $missingImages = $this->listMissingFiles(Image::cursor(), "image");

        $countMissingFiles = count($missingFiles);
        $totalFiles = File::count();
        $countMissingImages = count($missingImages);
        $totalImages = Image::count();

        \Log::info(
            "Missing a total of $countMissingImages images of $totalImages and $countMissingFiles files of $totalFiles."
        );
    }

    private function listMissingFiles($query, $itemType): array
    {
        $missingPaths = [];
        foreach ($query as $file) {
            if (
                !\Storage::exists(
                    $file->path . DIRECTORY_SEPARATOR . $file->filename
                )
            ) {
                $missingPaths[] = $file->path;
                if ($this->verbose) {
                    \Log::info("Missing {$file->path} for $itemType $file->id");
                }
                if ($this->delete) {
                    $file->delete();
                    \Log::info("Delete $itemType $file->id");
                }
            }
        }
        return $missingPaths;
    }
}
