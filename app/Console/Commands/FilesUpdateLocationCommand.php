<?php

namespace App\Console\Commands;

use App\Enums\FileMoveResult;
use App\Models\File;
use App\Models\Image;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class FilesUpdateLocationCommand extends Command
{
    protected $signature = "files:update:location
                            {--images : moves images if set, files if not}";

    protected $description = "Runs image::updateFilePath and file::updateFilePath on all files and images";

    public function handle(): void
    {
        if ($this->option("images")) {
            self::updateItemPaths(Image::with("imageable"), "image");
        } else {
            self::updateItemPaths(File::with("fileable"), "file");
        }
    }

    public static function updateItemPaths(Builder $query, string $type): void
    {
        \Log::info("Start batch update $type paths");
        $countUnchanged = 0;
        $countMoved = 0;
        $countFailed = 0;
        $countCopied = 0;
        $countProcessed = 0;
        $total = $query->count();

        foreach ($query->cursor() as $file) {
            if ($countProcessed % 100 === 0 && $countProcessed > 0) {
                $progress = ($countProcessed / $total) * 100;
                \Log::info(
                    sprintf(
                        "Path update progress: %.2f%%, $countMoved moved, $countCopied copied, $countUnchanged unchanged, $countFailed failed.",
                        $progress
                    )
                );
            }

            $countProcessed++;
            try {
                $moveResult = $file->updateFilePath();

                match ($moveResult) {
                    FileMoveResult::unchanged => $countUnchanged++,
                    FileMoveResult::moved => $countMoved++,
                    FileMoveResult::copied => $countCopied++,
                    FileMoveResult::failed => $countFailed++,
                };
            } catch (\Throwable $e) {
                \Log::warning(
                    "Failed updating path for '$file->url': {$e->getMessage()}"
                );
                $countFailed++;
                continue;
            }
            $file->save();
        }

        \Log::info(
            "$type path update results: $countMoved moved, $countCopied moved without deleting original, $countUnchanged unchanged, $countFailed failed."
        );
    }
}
