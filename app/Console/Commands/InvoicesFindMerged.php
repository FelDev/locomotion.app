<?php

namespace App\Console\Commands;

use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Log;

class InvoicesFindMerged extends Command
{
    protected $signature = "invoices:find:merged";

    /*
     * There used to be a bug where some invoices were reused, resulting in
     * items with different dates in the same invoice.
     *
     * Difference between creation times must be one second or less.
     */
    public function handle()
    {
        $invoices = Invoice::orderBy("id", "asc")->cursor();

        $mergedInvoicesCount = 0;
        foreach ($invoices as $invoice) {
            $invoiceCreationDate = new CarbonImmutable($invoice->created_at);

            $invoiceItems = $invoice->billItems;

            foreach ($invoiceItems as $item) {
                $itemCreationDate = new CarbonImmutable($item->created_at);

                if (
                    abs(
                        $invoiceCreationDate->diffInSeconds($itemCreationDate)
                    ) > 1
                ) {
                    $mergedInvoicesCount++;
                    Log::info(
                        "Invoice {$invoice->id} has bill items with different dates."
                    );
                    break;
                }
            }
        }

        Log::info("Found {$mergedInvoicesCount} merged invoices.");
    }
}
