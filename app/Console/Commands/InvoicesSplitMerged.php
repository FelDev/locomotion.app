<?php

namespace App\Console\Commands;

use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Log;

class InvoicesSplitMerged extends Command
{
    protected $signature = "invoices:split:merged";

    public function handle()
    {
        $invoicesToSplit = [
            881 => ["item_ids" => [[1136], [1343, 1344]]],
            892 => ["item_ids" => [[1151], [1328, 1329]]],
            893 => ["item_ids" => [[1152], [1321, 1322]]],
            945 => ["item_ids" => [[1244], [2012]]],
            956 => ["item_ids" => [[1259], [1272, 1273]]],
            1001 => ["item_ids" => [[1361], [2529, 2530, 2531, 2532]]],
            1511 => ["item_ids" => [[1915], [1959, 1960]]],
            1520 => ["item_ids" => [[1927], [2188]]],
            1521 => ["item_ids" => [[1928], [2030]]],
            1522 => ["item_ids" => [[1929], [1930, 1931, 1932]]],
            1551 => ["item_ids" => [[1970], [2393]]],
            1552 => ["item_ids" => [[1971], [2560]]],
            1553 => ["item_ids" => [[1972], [1984]]],
            1555 => ["item_ids" => [[1974], [2571, 2572]]],
            1557 => ["item_ids" => [[1976], [2035]]],
            1558 => ["item_ids" => [[1977], [2014]]],
            1559 => ["item_ids" => [[1978], [2622, 2623]]],
            1560 => ["item_ids" => [[1979], [2443, 2444]]],
            1587 => ["item_ids" => [[2015], [2032, 2033, 2034]]],
            1590 => ["item_ids" => [[2018], [2559]]],
            1685 => ["item_ids" => [[2138], [2987, 2988]]],
            1705 => ["item_ids" => [[2162], [3262, 3263]]],
            1708 => ["item_ids" => [[2165], [2197]]],
            1709 => ["item_ids" => [[2166], [2548, 2549]]],
            1712 => ["item_ids" => [[2169], [2203]]],
            1774 => ["item_ids" => [[2247], [2326]]],
            2051 => ["item_ids" => [[2581], [3032, 3033]]],
            2145 => ["item_ids" => [[2696], [2776, 2777, 2778, 2779]]],
            2146 => ["item_ids" => [[2697], [2745, 2746, 2747, 2748]]],
            2447 => ["item_ids" => [[3071], [3182, 3183]]],
        ];

        $invoiceIds = array_keys($invoicesToSplit);

        $invoices = Invoice::whereIn("id", $invoiceIds)
            ->orderBy("id", "asc")
            ->get();

        $shouldProceed = true;

        $invoiceCount = 0;
        $itemCount = 0;
        foreach ($invoices as $invoice) {
            $invoiceCount++;

            $items = $invoice->billItems;

            $mergedItemIds = array_merge(
                $invoicesToSplit[$invoice->id]["item_ids"][0],
                $invoicesToSplit[$invoice->id]["item_ids"][1]
            );

            // Sanity checks (no human typing mistake):
            // - No bill item is missing.
            // - All items belong to the invoices we pretend they blong to.
            // - All items have created_at not null.
            // - All items in new groups have less than one second difference.

            if (count($items) !== count($mergedItemIds)) {
                Log::error(
                    "Expected different number of bill items in invoice {$invoice->id}."
                );
                continue;
            }

            foreach ($items as $item) {
                $itemCount++;

                if (!in_array($item->id, $mergedItemIds)) {
                    Log::error(
                        "Bill item {$item->id} does not belong to invoice {$invoice->id}."
                    );
                    $shouldProceed = false;
                }

                if (!$item->created_at) {
                    Log::error("Bill item {$item->id} has NULL created_at.");
                    $shouldProceed = false;
                }
            }

            // Split items in two groups.
            $itemGroups = [];
            foreach (
                $invoicesToSplit[$invoice->id]["item_ids"]
                as $groupIndex => $groupIds
            ) {
                foreach ($groupIds as $id) {
                    foreach ($items as $item) {
                        if ($item->id == $id) {
                            //                          $itemGroups[$groupIndex][$id] = $item;
                            $itemGroups[$groupIndex][] = $item;
                        }
                    }
                }
            }

            if (!isset($itemGroups[1])) {
                Log::error(
                    "Items for new invoice not found. Split must have been done already."
                );
                $shouldProceed = false;
                break;
            }

            // Check creation times within groups. That validates that we did not make a human mistake creating groups.
            foreach ($itemGroups as $itemGroup) {
                $firstItemCreationTime = null;
                foreach ($itemGroup as $item) {
                    if (!$firstItemCreationTime) {
                        $firstItemCreationTime = new CarbonImmutable(
                            $item->created_at
                        );
                    } elseif (
                        abs(
                            $firstItemCreationTime->diffInSeconds(
                                new CarbonImmutable($item->created_at)
                            )
                        ) > 1
                    ) {
                        Log::error(
                            "Items in group have a more than one second difference for invoice {$invoice->id}."
                        );
                        $shouldProceed = false;
                    }
                }
            }

            // Store groups in invoicesToSplit, parallel to item_ids.
            $invoicesToSplit[$invoice->id]["items"] = $itemGroups;
        }

        if (!$shouldProceed) {
            Log::error("At least one error found. Will not proceed.");
            return;
        }

        foreach ($invoices as $invoice) {
            // Create invoice from second group.
            $newInvoiceItems = $invoicesToSplit[$invoice->id]["items"][1];

            // Set invoice time to the time of any item.
            $newInvoiceTime = new CarbonImmutable(
                $newInvoiceItems[0]->created_at
            );

            // Similar to User::createInvoice, but we set the date and other fields.
            $newInvoice = new Invoice();
            $newInvoice->period = $newInvoiceTime
                ->locale("fr_FR")
                ->format("m/Y");
            $newInvoice->user_id = $invoice->user_id;
            // payment_method_id remains null for loan invoices (all cases).
            $newInvoice->paid_at = $newInvoiceTime;
            $newInvoice->created_at = $newInvoiceTime;
            $newInvoice->updated_at = $newInvoiceTime;
            // type, balance_before, balance_after remain null for these "old" invoices.
            $newInvoice->save();

            // Move bill items to the new invoice.
            $newInvoice->billItems()->saveMany($newInvoiceItems);

            // Payment time was null before the invoice was reused.
            $invoice->paid_at = null;
            $invoice->save();

            Log::info("Invoice {$newInvoice->id} created from {$invoice->id}.");
        }

        Log::Info("Invoice count: $invoiceCount");
        Log::Info("Item count: $itemCount");
    }
}
