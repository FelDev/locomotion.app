<?php

namespace App\Console\Commands;

use App\Facades\Noke;
use App\Models\User;
use Carbon\CarbonImmutable;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Log;

class NokeSyncLoans extends Command
{
    protected $signature = 'noke:sync:loans
                            {--pretend : Do not call remote API}';

    protected $description = "Synchronize NOKE loans";

    private $pretend = false;

    public function handle(): void
    {
        Log::info("Fetching locks...");
        $locks = Noke::fetchLocks();
        Log::info("Fetching groups...");
        $groups = Noke::fetchGroups();
        $groupsIndex = [];
        foreach ($groups as $group) {
            $groupsIndex[$group->name] = $group;
        }

        foreach ($locks as $lock) {
            $this->updateLockGroup($lock, $groupsIndex);
        }

        Log::info("Done.");
    }

    private function updateLockGroup($lock, $groupsIndex): void
    {
        $mac = $lock->macAddress;
        $groupName = "API $mac";
        Log::info("Updating group {$groupName} users.");
        if (!isset($groupsIndex[$groupName])) {
            Log::error("No noke group with expected name: $groupName");
            return;
        }

        $nokeGroup = $groupsIndex[$groupName];
        $nokeGroup->userIds = self::computeGroupUsers($mac);
        $nokeGroup->lockIds = [$lock->id];

        try {
            $currentUserIds = array_map(
                fn($u) => $u->id,
                Noke::getGroupProfile($nokeGroup->id)->users ?? []
            );
            Log::info("Group $groupName has " . join(",", $currentUserIds));
        } catch (\Throwable $e) {
            Log::error($e->getMessage(), $e->getTrace());
            return;
        }

        if (
            empty(array_diff($nokeGroup->userIds, $currentUserIds)) &&
            count($nokeGroup->userIds) === count($currentUserIds)
        ) {
            Log::info("Group $groupName users unchanged.");
            return;
        }

        $userIds = join(",", $nokeGroup->userIds);
        Log::info("Updating $groupName with $userIds");

        if ($this->pretend) {
            return;
        }

        try {
            Noke::updateGroup($nokeGroup);
        } catch (\Throwable $e) {
            Log::error($e->getMessage(), $e->getTrace());
        }
    }

    private static function computeGroupUsers($mac): array
    {
        // 40842 is userId for api@locomotion.app
        $userIds = [intval(config("services.noke.api_user_id"))];

        $allowedUsers = self::getBorrowersForLoansWithPadlockMac($mac)->get();

        foreach ($allowedUsers as $user) {
            if (!!$user->noke_id) {
                if (is_numeric($user->noke_id)) {
                    $userIds[] = (int) $user->noke_id;
                } else {
                    Log::error("Unexpected noke id : $user->noke_id");
                }
            } else {
                Log::warning("User does not have noke id: $user->id!");
                try {
                    $nokeUser = Noke::createUser($user);
                    $userIds[] = $nokeUser->id;
                } catch (\Exception) {
                    Log::error("Could not create Noke user for user $user->id");
                }
            }
        }

        return array_values(array_unique($userIds));
    }

    /*
       Retrieve loans that are
       - active (not canceled),
       - with departure in less than 15 minutes,
       - with return 15 minutes ago or later,
       - with completed intention,
       - with padlock having the given MAC address.
     */
    public static function getBorrowersForLoansWithPadlockMac($mac): Builder
    {
        // Add a delay so a user may open the padlock just some time before the
        // scheduled departure time and just some time after the scheduled
        // return time (accounting for extensions).
        $loanStartDelay = CarbonImmutable::now()->addMinutes(15);
        $loanEndDelay = CarbonImmutable::now()->subMinutes(15);

        return User::whereHas(
            "borrower.loans",
            fn(Builder $loan) => $loan
                ->where("status", "!=", "canceled")
                ->where("departure_at", "<=", $loanStartDelay)
                ->where("actual_return_at", ">=", $loanEndDelay)
                ->whereHas("intention", function ($q) {
                    return $q->where("status", "completed");
                })
                ->whereHas(
                    "loanable.padlock",
                    fn($q) => $q->where("mac_address", $mac)
                )
        );
    }
}
