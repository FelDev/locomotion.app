<?php

namespace App\Console\Commands;

use App\Facades\Noke;
use App\Models\User;
use Illuminate\Console\Command;
use Log;

class NokeSyncUsers extends Command
{
    protected $signature = 'noke:sync:users
                            {--pretend : Do not call remote API}';

    protected $description = "Synchronize NOKE users";

    private $pretend = false;

    public function handle()
    {
        Log::info("Creating remote users...");
        $this->createUsers();

        Log::info("Done.");
    }

    public static function getLocalUsers()
    {
        return User::whereHas("borrower")
            ->whereHas(
                "approvedCommunities",
                fn($q) => $q->where("uses_noke", true)
            )
            ->select(
                "id",
                "email",
                "name",
                "last_name",
                "phone",
                "is_smart_phone",
                "noke_id"
            )
            ->get();
    }

    private function createUsers()
    {
        $users = $this->getLocalUsers();
        foreach ($users as $user) {
            if (!!$user->noke_id) {
                continue;
            }

            Log::info("Creating noke user {$user->email}.");

            if ($this->pretend) {
                continue;
            }

            try {
                Noke::createUser($user);
            } catch (\Throwable $e) {
                Log::error($e->getMessage(), $e->getTrace());
            }
        }
    }
}
