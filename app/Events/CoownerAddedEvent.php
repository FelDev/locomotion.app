<?php

namespace App\Events;

use App\Models\Coowner;
use App\Models\Loanable;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class CoownerAddedEvent
{
    use Dispatchable;

    /**
     * @var User
     */
    public $adder;
    /**
     * @var Loanable
     */
    public $loanable;
    /**
     * @var Coowner
     */
    public $coowner;

    public function __construct(
        User $adder,
        Loanable $loanable,
        Coowner $coowner
    ) {
        $this->adder = $adder;
        $this->loanable = $loanable;
        $this->coowner = $coowner;
    }
}
