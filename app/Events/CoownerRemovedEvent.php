<?php

namespace App\Events;

use App\Models\Coowner;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

class CoownerRemovedEvent
{
    use Dispatchable;

    /**
     * @var Coowner
     */
    public $coowner;
    /**
     * @var User
     */
    public $remover;

    public function __construct(Coowner $coowner, User $remover)
    {
        $this->coowner = $coowner;
        $this->remover = $remover;
    }
}
