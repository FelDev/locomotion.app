<?php

namespace App\Events;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;

/**
 * Event emitted when the loan is updated and requires re-approval from (co-)owners
 */
class LoanUpdatedEvent
{
    use Dispatchable;

    public function __construct(public User $updater, public Loan $loan)
    {
    }
}
