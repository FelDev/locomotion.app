<?php

namespace App\Events;

use App\Models\Pivots\CommunityUser;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RegistrationSubmittedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public function __construct(public CommunityUser $communityUser)
    {
    }
}
