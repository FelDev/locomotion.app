<?php

namespace App\Exports;

use App\Http\Requests\ParseFieldsHelper;
use App\Models\User;
use App\Repositories\RestRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BaseExport
{
    /**
     * Columns to export. E.g.:
     *
     * ```
     * [
     *   fooCollection.0.barCollection.0.bazField,
     *   fooCollection.0.barCollection.1.bazField,
     *   fooCollection.0.quuxField,
     *   fredField,
     * ]
     * ```
     */
    protected array $columns;

    /**
     * Tree reprensentation of fields to export, typically provided by the client. E.g.:
     *
     * ```
     * [
     *   fooCollection => [
     *     barCollection => [
     *       "bazField" => "bazField"
     *     ],
     *     "quuxField => "quuxField",
     *   ],
     *   "fredField" => "fredField",
     * ]
     * ```
     */
    protected array $fieldTree;

    /**
     * Tree representation of export columns. E.g.:
     *
     * ```
     * [
     *   fooCollection => [
     *     0 => [
     *       barCollection => [
     *         0 => [
     *           "bazField" => "bazField"
     *         ],
     *         1 => [
     *           "bazField" => "bazField"
     *         ],
     *       ],
     *       "quuxField => "quuxField",
     *     ],
     *     "fredField" => "fredField"
     * ],
     * ```
     */
    private array $columnTree;

    public function __construct(
        array $fieldTree,
        array $columns,
        protected $model,
        protected array $params,
        protected User $user
    ) {
        $this->columns = $columns;
        $this->fieldTree = $fieldTree;
        $this->columnTree = ParseFieldsHelper::parseFieldList($this->columns);
    }

    public static function fromFields(
        string $fieldList,
        $model,
        $request
    ): BaseExport {
        $fieldTree = ParseFieldsHelper::parseFieldList($fieldList);
        $columns = self::linearizeFieldTree($fieldTree, $model);
        $params = RestRepository::parseRequestParams($request);
        $user = $request->user();
        return new static($fieldTree, $columns, $model, $params, $user);
    }

    public static function fromColumns(
        array $columns,
        $model,
        $request
    ): BaseExport {
        $fieldTree = self::columnsToFieldTree($columns);
        $params = RestRepository::parseRequestParams($request);
        $user = $request->user();
        return new static($fieldTree, $columns, $model, $params, $user);
    }

    public function headings(): array
    {
        return $this->columns;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function jobChunkCount(): int
    {
        return 15;
    }

    public function getJobSize(): int
    {
        return $this->jobChunkCount() * $this->chunkSize();
    }

    /**
     * Required by FromQuery interface. This is the query used to generate the export.
     */
    public function query(): Builder
    {
        $query = RestRepository::for($this->model)->buildGetQuery(
            $this->params,
            $this->user,
            $this->fieldTree
        );

        // Ensure order exists if not set
        if (empty($query->getQuery()->orders)) {
            $query->orderBy($this->model->getKeyName());
        }

        return $query;
    }

    public function map($row): array
    {
        $values = [];
        self::extractColumnValues($row, $this->columnTree, $values);
        return $values;
    }

    public static function countLeaves(array $array): int
    {
        $count = 0;
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $count += self::countLeaves($value);
            } else {
                $count += 1;
            }
        }
        return $count;
    }

    /**
     * Appends to $values the $item properties (direct, from relationships or computed)
     * for each leaf in the $columnTree.
     *
     * This is called recursively for each node in the $columnTree, which lets us serialize
     * each node once, gaining speed and reducing memory usage.
     */
    public static function extractColumnValues(
        $item,
        $columnTree,
        &$values
    ): void {
        // If we've reached a non-existant relation from the root item, push as many nulls as
        // there are leaves in the tree
        if (!$item) {
            $columnCount = self::countLeaves($columnTree);
            $values = array_pad($values, count($values) + $columnCount, null);
            return;
        }

        // If we're iterating on a collection extract values for each item
        if (is_a($item, Collection::class)) {
            foreach ($columnTree as $index => $subTree) {
                self::extractColumnValues(
                    $item[$index] ?? null,
                    $subTree,
                    $values
                );
            }
            return;
        }

        // Otherwise we get values from attributes or relations
        $fields = array_keys($columnTree);
        $itemAttributes = self::toPartialArray($item, $fields);
        $pivotAttributes = [];
        if (isset($item->pivot)) {
            // add pivot items
            $pivotAttributes = self::toPartialArray($item->pivot, $fields);
        }

        foreach ($columnTree as $column => $subTree) {
            if (!is_array($subTree)) {
                // We want to push as many values as there are leaves in the $columnTree
                $value =
                    $itemAttributes[$column] ??
                    ($pivotAttributes[$column] ?? null);
                $values[] = self::valueToString($value);
                continue;
            }
            $relation = Str::camel($column);
            if (isset($item->{$relation})) {
                self::extractColumnValues(
                    $item->{$relation},
                    $subTree,
                    $values
                );
                continue;
            }

            if (isset($item->pivot?->{$relation})) {
                self::extractColumnValues(
                    $item->pivot->{$relation},
                    $subTree,
                    $values
                );
                continue;
            }

            // could not find relation
            self::extractColumnValues(null, $subTree, $values);
        }
    }

    private static function valueToString(mixed $value): string
    {
        if (is_null($value)) {
            return "";
        }

        if (is_array($value) || is_object($value)) {
            return json_encode($value);
        }

        return (string) $value;
    }

    private static function toPartialArray($item, array $fields)
    {
        // Append computed fields that we're looking for
        $item->append(array_intersect($fields, $item->computed));
        // Only serialize the attributes we're looking for. This can save a huge amount of memory,
        // since we're not casting community->areas when exporting users for example.
        $item->setVisible($fields);
        return $item->attributesToArray();
    }

    /**
     * Converts columns list
     *
     * ```
     * [
     *    fooCollection.0.barCollection.0.baz,
     *    fooCollection.0.barCollection.1.baz
     *    fooCollection.quux,
     *    fred
     * ]
     * ```
     * to fields tree
     *
     * ```
     * [
     *   fooCollection => [
     *     barCollection => [
     *       "baz" => "baz"
     *     ],
     *     "quux => "quux",
     *   ],
     *   "fred" => "fred",
     * ]
     * ```
     *
     */
    public static function columnsToFieldTree(array $columns): array
    {
        $relationString = implode(
            ",",
            array_unique(
                array_map(
                    // Remove collection access indexes
                    fn($column) => preg_replace("/\.\d+/", "", $column),
                    $columns
                )
            )
        );
        return ParseFieldsHelper::parseFieldList($relationString);
    }

    /**
     * Transforms a fieldTree into a flat array of columns, enumerating collection items.
     *
     * E.g.
     * ```
     * [
     *      fooCollection => [
     *          barCollection => [
     *              "bazField"
     *          ],
     *          "quuxField"
     *      ],
     *      "fredField"
     * ]
     * ```
     *
     * could  become
     *
     * ```
     * [
     *      fooCollection.0.barCollection.0.bazField,
     *      fooCollection.0.barCollection.1.bazField,
     *      fooCollection.0.barCollection.2.bazField,
     *      fooCollection.0.quuxField,
     *      fooCollection.1.barCollection.0.bazField,
     *      fooCollection.1.barCollection.1.bazField,
     *      fooCollection.1.barCollection.2.bazField,
     *      fooCollection.1.quuxField,
     *      fredField,
     * ]
     * ```
     */
    protected static function linearizeFieldTree(
        $fieldTree,
        $model,
        $parent = null
    ): array {
        $explodedFields = [];
        $collectionsCount = [];
        $collectionFields = [];

        foreach ($fieldTree as $field => $subTree) {
            // Direct field
            if (!is_array($subTree)) {
                $explodedFields[] = $subTree;
                continue;
            }

            $processed = self::linearizeRelationFields(
                $field,
                $model,
                $subTree,
                $explodedFields,
                $collectionsCount,
                $collectionFields
            );

            if (
                !$processed &&
                !!$parent &&
                ($pivotClass = $parent->getPivotClass())
            ) {
                $pivot = new $pivotClass();

                self::linearizeRelationFields(
                    $field,
                    $pivot,
                    $subTree,
                    $explodedFields,
                    $collectionsCount,
                    $collectionFields
                );
            }
        }

        foreach (array_keys($collectionsCount) as $name) {
            for ($i = 0; $i < $collectionsCount[$name]; $i++) {
                foreach ($collectionFields[$name] as $field) {
                    $explodedFields[] = "$name.$i.$field";
                }
            }
        }

        return $explodedFields;
    }

    private static function linearizeRelationFields(
        string $relation,
        mixed $model,
        array $subTree,
        mixed &$explodedFields,
        array &$collectionsCount,
        array &$collectionsFields
    ): bool {
        if ($model->isItemRelation($relation)) {
            $explodedFields = array_merge(
                $explodedFields,
                ParseFieldsHelper::joinFieldsTree($subTree, $relation)
            );
            return true;
        } elseif ($model->isCollectionRelation($relation)) {
            self::linearizeCollectionFields(
                $relation,
                $model,
                $subTree,
                $collectionsCount,
                $collectionsFields
            );
            return true;
        }
        return false;
    }

    private static function linearizeCollectionFields(
        string $relationName,
        $model,
        array $subTree,
        array &$collectionCount,
        array &$collectionFields
    ): void {
        $relation = Str::camel($relationName);
        if (!isset($collectionCount[$relationName])) {
            // Computes the count of relations from the $model to the $relation
            // This doesn't account for the nesting of $model. i.e. if we're already in
            // a relation like `$user->community->tags` we will get the count of `$community->tags`
            // which could be bigger than the tags actually used in $user communities.
            //
            // Optimising this is hard, and it doesn't happen often enough (requiring an export of a
            // model of with two or more one-to-many relations fields) to warrant the effort
            // currently.
            $relationCountName = "{$relationName}_count";
            $relationCount = $model
                ->newQuery()
                ->withCount($relation)
                ->pluck($relationCountName)
                ->max();

            $collectionCount[$relationName] = $relationCount;
        }

        if (!isset($collectionFields[$relationName])) {
            $collectionFields[$relationName] = [];
        }

        $subfields = self::linearizeFieldTree(
            $subTree,
            $model->{$relation}()->getRelated(),
            $model->{$relation}()
        );
        $collectionFields[$relationName] = $subfields;
    }
}
