<?php

namespace App\Exports;

class LoanableExport extends BaseExport
{
    public static function fromFields(
        string $fieldList,
        $model,
        $request
    ): BaseExport {
        return parent::fromColumns(
            [
                "id",
                "name",
                "type",
                "comments",
                "instructions",
                "location_description",
                "position",
                "owner.id",
                "owner.user.id",
                "owner.user.name",
                "owner.user.last_name",
                "owner.user.communities.0.id",
                "owner.user.communities.0.name",
                "car_insurer",
            ],
            $model,
            $request
        );
    }
}
