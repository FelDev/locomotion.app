<?php

namespace App\Facades;

use App\Services\FakeNokeService;
use App\Services\NokeService;
use Illuminate\Support\Facades\Facade;

/** @see NokeService */
class Noke extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "noke";
    }

    public static function ForTest()
    {
        static::swap($fake = new FakeNokeService(false));

        return $fake;
    }
}
