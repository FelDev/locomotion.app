<?php

namespace App\Helpers;

class Path
{
    public static function asAbsolute(string $path): string
    {
        return \Str::start($path, DIRECTORY_SEPARATOR);
    }

    public static function arrayDiffAbsolute(
        array $paths,
        array ...$otherPaths
    ): array {
        $pathsToRemove = [];
        foreach ($otherPaths as $pathArray) {
            $pathsToRemove[] = array_map(Path::asAbsolute(...), $pathArray);
        }

        return array_diff(
            array_map(Path::asAbsolute(...), $paths),
            ...$pathsToRemove
        );
    }

    public static function join(string $path, string $rest): string
    {
        return rtrim($path, DIRECTORY_SEPARATOR) . self::asAbsolute($rest);
    }

    public static function build(string ...$parts): string
    {
        return join(
            DIRECTORY_SEPARATOR,
            array_map(
                fn($part) => ltrim(
                    rtrim($part, DIRECTORY_SEPARATOR),
                    DIRECTORY_SEPARATOR
                ),
                $parts
            )
        );
    }

    public static function root(string $path): string
    {
        $path = ltrim($path, DIRECTORY_SEPARATOR);
        $parts = explode(DIRECTORY_SEPARATOR, $path);
        if (count($parts) < 2) {
            return "";
        }

        return $parts[0];
    }

    public static function isSameAbsolute(
        string $source,
        string $destination
    ): bool {
        return self::asAbsolute($source) === self::asAbsolute($destination);
    }
}
