<?php

namespace App\Helpers;

use App\Enums\FileMoveResult;
use Storage;

class StorageHelper
{
    public static function copy(
        string $source,
        string $destination
    ): FileMoveResult {
        if (Path::isSameAbsolute($source, $destination)) {
            return FileMoveResult::unchanged;
        }

        // Not overwriting
        if (Storage::exists($destination)) {
            return FileMoveResult::unchanged;
        }

        $moved = Storage::copy($source, $destination);

        if ($moved) {
            return FileMoveResult::copied;
        }

        // Sometimes moving fails with the s3 adapter with a 404 for the file to be
        // created. It's fairly mysterious. Work around: fetching the file and creating a new one.
        $data = Storage::get($source);
        if (!$data) {
            return FileMoveResult::failed;
        }

        $moved = Storage::put($destination, $data);

        if ($moved) {
            return FileMoveResult::copied;
        }
        return FileMoveResult::failed;
    }

    public static function move(
        string $source,
        string $destination
    ): FileMoveResult {
        if (Path::isSameAbsolute($source, $destination)) {
            return FileMoveResult::unchanged;
        }

        if (!Storage::exists($destination)) {
            // We use our copy to fall back to fetching and storing in case the Storage::copy call fails
            // In the background, Storage::move also simply does a copy followed by a delete.
            $copied = self::copy($source, $destination);
            if ($copied === FileMoveResult::failed) {
                return FileMoveResult::failed;
            }
        } else {
            // File already existed at destination
            if (!Storage::exists($source)) {
                return FileMoveResult::unchanged;
            }
        }

        if (!Storage::delete($source)) {
            \Log::warning(
                "Failed deleting original $source after copying it to $destination"
            );
            return FileMoveResult::copied;
        }
        return FileMoveResult::moved;
    }

    public static function moveDirectoryFiles(
        string $sourceDir,
        string $destDir,
        string $originalName,
        string $newName
    ): FileMoveResult {
        if (
            Path::isSameAbsolute($sourceDir, $destDir) &&
            $originalName === $newName
        ) {
            return FileMoveResult::unchanged;
        }

        $files = Storage::files($sourceDir);
        $movedFiles = [];

        $allFilesMoveResult = FileMoveResult::unchanged;

        foreach ($files as $filePath) {
            $filename = basename($filePath);

            $newFileName = str_replace($originalName, $newName, $filename);

            $newFilePath = Path::join($destDir, $newFileName);
            $moveResult = self::move($filePath, $newFilePath);
            if ($moveResult === FileMoveResult::failed) {
                // Attempt to move back the files that have been moved
                \Log::warning(
                    "Moving directories failed. Attempting to move back files from $destDir to $sourceDir."
                );
                foreach ($movedFiles as $originalFile => $movedFile) {
                    if (
                        self::move($movedFile, $originalFile) ===
                        FileMoveResult::failed
                    ) {
                        \Log::error(
                            "Failed moving back $movedFile to $originalFile."
                        );
                    }
                }

                throw new \Exception(
                    "Failed moving files from $sourceDir to $destDir"
                );
            }

            // As long as one file is copied rather than moved, the source directory will not be
            // fully empty, which we signal by returning the copied state.
            if ($allFilesMoveResult !== FileMoveResult::copied) {
                $allFilesMoveResult = $moveResult;
            }

            $movedFiles[$filePath] = $newFilePath;
        }

        return $allFilesMoveResult;
    }
}
