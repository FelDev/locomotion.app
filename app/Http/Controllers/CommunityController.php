<?php

namespace App\Http\Controllers;

use App\Events\RegistrationRejectedEvent;
use App\Exports\CommunitiesExport;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Community\CommunityUserTagRequest;
use App\Http\Requests\Community\CreateRequest;
use App\Http\Requests\Community\DestroyRequest;
use App\Http\Requests\Community\UpdateRequest;
use App\Models\Community;
use App\Models\User;
use App\Repositories\CommunityRepository;
use App\Repositories\UserRepository;
use Illuminate\Validation\ValidationException;

class CommunityController extends RestController
{
    public function __construct(
        CommunityRepository $repository,
        Community $model,
        UserController $userController,
        UserRepository $UserRepository
    ) {
        $this->repo = $repository;
        $this->model = $model;
        $this->userController = $userController;
        $this->userRepo = $UserRepository;
    }

    public function create(CreateRequest $request)
    {
        $item = parent::validateAndCreate($request);
        return $this->respondWithItem($request, $item, 201);
    }

    public function update(UpdateRequest $request, $id)
    {
        $item = parent::validateAndUpdate($request, $id);
        return $this->respondWithItem($request, $item);
    }

    public function destroy(DestroyRequest $request, $id)
    {
        return parent::validateAndDestroy($request, $id);
    }

    public function indexUsers(Request $request, $id)
    {
        $community = $this->repo->find(
            $request->redirectAuth(Request::class),
            $id
        );

        $request->merge(["communities.id" => $id]);

        return $this->userController->index($request);
    }

    public function retrieveUsers(Request $request, $id, $userId)
    {
        $community = $this->repo->find($request, $id);

        $request->merge(["communities.id" => $id]);

        return $this->userController->retrieve($request, $userId);
    }

    public function createUsers(Request $request, $id)
    {
        $community = $this->repo->find(
            $request->redirectAuth(Request::class),
            $id
        );

        $userId = $request->get("id");
        $user = $this->userRepo->find(
            $request->redirectAuth(Request::class),
            $userId
        );

        if ($community->users->where("id", $userId)->isEmpty()) {
            $community->users()->attach($userId);

            return $this->respondWithItem($request, $user);
        }

        return $this->respondWithItem(
            $request,
            $community->users->where("id", $userId)->first()
        );
    }

    public function updateUsers(Request $request, $id, $userId)
    {
        $community = $this->repo->find(
            $request->redirectAuth(Request::class),
            $id
        );

        $user = $this->userRepo->find(
            $request->redirectAuth(Request::class),
            $userId
        );

        if ($community->users->where("id", $userId)->isEmpty()) {
            return ErrorResponse::withMessage(null, 404);
        }

        $this->userRepo->update($request, $userId, $request->json()->all());

        return $this->respondWithItem(
            $request,
            $community
                ->users()
                ->where("users.id", $userId)
                ->first()
        );
    }

    public function destroyUsers(Request $request, $communityId, $userId)
    {
        $community = $this->repo->find(
            $request->redirectAuth(Request::class),
            $communityId
        );
        $user = $this->userRepo->find(
            $request->redirectAuth(Request::class),
            $userId
        );

        if ($community->users->where("id", $userId)->isEmpty()) {
            return ErrorResponse::withMessage(null, 404);
        }

        $wasApproved = !!$community->users()->find($userId)->pivot->approved_at;
        $community->users()->detach($user);

        if (!$wasApproved) {
            event(new RegistrationRejectedEvent($user, $community));
        }

        return $this->respondWithItem($request, $user);
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
                "chat_group_url" => "",
                "description" => "",
                "long_description" => "",
                "starting_guide_url" => "",
                "area" => [],
                "type" => "private",
                "pricings" => [],
                "uses_noke" => false,
            ],
            "form" => [
                "name" => [
                    "type" => "text",
                ],
                "description" => [
                    "type" => "textarea",
                ],
                "long_description" => [
                    "type" => "html",
                ],
                "starting_guide_url" => [
                    "type" => "text",
                ],
                "chat_group_url" => [
                    "type" => "text",
                ],
                "type" => [
                    "type" => "select",
                    "label" => "Type",
                    "options" => [
                        [
                            "text" => "Privée",
                            "value" => "private",
                        ],
                        [
                            "text" => "Quartier",
                            "value" => "borough",
                        ],
                    ],
                ],
                "uses_noke" => ["type" => "checkbox"],
                "requires_identity_proof" => ["type" => "checkbox"],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }
}
