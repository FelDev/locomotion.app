<?php

namespace App\Http\Controllers;

use App\Events\LoanHandoverContestationResolvedEvent;
use App\Events\LoanHandoverContestedEvent;
use App\Http\Requests\Action\HandoverRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Handover;
use App\Models\Loan;
use App\Repositories\HandoverRepository;
use App\Repositories\LoanRepository;
use App\Repositories\RestRepository;
use Gate;

class HandoverController extends RestController
{
    public function __construct(LoanRepository $repository, Handover $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(HandoverRequest $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);
        Gate::authorize("completeHandover", $loan);

        $handover = $loan->handover;
        $wasContested = $handover->isContested();
        $handover->fill($request->all());
        $handover->comments_on_contestation = "";
        $handover->complete();

        RestRepository::saveItemAndRelations($handover, $request->all());

        $handover->refresh();

        // Move forward if possible.
        LoanController::loanActionsForward($handover->loan);

        if ($wasContested) {
            event(
                new LoanHandoverContestationResolvedEvent(
                    $handover,
                    $request->user()
                )
            );
        }

        return $handover;
    }

    public function contest(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);
        Gate::authorize("contestHandoverInfo", $loan);

        $handover = $loan->handover;
        $handover->comments_on_contestation = $request->get(
            "comments_on_contestation"
        );
        $handover->contest();

        $handover->save();

        event(new LoanHandoverContestedEvent($handover, $request->user()));

        return $handover;
    }
}
