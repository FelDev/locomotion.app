<?php

namespace App\Http\Controllers;

use App\Helpers\Path;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Image;
use App\Repositories\ImageRepository;
use Intervention\Image\Exception\NotReadableException;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\ImageManager as ImageManager;

class ImageController extends FileController
{
    protected $imageableId;

    protected $types = ["user"];

    public function __construct(ImageRepository $image)
    {
        $this->repo = $image;
    }

    protected function upload($file, $field, $userId)
    {
        $uniq = uniqid();
        $uri = "/images/tmp/$userId/$uniq";

        $originalFilename = $file->getClientOriginalName();
        $filename =
            $field . "." . pathinfo($originalFilename, PATHINFO_EXTENSION);

        $manager = new ImageManager(["driver" => "imagick"]);
        try {
            $image = $manager->make($file)->orientate();
        } catch (NotReadableException) {
            return ErrorResponse::withMessage("Fichier illisible.", 422);
        }

        try {
            $filepath = Path::join($uri, $filename);
            $saved = Image::store($filepath, $image);
            if (!$saved) {
                abort(500, "Fichier non sauvegardé: $filepath");
            }
        } catch (NotSupportedException) {
            return ErrorResponse::withMessage("Format non supporté.", 422);
        }

        $request = new Request();
        $request->merge([
            "path" => $uri,
            "original_filename" => $originalFilename,
            "filename" => $filename,
            "width" => $image->width(),
            "height" => $image->height(),
            "field" => $field,
            "filesize" => $image->filesize(),
        ]);

        return $request->input();
    }
}
