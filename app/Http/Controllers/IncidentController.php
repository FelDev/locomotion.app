<?php

namespace App\Http\Controllers;

use App\Events\LoanIncidentCreatedEvent;
use App\Events\LoanIncidentResolvedEvent;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Models\Incident;
use App\Models\Loan;
use App\Repositories\IncidentRepository;
use Gate;
use Illuminate\Validation\ValidationException;

class IncidentController extends RestController
{
    public function __construct(IncidentRepository $repository, Incident $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(Request $request)
    {
        Gate::authorize("declareIncident", Loan::findOrFail($request->loan_id));
        $item = parent::validateAndCreate($request);

        event(new LoanIncidentCreatedEvent($item));

        return $this->respondWithItem($request, $item, 201);
    }

    public function complete(Request $request, $loanId, $incidentId)
    {
        $item = $this->repo->find($request, $incidentId);

        Gate::authorize("resolveIncident", $item->loan);

        $item->status = "completed";
        $item->save();

        event(new LoanIncidentResolvedEvent($item));

        return $item;
    }
}
