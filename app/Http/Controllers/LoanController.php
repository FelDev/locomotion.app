<?php

namespace App\Http\Controllers;

use App\Events\Loan\CanceledEvent;
use App\Events\LoanCreatedEvent;
use App\Events\LoanPartiallyValidatedEvent;
use App\Events\LoanUpdatedEvent;
use App\Http\ErrorResponse;
use App\Http\Requests\Action\CreateRequest as ActionCreateRequest;
use App\Http\Requests\Action\ExtensionRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Loan\CreateRequest;
use App\Http\Resources\DashboardLoanResource;
use App\Http\Resources\LoanResource;
use App\Models\Handover;
use App\Models\Intention;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Payment;
use App\Models\PrePayment;
use App\Models\Takeover;
use App\Models\User;
use App\Repositories\LoanRepository;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class LoanController extends RestController
{
    public function __construct(
        LoanRepository $repository,
        Loan $model,
        private IncidentController $incidentController,
        private ExtensionController $extensionController
    ) {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function create(CreateRequest $request)
    {
        Gate::authorize("create", Loan::class);

        if (!$request->get("community_id")) {
            $loanable = Loanable::accessibleBy($request->user())
                ->where("id", $request->get("loanable_id"))
                ->firstOrFail();
            $request->merge([
                "community_id" => $loanable->getCommunityForLoanBy(
                    $request->user()
                )->id,
            ]);
        }

        $item = parent::validateAndCreate($request);

        // Move loan forward if possible.
        self::loanActionsForward($item);

        event(new LoanCreatedEvent($request->user(), $item));

        return new Response(new LoanResource($item), 201);
    }

    private function isUpdatingLoanTime(Loan $loan, Request $request)
    {
        return ($request->departure_at &&
            $request->departure_at !== $loan->departure_at) ||
            ($request->duration_in_minutes &&
                $request->duration_in_minutes !== $loan->duration_in_minutes);
    }

    private function isUpdatingDurationOrDistance(Loan $loan, Request $request)
    {
        return ($request->duration_in_minutes &&
            $request->duration_in_minutes !== $loan->duration_in_minutes) ||
            ($request->estimated_distance &&
                $request->estimated_distance !== $loan->estimated_distance);
    }

    private function canUpdateLoanTime(Loan $loan)
    {
        return Carbon::now()->isBefore($loan->departure_at);
    }

    public function update(Request $request, $id)
    {
        $loan = Loan::findOrFail($id);

        $isUpdatingTime = $this->isUpdatingLoanTime($loan, $request);
        $isUpdatingDurationOrDistance = $this->isUpdatingDurationOrDistance(
            $loan,
            $request
        );

        if ($isUpdatingTime && !$this->canUpdateLoanTime($loan)) {
            throw ValidationException::withMessages([
                "departure_at" => __("validation.custom.departure_time"),
            ]);
        }

        Gate::authorize("update", $loan);
        // The loanable_id 'available' rule checks that loanable is available at requested time.
        /** @var Loan $loan */
        $loan = parent::validateAndUpdate($request, $id);

        $resetApproval =
            $isUpdatingTime &&
            !$loan->loanable->is_self_service &&
            $loan->intention;
        if ($resetApproval) {
            $loan->intention->resetApproval();
            $loan->intention->save();

            event(new LoanUpdatedEvent($request->user(), $loan));
        }

        // It is important not to reset loan actions when platform tip is
        // updated as it frequently happens at the payment step.
        if (
            $resetApproval ||
            ($isUpdatingDurationOrDistance && !$loan->is_free)
        ) {
            if ($loan->prePayment) {
                $loan->prePayment()->delete();
            }

            if ($loan->takeover) {
                $loan->takeover()->delete();
            }

            if ($loan->handover) {
                $loan->handover()->delete();
            }

            $loan->refresh();

            self::loanActionsForward($loan);
        }

        return new LoanResource($loan);
    }

    public function retrieve(Request $request, $id)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $id);

        Gate::authorize("view", $loan);

        return new LoanResource($loan);
    }

    public function destroy(Request $request, $id)
    {
        $loan = Loan::findOrFail($id);
        Gate::authorize("destroy", $loan);
        return parent::validateAndDestroy($request, $id);
    }

    public function cancel(Request $request, $id)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $id);

        // TODO: add more specific handling for 'reject' vs 'cancel'.
        $user = $request->user();
        if (
            !$loan->loanable->is_self_service &&
            $loan->loanable->isOwnerOrCoowner($user) &&
            $loan->intention->status === "in_process"
        ) {
            Gate::authorize("reject", $loan);
        } else {
            Gate::authorize("cancel", $loan);
        }

        $loan->cancel();
        $loan->save();

        $response = $this->respondWithItem($request, $loan);

        event(new CanceledEvent($request->user(), $loan));

        return $response;
    }

    public function retrieveBorrower(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);
        Gate::authorize("view", $loan);
        return $this->respondWithItem($request, $loan->borrower);
    }

    public function createAction(ActionCreateRequest $request, $id)
    {
        $request->merge(["loan_id" => $id]);

        return match ($request->type) {
            "extension" => $this->extensionController->create(
                $request->redirect(ExtensionRequest::class)
            ),
            "incident" => $this->incidentController->create($request),
            default => throw new \Exception("invalid action type"),
        };
    }

    public function template(Request $request)
    {
        $defaultDeparture = new Carbon();
        $defaultDeparture->minute = floor($defaultDeparture->minute / 10) * 10;
        $defaultDeparture->second = 0;

        $template = [
            "item" => [
                "departure_at" => $defaultDeparture->format("Y-m-d H:i:s"),
                "duration_in_minutes" => 60,
                "estimated_distance" => null,
                "platform_tip" => null,
                "message_for_owner" => "",
                "reason" => "",
                "incidents" => [],
                "borrower_id" => null,
                "borrower" => null,
                "loanable_id" => null,
                "loanable" => null,
            ],
            "form" => [
                "departure_at" => [
                    "type" => "datetime",
                ],
                "duration_in_minutes" => [
                    "type" => "number",
                ],
                "estimated_distance" => [
                    "type" => "number",
                ],
                "platform_tip" => [
                    "type" => "number",
                ],
                "message_for_owner" => [
                    "type" => "textarea",
                ],
                "reason" => [
                    "type" => "textarea",
                ],
                "community_id" => [
                    "type" => "relation",
                    "query" => [
                        "slug" => "communities",
                        "value" => "id",
                        "text" => "name",
                        "params" => [
                            "fields" => "id,name",
                        ],
                    ],
                ],
                "loanable_id" => [
                    "type" => "relation",
                    "query" => [
                        "slug" => "loanables",
                        "value" => "id",
                        "text" => "name",
                        "params" => [
                            "fields" => "id,name",
                        ],
                    ],
                ],
                "borrower_id" => [
                    "type" => "relation",
                    "query" => [
                        "slug" => "borrowers",
                        "value" => "id",
                        "text" => "user.full_name",
                        "params" => [
                            "fields" => "id,user.full_name",
                        ],
                    ],
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            $template["form"][$field]["rules"] = $this->formatRules($rules);
        }

        return $template;
    }

    // function to test if a loanable is available for the requested loan
    // verifies if there is no loan at the same time of the current loan to accept
    public function isAvailable(Request $request, $loanId)
    {
        try {
            // verify if the loan or the loanable exists, if not then it throws a ModelNotFoundException
            $loanable = null;
            $loan = $this->repo->find($request, $loanId);

            if ($loan) {
                $loanable = Loanable::accessibleBy($request->user())->find(
                    $loan->loanable_id
                );
            }
            if (!$loanable) {
                throw new ModelNotFoundException();
            }
        } catch (ModelNotFoundException) {
            return ErrorResponse::withMessage("Not found", 404);
        }

        $loanStart = new Carbon($loan->departure_at);
        $durationInMinutes = $loan->duration_in_minutes;

        $loanableAvailability = $loanable->isAvailable(
            $loanStart,
            $durationInMinutes,
            [$loanId]
        );

        return response(
            [
                "isAvailable" => $loanableAvailability,
            ],
            200
        );
    }

    /**
     * Marks the loan as being validated: it's information has been checked by
     * the loanable owner and payment can proceed.
     */
    public function validateInformation(Request $request, $loanId): Response
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("validateLoanInfo", $loan);

        if ($loan->borrower->user->is($request->user())) {
            if (!$loan->borrower_validated_at) {
                $loan->borrower_validated_at = new Carbon();
                $loan->save();
                event(new LoanPartiallyValidatedEvent($loan, $request->user()));
            }
            $loan->refresh();
            return response($loan->borrower_validated_at);
        }
        if ($loan->loanable->owner->user->is($request->user())) {
            if (!$loan->owner_validated_at) {
                $loan->owner_validated_at = new Carbon();
                $loan->save();
                event(new LoanPartiallyValidatedEvent($loan, $request->user()));
            }
            $loan->refresh();
            return response($loan->owner_validated_at);
        }

        return response("No validation needed from this user.", 404);
    }

    /*
       This method creates next loan action upon completion of an action.
       It also checks if any action may be autocompleted.
     */
    public static function loanActionsForward(Loan $loan)
    {
        $intention = $loan->intention;

        // Ensure intention exists.
        if (!$intention) {
            $intention = new Intention();

            // https://laravel.com/docs/9.x/eloquent-relationships#the-save-method
            $loan->intention()->save($intention);
        }

        // Ensure intention is still in process to not complete the action every time we call this function.
        if ($intention->status == "in_process") {
            if ($loan->loanable->is_self_service) {
                // Autocomplete intention if loanable is self-service.
                $intention->complete()->save();
            }
        }

        $loan->load("intention");
        if (!$intention->isCompleted()) {
            return $loan;
        }

        // Ensure pre-payment exists if intention is completed.
        $prePayment = $loan->prePayment;

        if (!$prePayment) {
            $prePayment = new PrePayment();
            $loan->prePayment()->save($prePayment);
        }

        // Pre-payment does not get autocompleted. It is where we ask for a financial contribution.

        $loan->load("prePayment");
        if (!$prePayment->isCompleted()) {
            return $loan;
        }

        // Ensure takeover exists if pre-payment is completed.
        $takeover = $loan->takeover;

        if (!$takeover) {
            $takeover = new Takeover();
            $loan->takeover()->save($takeover);
        }

        // Auto-complete takeover for self-service.
        // TODO: remove takeover for self-service altogether
        if ($loan->loanable->is_self_service) {
            $takeover->complete()->save();
        }

        $loan->load("takeover");
        if (!$takeover->isCompleted()) {
            return $loan;
        }

        // Ensure handover exists if takeover is completed.
        $handover = $loan->handover;

        if (!$handover) {
            $handover = new Handover();
            $loan->handover()->save($handover);
        }

        $loan->load("handover");
        if (!$handover->isCompleted()) {
            return $loan;
        }

        // Ensure payment exists if handover is completed.
        $payment = $loan->payment;

        if (!$payment) {
            $payment = new Payment();
            $loan->payment()->save($payment);
        }
        $loan->load("payment");

        // We don't complete payment here (yet?) because we would have to
        // generate the invoice which is done in PaymentController for the
        // moment.

        return $loan;
    }

    public function dashboard(Request $request)
    {
        $accessibleLoans = Loan::forDashboard($request->user());
        $now = CarbonImmutable::now();

        $completedLoans = (clone $accessibleLoans)
            ->where("status", "completed")
            ->orderBy("updated_at", "desc");

        $ongoingLoans = (clone $accessibleLoans)
            ->where("status", "in_process")
            ->orderBy("departure_at");
        $waitingLoans = (clone $ongoingLoans)->whereHas("intention", function (
            Builder $q
        ) {
            $q->where("status", "in_process");
        });
        $userId = $request->user()->id;
        $waitingLoansAsBorrower = (clone $waitingLoans)->whereHas(
            "borrower",
            function (Builder $q) use ($userId) {
                $q->where("user_id", $userId);
            }
        );
        $waitingLoansAsOwner = (clone $waitingLoans)->hasOwnerAccess($userId);
        $approvedLoans = (clone $ongoingLoans)->whereHas("intention", function (
            Builder $q
        ) {
            $q->where("status", "completed");
        });

        $uncontestedLoans = (clone $approvedLoans)
            ->where(function ($q) {
                $q->doesntHave("takeover")->orWhereHas("takeover", function (
                    Builder $q
                ) {
                    $q->where("status", "!=", "canceled");
                });
            })
            ->where(function ($q) {
                $q->doesntHave("handover")->orWhereHas("handover", function (
                    Builder $q
                ) {
                    $q->where("status", "!=", "canceled");
                });
            });

        $contestedLoans = (clone $approvedLoans)->where(function ($q) {
            $q->whereHas("takeover", function (Builder $q) {
                $q->where("status", "canceled");
            })->orWhereHas("handover", function (Builder $q) {
                $q->where("status", "canceled");
            });
        });

        // Started loans that aren't contested
        $startedLoans = (clone $uncontestedLoans)->where(
            "departure_at",
            "<=",
            $now
        );

        $approvedFutureLoans = (clone $uncontestedLoans)->where(
            "departure_at",
            ">",
            $now
        );

        return response(
            [
                "started" => [
                    "total" => $startedLoans->count(),
                    "loans" => DashboardLoanResource::collection(
                        $startedLoans->limit(5)->get()
                    ),
                ],
                "contested" => [
                    "total" => $contestedLoans->count(),
                    "loans" => DashboardLoanResource::collection(
                        $contestedLoans->limit(5)->get()
                    ),
                ],
                "waiting" => [
                    "total" => $waitingLoansAsBorrower->count(),
                    "loans" => DashboardLoanResource::collection(
                        $waitingLoansAsBorrower->limit(5)->get()
                    ),
                ],
                "need_approval" => [
                    "total" => $waitingLoansAsOwner->count(),
                    "loans" => DashboardLoanResource::collection(
                        $waitingLoansAsOwner->limit(5)->get()
                    ),
                ],
                "future" => [
                    "total" => $approvedFutureLoans->count(),
                    "loans" => DashboardLoanResource::collection(
                        $approvedFutureLoans->limit(5)->get()
                    ),
                ],
                "completed" => [
                    "total" => $completedLoans->count(),
                    "loans" => DashboardLoanResource::collection(
                        $completedLoans->limit(5)->get()
                    ),
                ],
            ],
            200
        );
    }
}
