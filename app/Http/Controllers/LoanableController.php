<?php

namespace App\Http\Controllers;

use App\Calendar\AvailabilityHelper;
use App\Calendar\DateIntervalHelper;
use App\Enums\LoanableTypes;
use App\Events\LoanableCreatedEvent;
use App\Helpers\Order as OrderHelper;
use App\Http\ErrorResponse;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\Loanable\AvailabilityRequest;
use App\Http\Requests\Loanable\EventsRequest;
use App\Http\Requests\Loanable\SearchRequest;
use App\Http\Requests\Loanable\TestRequest;
use App\Http\Resources\DashboardLoanableResource;
use App\Http\Resources\DashboardLoanResource;
use App\Http\Resources\LoanableResource;
use App\Models\Bike;
use App\Models\Car;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pricing;
use App\Models\User;
use App\Repositories\LoanableRepository;
use App\Repositories\LoanRepository;
use App\Rules\LoanableTypeListRule;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Validation\ValidationException;
use Validator;

class LoanableController extends RestController
{
    protected LoanRepository $loanRepo;
    protected LoanController $loanController;

    public function __construct(
        LoanableRepository $repository,
        Loanable $model,
        LoanRepository $loanRepository,
        LoanController $loanController
    ) {
        $this->repo = $repository;
        $this->model = $model;

        $this->loanRepo = $loanRepository;
        $this->loanController = $loanController;
    }

    public function create(Request $request)
    {
        $item = parent::validateAndCreate($request);

        event(new LoanableCreatedEvent($request->user(), $item));

        return $this->respondWithItem($request, $item, 201);
    }

    public function availability(AvailabilityRequest $request)
    {
        // Set time to 0 to ensure consistency with the fact that we expect dates.
        $dateRange = [
            (new CarbonImmutable($request->start))->setTime(0, 0, 0),
            (new CarbonImmutable($request->end))->setTime(0, 0, 0),
        ];

        // Include loans by default.
        $includeLoans =
            !$request->has("includeLoans") || $request->includeLoans;

        // AvailabilityRequest checks that the loanable exists and is accessible by the user.
        $loanable = Loanable::findOrFail($request->loanable_id);

        $events = [];

        // If availability mode and rules come from the request, then use that
        // to compute availability.
        if ($request->availabilityMode && $request->availabilityJson) {
            $availabilityMode = $request->availabilityMode;
            $availabilityRules = json_decode(
                $request->availabilityJson,
                true,
                512,
                JSON_THROW_ON_ERROR
            );
        } else {
            $availabilityMode = $loanable->availability_mode;
            $availabilityRules = $loanable->getAvailabilityRules();
        }

        // Availability or unavailability depending on responseMode.
        $availabilityIntervalsByDay = AvailabilityHelper::getDailyAvailability(
            [
                "available" => "always" == $availabilityMode,
                "rules" => $availabilityRules,
            ],
            $dateRange,
            $request->responseMode == "available"
        );

        if ($includeLoans) {
            // We do not want to check if loan is accessible by the request user here,
            // since in most cases it shouldn't be.
            $loans = Loan::where("loanable_id", $loanable->id)
                ->isPeriodUnavailable($dateRange[0], $dateRange[1])
                ->get();

            foreach ($loans as $loan) {
                $loanInterval = [
                    new Carbon($loan->departure_at),
                    new Carbon($loan->actual_return_at),
                ];

                $loanIntervalsByDay = AvailabilityHelper::splitIntervalByDay(
                    $loanInterval
                );

                foreach ($loanIntervalsByDay as $index => $loanInterval) {
                    $availabilityIntervals =
                        $availabilityIntervalsByDay[$index] ?? [];

                    if ($request->responseMode == "available") {
                        // Intervals are of availability.
                        $availabilityIntervalsByDay[
                            $index
                        ] = DateIntervalHelper::subtraction(
                            $availabilityIntervals,
                            $loanInterval
                        );
                    } else {
                        // Intervals are of unavailability.
                        $availabilityIntervalsByDay[
                            $index
                        ] = DateIntervalHelper::union(
                            $availabilityIntervals,
                            $loanInterval
                        );
                    }
                }
            }
        }

        // Generate events from intervals.
        foreach ($availabilityIntervalsByDay as $dailyAvailabilityIntervals) {
            foreach ($dailyAvailabilityIntervals as $availabilityInterval) {
                $events[] = [
                    "start" => $availabilityInterval[0]->toDateTimeString(),
                    "end" => $availabilityInterval[1]->toDateTimeString(),
                    "data" => [
                        "available" => $request->responseMode == "available",
                    ],
                ];
            }
        }

        return response($events, 200);
    }

    public function events(EventsRequest $request)
    {
        $start = new Carbon($request->start);
        $end = new Carbon($request->end);

        $loanable = Loanable::accessibleBy($request->user())
            ->where("id", "=", $request->loanable_id)
            ->first();

        $events = [];

        if ($loanable) {
            $availabilityRules = $loanable->getAvailabilityRules();
            $availabilityMode = $loanable->availability_mode;

            $intervals = AvailabilityHelper::getScheduleDailyIntervals(
                [
                    "available" => "always" == $availabilityMode,
                    "rules" => $availabilityRules,
                ],
                [$start, $end]
            );

            foreach ($intervals as $interval) {
                $events[] = [
                    "type" => "availability_rule",
                    "start" => $interval[0],
                    "end" => $interval[1],
                    "uri" => "/loanables/$loanable->id",
                    "data" => [
                        // availability_mode == "always" means that events are of unavailability.
                        "available" => $availabilityMode != "always",
                    ],
                ];
            }

            $loans = Loan::accessibleBy($request->user())
                ->where("loanable_id", "=", $loanable->id)
                // Departure before the end and return after the beginning of the period.
                ->where("departure_at", "<", $end)
                ->where("actual_return_at", ">", $start)
                ->get();

            foreach ($loans as $loan) {
                $events[] = [
                    "type" => "loan",
                    "start" => new Carbon($loan->departure_at),
                    "end" => new Carbon($loan->actual_return_at),
                    "uri" => "/loans/$loan->id",
                    "data" => [
                        "status" => $loan->status,
                    ],
                ];
            }

            // Event field definitions for sorting.
            $eventFieldDefs = [
                "type" => ["type" => "string"],
                "uri" => ["type" => "string"],
                "start" => ["type" => "carbon"],
                "end" => ["type" => "carbon"],
            ];

            $orderArray = OrderHelper::parseOrderRequestParam(
                $request->order,
                $eventFieldDefs
            );
            $events = OrderHelper::sortArray($events, $orderArray);

            // Prepare data for response.
            foreach ($events as $key => $event) {
                $events[$key]["start"] = $event["start"]->toDateTimeString();
                $events[$key]["end"] = $event["end"]->toDateTimeString();
            }
        }

        return response($events, 200);
    }

    public function loansDuringUnavailabilities(Request $request)
    {
        $loanable = Loanable::accessibleBy($request->user())->find(
            $request->loanable_id
        );

        // If availability mode and rules come from the request, then use that
        // to compute availability.
        if ($request->availability_mode || $request->availability_json) {
            $availabilityMode = $request->availability_mode ?? "always";
            $availabilityRules = $request->availability_json
                ? json_decode(
                    $request->availability_json,
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                )
                : [];
        } else {
            $availabilityMode = $loanable->availability_mode;
            $availabilityRules = $loanable->getAvailabilityRules();
        }

        $resource = DashboardLoanResource::collection(
            Loan::accessibleBy($request->user())
                ->where("loanable_id", $loanable->id)
                ->future()
                ->duringAvailability(
                    [
                        "available" => "always" == $availabilityMode,
                        "rules" => $availabilityRules,
                    ],
                    false
                )
        );
        $resource->withoutWrapping();
        return $resource;
    }

    public function update(Request $request, $id)
    {
        $loanable = Loanable::findOrFail($id);
        if ($request->owner || $request->owner_id) {
            Gate::authorize("updateOwner", $loanable);
        }

        if ($request->padlock || $request->padlock_id) {
            Gate::authorize("updatePadlock", $loanable);
        }

        if (!$request->user()->isAdmin() && $request->coowners) {
            abort(
                422,
                "Coowners should be set with loanables/coowners methods"
            );
        }
        Gate::authorize("update", $loanable);

        $item = parent::validateAndUpdate($request, $id);

        return $this->respondWithItem($request, $item);
    }

    public function destroy(Request $request, $id)
    {
        /** @var Loanable $item */
        $item = $this->repo->find($request, $id);
        Gate::authorize("delete", $item);

        if (
            $item
                ->loans()
                ->blockingLoanableDeletion()
                ->exists()
        ) {
            throw ValidationException::withMessages([
                __("validation.custom.vehicle_must_not_have_ongoing_loans"),
            ]);
        }

        return parent::validateAndDestroy($request, $id);
    }

    public function restore(Request $request, $id)
    {
        /** @var Loanable $item */
        $item = $this->repo->findWithTrashed($request, $id);
        Gate::authorize("restore", $item);

        /** @var Loanable $loanable */
        $loanable = parent::validateAndRestore($request, $id);

        if (!$request->boolean("restore_availability")) {
            // Delete all availabilities.
            $loanable->availability_json = [];
            $loanable->availability_mode = "never";
            $loanable->save();
        }
    }

    public function list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "types" => ["string", new LoanableTypeListRule()],
        ]);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        // Default to all types
        $types = LoanableTypes::possibleTypes;
        if ($request->has("types")) {
            $types = explode(",", $request->query("types"));
        }

        return response(
            LoanableResource::collection(
                Loanable::accessibleBy($request->user())
                    ->whereIn("type", $types)
                    ->hasAvailabilities()
                    ->get()
            ),
            200
        );
    }

    public function search(SearchRequest $request)
    {
        $departureAt = new Carbon($request->get("departure_at"));
        $durationInMinutes = $request->get("duration_in_minutes");

        $returnAt = $departureAt->copy()->add($durationInMinutes, "minutes");

        $loanables = Loanable::accessibleBy($request->user());

        // Check no other loans intersect
        $availableLoanables = $loanables
            ->whereDoesntHave("loans", function ($loans) use (
                $departureAt,
                $returnAt
            ) {
                return $loans->isPeriodUnavailable($departureAt, $returnAt);
            })
            ->get();

        // Check schedule is open for remaining loanables
        $availableLoanableIds = $availableLoanables
            ->reject(
                fn($loanable) => !$loanable->isLoanableScheduleOpen(
                    $departureAt,
                    $returnAt
                )
            )
            ->map(fn($loanable) => $loanable->id)
            ->values();

        return response($availableLoanableIds, 200);
    }

    /**
     * Returns the estimated price a possible loan
     *
     * @return object with 'price', 'insurance' and 'pricing' (name of the pricing rule) fields set.
     **/
    private function estimateLoanCost(
        Loanable $loanable,
        Community $community,
        int $estimatedDistance,
        int $durationInMinutes,
        Carbon $departureAt
    ): object {
        $pricing = $community->getPricingFor($loanable->type);
        if (!$pricing) {
            return (object) [
                "price" => 0,
                "insurance" => 0,
                "pricing" => "Gratuit",
            ];
        }

        $end = $departureAt->copy()->add($durationInMinutes, "minutes");
        $estimatedCost = $pricing->evaluateRule(
            $estimatedDistance,
            $durationInMinutes,
            $loanable,
            (object) [
                "days" => Loan::getCalendarDays($departureAt, $end),
                "start" => Pricing::dateToDataObject($departureAt),
                "end" => Pricing::dateToDataObject($end),
            ]
        );

        if (is_array($estimatedCost)) {
            // Price is time + distance.
            $price =
                ($estimatedCost["time"] ?? 0) +
                ($estimatedCost["distance"] ?? 0);

            $insurance = $estimatedCost["insurance"] ?? 0;
        } else {
            $price = $estimatedCost;
            $insurance = 0;
        }

        return (object) [
            "price" => $price,
            "insurance" => $insurance,
            "pricing" => $pricing->name,
        ];
    }

    public function test(TestRequest $request, $id)
    {
        $findRequest = $request->redirectAuth(Request::class);
        $item = $this->repo->find($findRequest, $id);

        $estimatedDistance = intval($request->get("estimated_distance", 0));
        $departureAt = new Carbon($request->get("departure_at"));
        $durationInMinutes = intval($request->get("duration_in_minutes"));
        $loanId = $request->get("loan_id");

        $communityId = $request->get("community_id");
        if ($communityId) {
            $community = Community::accessibleBy($request->user())->find(
                $communityId
            );
        } else {
            $community = $item->getCommunityForLoanBy($request->user());
        }
        $estimatedCost = self::estimateLoanCost(
            $item,
            $community,
            $estimatedDistance,
            $durationInMinutes,
            $departureAt
        );

        return response(
            [
                "community" => [
                    "id" => $community->id,
                    "name" => $community->name,
                ],
                "available" => $item->isAvailable(
                    $departureAt,
                    $durationInMinutes,
                    $loanId ? [$loanId] : []
                ),
                "price" => $estimatedCost->price,
                "insurance" => $estimatedCost->insurance,
                "pricing" => $estimatedCost->pricing,
            ],
            200
        );
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "type" => null,
                "comments" => "",
                "instructions" => "",
                "location_description" => "",
                "name" => "",
                "details" => new \stdClass(),
                "position" => [],
            ],
            "form" => [
                "general" => [
                    "name" => [
                        "type" => "text",
                    ],
                    "image" => [
                        "type" => "image",
                    ],
                    "position" => [
                        "type" => "point",
                    ],
                    "location_description" => [
                        "type" => "textarea",
                    ],
                    "comments" => [
                        "type" => "textarea",
                    ],
                    "instructions" => [
                        "type" => "textarea",
                    ],
                    "type" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Auto",
                                "value" => "car",
                            ],
                            [
                                "text" => "Vélo",
                                "value" => "bike",
                            ],
                            [
                                "text" => "Remorque",
                                "value" => "trailer",
                            ],
                        ],
                    ],
                    "owner_id" => [
                        "type" => "relation",
                        "query" => [
                            "slug" => "owners",
                            "value" => "id",
                            "text" => "user.full_name",
                            "params" => [
                                "fields" =>
                                    "id,user.full_name," .
                                    "user.communities.id,user.communities.name,",
                            ],
                        ],
                    ],
                    "is_self_service" => [
                        "type" => "checkbox",
                    ],
                    "padlock_id" => [
                        "type" => "relation",
                        "query" => [
                            "slug" => "padlocks",
                            "value" => "id",
                            "text" => "name",
                            "params" => [
                                "fields" => "id,name",
                                "!loanable" => "1",
                            ],
                        ],
                    ],
                ],
                "bike" => [
                    "model" => [
                        "type" => "text",
                    ],
                    "bike_type" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Régulier",
                                "value" => "regular",
                            ],
                            [
                                "text" => "Cargo",
                                "value" => "cargo",
                            ],
                            [
                                "text" => "Électrique",
                                "value" => "electric",
                            ],
                            [
                                "text" => "Roue fixe",
                                "value" => "fixed_wheel",
                            ],
                        ],
                    ],
                    "size" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Grand",
                                "value" => "big",
                            ],
                            [
                                "text" => "Moyen",
                                "value" => "medium",
                            ],
                            [
                                "text" => "Petit",
                                "value" => "small",
                            ],
                            [
                                "text" => "Enfant",
                                "value" => "kid",
                            ],
                        ],
                    ],
                ],
                "car" => [
                    "brand" => [
                        "type" => "text",
                    ],
                    "model" => [
                        "type" => "text",
                    ],
                    "pricing_category" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" =>
                                    "Petite auto (compacte, sous-compacte, hybride non-branchable)",
                                "value" => "small",
                            ],
                            [
                                "text" => "Grosse auto (van, VUS, pick-up)",
                                "value" => "large",
                            ],
                            [
                                "text" =>
                                    "Auto électrique (électrique, hybride branchable)",
                                "value" => "electric",
                            ],
                        ],
                    ],
                    "year_of_circulation" => [
                        "type" => "number",
                        "max" => (int) date("Y") + 1,
                        "min" => 1900,
                    ],
                    "transmission_mode" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Automatique",
                                "value" => "automatic",
                            ],
                            [
                                "text" => "Manuelle",
                                "value" => "manual",
                            ],
                        ],
                    ],
                    "engine" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Essence",
                                "value" => "fuel",
                            ],
                            [
                                "text" => "Diesel",
                                "value" => "diesel",
                            ],
                            [
                                "text" => "Électrique",
                                "value" => "electric",
                            ],
                            [
                                "text" => "Hybride",
                                "value" => "hybrid",
                            ],
                        ],
                    ],
                    "plate_number" => [
                        "type" => "text",
                    ],
                    "value_category" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "50 000$ et moins",
                                "value" => "lte50k",
                            ],
                            [
                                "text" => "50 001$ à 70 000$",
                                "value" => "lte70k",
                            ],
                            [
                                "text" => "70 001$ à 100 000$",
                                "value" => "lte100k",
                            ],
                        ],
                    ],
                    "has_onboard_notebook" => [
                        "type" => "checkbox",
                    ],
                    "has_report_in_notebook" => [
                        "type" => "checkbox",
                    ],
                    "report" => [
                        "type" => "file",
                    ],
                    "papers_location" => [
                        "type" => "select",
                        "options" => [
                            [
                                "text" => "Dans l'auto",
                                "value" => "in_the_car",
                            ],
                            [
                                "text" => "À récupérer avec la clé",
                                "value" => "to_request_with_car",
                            ],
                        ],
                    ],
                    "insurer" => [
                        "type" => "text",
                    ],
                    "has_informed_insurer" => [
                        "type" => "checkbox",
                    ],
                ],
                "trailer" => [
                    "maximum_charge" => [
                        "type" => "text",
                    ],
                    "dimensions" => [
                        "type" => "text",
                    ],
                ],
            ],
        ];

        // Only global admins may set padlocks. Remove from form template.
        if (!$request->user()->isAdmin()) {
            unset($template["form"]["general"]["padlock_id"]);
        }

        $user = $request->user();

        $generalRules = $this->model->getRules("template", $user);
        $generalRulesKeys = array_keys($generalRules);
        foreach ($generalRules as $field => $rules) {
            if (!isset($template["form"]["general"][$field])) {
                continue;
            }
            $template["form"]["general"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $bikeRules = Bike::getRules("template", $user);
        foreach ($bikeRules as $field => $rules) {
            if (in_array($field, $generalRulesKeys)) {
                continue;
            }
            if (!isset($template["form"]["bike"][$field])) {
                continue;
            }
            $template["form"]["bike"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $carRules = Car::getRules("template", $user);
        foreach ($carRules as $field => $rules) {
            if (in_array($field, $generalRulesKeys)) {
                continue;
            }
            if (!isset($template["form"]["car"][$field])) {
                continue;
            }
            $template["form"]["car"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        return $template;
    }

    public function indexLoans(Request $request, $id)
    {
        $this->repo->find($request->redirectAuth(), $id);

        return $this->loanController->index($request->redirect(Request::class));
    }

    // WARN This bypasses "accessibleBy" checks on loans. Make sure
    // that the transformers authorize the fields down the line.
    public function retrieveNextLoan(Request $request, $loanableId, $loanId)
    {
        $item = $this->repo->find($request, $loanableId);
        $loan = $this->loanRepo->find($request, $loanId);

        $loanReturnAt = (new Carbon($loan->departure_at))->add(
            $loan->duration_in_minutes,
            "minutes"
        );
        $nextLoan = $item
            ->loans()
            ->where("loans.id", "!=", $loanId)
            ->where("departure_at", ">=", $loanReturnAt)
            ->orderBy("departure_at", "asc")
            ->first();

        if (!$nextLoan) {
            return abort(404);
        }

        return $this->respondWithItem($request, $nextLoan);
    }

    public function dashboard(Request $request)
    {
        $user = $request->user();

        $ownedLoanables = Loanable::whereHas("owner.user", function (
            Builder $q
        ) use ($user) {
            $q->where("id", $user->id);
        })
            ->with("image")
            ->with("owner.user.avatar");

        $coOwnedLoanables = Loanable::whereHas("coowners.user", function (
            Builder $q
        ) use ($user) {
            $q->where("id", $user->id);
        })
            ->with("image")
            ->with("owner.user.avatar");

        return [
            "owned" => [
                "total" => $ownedLoanables->count(),
                "loanables" => DashboardLoanableResource::collection(
                    $ownedLoanables->limit(5)->get()
                ),
            ],
            "coowned" => [
                "total" => $coOwnedLoanables->count(),
                "loanables" => DashboardLoanableResource::collection(
                    $coOwnedLoanables->limit(5)->get()
                ),
            ],
        ];
    }

    protected function validateAction(array $data, User $user, string $action)
    {
        parent::validateAction($data, $user, $action);

        if ($action === "destroy") {
            return;
        }

        $loanableDetails = LoanableTypes::from(
            $data["type"]
        )->getLoanableModel();

        $detailsValidator = Validator::make(
            $data["details"],
            $loanableDetails->getRules($action, $user),
            $loanableDetails::$validationMessages
        );

        if ($detailsValidator->fails()) {
            throw new ValidationException($detailsValidator);
        }
    }
}
