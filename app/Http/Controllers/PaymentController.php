<?php

namespace App\Http\Controllers;

use App\Events\LoanPaidEvent;
use App\Http\Requests\BaseRequest;
use App\Models\Invoice;
use App\Models\Loan;
use App\Models\Payment;
use App\Repositories\LoanRepository;
use App\Repositories\PaymentRepository;
use Carbon\Carbon;
use Gate;

class PaymentController extends RestController
{
    public function __construct(LoanRepository $repository, Payment $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(BaseRequest $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("pay", $loan);
        $this->pay($loan);

        return $loan->payment;
    }

    /**
     * Creates the invoices and transfers the amounts from user's balance for the completion of this loan.
     *
     * @param bool $isAutomated Whether this is an automated payment or triggered by user action
     */
    public static function pay(Loan $loan, bool $isAutomated = false): void
    {
        // Prepare variables
        $price = $loan->actual_price;
        $insurance = $loan->actual_insurance;
        $platformTip = $loan->platform_tip;
        $expenses = $loan->handover->purchases_amount;
        $object = $loan->loanable->name;
        $prettyDate = (new Carbon($loan->departure_at))
            ->locale("fr_FR")
            ->isoFormat("LLL");

        // Update loan
        $loan->final_price = $price;
        $loan->final_insurance = $insurance;
        $loan->final_platform_tip = $platformTip;
        $loan->final_purchases_amount = $expenses;
        $loan->save();

        $loanMeta = [
            "entityType" => "loan",
            "entityId" => $loan->id,
            "entityUrl" => route("loans.retrieve", ["id" => $loan->id]),
        ];

        $borrowerUser = $loan->borrower->user;
        $ownerUser = $loan->loanable->owner->user;

        // Borrower invoice as a debit, since it is a payment
        $borrowerInvoice = $borrowerUser->createInvoice("debit");

        // Owner invoice as a credit, since the owner will receive this amount
        // from the loan. If borrower is owner, then it is debit.
        if (!$borrowerUser->is($ownerUser)) {
            $ownerInvoice = $ownerUser->createInvoice("credit");
        } else {
            $ownerInvoice = $borrowerInvoice;
        }

        // Add price to both invoices. Set to 0 if borrower is owner.
        $priceItem = [
            "item_type" => "loan.price",
            "label" => "Coût de l'emprunt de $object le $prettyDate",
            "amount" => $loan->final_price,
            "item_date" => date("Y-m-d"),
            "taxes_tps" => 0,
            "taxes_tvq" => 0,
            "amount_type" => null,
            "meta" => $loanMeta,
        ];

        if (!$borrowerUser->is($ownerUser)) {
            $priceItem["amount_type"] = "credit";
            $ownerInvoice->billItems()->create($priceItem);
        } else {
            $priceItem["amount"] = 0;
        }
        $priceItem["amount_type"] = "debit";
        $borrowerInvoice->billItems()->create($priceItem);

        if ($loan->final_insurance > 0) {
            $insuranceItem = [
                "item_type" => "loan.insurance",
                "label" => "Coût de l'assurance pour l'emprunt de $object le $prettyDate",
                "amount" => $loan->final_insurance,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "amount_type" => "debit",
                "meta" => $loanMeta,
            ];

            $borrowerInvoice->billItems()->create($insuranceItem);
        }

        // Add expenses to both invoices. Set to 0 if borrower is owner.
        if ($loan->final_purchases_amount > 0) {
            $expensesItem = [
                "item_type" => "loan.expenses",
                "label" => "Dépenses pour l'emprunt de $object le $prettyDate",
                "amount" => -$loan->final_purchases_amount,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "amount_type" => null,
                "meta" => $loanMeta,
            ];

            if (!$borrowerUser->is($ownerUser)) {
                $expensesItem["amount_type"] = "debit";
                $ownerInvoice->billItems()->create($expensesItem);
            } else {
                $expensesItem["amount"] = 0;
            }

            $expensesItem["amount_type"] = "credit";
            $borrowerInvoice->billItems()->create($expensesItem);
        }

        // Add platform tip to borrower's invoice only.
        if ($loan->final_platform_tip > 0) {
            // Compute taxes and subtract from the amount (avoid rounding errors)
            $taxesTps = round(($loan->final_platform_tip / 1.14975) * 0.05, 2);
            $taxesTvq = round(
                ($loan->final_platform_tip / 1.14975) * 0.09975,
                2
            );

            $tipWithoutTaxes =
                $loan->final_platform_tip - $taxesTps - $taxesTvq;

            $platformTipItem = [
                "item_type" => "loan.platformTip",
                "label" => "Contribution volontaire pour l'emprunt de $object le $prettyDate",
                "amount" => $tipWithoutTaxes,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => $taxesTps,
                "taxes_tvq" => $taxesTvq,
                "amount_type" => "debit",
                "meta" => $loanMeta,
            ];

            $borrowerInvoice->billItems()->create($platformTipItem);
        }

        $debitAmount = $price + $insurance + $platformTip - $expenses;
        $creditAmount = $price - $expenses;

        if (!$borrowerUser->is($ownerUser)) {
            // If both users are the same, then only one invoice is created.
            $ownerInvoice->balance_before = $ownerUser->balance;
        }
        $borrowerInvoice->balance_before = $borrowerUser->balance;

        // Update balances
        if ($borrowerUser->is($ownerUser)) {
            // If the borrower is the owner we do a single atomic addToBalance
            // or removeFromBalance instead of both calls, so we can allow
            // temporarily going below a balance of zero if the final balance
            // is above zero (e.g. initial balance is 0.5 => debit 1 => balance
            // is -0.5 => credit 1 ==> final balance is 0.5)
            $movement = $creditAmount - $debitAmount;

            if ($movement >= 0) {
                $ownerUser->addToBalance($movement);
            } else {
                $borrowerUser->removeFromBalance($movement * -1);
            }
        } else {
            $borrowerUser->removeFromBalance($debitAmount);
            $ownerUser->refresh();
            $ownerUser->addToBalance($creditAmount);
        }

        if (!$borrowerUser->is($ownerUser)) {
            // If both users are the same, then only one invoice is created.
            $ownerInvoice->balance_after = $ownerUser->balance;
            $ownerInvoice->pay();
        }
        $borrowerInvoice->balance_after = $borrowerUser->balance;
        $borrowerInvoice->pay();

        // Save payment
        // Borrower invoice is always created.
        $payment = $loan->payment;
        $payment->borrower_invoice_id = $borrowerInvoice->id;
        $payment->owner_invoice_id = $ownerInvoice->id;
        $payment->complete();
        $payment->save();

        // Send emails after an automated or manual action
        if (
            !$loan->loanable->is_self_service &&
            $loan->loanable->owner &&
            $loan->total_final_cost > 0
        ) {
            if ($isAutomated) {
                event(
                    new LoanPaidEvent(
                        $borrowerUser,
                        $borrowerInvoice->getTransformer()->transform([
                            "fields" => ["*" => "*"],
                        ]),
                        "Conclusion automatique de votre emprunt",
                        "Votre emprunt est terminé depuis 48h. Il est désormais clôturé!"
                    )
                );

                // Trigger event for owner if exists and is not also the borrower.
                if ($loan->loanable->owner && !$borrowerUser->is($ownerUser)) {
                    event(
                        new LoanPaidEvent(
                            $ownerUser,
                            $ownerInvoice->getTransformer()->transform([
                                "fields" => ["*" => "*"],
                            ]),
                            null,
                            "Votre emprunt est désormais clôturé!"
                        )
                    );
                }
            } else {
                event(
                    new LoanPaidEvent(
                        $borrowerUser,
                        $borrowerInvoice->getTransformer()->transform([
                            "fields" => ["*" => "*"],
                        ])
                    )
                );

                // Trigger event for owner if exists and is not also the borrower.
                if ($loan->loanable->owner && !$borrowerUser->is($ownerUser)) {
                    event(
                        new LoanPaidEvent(
                            $ownerUser,
                            $ownerInvoice->getTransformer()->transform([
                                "fields" => ["*" => "*"],
                            ])
                        )
                    );
                }
            }
        }
    }
}
