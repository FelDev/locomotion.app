<?php

namespace App\Http\Controllers;

use App\Http\Requests\BaseRequest as Request;
use App\Models\Loan;
use App\Models\PrePayment;
use App\Repositories\LoanRepository;
use App\Repositories\PrePaymentRepository;
use Gate;

class PrePaymentController extends RestController
{
    public function __construct(LoanRepository $repository, PrePayment $model)
    {
        $this->repo = $repository;
        $this->model = $model;
    }

    public function complete(Request $request, $loanId)
    {
        /** @var Loan $loan */
        $loan = $this->repo->find($request, $loanId);

        Gate::authorize("prepay", $loan);

        $this->validate($request, [
            "platform_tip" => ["numeric", "nullable", "min:0"],
        ]);

        if ($request->has("platform_tip")) {
            $loan->platform_tip = $request->get("platform_tip");
            $loan->save();
            $loan->refresh();
        }

        $prePayment = $loan->prePayment;
        $prePayment->complete();
        $prePayment->save();
        $prePayment->refresh();

        // Move forward if possible.
        LoanController::loanActionsForward($prePayment->loan);

        return $prePayment;
    }
}
