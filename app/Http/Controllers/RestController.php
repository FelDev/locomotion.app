<?php

namespace App\Http\Controllers;

use App\Exports\BaseExport;
use App\Helpers\Path;
use App\Http\Requests\BaseRequest as Request;
use App\Jobs\AppendExportToFile;
use App\Jobs\NotifyUserExportCompleted;
use App\Models\AuthenticatableBaseModel;
use App\Models\BaseModel;
use App\Models\Pivots\BasePivot;
use App\Models\User;
use App\Repositories\RestRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class RestController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $repo;
    protected $model;

    protected $canReturnCsv = true;

    public function indexCsv(Request $request)
    {
        if (!$request->user()->isAdmin()) {
            abort(403, "Only global admins can export data");
        }

        return $this->respondWithCsv($request, $this->model);
    }

    public function index(Request $request)
    {
        if (
            $this->canReturnCsv &&
            $request->headers->get("accept") == "text/csv"
        ) {
            return $this->indexCsv($request);
        }

        [$items, $total] = $this->repo->get($request);
        return $this->respondWithCollection($request, $items, $total);
    }

    public function retrieve(Request $request, $id)
    {
        $item = $this->repo->find($request, $id);

        return $this->respondWithItem($request, $item);
    }

    protected function respondWithCollection($request, $items, $total)
    {
        $perPage = $request->get("per_page") ?: 10;
        if ($perPage === "*") {
            $perPage = -1;
        }
        $page = $request->get("page") ?: 1;

        if ($items->isEmpty()) {
            return new LengthAwarePaginator([], $total, $perPage, $page);
        }

        $results = $this->transformCollection($request, $items);

        return new LengthAwarePaginator($results, $total, $perPage, $page);
    }

    protected function validateAction(array $data, User $user, string $action)
    {
        $validator = Validator::make(
            $data,
            $this->model::getRules($action, $user),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    // Rules which expect a value to be present
    private const requiredLikeRules = ["required", "present", "accepted"];

    private static function isRequiredLikeRule($rule)
    {
        foreach (self::requiredLikeRules as $requiredLikeRule) {
            // This will match rules like "required_if"
            if (starts_with($rule, $requiredLikeRule)) {
                return true;
            }
        }
        return false;
    }

    // Validates input without the required rules for fields which may already be present on the model.
    protected function validateInputForUpdate(array $data, User $user)
    {
        $rules = $this->model::getRules("update", $user);
        $unRequiredRules = [];

        foreach ($rules as $field => $rule) {
            if (is_string($rule)) {
                $rule = explode("|", $rule);
            }
            $rule = array_filter(
                $rule,
                fn($r) => !is_string($r) || !self::isRequiredLikeRule($r)
            );
            if (!empty($rule)) {
                $unRequiredRules[$field] = $rule;
            }
        }

        $validator = Validator::make(
            $data,
            $unRequiredRules,
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    protected function validateAndCreate($request)
    {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "create");

        $item = $this->repo->create($input);

        $className = $request::class;
        $fieldsRequest = new $className();
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge(["fields" => $request->get("fields")]);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function respondWithItem($request, $item, $status = 200)
    {
        $result = $this->transformItem($request, $item);

        return response($result, $status);
    }

    protected function validateAndUpdate($request, $id)
    {
        $input = $request->json()->all();
        // Validatate update input
        $this->validateInputForUpdate($input, $request->user());

        $item = $this->model->findOrFail($id);
        $item->fill($input);

        foreach (
            array_merge($item->items, array_keys($item->morphOnes))
            as $relation
        ) {
            if (isset($input[$relation])) {
                $item->{$relation} = $input[$relation];
            }
        }

        // Validate the model is correct, after the update.
        $this->validateAction($item->toArray(), $request->user(), "update");

        $item = $this->repo->update($request, $id, $input);

        $className = $request::class;
        $fieldsRequest = new $className();
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge(["fields" => $request->get("fields")]);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function validateAndDestroy($request, $id)
    {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "destroy");

        return $this->repo->destroy($request, $id);
    }

    protected function validateAndRestore($request, $id)
    {
        $input = $request->json()->all();

        $this->validateAction($input, $request->user(), "destroy");

        return $this->repo->restore($request, $id);
    }

    protected function formatRules($rules)
    {
        if (is_string($rules)) {
            $rules = explode("|", $rules);
        }

        $cleanRules = [];
        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }
            $cleanRules[] = $rule;
        }

        $flipRules = array_flip($cleanRules);

        foreach ($flipRules as $key => $rule) {
            if (str_starts_with($key, "in:")) {
                unset($flipRules[$key]);
                [$_, $values] = explode(":", $key);
                $flipRules["oneOf"] = explode(",", $values);
            } elseif (strpos($key, ":")) {
                unset($flipRules[$key]);

                [$rule, $values] = explode(":", $key);
                $flipRules[$rule] = explode(",", $values);
                if (count($flipRules[$rule]) === 1) {
                    $flipRules[$rule] = array_pop($flipRules[$rule]);
                }

                switch ($rule) {
                    case "exists":
                        unset($flipRules[$rule]);
                        break;
                    case "regex":
                        $flipRules[$rule] = substr($flipRules[$rule], 1, -1);
                        break;
                    case "digits":
                        $flipRules[$rule] = (int) $flipRules[$rule];
                        break;
                    case "size":
                        $flipRules["max"] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    case "max":
                        $flipRules["max_value"] = (int) $flipRules[$rule];
                        unset($flipRules[$rule]);
                        break;
                    default:
                        break;
                }
            } else {
                $flipRules[$key] = true;
            }
        }

        return $flipRules;
    }

    protected function transformCollection($request, $items)
    {
        return $items->map(function ($item) use ($request) {
            return $this->transformItem($request, $item);
        });
    }

    protected function transformItem(
        $request,
        BasePivot|BaseModel|AuthenticatableBaseModel $item
    ) {
        return $item->getTransformer()->transform([
            "fields" => $request->getFields(),
            "!fields" => $request->getNotFields(),
            "pivot" => isset($item->pivot) ? $item->pivot : null,
        ]);
    }

    private function respondWithCsv($request, $model)
    {
        /** @var BaseExport $export */
        $export = $model::$export::fromFields(
            $request->get("fields", ""),
            $model,
            $request
        );

        $userId = $request->user()->id;
        $date = date("YmdHmi");
        $modelClassName = $model::class;
        $modelClassNameParts = explode("\\", $modelClassName);
        $modelName = array_pop($modelClassNameParts);
        $filename = Str::plural(strtolower($modelName));

        $path = "/storage/exports/";
        $basename = "$date.$userId.$filename.csv";
        $batchName = "exports_$basename";

        $url = Path::build(env("BACKEND_URL_FROM_BROWSER"), $path, $basename);

        $maxJobIndex = (int) floor(
            RestRepository::getCount($export->query()) / $export->getJobSize()
        );

        if ($maxJobIndex === 0) {
            $this->dispatchSync(
                new AppendExportToFile($export, $path, $basename)
            );

            return [
                "file" => $url,
                "queued" => false,
            ];
        }

        $jobs = [];
        for ($i = 0; $i <= $maxJobIndex; $i++) {
            $jobs[] = new AppendExportToFile($export, $path, $basename, $i);
        }
        $jobs[] = new NotifyUserExportCompleted($request->user(), $url);
        $batch = Bus::batch([
            // Nested array are chains of jobs
            $jobs,
        ])
            ->name($batchName)
            ->dispatch();

        return [
            "file" => $url,
            "queued" => true,
            "batch" => $batch->id,
        ];
    }
}
