<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Artisan;

class SchedulerController extends Controller
{
    private static function validateAndCall(
        $appKey,
        string $command,
        $params = []
    ) {
        if ($appKey !== config("app.key")) {
            return new Response("Wrong app key", 403);
        }

        Artisan::call($command, $params);
    }

    public static function nokeSyncLocks($appKey)
    {
        return self::validateAndCall($appKey, "noke:sync:locks");
    }

    public static function nokeSyncUsers($appKey)
    {
        return self::validateAndCall($appKey, "noke:sync:users");
    }

    public static function nokeSyncLoans($appKey)
    {
        return self::validateAndCall($appKey, "noke:sync:loans");
    }

    public static function actionsComplete($appKey)
    {
        return self::validateAndCall($appKey, "actions:complete");
    }

    public static function emailLoanUpcoming($appKey)
    {
        return self::validateAndCall($appKey, "email:loan:upcoming");
    }

    public static function emailPrePayement($appKey)
    {
        return self::validateAndCall($appKey, "email:loan:pre_payment_missing");
    }

    public static function filesListMissing(Request $request, $appKey)
    {
        $params = [
            "--verbose" => true,
            "--delete" => $request->boolean("delete", false),
        ];

        return self::validateAndCall($appKey, "files:list:missing", $params);
    }

    public static function filesCleanDB(Request $request, $appKey)
    {
        $params = ["--pretend" => $request->boolean("pretend", true)];

        return self::validateAndCall($appKey, "files:clean:db", $params);
    }

    public static function filesCleanStorage(Request $request, $appKey)
    {
        $params = [
            "--pretend" => $request->boolean("pretend", true),
            "--checkfiles" => $request->boolean("checkfiles", false),
        ];

        return self::validateAndCall($appKey, "files:clean:storage", $params);
    }

    public static function filesUpdateLocation(Request $request, $appKey)
    {
        $params = ["--images" => $request->boolean("images")];
        return self::validateAndCall($appKey, "files:update:location", $params);
    }
}
