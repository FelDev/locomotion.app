<?php

namespace App\Http\Controllers;

use App\Events\AddedToUserBalanceEvent;
use App\Events\BorrowerApprovedEvent;
use App\Events\BorrowerCompletedEvent;
use App\Events\ClaimedUserBalanceEvent;
use App\Events\RegistrationSubmittedEvent;
use App\Http\ErrorResponse;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\BaseRequest;
use App\Http\Requests\BaseRequest as Request;
use App\Http\Requests\User\AddToBalanceRequest;
use App\Http\Requests\User\BorrowerStatusRequest;
use App\Http\Requests\User\BorrowerSubmitRequest;
use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateEmailRequest;
use App\Http\Requests\User\UpdatePasswordRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Mail\UserMail;
use App\Models\Borrower;
use App\Models\Invoice;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use App\Repositories\CommunityRepository;
use App\Repositories\InvoiceRepository;
use App\Repositories\RestRepository;
use App\Repositories\UserRepository;
use Cache;
use Carbon\Carbon;
use Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Stripe;

class UserController extends RestController
{
    protected $communityRepo;
    protected $invoiceRepo;

    public function __construct(
        UserRepository $repository,
        User $model,
        CommunityRepository $communityRepo,
        InvoiceRepository $invoiceRepo
    ) {
        $this->repo = $repository;
        $this->model = $model;
        $this->invoiceRepo = $invoiceRepo;
        $this->communityRepo = $communityRepo;
    }

    public function create(CreateRequest $request)
    {
        $item = parent::validateAndCreate($request);

        return $this->respondWithItem($request, $item, 201);
    }

    private static function isChangedProof(User $user, $data): bool
    {
        if (
            !isset($data["residency_proof"]) &&
            !isset($data["identity_proof"])
        ) {
            return false;
        }

        $newProofs = array_map(function ($p) {
            return $p["id"];
        }, array_merge(
            $data["residency_proof"] ?? [],
            $data["identity_proof"] ?? []
        ));

        $previousProofs = $user->residencyProof
            ->merge($user->identityProof)
            ->map(function ($p) {
                return $p->id;
            })
            ->toArray();

        // Proof has changed if the number of files, or the ids of the files
        // are different.
        return sizeof($newProofs) !== sizeof($previousProofs) ||
            array_sort($previousProofs) != array_sort($newProofs);
    }

    public function markProofInvalid(BaseRequest $request, $id)
    {
        $user = User::findOrFail($id);
        Gate::authorize("checkProof", $user);
        $user->is_proof_invalid = true;
        $user->save();

        $this->respondWithItem($request, $user);
    }

    /**
     * Sends a registrationSubmittedEvent for every community where user has sufficient proofs.
     */
    private function submitUserProofs(User $user)
    {
        foreach ($user->communities as $community) {
            if (
                !$community->pivot->approved_at &&
                !$community->pivot->suspended_at &&
                (!$community->requires_identity_proof ||
                    $user->identityProof()->exists() > 0)
            ) {
                event(new RegistrationSubmittedEvent($community->pivot));
            }
        }
    }

    public function update(UpdateRequest $request, $id)
    {
        $user = User::with(["identityProof", "residencyProof"])->findOrFail(
            $id
        );

        Gate::authorize("update", $user);

        $proofChanged = static::isChangedProof($user, $request->json()->all());

        if ($proofChanged) {
            $request->merge([
                "is_proof_invalid" => false,
            ]);
        }

        $savedUser = parent::validateAndUpdate($request, $id);

        if ($proofChanged) {
            $this->submitUserProofs($user);
        }

        return $this->respondWithItem($request, $savedUser);
    }

    public function retrieve(Request $request, $id)
    {
        if ($id === "me") {
            $id = $request->user()->id;
        }

        $item = $this->repo->find($request, $id);
        return $this->respondWithItem($request, $item);
    }

    public function destroy(Request $request, $id)
    {
        $user = User::with(["borrower", "owner"])->findOrFail($id);

        Gate::authorize("deactivate", $user);

        $loansAsBorrower = $user->borrower
            ?->loans()
            ->blockingLoanableDeletion();
        $loansAsOwner = $user->owner
            ?->loanables()
            ->whereHas(
                "loans",
                fn(Builder $l) => $l->blockingLoanableDeletion()
            );
        if ($loansAsBorrower?->exists() || $loansAsOwner?->exists()) {
            throw ValidationException::withMessages([
                __("validation.custom.user_must_not_have_ongoing_loans"),
            ]);
        }

        if ($user->balance !== null && floatval($user->balance) !== 0.0) {
            throw ValidationException::withMessages([
                __("validation.custom.user_balance_not_zero"),
            ]);
        }

        return parent::validateAndDestroy($request, $id);
    }

    public function restore(Request $request, $id)
    {
        $user = User::withTrashed()->findOrFail($id);

        Gate::authorize("deactivate", $user);

        /** @var User $user */
        $user = parent::validateAndRestore($request, $id);

        if ($request->boolean("restore_loanables")) {
            $user->owner
                ->loanables()
                ->withTrashed()
                ->restore();

            if (!$request->boolean("restore_availability")) {
                // Delete all availabilities.
                $user->owner->loanables()->update([
                    "availability_json" => [],
                    "availability_mode" => "never",
                ]);
            }
        }
    }

    public function updateEmail(UpdateEmailRequest $request, $id)
    {
        //retrieve user to update
        $user = $this->repo->find($request->redirectAuth(Request::class), $id);

        // verify if the user who sent the request is not an admin. if so, we need to check for its current password.
        if (!$request->user()->isAdmin()) {
            $currentPassword = $request->get("password");

            // if the current password entered is invalid, return bad response
            if (!Hash::check($currentPassword, $user->password)) {
                return $this->respondWithItem($request, $user, 401);
            }
        }

        // change the email
        $updatedUser = $this->repo->update(
            $request,
            $id,
            $request->only("email")
        );
        return $this->respondWithItem($request, $updatedUser);
    }

    public function updatePassword(UpdatePasswordRequest $request, $id)
    {
        // retrieve user
        $user = $this->repo->find($request->redirectAuth(Request::class), $id);

        // Current password must be validated for non admins or if changing own password (even if admin)
        if (
            !$request->user()->isAdmin() ||
            $request->user()->id === $user->id
        ) {
            $currentPassword = $request->get("current");

            // if the current password entered is invalid, return bad response
            if (!Hash::check($currentPassword, $user->password)) {
                return $this->respondWithItem($request, $user, 401);
            }
        }

        // change the password
        $updatedUser = $this->repo->updatePassword(
            $request,
            $id,
            $request->get("new")
        );
        return $this->respondWithItem($request, $updatedUser);
    }

    public function addAllToCommunity(AdminRequest $request, $communityId)
    {
        [$items, $total] = $this->repo->get($request);

        /** @var User $user */
        foreach ($items as $user) {
            $user->communities()->sync([$communityId]);
        }
    }

    public function sendEmail(AdminRequest $request, $type)
    {
        [$items, $total] = $this->repo->get($request);

        $report = [];
        switch ($type) {
            case "password_reset":
                foreach ($items as $item) {
                    $authController = app(AuthController::class);
                    $passwordRequest = $request->redirectAs(
                        $item,
                        Request::class
                    );
                    $passwordRequest->merge([
                        "email" => $item->email,
                    ]);
                    $output = $authController->passwordRequest(
                        $passwordRequest
                    );
                    $report[] = [
                        "id" => $item->id,
                        "response" => array_merge($output->getData(), [
                            "status" => "success",
                        ]),
                    ];
                }
                break;
            case "registration_submitted":
            case "registration_approved":
            case "registration_rejected":
                foreach ($items as $item) {
                    $mailName = implode(
                        "\\",
                        array_map("ucfirst", explode("_", $type))
                    );
                    $mailableName = "\\App\\Mail\\{$mailName}";
                    try {
                        UserMail::queue(new $mailableName($item), $item);
                        $report[] = [
                            "id" => $item->id,
                            "response" => [
                                "status" => "success",
                            ],
                        ];
                    } catch (\Exception $e) {
                        $report[] = [
                            "id" => $item->id,
                            "response" => [
                                "status" => "error",
                                "message" => $e->getMessage(),
                            ],
                        ];
                    }
                }
                break;
            default:
                return abort(422);
        }

        return ["report" => $report];
    }

    public function indexCommunities(Request $request, $userId)
    {
        $user = $this->repo->find($request, $userId);

        $request->merge(["user_id" => $userId]);

        return $this->communityRepo->get($request);
    }

    public function retrieveCommunity(Request $request, $userId, $communityId)
    {
        $user = $this->repo->find($request, $userId);
        $community = $this->communityRepo->find($request, $communityId);

        if ($user->id && $community->id) {
            $data = [
                "communities" => ["id" => $community->id],
            ];
            $request->merge(["user_id" => $userId]);
            $request->merge(["community_id" => $communityId]);

            return $this->repo->get($user->id, $data);
        }
        return "";
    }

    public function createCommunityUser(Request $request, $userId, $communityId)
    {
        $user = $this->repo->find($request, $userId);
        $community = $this->communityRepo->find($request, $communityId);

        if ($user->communities->where("id", $communityId)->isEmpty()) {
            $user->communities()->attach($community);

            return $this->respondWithItem($request, $community);
        }

        return $this->respondWithItem(
            $request,
            $user->communities->where("id", $communityId)->first()
        );
    }

    public function deleteCommunityUser(Request $request, $userId, $communityId)
    {
        $community = $this->communityRepo->find($request, $communityId);
        $user = $this->repo->find($request, $userId);

        if ($user->communities->where("id", $communityId)->isNotEmpty()) {
            $user->communities()->detach($community);
        }

        return $community;
    }

    public function submitBorrower(BorrowerSubmitRequest $request, $id)
    {
        $user = $request->user();
        Gate::authorize("update", $user);

        /** @var User $item */
        $item = $this->repo->find($request, $id);
        $borrower = RestRepository::for($item->borrower)->update(
            $request,
            $item->borrower->id,
            $request->json()->all()
        );
        $borrower->approved_at = null;
        $borrower->submitted_at = Carbon::now();
        $borrower->save();

        event(new BorrowerCompletedEvent($item));

        return $this->respondWithItem($request, $borrower->refresh());
    }

    public function approveBorrower(BorrowerStatusRequest $request, $id)
    {
        $item = $this->repo->find($request, $id);

        if (!$item->borrower->approved_at) {
            $item->borrower->approved_at = new Carbon();
            $item->borrower->save();

            event(new BorrowerApprovedEvent($item));
        }

        return $this->respondWithItem($request, $item->borrower);
    }

    public function retrieveBorrower(Request $request, $id)
    {
        $item = $this->repo->find($request, $id);

        return $this->respondWithItem($request, $item->borrower);
    }

    public function suspendBorrower(BorrowerStatusRequest $request, $id)
    {
        $item = $this->repo->find($request, $id);

        if (!$item->borrower->suspended_at) {
            $item->borrower->suspended_at = new \DateTime();
            $item->borrower->save();
        }

        return $this->respondWithItem($request, $item->borrower);
    }

    public function unsuspendBorrower(BorrowerStatusRequest $request, $id)
    {
        $item = $this->repo->find($request, $id);

        if ($item->borrower->suspended_at) {
            $item->borrower->suspended_at = null;
            $item->borrower->save();
        }

        return $this->respondWithItem($request, $item->borrower);
    }

    public function getBalance(Request $request, $userId)
    {
        $user = $this->repo->find($request, $userId);

        return $user->balance;
    }

    public function addToBalance(AddToBalanceRequest $request, $userId)
    {
        $findRequest = $request->redirectAuth(Request::class);
        $user = $this->repo->find($findRequest, $userId);

        $transactionId = $request->get("transaction_id");
        $amount = $request->get("amount");
        $paymentMethodId = $request->get("payment_method_id");

        if ($paymentMethodId) {
            $paymentMethod = $user->paymentMethods
                ->where("id", $paymentMethodId)
                ->first();
        } else {
            $paymentMethod = $user->defaultPaymentMethod;
        }

        if (!$paymentMethod) {
            return ErrorResponse::withMessage("no_payment_method", 400);
        }

        $amountForDisplay = Invoice::formatAmountForDisplay($amount);
        $amountWithFee = Stripe::computeAmountWithFee($amount, $paymentMethod);
        $amountWithFeeInCents = intval($amountWithFee * 100);
        if ($amountWithFeeInCents <= 0) {
            return ErrorResponse::withMessage(
                "amount_in_cents_is_nothing",
                400
            );
        }

        $fee = round($amountWithFee - $amount, 2);
        $feeForDisplay = Invoice::formatAmountForDisplay($fee);

        $charge = null;
        try {
            $customerId = $user->getStripeCustomer()->id;
            $charge = Stripe::createCharge(
                $amountWithFeeInCents,
                $customerId,
                "Ajout au compte LocoMotion: {$amountForDisplay}$ + {$feeForDisplay}$ (frais)",
                $paymentMethod->external_id
            );
        } catch (\Exception $e) {
            $message = $e->getMessage();
            return ErrorResponse::withError("Stripe: {$message}", 400);
        }

        try {
            $invoice = new Invoice();

            $invoice->period = Carbon::now()
                ->locale("fr_FR")
                ->isoFormat("LLLL");
            // Set invoice type to credit, since we're adding to the balance
            $invoice->type = "credit";
            $invoice->user()->associate($user);
            $invoice->save();

            $stripeMeta = [
                "sourceType" => "stripe",
                "stripeChargeId" => $charge->id,
            ];

            $invoice->billItems()->create([
                "item_type" => "provision.net",
                "label" =>
                    "Ajout au compte LocoMotion : " . "{$amountForDisplay}$",
                "amount" => $amount,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "amount_type" => "credit",
                "meta" => array_merge($stripeMeta, [
                    "destType" => "user",
                    "destUserId" => $user->id,
                    "destUserName" => $user->name,
                    "destUserEmail" => $user->email,
                ]),
            ]);
            $invoice->billItems()->create([
                "item_type" => "provision.transactionFees",
                "label" => "Frais de transaction Stripe : " . "{$fee}$",
                "amount" => $fee,
                "item_date" => date("Y-m-d"),
                "taxes_tps" => 0,
                "taxes_tvq" => 0,
                "amount_type" => "credit",
                "meta" => array_merge($stripeMeta, [
                    "destType" => "transactionFees",
                ]),
            ]);

            $invoice->balance_before = $user->balance;
            $user->balance += $amount;
            $invoice->balance_after = $user->balance;

            $user->transaction_id = $transactionId + 1;
            $user->save();

            // Will save invoice so no need to save balance_* here.
            $invoice->payWith($paymentMethod);

            $this->triggerInvoicePaidEvent($user, $invoice);

            return response($user->balance, 200);
        } catch (\Exception $e) {
            if ($charge) {
                \Stripe\Refund::create([
                    "charge" => $charge->id,
                ]);
            }

            $user->balance -= $amount;
            $user->transaction_id = $transactionId - 1;
            $user->save();

            return ErrorResponse::withMessage($e->getMessage(), 500);
        }
    }

    public function claimBalance(Request $request, $userId)
    {
        $user = $this->repo->find($request, $userId);

        if (Cache::get("user:claim:$userId")) {
            return ErrorResponse::withMessage("Déjà soumis.", 429);
        }

        event(new ClaimedUserBalanceEvent($user));

        Cache::put("user:claim:$userId", 14400);

        return ErrorResponse::withMessage("", 201);
    }

    public function template(Request $request)
    {
        $template = [
            "item" => [
                "name" => "",
                "borrower" => new \stdClass(),
                "accept_conditions" => false,
            ],
            "form" => [
                "general" => [
                    "email" => [
                        "type" => "email",
                    ],
                    "name" => [
                        "type" => "text",
                    ],
                    "last_name" => [
                        "type" => "text",
                    ],
                    "avatar" => [
                        "type" => "image",
                    ],
                    "description" => [
                        "type" => "textarea",
                    ],
                    "date_of_birth" => [
                        "type" => "date",
                        "initial_view" => "year",
                    ],
                    "address" => [
                        "type" => "text",
                    ],
                    "postal_code" => [
                        "type" => "text",
                    ],
                    "phone" => [
                        "type" => "text",
                    ],
                    "is_smart_phone" => [
                        "type" => "checkbox",
                    ],
                    "other_phone" => [
                        "type" => "text",
                    ],
                    "accept_conditions" => [
                        "type" => "checkbox",
                    ],
                    "bank_transit_number" => [
                        "type" => "text",
                    ],
                    "bank_institution_number" => [
                        "type" => "text",
                    ],
                    "bank_account_number" => [
                        "type" => "text",
                    ],
                ],
                "borrower" => [
                    "drivers_license_number" => [
                        "type" => "text",
                    ],
                    "has_not_been_sued_last_ten_years" => [
                        "type" => "checkbox",
                    ],
                    "gaa" => [
                        "type" => "files",
                    ],
                    "saaq" => [
                        "type" => "files",
                    ],
                ],
            ],
        ];

        $modelRules = $this->model->getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"]["general"][$field])) {
                continue;
            }
            $template["form"]["general"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        $borrowerRules = Borrower::getRules("template", $request->user());
        foreach ($modelRules as $field => $rules) {
            if (!isset($template["form"]["borrower"][$field])) {
                continue;
            }
            $template["form"]["borrower"][$field]["rules"] = $this->formatRules(
                $rules
            );
        }

        return $template;
    }

    public function acceptConditions(Request $request)
    {
        $request->user()->acceptConditions();
        $request->user()->save();
    }

    private function triggerInvoicePaidEvent($user, $invoice)
    {
        $invoiceRequest = new Request();
        $invoiceRequest->setUserResolver(function () use ($user) {
            return $user;
        });
        $invoiceRequest->merge(["fields" => "*,bill_items.*"]);

        $item = $this->invoiceRepo->find($invoiceRequest, $invoice->id);
        $invoiceTransformer = $item->getTransformer();

        event(
            new AddedToUserBalanceEvent(
                $user,
                $invoiceTransformer->transform([
                    "fields" => ["*" => "*"],
                ])
            )
        );
    }
}
