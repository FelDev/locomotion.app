<?php

namespace App\Http\Middleware;

use Auth;
use Carbon\Carbon;
use Closure;
use Laravel\Passport\Token;
use Symfony\Component\HttpFoundation\Response;

class AuthenticateWithQueryParam
{
    public function handle($request, Closure $next)
    {
        $tokenString = $request->query("token");

        if (!$tokenString) {
            return false;
        }

        $token = Token::where("revoked", false)
            ->where("expires_at", ">", Carbon::now())
            ->find($tokenString);

        if (!$token || $token->client->revoked || !$token->user) {
            return new Response("Accès interdit", 403);
        }

        $user = $token->user;
        $user->withAccessToken($token);
        $request->setUserResolver(fn() => $user);
        Auth::setUser($user);

        return $next($request);
    }
}
