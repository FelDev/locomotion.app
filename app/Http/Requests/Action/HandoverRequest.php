<?php

namespace App\Http\Requests\Action;

use App\Enums\LoanableTypes;
use App\Http\Requests\BaseRequest;
use App\Models\Loan;

class HandoverRequest extends BaseRequest
{
    public function rules()
    {
        $loanId = $this->route("loan_id") ?: $this->get("loan_id");
        $loan = Loan::accessibleBy($this->user())->find($loanId);

        if ($loan->loanable->type === LoanableTypes::Car) {
            $pricing = $loan->community->getPricingFor($loan->loanable->type);

            if (!$pricing) {
                $price = 0;
            }

            $values = $pricing->evaluateRule(
                $this->get("mileage_end") - $loan->takeover->mileage_beginning,
                $loan->actual_duration_in_minutes,
                $loan->loanable,
                $loan
            );

            $price = max(
                0,
                ($values["time"] ?? 0) + ($values["distance"] ?? 0)
            );

            return [
                "mileage_end" => ["required", "integer"],
                "purchases_amount" => ["numeric", "lte:$price"],
            ];
        }

        return [];
    }

    public function messages()
    {
        return [
            "purchases_amount.lte" => __("validation.custom.purchases_amount"),
        ];
    }

    public function attributes()
    {
        return [
            "purchases_amount" => "Total des dépenses",
        ];
    }
}
