<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Loanable */
class ArchivedLoanableResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "name" => "(Archivé) $this->name",
            "deleted_at" => $this->deleted_at,
            "owner" => new OwnerUserResource($this->owner),
            "coowners" => [],
            "show_owner_as_contact" => $this->show_owner_as_contact,
        ];
    }
}
