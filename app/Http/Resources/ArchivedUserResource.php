<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\User */
class ArchivedUserResource extends JsonResource
{
    public function toArray($request)
    {
        if ($request->user()->can("seeArchivedDetails", $this->getModel())) {
            return [
                "id" => $this->id,
                "name" => "(Archivé) $this->name",
                "last_name" => $this->last_name,
                "full_name" => "(Archivé) $this->full_name",
                "deleted_at" => $this->deleted_at,
                "email" => $this->email,
            ];
        }

        return [
            "id" => $this->id,
            "name" => "Archivé",
            "last_name" => "Archivé",
            "full_name" => "Compte archivé",
            "deleted_at" => $this->deleted_at,
        ];
    }
}
