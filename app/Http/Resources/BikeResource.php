<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Bike */
class BikeResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "model" => $this->model,
            "size" => $this->size,
            "bike_type" => $this->bike_type,
        ];
    }
}
