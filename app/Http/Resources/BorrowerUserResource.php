<?php

namespace App\Http\Resources;

use App\Models\Borrower;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Borrower */
class BorrowerUserResource extends JsonResource
{
    public function __construct(
        Borrower $borrower,
        private readonly bool $showUserPhone = false
    ) {
        parent::__construct($borrower);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => new UserBriefResource($this->user, $this->showUserPhone),
        ];
    }
}
