<?php

namespace App\Http\Resources;

use App\Models\Loanable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Loanable */
class DashboardLoanableResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->trashed()) {
            return (new ArchivedLoanableResource($this))->toArray($request);
        }

        return [
            "id" => $this->id,
            "image" => new ImageResource($this->image),
            "is_self_service" => $this->is_self_service,
            "name" => $this->name,
            "type" => $this->type,
            "owner" => new OwnerUserResource($this->owner),
            "coowners" => CoownerUserResource::collection($this->coowners),
        ];
    }
}
