<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Extension */
class ExtensionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
            "new_duration" => $this->new_duration,
            "comments_on_extension" => $this->comments_on_extension,
            "contested_at" => $this->contested_at,
            "comments_on_contestation" => $this->comments_on_contestation,
            "message_for_borrower" => $this->message_for_borrower,
        ];
    }
}
