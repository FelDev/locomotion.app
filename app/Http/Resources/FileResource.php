<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\File */
class FileResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "path" => $this->path,
            "filename" => $this->filename,
            "url" => $this->url,
        ];
    }
}
