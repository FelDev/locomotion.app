<?php

namespace App\Http\Resources;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin Image */
class ImageResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "path" => $this->path,
            "filename" => $this->filename,
            "original_filename" => $this->original_filename,
            "width" => $this->width,
            "height" => $this->height,
            "url" => $this->url,
            "sizes" => $this->sizes,
        ];
    }
}
