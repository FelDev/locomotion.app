<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Intention */
class IntentionResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "executed_at" => $this->executed_at,
            "status" => $this->status,
            "message_for_borrower" => $this->message_for_borrower,
        ];
    }
}
