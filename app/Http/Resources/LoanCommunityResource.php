<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Community */
class LoanCommunityResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "chat_group_url" => $this->chat_group_url,
            "starting_guide_url" => $this->starting_guide_url,
        ];
    }
}
