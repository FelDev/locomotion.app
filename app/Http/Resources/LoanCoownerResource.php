<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Coowner */
class LoanCoownerResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => new UserBriefResource(
                $this->user,
                $this->show_as_contact
            ),
            "title" => $this->title,
            "show_as_contact" => $this->show_as_contact,
        ];
    }
}
