<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Loan */
class LoanResource extends JsonResource
{
    public static $wrap = false;
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "created_at" => $this->created_at,
            "departure_at" => $this->departure_at,
            "duration_in_minutes" => $this->duration_in_minutes,
            "estimated_distance" => $this->estimated_distance,
            "reason" => $this->reason,
            "message_for_owner" => $this->message_for_owner,
            "final_price" => $this->final_price,
            "platform_tip" => $this->platform_tip,
            "final_insurance" => $this->final_insurance,
            "final_platform_tip" => $this->final_platform_tip,
            "canceled_at" => $this->canceled_at,
            "final_purchases_amount" => $this->final_purchases_amount,
            "status" => $this->status,
            "actual_return_at" => $this->actual_return_at,
            "owner_validated_at" => $this->owner_validated_at,
            "borrower_validated_at" => $this->borrower_validated_at,
            "actual_duration_in_minutes" => $this->actual_duration_in_minutes,
            "actual_insurance" => $this->actual_insurance,
            "actual_price" => $this->actual_price,
            "calendar_days" => $this->calendar_days,
            "is_contested" => $this->is_contested,
            "is_free" => $this->is_free,
            "needs_validation" => $this->needs_validation,
            "total_actual_cost" => $this->total_actual_cost,
            "total_final_cost" => $this->total_final_cost,

            "community" => new LoanCommunityResource($this->community),
            "intention" => new IntentionResource($this->intention),
            "takeover" => new TakeoverResource($this->takeover),
            "pre_payment" => new PrePaymentResource($this->prePayment),
            "extensions" => ExtensionResource::collection($this->extensions),
            "incidents" => IncidentResource::collection($this->incidents),
            "handover" => new HandoverResource($this->handover),
            "payment" => new PaymentResource($this->payment),

            "borrower" => new BorrowerUserResource(
                $this->borrower,
                $this->canSeeLoanParticipantInfo($request->user())
            ),
            "loanable" => new LoanLoanableResource(
                $this->loanable,
                $this->resource
            ),
        ];
    }

    private function canSeeLoanParticipantInfo(User $user)
    {
        return $user->isAdmin() ||
            $user->isAdminOfCommunity($this->community_id) ||
            $this->borrower->user->is($user) ||
            $this->loanable->isOwnerOrCoowner($user);
    }
}
