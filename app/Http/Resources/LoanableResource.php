<?php

namespace App\Http\Resources;

use App\Enums\LoanableTypes;
use Illuminate\Http\Resources\Json\JsonResource;

/** @mixin \App\Models\Loanable */
class LoanableResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => $this->type,
            "show_owner_as_contact" => $this->show_owner_as_contact,
            "position_google" => $this->position_google,
            "position" => $this->position,
            "comments" => $this->comments,
            "is_self_service" => $this->is_self_service,
            "location_description" => $this->location_description,
            "name" => $this->name,

            "coowners" => CoownerUserResource::collection(
                $this->coowners->load("user.avatar")
            ),
            "owner" => new OwnerUserResource($this->owner->load("user.avatar")),
            "image" => new ImageResource($this->image),
            "details" => $this->getLoanableDetailsResource($this->details),
        ];
    }

    private function getLoanableDetailsResource($data): JsonResource
    {
        return match ($this->type) {
            LoanableTypes::Bike => new BikeResource($data),
            LoanableTypes::Trailer => new TrailerResource($data),
            LoanableTypes::Car => new CarResource($data),
        };
    }
}
