<?php

namespace App\Http;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;

class RouteHelper
{
    public static function resource($slug, $controller = null, $except = [])
    {
        $camelSlug = Str::camel(str_replace("-", "_", $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $controller = $controller ?: ucfirst("{$camelSlug}Controller");

        Route::prefix(str_replace("-", "_", $pluralSlug))->group(
            function () use ($controller, $pluralSlug, $except) {
                if (!in_array("index", $except)) {
                    static::indexRoute(
                        $controller,
                        $pluralSlug,
                        "$pluralSlug.index"
                    );
                }

                if (!in_array("create", $except)) {
                    Route::post("/", "$controller@create")->name(
                        "$pluralSlug.create"
                    );
                }

                if (!in_array("restore", $except)) {
                    Route::put("/{id}/restore", "$controller@restore")
                        ->where("id", "[0-9]+")
                        ->name("$pluralSlug.restore");
                }

                if (!in_array("retrieve", $except)) {
                    static::retrieveRoute(
                        $controller,
                        $pluralSlug,
                        "$pluralSlug.retrieve"
                    );
                }

                if (!in_array("update", $except)) {
                    Route::put("/{id}", "$controller@update")
                        ->where("id", "[0-9]+")
                        ->name("$pluralSlug.update");
                }

                if (!in_array("destroy", $except)) {
                    Route::delete("/{id}", "$controller@destroy")
                        ->where("id", "[0-9]+")
                        ->name("$pluralSlug.destroy");
                }

                if (!in_array("template", $except)) {
                    Route::options("/", "$controller@template")->name(
                        "$pluralSlug.template"
                    );
                }
            }
        );
    }

    public static function index($slug, $options = [])
    {
        $camelSlug = Str::camel(str_replace("-", "_", $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $routeName = array_get($options, "name") ?: "$pluralSlug.index";

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(function () use (
            $controller,
            $pluralSlug,
            $routeName
        ) {
            static::indexRoute($controller, $pluralSlug, $routeName);
        });
    }

    private static function indexRoute($controller, $pluralSlug, $routeName)
    {
        Route::get("/", "$controller@index")->name($routeName);
    }

    public static function retrieve($slug, $options = [])
    {
        $camelSlug = Str::camel(str_replace("-", "_", $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $routeName = array_get($options, "name") ?: "$pluralSlug.retrieve";

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(function () use (
            $controller,
            $pluralSlug,
            $routeName
        ) {
            static::retrieveRoute($controller, $pluralSlug, $routeName);
        });
    }

    private static function retrieveRoute($controller, $pluralSlug, $routeName)
    {
        Route::get("/{id}", "$controller@retrieve")
            ->where("id", "[0-9]+")
            ->name($routeName);
    }

    public static function subresource(
        $parentSlug,
        $slug,
        $controller = null,
        $except = []
    ) {
        $parentCamelSlug = Str::camel(str_replace("-", "_", $parentSlug));
        $parentPluralSlug = Pluralizer::plural($parentSlug);

        $pluralSlug = Pluralizer::plural($slug);

        $controller = $controller ?: ucfirst("{$parentCamelSlug}Controller");
        $methodSuffix = ucfirst(Str::camel(Pluralizer::plural($slug)));

        if (!in_array("index", $except)) {
            Route::get(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug",
                "$controller@index{$methodSuffix}"
            )
                ->where("{$parentSlug}_id", "[0-9]+")
                ->name("{$parentPluralSlug}_{$pluralSlug}.index");
        }

        if (!in_array("create", $except)) {
            Route::post(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug",
                "$controller@create{$methodSuffix}"
            )
                ->where("{$parentSlug}_id", "[0-9]+")
                ->name("{$parentPluralSlug}_{$pluralSlug}.create");
        }
        if (!in_array("retrieve", $except)) {
            Route::get(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@retrieve{$methodSuffix}"
            )
                ->where("{$parentSlug}_id", "[0-9]+")
                ->where("{$slug}_id", "[0-9]+")
                ->name("{$parentPluralSlug}_{$pluralSlug}.retrieve");
        }
        if (!in_array("update", $except)) {
            Route::put(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@update{$methodSuffix}"
            )
                ->where("{$parentSlug}_id", "[0-9]+")
                ->where("{$slug}_id", "[0-9]+")
                ->name("{$parentPluralSlug}_{$pluralSlug}.update");
        }
        if (!in_array("destroy", $except)) {
            Route::delete(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@destroy{$methodSuffix}"
            )
                ->where("{$parentSlug}_id", "[0-9]+")
                ->name("{$parentPluralSlug}_{$pluralSlug}.destroy");
        }

        if (!in_array("restore", $except)) {
            Route::put(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}/restore",
                "$controller@restore{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.restore");
        }
    }
}
