<?php

namespace App\Jobs;

use App\Exports\BaseExport;
use App\Helpers\Path;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\File;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AppendExportToFile implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        Batchable;

    public function __construct(
        private readonly BaseExport $export,
        private readonly string $path,
        private readonly string $filename,
        private readonly int $jobNumber = 0
    ) {
    }

    public function getJobNumber(): int
    {
        return $this->jobNumber;
    }

    private function getLocalFilePath(): string
    {
        return storage_path("framework/exports/$this->filename");
    }

    private function getStorageFilePath(): string
    {
        return Path::join($this->path, $this->filename);
    }

    public function handle(): void
    {
        $export = $this->export;
        $localFilePath = $this->getLocalFilePath();
        if ($this->jobNumber !== 0) {
            $localFileSaved = \File::put(
                $localFilePath,
                \Storage::get($this->getStorageFilePath())
            );

            if (!$localFileSaved) {
                throw new \Exception(
                    "Could not copy in progress export from server"
                );
            }

            $file = fopen($localFilePath, "a");
        } else {
            $file = fopen($localFilePath, "w");
            fputcsv($file, $this->export->headings());
        }

        $page = $this->jobNumber * $this->export->jobChunkCount() + 1;
        $count = $this->export->chunkSize();

        $query = $this->export->query();
        $chunkCount = $this->export->jobChunkCount();

        for ($i = 0; $i < $chunkCount; $i++) {
            $lines = $query->forPage($page + $i, $count)->get();
            foreach ($lines as $line) {
                fputcsv($file, $export->map($line));
            }
            unset($lines);
        }

        fclose($file);

        $saved = \Storage::putFileAs(
            $this->path,
            new File($localFilePath),
            $this->filename
        );

        if (!$saved) {
            throw new \Exception(
                "Could not save export $localFilePath to storage."
            );
        }

        \File::delete($localFilePath);
    }

    public function failed()
    {
        $localFilePath = $this->getLocalFilePath();
        if (\File::exists($localFilePath)) {
            \File::delete($localFilePath);
        }
    }
}
