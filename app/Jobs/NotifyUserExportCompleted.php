<?php

namespace App\Jobs;

use App\Mail\ExportCompletedMail;
use App\Mail\UserMail;
use App\Models\User;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotifyUserExportCompleted implements ShouldQueue
{
    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels,
        Batchable;

    public function __construct(
        private readonly User $user,
        private readonly string $exportUrl
    ) {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getExportUrl(): string
    {
        return $this->exportUrl;
    }

    public function handle(): void
    {
        UserMail::queue(new ExportCompletedMail($this->exportUrl), $this->user);
    }
}
