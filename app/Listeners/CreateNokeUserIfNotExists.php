<?php

namespace App\Listeners;

use App\Events\LoanCreatedEvent;
use App\Events\RegistrationApprovedEvent;
use App\Facades\Noke;
use Exception;
use Illuminate\Support\Facades\Log;

class CreateNokeUserIfNotExists
{
    /**
     * @throws Exception When we fail to create a noke user for a new loan requiring it.
     */
    public function handle($event): void
    {
        if ($event instanceof LoanCreatedEvent) {
            if ($event->loan->loanable->padlock && !$event->user->noke_id) {
                try {
                    Noke::createUser($event->user);
                } catch (\Throwable $e) {
                    throw new Exception(
                        "Could not create a noke user for {$event->user->id} required for this loan.",
                        500,
                        $e
                    );
                }
            }
        }

        if ($event instanceof RegistrationApprovedEvent) {
            if (
                !$event->communityUser->community->uses_noke ||
                $event->communityUser->user->noke_id
            ) {
                return;
            }

            try {
                Noke::createUser($event->communityUser->user);
            } catch (\Throwable $e) {
                // it's OK if the noke account is not created immediately, since it can still
                // be created when a new loan is made or when we sync noke users
                Log::error($e->getMessage(), $e->getTrace());
            }
        }
    }
}
