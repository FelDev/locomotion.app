<?php

namespace App\Listeners;

use App\Events\CoownerRemovedEvent;
use App\Mail\CoownerRemovedMail;
use App\Mail\UserMail;

class SendCoownerRemovedMail
{
    public function handle(CoownerRemovedEvent $event)
    {
        if (!$event->coowner->user->is($event->remover)) {
            // Must send now rather than queue, as coowner will cease to exist.
            UserMail::queue(
                new CoownerRemovedMail($event->coowner, $event->remover),
                $event->coowner->user
            );
        }
    }
}
