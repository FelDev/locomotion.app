<?php

namespace App\Listeners;

use App\Events\LoanCompletedEvent;
use App\Mail\Loan\LoanCompleted;
use App\Mail\UserMail;
use App\Models\Loan;
use App\Models\User;

class SendLoanCompletedEmails
{
    public function handle(LoanCompletedEvent $event)
    {
        $loan = $event->loan;

        self::sendMail($loan->borrower->user, $loan, false);

        if (!$loan->loanable->is_self_service) {
            $loan->loanable->forAllCoowners(function (User $user) use ($loan) {
                self::sendMail($user, $loan, true);
            }, $loan->borrower->user);
        }
    }

    private static function sendMail(User $user, Loan $loan, bool $isOwner)
    {
        UserMail::queueMandrill(
            new LoanCompleted($user, $loan, $isOwner),
            $user
        );
    }
}
