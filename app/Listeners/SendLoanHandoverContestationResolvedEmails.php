<?php

namespace App\Listeners;

use App\Events\LoanHandoverContestationResolvedEvent;
use App\Mail\Loan\HandoverContestationResolved as LoanHandoverContestationResolved;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanHandoverContestationResolvedEmails
{
    /*
       Send contestation-resolved notification to owner and borrower.
       Only send one copy if owner is borrower.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanHandoverContestationResolvedEvent $event)
    {
        $loan = $event->handover->loan;
        $admin = $event->user;
        $borrower = $loan->borrower;

        UserMail::queue(
            new LoanHandoverContestationResolved(
                $event->handover,
                $loan,
                $borrower->user,
                $admin
            ),
            $borrower->user
        );

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan,
            $admin
        ) {
            return new LoanHandoverContestationResolved(
                $event->handover,
                $loan,
                $user,
                $admin
            );
        }, $borrower->user);
    }
}
