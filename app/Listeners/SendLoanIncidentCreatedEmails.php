<?php

namespace App\Listeners;

use App\Models\User;
use App\Events\LoanIncidentCreatedEvent;
use App\Mail\LoanIncidentCreated;
use App\Mail\LoanIncidentReviewable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Mail;

class SendLoanIncidentCreatedEmails
{
    /*
       Send loan-incident-created notification to owner if not also the
       borrower.

       Also notify admins because they are the only ones who can resolve incidents.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanIncidentCreatedEvent $event)
    {
        $loan = $event->incident->loan;
        $borrower = $loan->borrower;
        $owner = $loan->loanable->owner;

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan,
            $borrower
        ) {
            return new LoanIncidentCreated(
                $event->incident,
                $loan,
                $borrower,
                $user
            );
        }, $borrower->user);

        foreach ($loan->community->admins() as $admin) {
            Mail::queue(
                (new LoanIncidentReviewable(
                    $event->incident,
                    $loan,
                    $borrower,
                    $owner,
                    $loan->community
                ))->to($admin->email, $admin->full_name)
            );
        }
    }
}
