<?php

namespace App\Listeners;

use App\Events\LoanIncidentResolvedEvent;
use App\Mail\LoanIncidentResolved;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanIncidentResolvedEmails
{
    /*
       Send incident-resolved notification to owner and borrower.
       Only send one copy if owner is borrower.

       These rules apply for on-demand as well as self-service vehicles.
    */
    public function handle(LoanIncidentResolvedEvent $event)
    {
        $loan = $event->incident->loan;
        $borrower = $loan->borrower;

        UserMail::queue(
            new LoanIncidentResolved($event->incident, $loan, $borrower->user),
            $borrower->user
        );

        $loan->loanable->queueMailToOwners(function (User $user) use (
            $event,
            $loan
        ) {
            return new LoanIncidentResolved($event->incident, $loan, $user);
        }, $borrower->user);
    }
}
