<?php

namespace App\Listeners;

use App\Events\LoanIntentionAcceptedEvent;
use App\Mail\LoanIntentionAccepted;
use App\Mail\UserMail;

class SendLoanIntentionAcceptedEmails
{
    /*
       Send loan-intention-accepted notification to borrower if loanable is not
       self-service and borrower is not also the owner.
    */
    public function handle(LoanIntentionAcceptedEvent $event)
    {
        $loan = $event->intention->loan;
        $loanable = $loan->loanable;
        $owner = $loanable->owner;
        $borrower = $loan->borrower;

        if (
            !$loanable->is_self_service &&
            $owner &&
            $owner->user->id !== $borrower->user->id
        ) {
            UserMail::queue(
                new LoanIntentionAccepted(
                    $event->intention,
                    $loan,
                    $borrower,
                    $owner
                ),
                $borrower->user
            );
        }
    }
}
