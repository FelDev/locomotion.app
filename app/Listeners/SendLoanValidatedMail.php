<?php

namespace App\Listeners;

use App\Events\LoanPartiallyValidatedEvent;
use App\Mail\Loan\Validated as LoanValidatedMail;
use App\Mail\UserMail;
use App\Models\User;

class SendLoanValidatedMail
{
    public function handle(LoanPartiallyValidatedEvent $event)
    {
        if (
            $event->loan->isFullyValidated() ||
            !$event->loan->needs_validation
        ) {
            return;
        }

        $borrowerUser = $event->loan->borrower->user;
        if ($event->validator->is($borrowerUser)) {
            $event->loan->loanable->queueMailToOwners(function (
                User $coowner
            ) use ($event) {
                return new LoanValidatedMail(
                    $event->loan,
                    $coowner,
                    $event->validator,
                    false
                );
            });
        } else {
            UserMail::queue(
                new LoanValidatedMail(
                    $event->loan,
                    $borrowerUser,
                    $event->validator,
                    true
                ),
                $borrowerUser
            );
        }
    }
}
