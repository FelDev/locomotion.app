<?php

namespace App\Listeners;

use App\Events\RegistrationApprovedEvent;
use App\Mail\Registration\Approved as RegistrationApproved;
use App\Mail\UserMail;

class SendRegistrationApprovedEmails
{
    public function handle(RegistrationApprovedEvent $event)
    {
        $user = $event->communityUser->user;

        if (!isset($user->meta["sent_registration_approved_email"])) {
            UserMail::queueMandrill(
                new RegistrationApproved($event->communityUser),
                $user
            );

            // Save Meta
            $meta = $user->meta;
            $meta["sent_registration_approved_email"] = true;
            $user->meta = $meta;
            $user->save();
        }
    }
}
