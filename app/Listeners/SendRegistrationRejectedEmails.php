<?php

namespace App\Listeners;

use App\Events\RegistrationRejectedEvent;
use App\Mail\Registration\Rejected as RegistrationRejected;
use App\Mail\UserMail;

class SendRegistrationRejectedEmails
{
    public function handle(RegistrationRejectedEvent $event)
    {
        $user = $event->user;

        UserMail::queue(new RegistrationRejected($user), $user);
    }
}
