<?php

namespace App\Listeners;

use App\Events\RegistrationSubmittedEvent;
use App\Mail\Registration\Reviewable as RegistrationReviewable;
use App\Mail\Registration\Submitted as RegistrationSubmitted;
use App\Mail\UserMail;
use Carbon\Carbon;

class SendRegistrationSubmittedEmails
{
    public function handle(RegistrationSubmittedEvent $event)
    {
        $user = $event->communityUser->user;
        $community = $event->communityUser->community;

        $meta = $user->meta;
        // Send the email to the user if that's the first time he registers
        if (!isset($meta["sent_registration_submitted_email"])) {
            UserMail::queueMandrill(new RegistrationSubmitted($user), $user);
            // Save Meta
            $meta["sent_registration_submitted_email"] = true;
            $user->meta = $meta;
            $user->save();
        }

        // Avoid sending proof updated emails too frequently
        try {
            if (
                isset(
                    $meta["last_registration_reviewable_date"][$community->id]
                ) &&
                Carbon::now()
                    ->subHour()
                    ->isBefore(
                        $meta["last_registration_reviewable_date"][
                            $community->id
                        ]
                    )
            ) {
                return;
            }
        } catch (\Exception $e) {
            \Log::warning(
                "user $user->id meta.last_registration_reviewable for community " .
                    "$community->id is not a valid date: '" .
                    "{$meta["last_registration_reviewable_date"][$community->id]}'."
            );
        }

        foreach ($community->admins() as $admin) {
            UserMail::queue(
                new RegistrationReviewable($user, $community),
                $admin
            );
        }

        $meta = $user->meta;
        if (
            !isset($meta["last_registration_reviewable_date"]) ||
            !is_array($meta["last_registration_reviewable_date"])
        ) {
            $meta["last_registration_reviewable_date"] = [];
        }

        $meta["last_registration_reviewable_date"][
            $community->id
        ] = Carbon::now()->toISOString();
        $user->meta = $meta;
        $user->save();
    }
}
