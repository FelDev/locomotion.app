<?php

namespace App\Listeners;

use App\Events\UserEmailUpdated;
use Exception;
use Noke;

class UpdateNokeUserEmail
{
    /**
     * @throws Exception
     */
    public function handle(UserEmailUpdated $event)
    {
        if (!in_array(app()->environment(), ["production", "testing"])) {
            return;
        }

        if (!$event->user->noke_id) {
            return;
        }

        try {
            $nokeUser = Noke::findUserById($event->user->noke_id);
            $nokeUser->username = $event->newEmail;
            Noke::updateUser($nokeUser);
        } catch (\Throwable $e) {
            throw new Exception(
                "Could not update the email for the associated noke user for user {$event->user->id}",
                $e
            );
        }
    }
}
