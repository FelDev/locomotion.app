<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Mime\Email;

abstract class BaseMailable extends Mailable
{
    public function send($mailer)
    {
        $mailableClass = static::class;
        // Prepare a semicolon-separated string of email
        // addresses.
        $emailAddresses = implode(
            "; ",
            array_map(function ($recipient) {
                return $recipient["address"];
            }, $this->to)
        );

        // Add some extra parameters to the message in
        // order to write them in the logs.
        $this->withSymfonyMessage(function (Email $message) use (
            $mailableClass
        ) {
            $message->mailable_class = $mailableClass;
            $message->sent_at = date("Y-m-d H:i:s");
        });

        Log::info("OK Sending $mailableClass to {$emailAddresses}");

        parent::send($mailer);
    }
}
