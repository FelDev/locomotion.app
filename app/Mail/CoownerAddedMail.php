<?php

namespace App\Mail;

use App\Models\Loanable;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoownerAddedMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    public $adder;
    /**
     * @var User
     */
    public $added;
    /**
     * @var Loanable
     */
    public $loanable;

    public function __construct(User $adder, User $added, Loanable $loanable)
    {
        $this->adder = $adder;
        $this->added = $added;
        $this->loanable = $loanable;
    }

    public function build(): self
    {
        return $this->view("emails.loanable.coowner_added")
            ->subject("LocoMotion - Nouveaux droits de gestion d'un véhicule")
            ->text("emails.loanable.coowner_added_text")
            ->with([
                "title" => "Nouveaux droits de gestion d'un véhicule",
            ]);
    }
}
