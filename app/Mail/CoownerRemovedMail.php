<?php

namespace App\Mail;

use App\Models\Coowner;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CoownerRemovedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Coowner
     */
    public $coowner;
    /**
     * @var User
     */
    public $remover;

    public function __construct(Coowner $coowner, User $remover)
    {
        $this->coowner = $coowner;
        $this->remover = $remover;
    }

    public function build(): self
    {
        return $this->view("emails.loanable.coowner_removed")
            ->subject("LocoMotion - Droits de gestion de véhicule révoqués")
            ->text("emails.loanable.coowner_removed_text")
            ->with([
                "title" => "Droits de gestion de véhicule révoqués",
            ]);
    }
}
