<?php

namespace App\Mail\Loan;

use App\Mail\BaseMailable;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class UpcomingAsOwner extends BaseMailable
{
    use Queueable, SerializesModels;

    public $user;
    public $loan;
    public $borrowerUser;

    public function __construct(User $user, Loan $loan, User $borrowerUser)
    {
        $this->user = $user;
        $this->loan = $loan;
        $this->borrowerUser = $borrowerUser;
    }

    public function build()
    {
        return $this->view("emails.loan.upcoming_as_owner")
            ->subject(
                "LocoMotion - Un emprunt de votre véhicule commence dans 3 heures!"
            )
            ->text("emails.loan.upcoming_as_owner_text")
            ->with([
                "title" =>
                    "LocoMotion - Un emprunt de votre véhicule commence dans 3 heures!",
            ]);
    }
}
