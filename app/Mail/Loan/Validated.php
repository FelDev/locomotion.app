<?php

namespace App\Mail\Loan;

use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Validated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * @var Loan
     */
    public $loan;
    /**
     * @var User
     */
    public $recipient;
    /**
     * @var User
     */
    public $validator;
    /**
     * @var bool
     */
    public $isBorrower;

    public function __construct(
        Loan $loan,
        User $recipient,
        User $validator,
        bool $isBorrower
    ) {
        $this->loan = $loan;
        $this->recipient = $recipient;
        $this->validator = $validator;
        $this->isBorrower = $isBorrower;
    }

    public function build(): self
    {
        return $this->view("emails.loan.validated")
            ->subject("LocoMotion - Données d'emprunt à valider")
            ->text("emails.loan.validated_text")
            ->with([
                "title" => "Données d'emprunt à valider",
            ]);
    }
}
