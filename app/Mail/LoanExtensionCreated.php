<?php

namespace App\Mail;

use App\Models\Borrower;
use App\Models\Extension;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanExtensionCreated extends BaseMailable
{
    use Queueable, SerializesModels;

    public $borrower;
    public $extension;
    public $loan;
    public $recipient;

    public function __construct(
        Extension $extension,
        Loan $loan,
        Borrower $borrower,
        User $recipient
    ) {
        $this->borrower = $borrower;
        $this->extension = $extension;
        $this->loan = $loan;
        $this->recipient = $recipient;
    }

    public function build()
    {
        return $this->view("emails.loan.extension_created")
            ->subject("LocoMotion - Demande d'extension")
            ->text("emails.loan.extension_created_text")
            ->with([
                "title" => "Demande d'extension",
            ]);
    }
}
