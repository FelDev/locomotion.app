<?php

namespace App\Mail;

use App\Models\Borrower;
use App\Models\Incident;
use App\Models\Loan;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class LoanIncidentCreated extends BaseMailable
{
    use Queueable, SerializesModels;

    public $borrower;
    public $incident;
    public $loan;
    public $recipient;

    public function __construct(
        Incident $incident,
        Loan $loan,
        Borrower $borrower,
        User $recipient
    ) {
        $this->borrower = $borrower;
        $this->incident = $incident;
        $this->loan = $loan;
        $this->recipient = $recipient;
    }

    public function build()
    {
        return $this->view("emails.loan.incident_created")
            ->subject("LocoMotion - Rapport d'incident")
            ->text("emails.loan.incident_created_text")
            ->with([
                "title" => "Rapport d'incident",
            ]);
    }
}
