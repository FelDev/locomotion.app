<?php

namespace App\Mail;

use Symfony\Component\Mime\Email;

abstract class MandrillMailable extends BaseMailable
{
    public $template = "base";
    public $templateVars = [];
    public $subject;
    public $body = "";
    public $text = "";

    public function send($mailer)
    {
        $this->withSymfonyMessage(function (Email $message) {
            $message->template = $this->template;
            $message->templateVars = $this->templateVars;
        });

        parent::send($mailer);
    }

    public function build()
    {
        return $this->subject($this->subject)
            ->view("emails.mandrill")
            ->text("emails.mandrill_text")
            ->with($this->templateVars);
    }
}
