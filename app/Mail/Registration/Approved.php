<?php

namespace App\Mail\Registration;

use App\Mail\MandrillMailable;
use App\Models\Pivots\CommunityUser;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;

class Approved extends MandrillMailable
{
    use Queueable, SerializesModels;

    public CommunityUser $communityUser;
    public $template = "registration-approved";

    public function __construct(CommunityUser $communityUser)
    {
        $this->communityUser = $communityUser;
        $this->subject = "Bienvenue dans LocoMotion, c'est parti!";
        $this->templateVars = [
            "FNAME" => $communityUser->user->name,
            "NEIGHBOURHOOD" => $communityUser->community->name,
            "full_name" => $communityUser->user->full_name,
            "last_name" => $communityUser->user->last_name,
            "title" => "Bienvenue dans LocoMotion, c'est parti!",
            "starting_guide_url" =>
                $communityUser->community->starting_guide_url,
        ];
    }
}
