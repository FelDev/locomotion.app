<?php

namespace App\Mail\Transport;

use GuzzleHttp\Client;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\AbstractTransport;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Message;

class MandrillTransport extends AbstractTransport
{
    private Client $client;

    public function __construct(private $config, ?Client $client = null)
    {
        $this->client = $client ?? new Client($this->config);
        parent::__construct();
    }

    private function getKey()
    {
        return $this->config["secret"] ?? null;
    }

    public function doSend(SentMessage $message): void
    {
        $originalMessage = $message->getOriginalMessage();
        if (!($originalMessage instanceof Message)) {
            $messageClass = get_class($originalMessage);
            throw new \Exception(
                "Failed sending mandrill message of type {$messageClass}. Expected Message."
            );
        }

        if ($this->getKey() === null) {
            if (\App::environment() === "production") {
                \Log::error("Cannot send email, Mandrill key missing.");
            }

            $recipients = join(
                ",",
                array_map(
                    fn(Address $address) => $address->getAddress(),
                    $this->getTo($originalMessage)
                )
            );

            \Log::info(
                "Would have sent '{$originalMessage->getSubject()}' mandrill message to $recipients with template $originalMessage->template"
            );
            return;
        }

        $this->sendTemplate($originalMessage);
    }

    protected function getTo(Message $message): array
    {
        $to = [];

        if ($message->getTo()) {
            $to = array_merge($to, $message->getTo());
        }

        if ($message->getCc()) {
            $to = array_merge($to, $message->getCc());
        }

        if ($message->getBcc()) {
            $to = array_merge($to, $message->getBcc());
        }

        return $to;
    }

    private function sendTemplate(Message $message)
    {
        $templateContent = [
            [
                "name" => "main",
                "content" => " ",
            ],
        ];

        // Formating $globalMergeVars;
        $globalMergeVars = [];
        foreach ($message->templateVars as $name => $content) {
            $globalMergeVars[] = [
                "name" => $name,
                "content" => (string) $content,
            ];
        }

        /** @var Address $from */
        $from = $message->getFrom()[0];

        $messageConfig = [
            "subject" => $message->getSubject(),
            "from_email" => $from->getAddress(),
            "from_name" => $from->getName(),
            "to" => array_map(function (Address $address) {
                return [
                    "email" => $address->getAddress(),
                    "name" => $address->getName(),
                    "type" => "to",
                ];
            }, $this->getTo($message)),
            "global_merge_vars" => $globalMergeVars,
            "tags" => [
                "locomotion",
                "locomotion_template",
                "locomotion_template_$message->template",
            ],
            "auto_text" => true,
        ];

        $params = [
            "key" => $this->getKey(),
            "template_name" => $message->template,
            "template_content" => $templateContent,
            "message" => $messageConfig,
        ];

        // TODO: Look into using Mailchimp Transactional api rather than Guzzle client
        return $this->client->request(
            "POST",
            "https://mandrillapp.com/api/1.0/messages/send-template.json",
            ["json" => $params]
        );
    }

    public function __toString(): string
    {
        return "mandrill";
    }
}
