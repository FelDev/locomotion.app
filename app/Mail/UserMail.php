<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;

class UserMail
{
    public static function queue(Mailable $mailable, User $user): void
    {
        if ($user->trashed()) {
            return;
        }
        \Mail::queue($mailable->to($user->email, $user->full_name));
    }

    public static function queueMandrill(Mailable $mailable, User $user): void
    {
        if ($user->trashed()) {
            return;
        }

        Mail::mailer("mandrill")->queue(
            $mailable->to($user->email, $user->full_name)
        );
    }
}
