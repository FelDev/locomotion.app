<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bike extends LoanableDetails
{
    use HasFactory;

    public static $rules = [
        "bike_type" => ["required", "in:regular,cargo,electric,fixed_wheel"],
        "model" => ["required"],
        "size" => ["required", "in:big,medium,small,kid"],
    ];

    protected $fillable = ["bike_type", "model", "size"];
}
