<?php

namespace App\Models;

use App\Events\BorrowerApprovedEvent;
use App\Events\BorrowerCompletedEvent;
use App\Models\Loan;
use App\Models\User;
use App\Casts\TimestampWithTimezoneCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Borrower extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    public static $rules = [
        "user_id" => ["required"],
        "drivers_license_number" => ["nullable"],
        "has_not_been_sued_last_ten_years" => ["boolean"],
        "approved_at" => ["nullable", "date"],
    ];

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            "submit" => [
                "user_id" => ["required"],
                "drivers_license_number" => ["required"],
                "has_not_been_sued_last_ten_years" => [
                    "boolean",
                    "required",
                    "accepted",
                ],
                "saaq" => ["required"],
                "gaa" => ["required"],
            ],
            default => self::$rules,
        };
    }

    public static $filterTypes = [
        "submitted_at" => "date",
        "approved_at" => "date",
    ];

    protected $fillable = [
        "drivers_license_number",
        "has_not_been_sued_last_ten_years",
        "user_id",
    ];

    protected $casts = [
        "approved_at" => TimestampWithTimezoneCast::class,
        "suspended_at" => TimestampWithTimezoneCast::class,
    ];

    public $items = ["user"];

    public $computed = ["approved", "suspended", "validated"];

    public $morphOnes = [
        "insurance" => "fileable",
    ];

    public $morphManys = [
        "saaq" => "fileable",
        "gaa" => "fileable",
    ];

    /**
     * We include archived users here, so we can get some information from borrowers when fetching
     * loans. The @see ArchivedUserResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    public $collections = ["loans", "saaq", "gaa"];

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }

    public function getApprovedAttribute()
    {
        return !!$this->approved_at;
    }

    public function getSuspendedAttribute()
    {
        return !!$this->suspended_at;
    }

    public function getValidatedAttribute()
    {
        return $this->approved && !$this->suspended;
    }

    public function gaa()
    {
        return $this->morphMany(File::class, "fileable")->where("field", "gaa");
    }

    public function insurance()
    {
        return $this->morphOne(File::class, "fileable")->where(
            "field",
            "insurance"
        );
    }

    public function saaq()
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "saaq"
        );
    }
}
