<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Car extends LoanableDetails
{
    public static $rules = [
        "brand" => ["required"],
        "engine" => ["required", "in:fuel,diesel,electric,hybrid"],
        "insurer" => ["required"],
        "has_informed_insurer" => [],
        "value_category" => ["required", "in:lte50k,lte70k,lte100k"],
        "model" => ["required"],
        "papers_location" => ["required", "in:in_the_car,to_request_with_car"],
        "plate_number" => ["required"],
        "pricing_category" => ["required", "in:small,large,electric"],
        "transmission_mode" => ["required", "in:manual,automatic"],
        "year_of_circulation" => ["required", "digits:4", "numeric"],
    ];

    public static function getRules($action = "", $auth = null)
    {
        $rules = parent::getRules($action, $auth);
        $rules["year_of_circulation"][] = "max:" . ((int) date("Y") + 1);
        return $rules;
    }

    protected $fillable = [
        "brand",
        "engine",
        "has_informed_insurer",
        "has_report_in_notebook",
        "has_onboard_notebook",
        "insurer",
        "value_category",
        "model",
        "papers_location",
        "plate_number",
        "pricing_category",
        "transmission_mode",
        "year_of_circulation",
    ];

    public $items = ["owner"];

    public $computed = ["daily_premium_from_value_category"];

    public $morphOnes = [
        "report" => "fileable",
    ];

    public function report(): MorphOne
    {
        return $this->morphOne(File::class, "fileable")->where(
            "field",
            "report"
        );
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // If borrower is not validated, user can only see their own Cars.
        if (!$user->borrower || !$user->borrower->validated) {
            return $query->whereHas(
                "loanable",
                fn($q) => $q->ownedOrCoownedBy($user)
            );
        }

        return parent::scopeAccessibleBy($query, $user);
    }

    public function getDailyPremiumFromValueCategoryAttribute()
    {
        return match ($this->value_category) {
            "lte50k" => 4,
            "lte70k" => 5,
            "lte100k" => 6,
            default => 4,
        };
    }
}
