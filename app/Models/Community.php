<?php

namespace App\Models;

use App\Casts\PointCast;
use App\Casts\MultiPolygonCast;
use App\Enums\LoanableTypes;
use App\Models\Pivots\CommunityUser;
use App\Rules\IsGeoJsonMultiPolygon;
use App\Transformers\CommunityTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

class Community extends BaseModel
{
    use PostgisTrait, SoftDeletes;
    use HasFactory;

    public static $rules = [
        "name" => "required",
        "chat_group_url" => "url|nullable",
        "description" => "nullable",
        "long_description" => "nullable",
        "starting_guide_url" => "url|nullable",
        "area" => "nullable",
        "type" => ["nullable", "in:private,borough"],
        "uses_noke" => "boolean",
        "requires_identity_proof" => "boolean",
    ];

    public static $filterTypes = [
        "id" => "number",
        "name" => "text",
        "type" => ["borough", "private"],
    ];

    protected static $transformer = CommunityTransformer::class;

    public static function getRules($action = "", $auth = null)
    {
        return match ($action) {
            "destroy" => [],
            default => array_merge(static::$rules, [
                "area" => ["nullable", new IsGeoJsonMultiPolygon()],
            ]),
        };
    }
    protected static array $customColumns = [
        "center" => "ST_Centroid(%table%.area::geometry)",
        "approved_users_count" => <<<SQL
  (
    SELECT count(id)
    FROM community_user
    WHERE community_user.community_id = %table%.id
    AND community_user.approved_at IS NOT NULL
    AND community_user.suspended_at IS NULL
  )
SQL
    ,
    ];

    protected $fillable = [
        "area",
        "chat_group_url",
        "description",
        "long_description",
        "starting_guide_url",
        "name",
        "type",
        "uses_noke",
        "requires_identity_proof",
    ];

    protected $postgisFields = ["center", "area"];

    protected $postgisTypes = [
        "center" => [
            "geomtype" => "point",
        ],
        "area" => [
            "geomtype" => "geography",
        ],
    ];

    protected $casts = [
        "center" => PointCast::class,
        "area" => MultiPolygonCast::class,
    ];

    public $items = ["parent"];

    public $collections = [
        "users",
        "pricings",
        "loanables",
        "allowed_loanable_types",
    ];

    public $computed = ["center_google", "approved_users_count"];

    public function admins()
    {
        $globalAdmins = User::globalAdmins()->get();
        $localAdmins = $this->communityAdmins()->get();
        return $globalAdmins->merge($localAdmins);
    }

    public function allowedLoanableTypes()
    {
        return $this->belongsToMany(
            LoanableTypeDetails::class,
            "community_loanable_types",
            "community_id", // community_loanable_types.community_id
            "loanable_type_id" // community_loanable_types.loanable_type_id
        );
    }

    public function loanables()
    {
        return $this->hasMany(Loanable::class);
    }

    public function pricings()
    {
        return $this->hasMany(Pricing::class)->orderBy("object_type", "desc");
    }

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->using(CommunityUser::class)
            ->withTimestamps()
            ->withPivot([
                "id",
                "approved_at",
                "created_at",
                "role",
                "suspended_at",
                "updated_at",
            ]);
    }

    public function approvedUsers()
    {
        return $this->users()
            ->whereNotNull("community_user.approved_at")
            ->whereNull("community_user.suspended_at");
    }

    public function communityAdmins()
    {
        return $this->approvedUsers()->where("community_user.role", "admin");
    }

    public function getPricingFor(LoanableTypes $loanableType)
    {
        return $this->pricings
            ->where("object_type", $loanableType->value)
            ->first() ?:
            $this->pricings->where("object_type", null)->first();
    }

    public function getCenterGoogleAttribute()
    {
        if (!$this->center) {
            return null;
        }

        return [
            "lat" => $this->center[0],
            "lng" => $this->center[1],
        ];
    }

    public function getApprovedUsersCountAttribute()
    {
        if (isset($this->attributes["approved_users_count"])) {
            return $this->attributes["approved_users_count"];
        }

        return $this->users->count();
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->where(function ($q) use ($user) {
            return $q
                ->whereHas("users", function ($q2) use ($user) {
                    return $q2->where("users.id", $user->id);
                })
                ->orWhere("communities.type", "!=", "private");
        });
    }

    public function scopeWithApprovedUser(Builder $query, int $userId)
    {
        return $query->whereHas(
            "users",
            fn($q) => $q
                ->where("community_user.user_id", $userId)
                ->whereNotNull("community_user.approved_at")
                ->whereNull("community_user.suspended_at")
        );
    }

    public function scopeWithCommunityAdmin(Builder $query, int $userId)
    {
        return $query->whereHas(
            "users",
            fn($q) => $q
                ->where("community_user.user_id", $userId)
                ->whereNotNull("community_user.approved_at")
                ->whereNull("community_user.suspended_at")
                ->where("community_user.role", "admin")
        );
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getTable() . ".name";

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    public function scopeFor(Builder $query, $for, $user)
    {
        if (!$user) {
            $for = "read";
        }

        if ($user->isAdmin()) {
            return $query;
        }

        return match ($for) {
            "loan" => $query->withApprovedUser($user->id),
            "edit" => $query
                ->whereHas("users", function ($q) use ($user) {
                    return $q
                        ->where("community_user.user_id", $user->id)
                        ->where("community_user.role", "admin");
                })
                ->orWhereHas("users", function ($q) use ($user) {
                    return $q
                        ->where("users.id", $user->id)
                        ->where("users.role", "admin");
                }),
            default => $query,
        };
    }

    public function scopeApprovedInAny(Builder $query, $communityIds): Builder
    {
        return $query
            ->whereIn("community_user.community_id", $communityIds)
            ->whereNotNull("community_user.approved_at")
            ->whereNull("community_user.suspended_at");
    }
}
