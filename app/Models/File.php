<?php

namespace App\Models;

use App\Enums\FileMoveResult;
use App\Helpers\Path;
use App\Helpers\StorageHelper;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Token;
use PHPUnit\Util\Exception;
use Storage;

class File extends BaseModel
{
    use HasFactory;

    public static $rules = [
        "fileable_type" => "nullable",
        "fileable_id" => "nullable",
        "path" => "required",
        "filename" => "required",
        "original_filename" => "required",
        "filesize" => "required",
        "field" => "nullable",
    ];

    protected $fillable = [
        "field",
        "fileable_id",
        "fileable_type",
        "filename",
        "filesize",
        "original_filename",
        "path",
    ];

    protected $hidden = ["fileable", "fileable_type", "fileable_id"];

    protected $appends = ["url"];

    public static function fetch($path): ?string
    {
        return Storage::get($path);
    }

    public static function store(
        $path,
        \Illuminate\Http\File|UploadedFile $file
    ): bool {
        return Storage::putFileAs(dirname($path), $file, basename($path));
    }

    public function updateFilePath()
    {
        $this->load("fileable");
        if (!$this->fileable) {
            return FileMoveResult::unchanged;
        }

        $filePath = $this->generatePath();
        $newFilename = $this->generateUniqueName();
        $destination = Path::asAbsolute(Path::join($filePath, $newFilename));
        $source = $this->full_path;

        $moveResult = StorageHelper::move($source, $destination);
        if ($moveResult === FileMoveResult::failed) {
            throw new Exception("Failed moving $source to $destination");
        }

        $this->path = $filePath;
        $this->filename = $newFilename;

        return $moveResult;
    }

    public function generatePath()
    {
        return Path::build(
            "files",
            $this->fileable->getMorphClass(),
            $this->fileable_id,
            $this->field
        );
    }

    public function generateUniqueName()
    {
        return hash("md5", $this->id . config("app.key")) .
            "." .
            pathinfo($this->filename, PATHINFO_EXTENSION);
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function (File $model) {
            $model->updateFilePath();
        });

        self::deleted(function (File $model) {
            $model->deleteFile();
        });
    }

    public function deleteFile(): void
    {
        $deleted = Storage::delete($this->full_path);

        if (!$deleted) {
            \Log::warning("[File.php] Failed to delete file $this->full_path");
        }
    }

    public $items = ["user"];

    public function fileable()
    {
        return $this->morphTo();
    }

    public function borrower()
    {
        return $this->belongsTo(
            Borrower::class,
            "id",
            "fileable_id"
        )->whereFileableType(Borrower::class);
    }

    public function getUrlAttribute(): string
    {
        $tokenQueryString = $this->getTokenQueryString();
        $appUrl = env("BACKEND_URL_FROM_BROWSER");
        return self::formatPath($appUrl, $this->full_path, $tokenQueryString);
    }

    protected function getTokenQueryString(): string
    {
        if ($user = Auth::user()) {
            $token = Token::whereUserId($user->id)
                ->whereRevoked(false)
                ->orderBy("expires_at", "desc")
                ->limit(1)
                ->first();
            if ($token) {
                return "?token=$token->id";
            }
        }

        return "";
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // File is...
        return $query
            // ...associated to a borrower
            ->where(function ($q) use ($user) {
                return $q->whereHas("borrower", function ($q) use ($user) {
                    return $q->whereHas("user", function ($q) use ($user) {
                        return $q->accessibleBy($user);
                    });
                });
            })
            // ...or a temporary file
            ->orWhere(function ($q) {
                return $q->whereFileableType(null);
            });
    }

    public function getFullPathAttribute(): string
    {
        return Path::join($this->path, $this->filename);
    }

    private static function formatPath($basePath, $filePath, $token): string
    {
        return Path::join($basePath, $filePath) . $token;
    }
}
