<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Handover extends Action
{
    use HasFactory;
    public static $rules = [
        "status" => "required",
        "mileage_end" => "required",
        "comments_by_borrower" => "nullable",
        "comments_by_owner" => "nullable",
        "purchases_amount" => "required",
    ];

    protected $fillable = [
        "mileage_end",
        "comments_by_borrower",
        "comments_by_owner",
        "purchases_amount",
    ];

    public $readOnly = false;

    public $morphOnes = [
        "image" => "imageable",
        "expense" => "imageable",
    ];

    public function expense()
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "expense"
        );
    }

    public function image()
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "image"
        );
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    protected static array $customColumns = [
        "type" => "'handover'",
    ];

    public function complete($at = null)
    {
        $this->executed_at = new Carbon($at);
        $this->status = "completed";

        return $this;
    }

    public function isCompleted()
    {
        return $this->status == "completed";
    }

    public function contest($at = null)
    {
        // Status = canceled means contested.
        $this->executed_at = new Carbon($at);
        $this->status = "canceled";

        return $this;
    }

    public function isContested()
    {
        // Status = canceled means contested.
        return $this->status == "canceled";
    }
}
