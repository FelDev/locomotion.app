<?php

namespace App\Models;

use App\Enums\FileMoveResult;
use App\Helpers\Path;
use App\Helpers\StorageHelper;
use App\Models\Pivots\CommunityUser;
use Auth;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Intervention\Image\ImageManager;
use Laravel\Passport\Token;
use Storage;

class Image extends BaseModel
{
    use HasFactory;

    public static $rules = [
        "imageable_type" => "nullable",
        "imageable_id" => "nullable",
        "path" => "required",
        "filename" => "required",
        "original_filename" => "required",
        "field" => "nullable",
        "width" => "required",
        "height" => "required",
        "orientation" => "required",
    ];

    protected $fillable = [
        "field",
        "filename",
        "filesize",
        "height",
        "imageable_id",
        "imageable_type",
        "original_filename",
        "path",
        "width",
    ];

    public static $sizes = [
        "thumbnail" => "256x@fit",
    ];

    public static function fetch($path): ?\Intervention\Image\Image
    {
        $file = Storage::get($path);
        if (!$file) {
            return null;
        }

        $manager = new ImageManager(["driver" => "imagick"]);
        return $manager->make($file);
    }

    public static function store($path, $image): bool
    {
        $image->stream();
        return Storage::put($path, $image->__toString());
    }

    public function updateFilePath()
    {
        $this->load("imageable");
        if (!$this->imageable) {
            $this->syncThumbnailFiles();
            return FileMoveResult::unchanged;
        }

        $filePath = $this->generatePath();

        if ($filePath === $this->path) {
            return FileMoveResult::unchanged;
        }

        $newFilename =
            $this->field . "." . pathinfo($this->filename, PATHINFO_EXTENSION);

        // Move folder with all the thumbnails.
        $moveResult = StorageHelper::moveDirectoryFiles(
            $this->path,
            $filePath,
            $this->filename,
            $newFilename
        );

        if ($moveResult !== FileMoveResult::failed) {
            // Cleanup previous directory
            $this->deleteDirectory();

            $this->path = $filePath;
            $this->filename = $newFilename;
            $this->syncThumbnailFiles();
        }

        return $moveResult;
    }

    public function deleteDirectory(): void
    {
        $deleted = Storage::deleteDirectory($this->path);
        if (!$deleted) {
            \Log::warning("[Image.php] Failed to delete directory $this->path");
        }
    }

    public function getThumbnailSizes()
    {
        if ($this->imageable) {
            return $this->imageable::$sizes ?? self::$sizes;
        }
        return self::$sizes;
    }

    /**
     * Ensures expected thumbnails are present in the file folder and that no extra files are
     * present in the image directory.
     */
    public function syncThumbnailFiles(): void
    {
        $sizes = $this->getThumbnailSizes();

        // Ensure expected sizes exist
        $expectedSizePaths = [];
        $imagePath = $this->full_path;
        $canvas = null;
        foreach ($sizes as $name => $size) {
            $targetPath = $this->getImageSizePath($name);
            $expectedSizePaths[] = $targetPath;
            if (Storage::exists($targetPath)) {
                continue;
            }

            // Fetch the image at most once for all sizes to generate
            if (!$canvas) {
                $canvas = self::fetch($imagePath);
                if (!$canvas) {
                    \Log::error("Could not create thumbnail for $imagePath");
                    return;
                }
            }

            $this->createAndSaveThumbnail($size, $targetPath, $canvas);
        }

        $expectedSizePaths[] = $this->full_path;

        $toRemove = Path::arrayDiffAbsolute(
            Storage::files($this->path),
            $expectedSizePaths
        );

        $deleted = Storage::delete($toRemove);
        if (!$deleted) {
            \Log::warning("Failed deleting image $this->id old thumbnails.");
        }
    }

    /**
     * Creates a thumnail image and saves it to storage
     *
     * @param string $size the size for the thumbnail
     * @param string $targetPath where to save the thumbnail
     */
    private function createAndSaveThumbnail(
        string $size,
        string $targetPath,
        $canvas
    ): void {
        // Generic image generation
        $properties = explode("@", $size);

        $dimensions = explode("x", array_shift($properties));

        $type = array_pop($properties) ?: "fit";

        $width = $dimensions[0] ?? null;
        $height = $dimensions[1] ?? null;
        if ($type === "fit") {
            $canvas->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        } elseif ($type === "crop") {
            $x = $this->imageable->crop_x;
            $y = $this->imageable->crop_y;

            $origHeight = $canvas->height();
            $origWidth = $canvas->width();

            if ($origHeight / $origWidth <= 1) {
                $cropHeight = $origHeight;
                $cropWidth = intval(($origHeight * 2) / 3);
            } else {
                $cropWidth = $origWidth;
                $cropHeight = intval($origWidth * 1.5);
            }

            $canvas->crop($cropWidth, $cropHeight, $x, $y);

            if ($width || $height) {
                $canvas->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
        }

        $saved = Image::store($targetPath, $canvas);
        if (!$saved) {
            \Log::error("Could not save thumbnail $targetPath");
        }
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function (Image $model) {
            $model->updateFilePath();
        });

        self::deleting(function (Image $model) {
            $model->deleteDirectory();
        });
    }

    protected $hidden = ["imageable", "imageable_type", "imageable_id"];

    protected $appends = ["sizes", "url"];

    public function imageable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(
            User::class,
            "imageable_id"
        )->whereImageableType(User::class);
    }

    public function communityUser()
    {
        return $this->belongsTo(
            CommunityUser::class,
            "imageable_id"
        )->whereImageableType(CommunityUser::class);
    }

    public function getUrlAttribute()
    {
        $tokenQueryString = $this->getTokenQueryString();
        $appUrl = env("BACKEND_URL_FROM_BROWSER");

        return self::formatPath($appUrl, $this->full_path, $tokenQueryString);
    }

    public function getSizesAttribute()
    {
        $base = env("BACKEND_URL_FROM_BROWSER");
        $tokenQueryString = $this->getTokenQueryString();

        $sizes = [
            "original" => self::formatPath(
                $base,
                $this->full_path,
                $tokenQueryString
            ),
        ];

        foreach (array_keys($this->getThumbnailSizes()) as $name) {
            $sizes[$name] = self::formatPath(
                $base,
                $this->getImageSizePath($name),
                $tokenQueryString
            );
        }

        return $sizes;
    }

    public function getFullPathAttribute(): string
    {
        return Path::join($this->path, $this->filename);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if (!$user) {
            return $query->where(\DB::raw("1 = 0"));
        }

        if ($user->isAdmin()) {
            return $query;
        }

        // Image is...
        return $query
            // ...associated to a user
            ->where(function ($q) use ($user) {
                return $q->whereHas("user", function ($q) use ($user) {
                    return $q->accessibleBy($user);
                });
            })
            // ...or associated to a community user
            ->orWhere(function ($q) use ($user) {
                return $q->whereHas("communityUser", function ($q) use ($user) {
                    return $q->accessibleBy($user);
                });
            })
            // ...or a temporary file
            ->orWhere(function ($q) {
                return $q->whereImageableType(null);
            });
    }

    protected function getTokenQueryString()
    {
        if ($user = Auth::user()) {
            $token = Token::whereUserId($user->id)
                ->whereRevoked(false)
                ->orderBy("expires_at", "desc")
                ->limit(1)
                ->first();
            if ($token) {
                return "?token=$token->id";
            }
        }

        return "";
    }

    /**
     * @param int|string $name
     * @return string
     */
    private function getImageSizePath(int|string $name): string
    {
        return Path::join($this->path, $name . "_" . $this->filename);
    }

    private static function formatPath($basePath, $imagePath, $token): string
    {
        return $basePath . Path::asAbsolute($imagePath) . $token;
    }

    /**
     * @return string
     */
    private function generatePath(): string
    {
        return Path::build(
            "images",
            $this->imageable->getMorphClass(),
            $this->imageable_id,
            $this->field,
            $this->generateUniqueFolderName()
        );
    }

    public function generateUniqueFolderName(): string
    {
        return hash("md5", $this->id . config("app.key"));
    }
}
