<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Intention extends Action
{
    use HasFactory;

    protected static array $customColumns = [
        "type" => "'intention'",
    ];

    protected $fillable = ["message_for_borrower"];

    public $readOnly = false;

    public $items = ["loan"];

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function complete($at = null)
    {
        $this->executed_at = new Carbon($at);
        $this->status = "completed";

        return $this;
    }

    public function isCompleted()
    {
        return $this->status == "completed";
    }

    public function cancel($at = null)
    {
        $this->executed_at = new Carbon($at);
        $this->status = "canceled";

        return $this;
    }

    public function isCanceled()
    {
        return $this->status == "canceled";
    }

    public function resetApproval()
    {
        $this->status = "in_process";
        $this->executed_at = null;
        return $this;
    }
}
