<?php

namespace App\Models;

use App\Calendar\AvailabilityHelper;
use App\Casts\TimestampWithTimezoneCast;
use App\Enums\LoanableTypes;
use App\Events\LoanCompletedEvent;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Loan extends BaseModel
{
    use SoftDeletes;
    use HasFactory;

    protected $hidden = ["actual_pricing"];

    public static $rules = [
        "departure_at" => ["required"],
        "duration_in_minutes" => ["integer", "required", "min:15"],
        "estimated_distance" => ["integer", "nullable"],
        // Allow setting platform tip later.
        "platform_tip" => ["nullable", "numeric", "min:0"],
        "loanable_id" => "available",
        "message_for_owner" => ["present"],
        "reason" => ["required"],
    ];

    public static $filterTypes = [
        "id" => "number",
        "actual_duration_in_minutes" => "number",
        "departure_at" => "date",
        "actual_return_at" => "date",
        "calendar_days" => "number",
        "status" => ["in_process", "completed", "canceled"],
    ];

    public static function boot()
    {
        parent::boot();

        // Update loan.status and loan.actual_return_at whenever an action is changed.
        foreach (
            [
                Extension::class,
                Handover::class,
                Incident::class,
                Intention::class,
                Payment::class,
                PrePayment::class,
                Takeover::class,
            ]
            as $class
        ) {
            $class::saved(function ($model) {
                $changed = false;

                $loan = $model->loan;

                $newStatus = $loan->getStatusFromActions();
                if ($newStatus != $loan->status) {
                    $loan->status = $newStatus;
                    $changed = true;
                    if ($loan->status === "completed") {
                        event(new LoanCompletedEvent($loan));
                    }
                }

                // Work with Carbon objects.
                $curReturnAt = $loan->actual_return_at
                    ? Carbon::parse($loan->actual_return_at)
                    : null;
                $newReturnAt = $loan->getActualReturnAtFromActions();

                if (!$curReturnAt || !$newReturnAt->equalTo($curReturnAt)) {
                    $loan->actual_return_at = $newReturnAt;
                    $changed = true;
                }

                if ($changed) {
                    $loan->save();
                }
            });

            $class::deleted(function ($model) {
                $changed = false;

                $loan = $model->loan;

                $newStatus = $loan->getStatusFromActions();
                if ($newStatus != $loan->status) {
                    $loan->status = $newStatus;
                    $changed = true;
                }

                // Work with Carbon objects.
                $curReturnAt = $loan->actual_return_at
                    ? Carbon::parse($loan->actual_return_at)
                    : null;
                $newReturnAt = $loan->getActualReturnAtFromActions();

                if (!$curReturnAt || !$newReturnAt->equalTo($curReturnAt)) {
                    $loan->actual_return_at = $newReturnAt;
                    $changed = true;
                }

                if ($changed) {
                    $loan->save();
                }
            });
        }

        Handover::saved(function (Handover $model) {
            if ($model->isContested()) {
                $model->loan->borrower_validated_at = null;
                $model->loan->owner_validated_at = null;
                $model->loan->save();
            }
        });

        Takeover::saved(function (Takeover $model) {
            if ($model->isContested()) {
                $model->loan->borrower_validated_at = null;
                $model->loan->owner_validated_at = null;
                $model->loan->save();
            }
        });

        // Update loan.status and loan.actual_return_at before saving
        self::saving(function ($loan) {
            if ($loan->canceled_at && "canceled" != $loan->status) {
                $loan->status = "canceled";
            } elseif (!$loan->canceled_at) {
                $loan->status = $loan->getStatusFromActions();
            }

            $loan->actual_return_at = $loan->getActualReturnAtFromActions();
        });
    }

    protected static array $customColumns = [
        "calendar_days" => <<<SQL
EXTRACT(
    'day'
    FROM
        date_trunc('day', actual_return_at)
        - date_trunc('day', departure_at)
        + interval '1 day'
)::integer
SQL
        ,
        // See comments in getActualDurationInMinutesAttribute
        "actual_duration_in_minutes" => <<<SQL
GREATEST(
    (    DATE_PART('day',    actual_return_at::timestamp - departure_at::timestamp) * 24
       + DATE_PART('hour',   actual_return_at::timestamp - departure_at::timestamp)) * 60 +
       + DATE_PART('minute', actual_return_at::timestamp - departure_at::timestamp),
    0)
SQL
    ,
    ];

    // TODO: Move to a calendar helper (#1080).
    public static function getCalendarDays($start, $end)
    {
        // These variables are built gradually to become start and end of the
        // day as we move forward, hence their name.
        $startOfDaysCovered = $start->copy()->setMilliseconds(0);
        $endOfDaysCovered = $end->copy()->setMilliseconds(0);

        // Milliseconds must be set so the comparison is accurate.
        // Return 0 for degenerate loan intervals.
        if ($endOfDaysCovered->lessThanOrEqualTo($startOfDaysCovered)) {
            return 0;
        }

        // Snap to start of day.
        $startOfDaysCovered = $startOfDaysCovered
            ->setHours(0)
            ->setMinutes(0)
            ->setSeconds(0);

        // Snap to end of day (beginning of next day). Consider [, ) intervals.
        if (
            $endOfDaysCovered->hour > 0 ||
            $endOfDaysCovered->minute > 0 ||
            $endOfDaysCovered->second > 0
        ) {
            $endOfDaysCovered = $endOfDaysCovered
                ->addDays(1)
                ->setHours(0)
                ->setMinutes(0)
                ->setSeconds(0);
        }

        $days = $startOfDaysCovered->diffInDays($endOfDaysCovered, false);

        return $days;
    }

    public static function getRules($action = "", $auth = null)
    {
        $rules = parent::getRules($action, $auth);
        switch ($action) {
            case "create":
                $rules["community_id"] = "required";
                return $rules;
            default:
                return $rules;
        }
    }

    protected $casts = [
        "departure_at" => TimestampWithTimezoneCast::class,
        "canceled_at" => TimestampWithTimezoneCast::class,
        "actual_return_at" => TimestampWithTimezoneCast::class,
        "meta" => "array",
        "platform_tip" => "float",
    ];

    protected $fillable = [
        "borrower_id",
        "canceled_at",
        "community_id",
        "departure_at",
        "duration_in_minutes",
        "estimated_distance",
        "loanable_id",
        "platform_tip",
        "message_for_owner",
        "reason",
    ];

    public $computed = [
        "actual_price",
        "actual_insurance",
        "actual_duration_in_minutes",
        "actual_distance",
        "calendar_days",
        "is_contested",
        "total_actual_cost",
        "total_final_cost",
        "is_free",
        "needs_validation",
    ];

    public $items = [
        "borrower",
        "community",
        "handover",
        "intention",
        "loanable",
        "payment",
        "pre_payment",
        "takeover",
    ];

    public $collections = ["extensions", "incidents"];

    public function borrower()
    {
        return $this->belongsTo(Borrower::class);
    }

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function extensions()
    {
        return $this->hasMany(Extension::class);
    }

    public function handover()
    {
        return $this->hasOne(Handover::class);
    }

    public function incidents()
    {
        return $this->hasMany(Incident::class);
    }

    public function intention()
    {
        return $this->hasOne(Intention::class);
    }

    /**
     * We include archived loanables here, so we can get some information when fetching loans.
     * The @see ArchivedLoanableResource should be used to strip any personal information
     * that shouldn't be exposed.
     */
    public function loanable()
    {
        return $this->belongsTo(Loanable::class)->withTrashed();
    }

    public function payment()
    {
        return $this->hasOne(Payment::class);
    }

    public function prePayment()
    {
        return $this->hasOne(PrePayment::class);
    }

    public function takeover()
    {
        return $this->hasOne(Takeover::class);
    }

    public function getActualDurationInMinutesAttribute()
    {
        $actualDurationInMinutes = (new Carbon(
            $this->departure_at
        ))->diffInMinutes(new Carbon($this->actual_return_at), false);

        // If the payment was made before the loan departure time, then return
        // a duration of 0. Negative durations would enable a borrower to earn cash!
        if ($actualDurationInMinutes < 0) {
            return 0;
        }

        return $actualDurationInMinutes;
    }

    /**
     * Current distance for the loan, either estimated or real.
     */
    public function getActualDistanceAttribute()
    {
        if (
            $this->takeover &&
            $this->handover &&
            ($this->handover->isCompleted() || $this->handover->isContested())
        ) {
            return $this->handover->mileage_end -
                $this->takeover->mileage_beginning;
        }
        return $this->estimated_distance;
    }

    public function getCalendarDaysAttribute()
    {
        return static::getCalendarDays(
            new Carbon($this->departure_at),
            new Carbon($this->actual_return_at)
        );
    }

    public function getIsFreeAttribute()
    {
        return $this->actual_price === 0 && $this->actual_insurance === 0;
    }

    /**
     * Current pricing for the loan, either estimated or real.
     */
    public function getActualPricingAttribute()
    {
        $pricing = $this->community->getPricingFor($this->loanable->type);

        if (!$pricing) {
            return 0;
        }

        $distance = $this->actual_distance ?? 0;

        return $pricing->evaluateRule(
            $distance,
            $this->actual_duration_in_minutes,
            $this->loanable,
            $this
        );
    }

    /**
     * Current price for the loan, either estimated or real.
     */
    public function getActualPriceAttribute()
    {
        $pricing = $this->actual_pricing;

        if (is_array($pricing)) {
            $timeAndDistance =
                ($pricing["time"] ?? 0) + ($pricing["distance"] ?? 0);
        } else {
            $timeAndDistance = $pricing;
        }

        return max(0, $timeAndDistance);
    }

    /**
     * Current insurance for the loan, either estimated or real.
     */
    public function getActualInsuranceAttribute()
    {
        $pricing = $this->actual_pricing;

        if (is_array($pricing)) {
            $insurance = $pricing["insurance"] ?? 0;
        } else {
            $insurance = 0;
        }

        return max(0, $insurance);
    }

    /**
     * Current total cost for the loan, either estimated or real.
     */
    public function getTotalActualCostAttribute()
    {
        $actual_expenses = $this->handover
            ? $this->handover->purchases_amount
            : 0;

        return round(
            $this->actual_price +
                $this->actual_insurance +
                $this->platform_tip -
                $actual_expenses,
            2
        );
    }

    public function getTotalFinalCostAttribute()
    {
        return $this->final_price +
            $this->final_insurance +
            $this->final_platform_tip -
            $this->final_purchases_amount;
    }

    public function getIsContestedAttribute(): bool
    {
        $takeover = $this->takeover;
        if ($takeover && $takeover->isContested()) {
            return true;
        }

        $handover = $this->handover;
        return $handover && $handover->isContested();
    }

    public function scopeIsPeriodUnavailable(
        Builder $query,
        $departureAt,
        $returnAt
    ) {
        $query
            ->where("status", "!=", "canceled")
            ->whereHas("intention", function ($q) {
                return $q->where("status", "=", "completed");
            })
            /*
                Intersection if: a1 > b0 and a0 < b1

                    a0           a1
                    [------------)
                          [------------)
                          b0           b1
            */
            ->where("actual_return_at", ">", $departureAt)
            ->where("departure_at", "<", $returnAt);
    }

    public function scopeForUser(Builder $query, $userId, $negative = false)
    {
        return $query->where(
            fn(Builder $q) => $q
                ->whereHas(
                    "borrower",
                    fn(Builder $q) => $q->where("user_id", $userId)
                )
                ->orWhere(fn(Builder $q) => $q->hasOwnerAccess($userId))
        );
    }

    public function scopeForDashboard(Builder $query, User $user)
    {
        return $query
            ->with(
                "borrower.user.avatar",
                "handover",
                "extensions",
                "incidents",
                "intention",
                "loanable.owner.user.avatar",
                "payment",
                "prePayment",
                "takeover",
                "community"
            )
            ->forUser($user->id);
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        if ($user->owner) {
            $ownerId = $user->owner->id;
            $query = $query->whereHas("loanable", function ($q) use ($ownerId) {
                return $q->where("owner_id", $ownerId);
            });
        }

        if ($user->borrower) {
            $borrowerId = $user->borrower->id;
            $query = $query->orWhere("borrower_id", $borrowerId);
        }

        $query->orWhereHas("loanable.coowners", function (Builder $q) use (
            $user
        ) {
            $q->where("user_id", $user->id);
        });

        // Or belonging to its admin communities
        return $query->orWhereHas(
            "community.users",
            fn($q) => $q
                ->where("community_user.user_id", $user->id)
                ->where("community_user.role", "admin")
        );
    }

    public function scopeCurrentStep(Builder $query, $step, $negative = false)
    {
        switch ($step) {
            case "pre_payment":
                $step = "prePayment";
            // no break (just rename the step and carry on)
            case "intention":
            case "takeover":
            case "handover":
            case "payment":
                return $query
                    ->whereHas($step, function ($q) {
                        return $q->where("status", "in_process");
                    })
                    ->whereDoesntHave("incidents", function ($q) {
                        return $q->where("status", "in_process");
                    })
                    ->whereDoesntHave("extensions", function ($q) {
                        return $q->where("status", "in_process");
                    });
            case "incident":
            case "extension":
                return $query->whereHas("{$step}s", function ($q) {
                    return $q->where("status", "in_process");
                });
            default:
                return $query;
        }
    }

    public function scopePrepaid(
        Builder $query,
        $value = true,
        $negative = false
    ) {
        // Negative case
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
            return $query->where(function ($q) {
                return $q
                    ->whereHas("prePayment", function ($q) {
                        return $q->where("status", "!=", "completed");
                    })
                    ->orWhereDoesntHave("prePayment");
            });
        }

        // Positive case
        return $query->whereHas("prePayment", function ($q) {
            return $q->where("status", "completed");
        });
    }

    public function scopeHasOwnerAccess(Builder $query, int $userId)
    {
        $query->where(function (Builder $q) use ($userId) {
            $q->whereHas("loanable.owner", function (Builder $q) use ($userId) {
                $q->where("user_id", $userId);
            })->orWhereHas("loanable.coowners", function (Builder $q) use (
                $userId
            ) {
                $q->where("user_id", $userId);
            });
        });
    }

    public function scopeCompleted(
        Builder $query,
        $value = true,
        $negative = false
    ) {
        // Negative case
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
            return $query->where("status", "!=", "completed");
        }

        // Positive case
        return $query->where("status", "=", "completed");
    }

    public function scopeCanceled(
        Builder $query,
        $value = true,
        $negative = false
    ) {
        // Negative case
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
            return $query->where("status", "!=", "canceled");
        }

        // Positive case
        return $query->where("status", "=", "canceled");
    }

    public function scopeHasActiveIncidents(
        Builder $query,
        $value = true,
        $negative = false
    ) {
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
            return $query->whereDoesntHave(
                "incidents",
                fn(Builder $q) => $q->where("status", "in_process")
            );
        }

        return $query->whereHas(
            "incidents",
            fn(Builder $q) => $q->where("status", "in_process")
        );
    }

    public function scopeContested(
        Builder $query,
        $value = true,
        $negative = false
    ) {
        // Negative case
        if (filter_var($value, FILTER_VALIDATE_BOOLEAN) === $negative) {
            return $query
                ->where(
                    fn(Builder $q) => $q
                        ->whereDoesntHave("handover")
                        ->orWhereHas(
                            "handover",
                            fn(Builder $q) => $q->where(
                                "status",
                                "!=",
                                "canceled"
                            )
                        )
                )
                ->where(
                    fn(Builder $q) => $q
                        ->whereDoesntHave("takeover")
                        ->orWhereHas(
                            "takeover",
                            fn(Builder $q) => $q->where(
                                "status",
                                "!=",
                                "canceled"
                            )
                        )
                );
        }

        return $query->where(
            fn(Builder $q) => $q
                ->whereHas(
                    "handover",
                    fn(Builder $q) => $q->where("status", "canceled")
                )
                ->orWhereHas(
                    "takeover",
                    fn(Builder $q) => $q->where("status", "canceled")
                )
        );
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit" => $query->whereHas(
                "community.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            default => $query,
        };
    }

    public function scopeFuture(Builder $query)
    {
        return $query
            ->where("status", "in_process")
            ->where("departure_at", ">", Carbon::now());
    }

    /**
     * Returns loans which are in periods of availability or unavailability given the rules.
     *
     * @param Builder $query Loan query
     * @param array $availabilityParams Rules which can be interpreted by AvailabilityHelper
     * @param bool $isAvailable whether to return loans in which are in periods of availability of unavailability.
     * @return Collection the loans
     */
    public function scopeDuringAvailability(
        Builder $query,
        array $availabilityParams,
        bool $isAvailable = true
    ): Collection {
        return $query
            ->get()
            ->filter(
                fn(Loan $loan) => $isAvailable ==
                    AvailabilityHelper::isScheduleAvailable(
                        $availabilityParams,
                        [
                            new Carbon($loan->departure_at),
                            new Carbon($loan->actual_return_at),
                        ]
                    )
            )
            ->values();
    }

    public function cancel($at = null)
    {
        $this->canceled_at = new Carbon($at);
        $this->status = "canceled";

        return $this;
    }

    public function isCanceled()
    {
        return $this->canceled_at || $this->status == "canceled";
    }

    /*
      This function is used to compute the status attribute of a loan and
      should be the single source of truth.

      Possible states:
        - in_process
        - canceled
        - completed

      Only accounts for actions, not any fields from the loan itself.
      Loan.canceled_at has precedence over this status.

      Refer to database/migrations/2022_04_12_144045_set_loan_status.php
      to see how older cases were accounted for.
    */
    public function getStatusFromActions()
    {
        // Loan is canceled if pre-payment is canceled.
        if ($this->prePayment && $this->prePayment->status == "canceled") {
            return "canceled";
        }

        if ($this->payment) {
            return match ($this->payment->status) {
                "in_process", "completed" => $this->payment->status,
                default => throw new \Exception(
                    "Unexpected status for loan action: payment."
                ),
            };
        }

        if ($this->intention && $this->intention->isCanceled()) {
            return "canceled";
        }

        return "in_process";
    }

    /*
      This function is used to compute the loan.actual_return_at attribute and
      should be the single source of truth.

      Acounts for loan duration_in_minutes, accepted extensions and early payments.

      Refer to database/migrations/2022_06_08_093323_set_actual_return_at.php
      to see how older cases were accounted for.
    */
    public function getActualReturnAtFromActions()
    {
        $departureAt = CarbonImmutable::parse($this->departure_at);

        $durationMinutes = $this->duration_in_minutes;

        // Account for the longest accepted extension
        foreach ($this->extensions as $extension) {
            if ("extension" == $extension->type && $extension->isCompleted()) {
                if ($extension->new_duration > $durationMinutes) {
                    $durationMinutes = $extension->new_duration;
                }
            }
        }

        $returnAt = $departureAt->addMinutes($durationMinutes);

        // Account for early payment.
        $payment = $this->payment;
        if ($payment && $payment->isCompleted()) {
            $paymentTime = CarbonImmutable::parse($payment->executed_at);
            if ($paymentTime->lessThan($returnAt)) {
                $returnAt = $paymentTime;
            }
        }

        return $returnAt;
    }

    public function hasBorrowerValidated(): bool
    {
        return !!$this->borrower_validated_at;
    }

    public function borrowerIsOwner(): bool
    {
        return $this->loanable->owner &&
            $this->borrower->user->is($this->loanable->owner->user);
    }

    public function hasOwnerValidated(): bool
    {
        return !!$this->owner_validated_at;
    }

    public function isFullyValidated(): bool
    {
        return $this->hasBorrowerValidated() && $this->hasOwnerValidated();
    }

    public function borrowerCanPay(): bool
    {
        return floatval($this->borrower->user->balance) >=
            $this->total_actual_cost;
    }

    public function getNeedsValidationAttribute(): bool
    {
        return $this->loanable->type === LoanableTypes::Car &&
            !$this->is_free &&
            !$this->loanable->is_self_service &&
            $this->validationLimit()->isAfter(Carbon::now()) &&
            !$this->borrowerIsOwner();
    }

    public function validationLimit(): Carbon
    {
        return Carbon::parse($this->actual_return_at)->addHours(48);
    }

    public function scopeBlockingLoanableDeletion(Builder $query)
    {
        return $query
            ->where("status", "in_process")
            ->whereHas(
                "prePayment",
                fn(Builder $q) => $q->where("status", "completed")
            );
    }
}
