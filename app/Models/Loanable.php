<?php

namespace App\Models;

use App\Calendar\AvailabilityHelper;
use App\Casts\PointCast;
use App\Enums\LoanableTypes;
use App\Exports\LoanableExport;
use App\Mail\UserMail;
use App\Transformers\LoanableTransformer;
use Carbon\Carbon;
use Closure;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;

/*
 * Soft-deleted (deleted_at not null) loanables are considered archived. They will not be returned
 * for default queries, but they can be restored.
 */
class Loanable extends BaseModel
{
    use PostgisTrait, SoftDeletes, HasFactory;

    public static $export = LoanableExport::class;

    protected static $transformer = LoanableTransformer::class;

    public static $filterTypes = [
        "id" => "number",
        "name" => "text",
        "type" => LoanableTypes::possibleTypes,
        "deleted_at" => "date",
        "is_deleted" => "boolean",
    ];

    public static $rules = [
        "type" => ["required", "in:bike,car,trailer"],
        "comments" => "present",
        "instructions" => "present",
        "location_description" => "present",
        "name" => "required",
        "position" => "required",
        "owner.id" => "required_without:owner_id",
        "owner_id" => "required_without:owner",
        "details" => "required",
        "show_owner_as_contact" => "boolean",
    ];

    protected $fillable = [
        "type",
        "availability_json",
        "availability_mode",
        "comments",
        "instructions",
        "is_self_service",
        "location_description",
        "name",
        "position",
        "show_owner_as_contact",
    ];

    public function details()
    {
        return $this->morphTo(__FUNCTION__, "type", "id");
    }

    public static function boot()
    {
        parent::boot();

        // Handle archiving loanables.
        self::deleted(function (Loanable $model) {
            // Cancel all ongoing loans
            $model
                ->loans()
                ->where("status", "in_process")
                ->each(fn(Loan $loan) => $loan->cancel()->save());
        });

        self::restored(function ($model) {
            $model->loans()->restore();
        });
    }

    public function getCommunityIdsAttribute()
    {
        return $this->owner->user->communities->pluck("id")->toArray();
    }

    protected $postgisFields = ["position"];

    protected $postgisTypes = [
        "position" => [
            "geomtype" => "geography",
        ],
    ];

    protected $casts = [
        "position" => PointCast::class,
        "type" => LoanableTypes::class,
    ];

    protected $with = ["details"];

    public $computed = [
        "car_insurer",
        "events",
        "has_padlock",
        "position_google",
    ];

    public $items = ["owner", "padlock", "details", "current_loan"];

    public $morphOnes = [
        "image" => "imageable",
    ];

    public function image()
    {
        return $this->morphOne(Image::class, "imageable");
    }

    public function owner()
    {
        return $this->belongsTo(Owner::class);
    }

    public function padlock()
    {
        return $this->hasOne(Padlock::class, "loanable_id");
    }

    public $collections = ["loans", "coowners"];

    public function loans()
    {
        return $this->hasMany(Loan::class);
    }

    public function currentLoan()
    {
        return $this->hasOne(Loan::class)->ofMany(
            ["id" => "max"],
            fn(Builder $q) => $q->where("status", "in_process")
        );
    }

    public function coowners()
    {
        return $this->hasMany(Coowner::class)->orderBy("id");
    }

    public function addCoowner(int $userId)
    {
        $coowner = new Coowner();
        $coowner->user_id = $userId;
        $coowner->loanable_id = $this->id;

        $this->coowners()->save($coowner);

        $coowner->refresh();
        return $coowner;
    }

    public function removeCoowner(int $userId)
    {
        $this->coowners()
            ->where("user_id", "=", $userId)
            ->delete();
    }

    public function isOwner(User $user): bool
    {
        return $this->owner && $this->owner->user->is($user);
    }

    public function isCoowner(User $user): bool
    {
        return $this->coowners->where("user_id", $user->id)->isNotEmpty();
    }

    public function isOwnerOrCoowner(User $user): bool
    {
        return $this->isOwner($user) || $this->isCoowner($user);
    }

    public function getAvailabilityRules()
    {
        try {
            return $this->availability_json
                ? json_decode(
                    $this->availability_json,
                    true,
                    512,
                    JSON_THROW_ON_ERROR
                )
                : [];
        } catch (\Throwable) {
            // Logging the error rather than throwing allows the rules to be modified.
            Log::error(
                "Could not parse availability json: \"$this->availability_json\" for loanable id: $this->id."
            );
            return [];
        }
    }

    public function isAvailable(
        $departureAt,
        $durationInMinutes,
        $ignoreLoanIds = []
    ) {
        if (!is_a(Carbon::class, $departureAt)) {
            $departureAt = new Carbon($departureAt);
        }

        $returnAt = $departureAt->copy()->add($durationInMinutes, "minutes");

        if (!$this->isLoanableScheduleOpen($departureAt, $returnAt)) {
            return false;
        }

        $query = Loan::where("loanable_id", $this->id);

        if ($ignoreLoanIds) {
            $query = $query->whereNotIn("loans.id", $ignoreLoanIds);
        }

        $query->isPeriodUnavailable($departureAt, $returnAt);

        return $query->get()->count() === 0;
    }

    public function isLoanableScheduleOpen($departureAt, $returnAt)
    {
        $loanInterval = [$departureAt, $returnAt];

        return AvailabilityHelper::isScheduleAvailable(
            [
                "available" => "always" == $this->availability_mode,
                "rules" => $this->getAvailabilityRules(),
            ],
            $loanInterval
        );
    }

    public function getCommunityForLoanBy(User $user): ?Community
    {
        $userComunities = $user->getAccessibleCommunityIds()->toArray();
        $loanableCommunities = $this->owner->user
            ->getAccessibleCommunityIds()
            ->toArray();

        $communityId = current(
            array_intersect($userComunities, $loanableCommunities)
        );
        return Community::where("id", $communityId)->first();
    }

    public function getEventsAttribute()
    {
        // Generate events for the next year.
        $dateRange = [new Carbon(), (new Carbon())->addYear()];

        $dailyIntervals = AvailabilityHelper::getScheduleDailyIntervals(
            ["rules" => $this->getAvailabilityRules()],
            $dateRange
        );

        // Create events from intervals.
        $events = [];
        foreach ($dailyIntervals as $interval) {
            $events[] = [
                "start" => $interval[0]->format("Y-m-d H:i:s"),
                "end" => $interval[1]->format("Y-m-d H:i:s"),
            ];
        }

        return $events;
    }

    public function getHasPadlockAttribute()
    {
        return !!$this->padlock;
    }

    public function getCarInsurerAttribute()
    {
        if ($this->type === LoanableTypes::Car && $this->details) {
            return $this->details->insurer;
        }

        return null;
    }

    public function getPositionGoogleAttribute()
    {
        return [
            "lat" => $this->position[0],
            "lng" => $this->position[1],
        ];
    }

    /**
     * Calls the callback with each Owner or Coowner user, unless it's the $exception.
     */
    public function forAllCoowners(Closure $callback, User $exception = null)
    {
        if ($this->owner && !$this->owner->user->is($exception)) {
            $callback($this->owner->user);
        }

        foreach ($this->coowners as $coowner) {
            if (!$coowner->user->is($exception)) {
                $callback($coowner->user);
            }
        }
    }

    /**
     * Queues a mail to all Owner or Coowner users, unless they're the $exception.
     */
    public function queueMailToOwners(
        Closure $createMailFunction,
        User $exception = null
    ) {
        $this->forAllCoowners(function (User $user) use ($createMailFunction) {
            UserMail::queue($createMailFunction($user), $user);
        }, $exception);
    }

    public function handleInstructionVisibility(User $user, Loan $loan = null)
    {
        if (
            $user->can("viewInstructions", $this) ||
            ($loan && $user->can("viewInstructions", $loan))
        ) {
            $this->makeVisible("instructions");
        } else {
            $this->makeHidden("instructions");
        }
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        $allowedTypes = [LoanableTypes::Bike, LoanableTypes::Trailer];
        if ($user->borrower && $user->borrower->validated) {
            $allowedTypes[] = LoanableTypes::Car;
        }

        return $query
            ->where(
                fn(Builder $q) => $q
                    //
                    ->whereHas(
                        "owner.user.approvedCommunities",
                        fn(Builder $communityUser) => $communityUser->whereHas(
                            "approvedUsers",
                            fn(builder $approvedUser) => $approvedUser->where(
                                "users.id",
                                $user->id
                            )
                        )
                    )
                    ->whereIn("type", $allowedTypes)

                    // Community Admins see all types of loanables
                    ->orWhereHas(
                        "owner.user.communities.communityAdmins",
                        fn(Builder $admin) => $admin->where(
                            "users.id",
                            $user->id
                        )
                    )
            )
            ->orWhere(fn(Builder $q) => $q->ownedOrCoownedBy($user));
    }

    public function scopeOwnedOrCoownedBy(Builder $query, User $user)
    {
        if ($user->owner) {
            return $query->whereHas(
                "owner",
                fn($q) => $q->where("owners.id", $user->owner->id)
            );
        }

        $query->orWhereHas(
            "coowners",
            fn(Builder $q) => $q->where("user_id", $user->id)
        );

        return $query;
    }

    public function scopeHasAvailabilities(Builder $query)
    {
        return $query->where(function ($q) {
            return $q
                ->where("availability_mode", "!=", "never")
                ->orWhereJsonLength("availability_json", ">", 0);
        });
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getTable() . ".name";

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit" => $query->whereHas(
                "owner.user.approvedCommunities.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            default => $query,
        };
    }

    public function scopeAccessibleInCommunity(Builder $query, int $communityId)
    {
        return $query->whereHas(
            "owner.user.approvedCommunities",
            fn(Builder $q) => $q->where("communities.id", $communityId)
        );
    }
}
