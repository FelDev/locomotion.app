<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

abstract class LoanableDetails extends BaseModel
{
    use HasFactory;

    public $timestamps = false;
    public $incrementing = false;

    public function loanable()
    {
        return $this->belongsTo(Loanable::class, "id", "id");
    }

    public function scopeAccessibleBy(Builder $query, User $user)
    {
        return $query->whereHas("loanable", fn($q) => $q->accessibleBy($user));
    }
}
