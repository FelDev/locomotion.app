<?php

namespace App\Models;

use App\Enums\LoanableTypes;

class LoanableTypeDetails extends BaseModel
{
    protected $casts = [
        "name" => LoanableTypes::class,
    ];
}
