<?php

namespace App\Models;

use App\Casts\Uppercase;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Padlock extends BaseModel
{
    use HasFactory;
    public static $rules = [
        "external_id" => "required",
        "mac_address" => "required|unique:padlocks,mac_address",
        "name" => "required",
    ];

    public static $filterTypes = [
        "external_id" => "text",
        "name" => "text",
        "mac_address" => "text",
    ];

    protected $casts = [
        "mac_address" => Uppercase::class,
    ];

    protected $fillable = ["external_id", "mac_address", "name"];

    public $items = ["loanable"];

    public function loanable()
    {
        return $this->belongsTo(Loanable::class);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->where("1 = 0");
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getTable() . ".name";

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }
}
