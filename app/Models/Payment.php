<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Action
{
    use HasFactory;

    protected static array $customColumns = [
        "type" => "'payment'",
    ];

    protected $fillable = ["loan_id"];

    public $readOnly = false;

    public $items = ["borrower_invoice", "owner_invoice", "loan"];

    public function borrowerInvoice()
    {
        return $this->belongsTo(Invoice::class, "borrower_invoice_id");
    }

    public function ownerInvoice()
    {
        return $this->belongsTo(Invoice::class, "owner_invoice_id");
    }

    public function loan()
    {
        return $this->belongsTo(Loan::class);
    }

    public function complete($at = null)
    {
        $this->executed_at = new Carbon($at);
        $this->status = "completed";

        return $this;
    }

    public function isCompleted()
    {
        return $this->status == "completed";
    }
}
