<?php

namespace App\Models\Pivots;

use App\Events\RegistrationApprovedEvent;
use App\Models\Community;
use App\Models\File;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class CommunityUser extends BasePivot
{
    public $items = ["user", "community"];

    public static function boot()
    {
        parent::boot();

        // Do not fetch community_users for archived users
        static::addGlobalScope(
            "active-user",
            fn(Builder $builder) => $builder->whereHas("user")
        );

        self::saved(function (CommunityUser $model) {
            if (
                !$model->originalIsEquivalent("approved_at") &&
                !!$model->approved_at &&
                !$model->suspended_at
            ) {
                event(new RegistrationApprovedEvent($model));
            }
        });
    }

    public static $filterTypes = [
        "updated_at" => "date",
        "approved_at" => "date",
        "suspended_at" => "date",
    ];

    protected $casts = [
        "approved_at" => "datetime",
    ];

    protected $fillable = [
        "approved_at",
        "community_id",
        "role",
        "suspended_at",
        "user_id",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function community()
    {
        return $this->belongsTo(Community::class);
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return $query->whereHas("user", function ($q) use ($user) {
            return $q->accessibleBy($user);
        });
    }

    public function scopeNeedApproval(Builder $query)
    {
        return $query->whereNull([
            "community_user.approved_at",
            "community_user.suspended_at",
        ]);
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }
        return match ($for) {
            "edit" => $query->whereHas(
                "community.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            default => $query,
        };
    }
}
