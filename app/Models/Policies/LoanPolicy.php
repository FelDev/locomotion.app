<?php

namespace App\Models\Policies;

use App\Models\Loan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\HandlesAuthorization;

class LoanPolicy
{
    use HandlesAuthorization;

    private static function isAdminOfLoanCommunity(User $user, Loan $loan): bool
    {
        if (!$loan->community_id) {
            return false;
        }
        return $user->isAdminOfCommunity($loan->community_id);
    }

    private static function isLoanAdmin(User $user, Loan $loan): bool
    {
        return $user->isAdmin() || self::isAdminOfLoanCommunity($user, $loan);
    }

    private static function isBorrower(User $user, Loan $loan): bool
    {
        return $user->is($loan->borrower->user);
    }

    private static function isCoownerOrOwner(User $user, Loan $loan): bool
    {
        return $loan->loanable->isOwnerOrCoowner($user);
    }

    public function create(User $user): bool
    {
        return $user->approvedCommunities->isNotEmpty() || $user->isAdmin();
    }

    public function update(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function destroy(User $user, Loan $loan): bool
    {
        return $user->isAdmin();
    }

    public function view(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function viewInstructions(User $user, Loan $loan): bool
    {
        if (
            self::isBorrower($user, $loan) &&
            $loan->intention &&
            $loan->intention->status === "completed"
        ) {
            return true;
        }
        return $user->can("viewInstructions", $loan->loanable);
    }

    public function accept(User $user, Loan $loan)
    {
        if (
            $loan->status !== "in_process" ||
            !$loan->intention ||
            $loan->intention->status !== "in_process"
        ) {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "intention"])
            );
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function reject(User $user, Loan $loan)
    {
        if (
            $loan->status !== "in_process" ||
            !$loan->intention ||
            $loan->intention->status !== "in_process"
        ) {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "intention"])
            );
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function cancel(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        if (
            $loan->payment &&
            $loan->payment->status === "completed" &&
            $loan->total_final_cost > 0
        ) {
            return $this->deny(
                __("state.payment.must_not_be_completed_with_funds")
            );
        }

        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if (
            !$loan->is_free &&
            $loan->takeover &&
            $loan->takeover->status !== "in_process" &&
            !Carbon::now()->isBefore($loan->departure_at)
        ) {
            return $this->deny(__("state.loan.must_not_be_ongoing_with_cost"));
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isBorrower($user, $loan);
    }

    public function resume(User $user, Loan $loan)
    {
        if ($loan->loanable->trashed()) {
            return $this->deny(__("state.loanable.must_be_active"));
        }

        if ($loan->loanable->owner->user->trashed()) {
            return $this->deny(__("state.owner.must_be_active"));
        }

        if ($loan->borrower->user->trashed()) {
            return $this->deny(__("state.borrower.must_be_active"));
        }

        if (!$loan->isCanceled()) {
            return $this->deny(__("state.loan.must_be_canceled"));
        }

        return self::isLoanAdmin($user, $loan);
    }

    public function declareExtension(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function acceptExtension(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function rejectExtension(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        return self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function cancelExtension(User $user, Loan $loan): bool
    {
        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function declareIncident(User $user, Loan $loan)
    {
        if (
            $loan
                ->incidents()
                ->where("status", "in_process")
                ->exists()
        ) {
            return $this->deny(__("state.loan.must_not_have_open_incident"));
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function resolveIncident(User $user, Loan $loan): bool
    {
        return self::isLoanAdmin($user, $loan);
    }

    public function changeTakeoverInfo(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        // Admins can change info at all times
        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan)
        ) {
            return false;
        }

        if (!$loan->takeover || $loan->takeover->status !== "in_process") {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "takeover"])
            );
        }

        return true;
    }

    public function completeTakeover(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        // Cannot complete takeover before loan departure.
        if ((new Carbon())->lt(new Carbon($loan->departure_at))) {
            return $this->deny(
                __("state.action.must_be_after_departure", [
                    "action" => "takeover",
                ])
            );
        }

        // Admins can complete takeovers.
        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan)
        ) {
            return false;
        }

        if (!$loan->takeover || $loan->takeover->status !== "in_process") {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "takeover"])
            );
        }

        return true;
    }

    public function contestTakeoverInfo(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan) &&
            !$user->isAdmin() &&
            !self::isAdminOfLoanCommunity($user, $loan)
        ) {
            return false;
        }

        if (!$loan->takeover || $loan->takeover->status !== "completed") {
            return $this->deny(
                __("state.action.must_be_completed", ["action" => "takeover"])
            );
        }

        return true;
    }

    public function changeHandoverInfo(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        // Admins can change info at all times
        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan)
        ) {
            return false;
        }

        if (!$loan->handover || $loan->handover->status !== "in_process") {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "handover"])
            );
        }

        return true;
    }

    public function completeHandover(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        // Cannot complete takeover before loan departure.
        if ((new Carbon())->lt(new Carbon($loan->departure_at))) {
            return $this->deny(
                __("state.action.must_be_after_departure", [
                    "action" => "handover",
                ])
            );
        }

        // Admins can complete handovers.
        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan)
        ) {
            return false;
        }

        if (!$loan->handover || $loan->handover->status !== "in_process") {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "handover"])
            );
        }

        return true;
    }

    public function contestHandoverInfo(User $user, Loan $loan)
    {
        if ($loan->status !== "in_process") {
            return $this->deny(__("state.loan.must_be_ongoing"));
        }

        if (
            !self::isBorrower($user, $loan) &&
            !self::isCoownerOrOwner($user, $loan) &&
            !$user->isAdmin() &&
            !self::isAdminOfLoanCommunity($user, $loan)
        ) {
            return false;
        }

        if (!$loan->handover || $loan->handover->status !== "completed") {
            return $this->deny(
                __("state.action.must_be_completed", ["action" => "handover"])
            );
        }

        return true;
    }

    public function validateLoanInfo(User $user, Loan $loan)
    {
        if (!$loan->handover || $loan->handover->status !== "completed") {
            return $this->deny(
                __("state.action.must_be_completed", ["action" => "handover"])
            );
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan) ||
            $user->isAdmin();
    }

    public function resolveContestedLoan(User $user, Loan $loan)
    {
        if (!$loan->is_contested) {
            return $this->deny(__("state.loan.must_be_contested"));
        }

        return self::isLoanAdmin($user, $loan);
    }

    public function prepay(User $user, Loan $loan)
    {
        if (
            $loan->status !== "in_process" ||
            !$loan->prePayment ||
            $loan->prePayment->status !== "in_process"
        ) {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "prepayment"])
            );
        }

        return self::isBorrower($user, $loan) ||
            self::isLoanAdmin($user, $loan);
    }

    public function pay(User $user, Loan $loan)
    {
        if (
            $loan->status !== "in_process" ||
            !$loan->payment ||
            $loan->payment->status !== "in_process"
        ) {
            return $this->deny(
                __("state.action.must_be_ongoing", ["action" => "payment"])
            );
        }

        // Cannot pay (or complete loan) before loan departure.
        if ((new Carbon())->lt(new Carbon($loan->departure_at))) {
            return $this->deny(
                __("state.action.must_be_after_departure", [
                    "action" => "payment",
                ])
            );
        }

        if (!$loan->takeover || !$loan->takeover->isCompleted()) {
            return $this->deny(
                __("state.action.must_be_completed", ["action" => "takeover"])
            );
        }

        if (!$loan->handover || !$loan->handover->isCompleted()) {
            return $this->deny(
                __("state.action.must_be_completed", ["action" => "handover"])
            );
        }

        if (!$loan->borrowerCanPay()) {
            return $this->deny(__("state.payment.borrower_cant_pay"));
        }

        if (self::isLoanAdmin($user, $loan)) {
            return true;
        }

        if ($loan->needs_validation && !$loan->isFullyValidated()) {
            return $this->deny(__("state.payment.loan_needs_validation"));
        }

        return self::isBorrower($user, $loan) ||
            self::isCoownerOrOwner($user, $loan);
    }
}
