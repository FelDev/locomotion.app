<?php

namespace App\Models\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function update(User $accessingUser, User $accessedUser): bool
    {
        if ($accessingUser->id == $accessedUser->id) {
            return true;
        }

        return $accessingUser->isAdmin();
    }

    public function deactivate(User $accessingUser, User $accessedUser): bool
    {
        return $accessingUser->isAdmin();
    }

    public function listArchived(User $accessingUser)
    {
        return $accessingUser->isAdmin();
    }

    public function seeArchivedDetails(
        User $accessingUser,
        User $accessedUser
    ): bool {
        return $accessingUser->isAdmin();
    }

    public function checkProof(User $accessingUser, User $accessedUser): bool
    {
        return $accessingUser->isAdmin() ||
            $accessingUser->isAdminOfCommunityFor($accessedUser->id);
    }
}
