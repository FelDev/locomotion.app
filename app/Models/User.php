<?php

namespace App\Models;

use App\Casts\PointCast;
use App\Events\UserEmailUpdated;
use App\Mail\PasswordRequest;
use App\Mail\UserMail;
use App\Models\Pivots\CommunityUser;
use App\Services\GeocoderService;
use App\Transformers\UserTransformer;
use Database\Seeders\CommunitiesTableSeeder;
use Geocoder\Provider\GoogleMaps\Model\GoogleAddress;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Passport\HasApiTokens;
use Mail;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;
use Noke;
use Stripe;

/*
 * Soft-deleted (deleted_at not null) Users are considered archived. They will not be returned by
 * default in queries, but they can be restored.
 */
class User extends AuthenticatableBaseModel
{
    use HasApiTokens, Notifiable;
    use HasFactory;
    use SoftDeletes;
    use PostgisTrait;

    public static $rules = [
        "accept_conditions" => ["nullable", "accepted"],
        "address" => ["nullable"],
        "date_of_birth" => ["nullable", "date", "before:18 years ago"],
        "description" => "nullable",
        "email" => "email",
        "is_smart_phone" => "nullable|boolean",
        "last_name" => "nullable",
        "name" => ["nullable"],
        "other_phone" => ["nullable"],
        "password" => ["min:8"],
        "phone" => ["nullable"],
        "postal_code" => ["nullable"],
    ];

    public static $filterTypes = [
        "id" => "number",
        "created_at" => "date",
        "full_name" => "text",
        "address" => "text",
        "email" => "text",
    ];

    public $computed = ["admin_link"];

    public static function getRules($action = "", $auth = null)
    {
        switch ($action) {
            case "submit":
                $rules = array_merge(static::$rules, [
                    "address" => "required",
                    "date_of_birth" => "required",
                    "first_name" => "required",
                    "last_name" => "required",
                    "telephone" => "required",
                ]);
                break;
            case "template":
                $rules = parent::getRules($action, $auth);
                $rules["name"][] = "required";
                $rules["phone"][] = "required";
                $rules["address"][] = "required";
                break;
            default:
                $rules = parent::getRules($action, $auth);
                break;
        }

        if ($auth && $auth->isAdmin()) {
            unset($rules["accept_conditions"]);
            unset($rules["avatar"]);
        }

        return $rules;
    }

    protected static $transformer = UserTransformer::class;

    public static function booted()
    {
        self::saving(function (User $user) {
            // Make sure the date for accepting the conditions is set
            // Todo(#1121) remove the 'accept_conditions' field and move this
            // code when updating an user.
            if (
                $user->isDirty("accept_conditions") &&
                $user->accept_conditions
            ) {
                $user->acceptConditions();
            }
        });

        self::saved(function (User $user) {
            if ($user->id) {
                if ($user->wasChanged("address")) {
                    $user->updateAddressAndRelocateCommunity($user->address);
                }
            }
        });

        self::updated(function ($model) {
            // Detect email change
            if ($model->wasChanged("email")) {
                $previousEmail = $model->getOriginal("email");
                event(
                    new UserEmailUpdated($model, $previousEmail, $model->email)
                );
            }
        });

        self::deleted(function (User $user) {
            // Delete vehicles
            $user->owner?->loanables()->delete();

            // Delete ongoing loans
            $user->borrower
                ?->loans()
                ->where("status", "in_process")
                ->each(fn(Loan $loan) => $loan->cancel()->save());

            // remove connection tokens
            $user->tokens->each(fn($token) => $token->revoke());

            // Remove admin privileges
            $user
                ->communities()
                ->newPivotQuery()
                ->update(["community_user.role" => null]);

            $user
                ->communities()
                ->newPivotQuery()
                ->whereNotNull("approved_at")
                ->whereNull("suspended_at")
                ->update(["suspended_at" => Carbon::now()]);

            $user->role = null;
            $user->save();
        });
    }

    protected static array $customColumns = [
        "full_name" => "CONCAT(%table%.name, ' ', %table%.last_name)",
    ];

    protected $fillable = [
        "accept_conditions",
        "name",
        "last_name",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
        "is_proof_invalid",
        "bank_account_number",
        "bank_institution_number",
        "bank_transit_number",
    ];

    protected $hidden = ["password", "current_bill"];

    protected $casts = [
        "accept_conditions" => "boolean",
        "balance" => "decimal:2",
        "email_verified_at" => "datetime",
        "meta" => "array",
        "address_position" => PointCast::class,
    ];

    protected $with = [];

    public $collections = [
        "actions",
        "invoices",
        "communities",
        "files",
        "loans",
        "loanables",
        "payment_methods",
    ];

    protected $postgisFields = ["address_position"];

    protected $postgisTypes = [
        "address_position" => [
            "geomtype" => "geography",
        ],
    ];

    public $items = ["borrower", "owner", "google_account"];

    public $morphOnes = [
        "avatar" => "imageable",
    ];

    public $morphManys = [
        "residency_proof" => "fileable",
        "identity_proof" => "fileable",
    ];

    public $appends = ["available_loanable_types"];

    public function avatar()
    {
        return $this->morphOne(Image::class, "imageable")->where(
            "field",
            "avatar"
        );
    }

    public function residencyProof()
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "residency_proof"
        );
    }

    public function identityProof()
    {
        return $this->morphMany(File::class, "fileable")->where(
            "field",
            "identity_proof"
        );
    }

    public function scopeHasProof(Builder $query, $value, $negative)
    {
        $value = filter_var(
            $value,
            FILTER_VALIDATE_BOOLEAN,
            FILTER_NULL_ON_FAILURE
        );
        if (is_null($value)) {
            return $query;
        }

        if ($value === $negative) {
            return $query
                ->whereDoesntHave("identityProof")
                ->whereDoesntHave("residencyProof");
        }

        return $query->where(
            fn(Builder $q) => $q
                ->whereHas("residencyProof")
                ->orWhereHas("identityProof")
        );
    }

    public function actions()
    {
        return $this->hasMany(Action::class);
    }

    public function googleAccount()
    {
        return $this->hasOne(GoogleAccount::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function borrower()
    {
        return $this->hasOne(Borrower::class);
    }

    public function getAvailableLoanableTypesAttribute()
    {
        $set = [];

        foreach ($this->approvedCommunities as $community) {
            foreach ($community->allowedLoanableTypes as $loanableType) {
                $set[$loanableType->name->value] = true;
            }
        }

        return array_keys($set);
    }

    // main_community
    //
    // In 99% of the cases, a user is (or should be) associated
    // with a single geographical community we call main_community.
    //
    // Exceptionnaly, other scenarios may involve non-geographical communities such as Eco-Village, Schools, etc.
    // But the app won't react well to these exceptions because of the assumption below.
    //
    public function getMainCommunityAttribute()
    {
        return $this->communities->first();
    }

    private function assignCommunityForDevelopment(string $address): bool
    {
        if (app()->environment() !== "local" || env("GOOGLE_API_KEY")) {
            return false;
        }

        // Locally, without Geolocalisation, use seed RPP community for magic address
        if (str_contains($address, "6450")) {
            $this->AttachMainCommunity(
                Community::find(CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID)
            );
        }

        return true;
    }

    /**
     * Update User Address And Relocate Community
     *
     * @param  String $full_text_address
     * @return void
     *
     * SCENARIOS COVERED:
     *  1) Moved within the same community
     *  2) Moved from covered to non-covered
     *  3) Moved from non-covered to non-covered
     *  4) Moved from covered to covered
     *  5) Moved from non-covered to covered
     *
     */
    public function updateAddressAndRelocateCommunity(string $full_text_address)
    {
        if ($this->assignCommunityForDevelopment($full_text_address)) {
            return;
        }

        // Geocode the text address into an Address object
        /** @var GoogleAddress $address */
        $address = GeocoderService::geocode($full_text_address);
        $model = $this;

        // If the address has been located by the geocoder
        if ($address) {
            // Re-save newly formatted postal_code and address
            //
            // For the sake of keeping the data integrity of postal_code but technically we don't need it anymore
            // Save Quietly in order to prevent an infinite loop within this self:saved and $this->save();
            User::withoutEvents(function () use ($address, $model) {
                $model->postal_code = $address->getPostalCode();
                $model->address = GeocoderService::formatAddressToText(
                    $address
                );
                $model->address_position = $address->getCoordinates();
                $model->save();
            });

            $coordinates = $address->getCoordinates();

            // Find if the new address is within a community
            $latitude = $coordinates->getLatitude();
            $longitude = $coordinates->getLongitude();
            $community = GeocoderService::findCommunityFromCoordinates(
                $latitude,
                $longitude
            );

            // If so, attach or switch community
            if ($community) {
                $this->AttachMainCommunity($community);
            } else {
                \Log::warning(
                    "Could not find a community for coordinates $latitude $longitude for user $this->id with address '$this->address'"
                );
                if ($this->main_community) {
                    // User has moved from covered to a non-covered area
                    $this->DetachMainCommunity($this->main_community);
                }
            }
        } else {
            \Log::error(
                "Could not geocode user $this->id address '$this->address'"
            );
            // Users cannot have an invalid address
            abort(422, "The provided user address was not found.");
        }
    }

    /**
     * Attach a main community to the user
     *
     * @return void
     * Two safety guards are in place:
     * 1) Since it's technically possible a use is attache twice to a community, we detach first.
     * 2) We don't switch community if the user is already in it
     */
    public function AttachMainCommunity(Community $community)
    {
        if ($this->main_community) {
            if ($this->main_community->id !== $community->id) {
                $this->communities()->detach($this->communities);
                $this->communities()->detach($community);
                $this->communities()->attach($community);
            }
        } else {
            $this->communities()->detach($community);
            $this->communities()->attach($community);
        }
    }

    /**
     * Detach a main community from the user
     *
     * @return void
     */
    public function DetachMainCommunity(Community $community)
    {
        $this->communities()->detach($community);
    }

    /**  communities() should be deprecated at some point as 99% of users have only one community
     *  use $this->community instead
     */
    public function communities()
    {
        return $this->belongsToMany(Community::class)
            ->using(CommunityUser::class)
            ->withTimestamps()
            ->withPivot([
                "id",
                "approved_at",
                "created_at",
                "role",
                "suspended_at",
                "updated_at",
            ])
            ->distinct();
    }

    public function approvedCommunities()
    {
        return $this->communities()
            ->whereNotNull("approved_at")
            ->whereNull("suspended_at");
    }

    public function adminCommunities()
    {
        return $this->communities()->whereHas("users", function ($q) {
            return $q
                ->where("community_user.role", "admin")
                ->where("community_user.user_id", $this->id);
        });
    }

    public function defaultPaymentMethod()
    {
        return $this->hasOne(PaymentMethod::class)->whereIsDefault(true);
    }

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function loans()
    {
        return $this->hasManyThrough(Loan::class, Borrower::class);
    }

    public function loanables()
    {
        return $this->hasManyThrough(Loanable::class, Owner::class);
    }

    public function owner()
    {
        return $this->hasOne(Owner::class);
    }

    public function paymentMethods()
    {
        return $this->hasMany(PaymentMethod::class);
    }

    public function isAdmin()
    {
        return $this->role === "admin";
    }

    public function isCommunityAdmin()
    {
        return $this->adminCommunities()->exists();
    }

    public function isAdminOfCommunity(int $communityId)
    {
        return $this->adminCommunities()
            ->where("communities.id", $communityId)
            ->exists();
    }

    public function isAdminOfCommunityFor(int $userId)
    {
        return $this->adminCommunities()
            ->whereHas("users", function ($q) use ($userId) {
                return $q->where("community_user.user_id", $userId);
            })
            ->exists();
    }

    public function createInvoice($invoiceType)
    {
        $invoice = new Invoice();
        $invoice->user_id = $this->id;
        $invoice->period = \Carbon\Carbon::now()
            ->locale("fr_FR")
            ->format("m/Y");

        // Set the type of the invoice
        if ($invoiceType) {
            $invoice->type = $invoiceType;
        }

        $invoice->save();

        return $invoice;
    }

    public function getStripeCustomer()
    {
        return Stripe::getUserCustomer($this);
    }

    public function getAccessibleCommunityIds()
    {
        return $this->communities
            ->whereNotNull("pivot.approved_at")
            ->whereNull("pivot.suspended_at")
            ->pluck("id");
    }

    public function addToBalance($amount)
    {
        $this->balance = floatval($this->balance) + $amount;
        $this->save();
    }

    public function removeFromBalance($amount)
    {
        $this->balance = floatval($this->balance) - $amount;

        if (floatval($this->balance) < 0) {
            return abort(400);
        }

        $this->save();
    }

    public function updateBalance($amount)
    {
        if ($amount > 0) {
            $this->addToBalance($amount);
        } elseif ($amount < 0) {
            $this->removeFromBalance(-$amount);
        }
    }

    public function scopeApprovedInSharedCommunities(
        Builder $query,
        int $userId
    ) {
        return $query->where(
            fn($user) => $user->whereHas(
                "approvedCommunities",
                fn($community) => $community->withApprovedUser($userId)
            )
        );
    }

    public function scopeAccessibleInSharedCommunity(
        Builder $query,
        int $userId
    ) {
        // User can either access all users because they are community admin or
        // to all approved members because they are also approvet in this
        // community.
        return $query->where(
            fn($user) => $user
                ->whereHas(
                    "communities",
                    fn($community) => $community->withCommunityAdmin($userId)
                )
                ->orWhereHas(
                    "approvedCommunities",
                    fn($community) => $community->withApprovedUser($userId)
                )
        );
    }

    public function scopeAccessibleBy(Builder $query, $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        // A user has access to...
        return $query
            // ...himself or herself
            ->whereId($user->id)
            // ...or belonging to a community of which he or she is a member
            ->orWhere(fn($q) => $q->accessibleInSharedCommunity($user->id));
    }

    public function scopeSearch(Builder $query, $q)
    {
        if (!$q) {
            return $query;
        }

        $columnExpression = $this->getCustomColumnSql("full_name");

        // Use prepared statement to search.
        return $query->whereRaw(
            "unaccent($columnExpression) ILIKE unaccent(?)",
            ["%" . $q . "%"]
        );
    }

    public function scopeIsInCommunityRegion(Builder $query, $communityId)
    {
        return $query
            ->whereRaw(
                <<<SQL
    (SELECT public.ST_Contains(area::geometry, users.address_position::geometry)
     FROM communities 
     WHERE communities.type = 'borough' 
       and communities.id = $communityId
     )
SQL
            )
            ->whereDoesntHave("communities");
    }

    public function sendPasswordResetNotification($token)
    {
        UserMail::queue(new PasswordRequest($this, $token), $this);
    }

    public function getFullNameAttribute()
    {
        return trim($this->name . " " . $this->last_name);
    }

    public function getAdminLinkAttribute()
    {
        return env("FRONTEND_URL") . "/admin/users/" . $this->id;
    }

    public function acceptConditions()
    {
        $this->conditions_accepted_at = new Carbon();
        $this->accept_conditions = true;
    }

    public function scopeFor(Builder $query, ?string $for, User $user)
    {
        if ($user->isAdmin()) {
            return $query;
        }

        return match ($for) {
            "edit" => $query->whereHas(
                "communities.users",
                fn($q) => $q
                    ->where("community_user.user_id", $user->id)
                    ->whereNotNull("community_user.approved_at")
                    ->whereNull("community_user.suspended_at")
                    ->where("community_user.role", "admin")
            ),
            default => $query,
        };
    }

    public function scopeGlobalAdmins(Builder $query)
    {
        return $query->where("role", "admin");
    }

    public function scopeCommunityAdmins(Builder $query)
    {
        return $query->whereHas(
            "approvedCommunities",
            fn(Builder $q) => $q->where("role", "admin")
        );
    }

    public function __sleep()
    {
        if (\App::environment() === "testing") {
            $this->accessToken = null;
        }
        return parent::__sleep();
    }
}
