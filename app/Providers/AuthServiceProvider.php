<?php

namespace App\Providers;

use App\Models\Bike;
use App\Models\Car;
use App\Models\Policies\LoanablePolicy;
use App\Models\Trailer;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        \Gate::define("view-batch-status", function (User $user) {
            return $user->isAdmin() || $user->isCommunityAdmin();
        });

        $this->registerPolicies();

        Passport::routes();
    }
}
