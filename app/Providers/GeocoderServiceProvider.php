<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\LocoMotionGeocoderService;
use App\Services\GeocoderService;

class GeocoderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->instance("geocoder", new LocoMotionGeocoderService());
    }
}
