<?php

namespace App\Providers;

use App\Mail\Transport\MandrillTransport;
use Illuminate\Mail\MailManager;
use Illuminate\Support\ServiceProvider;

class MandrillServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->resolving(MailManager::class, function (
            MailManager $manager
        ) {
            $manager->extend("mandrill", function () {
                return new MandrillTransport(config("services.mandrill"));
            });
        });
    }
}
