<?php

namespace App\Providers;

use App\Services\FakeNokeService;
use App\Services\NokeService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class NokeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton("noke", function ($app) {
            if (app()->environment() === "production") {
                return new NokeService(new Client());
            }

            return new FakeNokeService();
        });
    }
}
