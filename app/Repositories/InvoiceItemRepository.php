<?php

namespace App\Repositories;

use App\Models\BillItem;

class InvoiceItemRepository extends RestRepository
{
    public function __construct(BillItem $model)
    {
        $this->model = $model;
    }
}
