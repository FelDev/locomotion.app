<?php

namespace App\Repositories;

use App\Models\BaseModel;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class RestRepository
{
    protected $model;

    protected $reserved = [
        "!fields",
        "fields",
        "for",
        "order",
        "page",
        "per_page",
        "q",
    ];

    /**
     * @param $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    public static function for($model): RestRepository
    {
        return new RestRepository($model);
    }

    public static function parseRequestParams($request)
    {
        // $request->all replaces all '.' that we might want to keep with '_'
        $params =
            self::parseQueryString(
                array_get($request->server->all(), "QUERY_STRING")
            ) ?:
            $request->all();

        // Validate there aren't extra params in request->all() that we might have missed
        $paramKeys = array_keys($params);
        $underscoredParamKeys = array_map(
            fn($val) => str_replace(".", "_", $val),
            $paramKeys
        );
        $missedParams = array_diff_key(
            $request->all(),
            array_flip($underscoredParamKeys)
        );

        return array_merge($params, $missedParams);
    }

    public function get($request)
    {
        $fields = $request->getFields();
        $user = $request->user();
        $query = $this->buildGetQuery(
            self::parseRequestParams($request),
            $user,
            $fields
        );

        // TODO Move to controller
        $perPage = $request->get("per_page") ?: 10;
        $page = $request->get("page") ?: 1;

        if ($fields && $perPage !== "*") {
            $this->applyWithFromQuery($fields, $query);
        }

        $total = self::getCount($query);

        if ($perPage !== "*") {
            $query = $query->take($perPage)->skip($perPage * ($page - 1));
        }

        try {
            if (empty($query->getEagerLoads())) {
                $items = $query->cursor();
            } else {
                $items = $query->get();
            }
        } catch (RelationNotFoundException $e) {
            throw ValidationException::withMessages([
                "includes" => "relationship does not exist",
            ]);
        }

        return [$items, $total];
    }

    public function find($request, $id)
    {
        if (!intval($id)) {
            abort(422, "Numeric ids.");
        }

        $params = self::parseRequestParams($request);
        return $this->buildBaseQuery(
            $params,
            $request->user(),
            $request->getFields()
        )->findOrFail($id);
    }

    /*
      This method is used to get items to be restored as it returns items that were
      soft-deleted.

      It's a copy of find(), but using the withTrashed() method.
    */
    public function findWithTrashed($request, $id)
    {
        if (!intval($id)) {
            abort(422, "Numeric ids.");
        }

        $params = self::parseRequestParams($request);
        return $this->buildBaseQuery(
            $params,
            $request->user(),
            $request->getFields()
        )
            ->withTrashed()
            ->findOrFail($id);
    }

    public function create($data)
    {
        $this->model->fill($data);
        static::saveItemAndRelations($this->model, $data);

        $model = $this->model;

        $className = get_class($this->model);
        $this->model = new $className();

        return $model;
    }

    public function update($request, $id, $data)
    {
        $query = $this->model;

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);
        $this->model->fill($data);
        static::saveItemAndRelations($this->model, $data);

        $className = get_class($this->model);
        $this->model = new $className();

        return $this->model->find($id);
    }

    public static function saveItemAndRelations($model, $relationData)
    {
        $model->save();
        // We sync the original here, so that checks for $model->wasChanged are false
        // on the second save.
        $model->syncOriginal();

        static::saveRelations($model, $relationData);

        $model->save();
    }

    public function destroy($request, $id)
    {
        $query = $this->model;

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->delete();

        return $this->model;
    }

    public function restore($request, $id)
    {
        $query = $this->model->withTrashed();

        if (method_exists($query, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->restore();

        return $this->model;
    }

    protected function getOrderByFields($param)
    {
        return explode(",", str_replace("-", "", $param));
    }

    protected function orderBy($query, $def)
    {
        if ($def === "") {
            return $query;
        }

        $columns = explode(",", $def);

        foreach ($columns as $column) {
            if (str_starts_with($column, "-")) {
                $direction = "desc";
                $column = substr($column, 1);
            } else {
                $direction = "asc";
            }

            if (str_contains($column, ".")) {
                $query = $this->orderByRelation($query, $column, $direction);
            } elseif ($this->model->isCustomColumn($column)) {
                $query = $query
                    ->withCustomColumn($column)
                    ->orderBy($column, $direction);
            } else {
                $table = $this->model->getTable();
                $query = $query->orderBy("$table.$column", $direction);
            }
        }

        return $query;
    }

    protected function orderByRelation(Builder $query, $column, $direction)
    {
        $currentModel = $this->model;
        $currentTable = $this->model->getTable();

        while (str_contains($column, ".")) {
            [$relation, $column] = explode(".", $column, 2);
            [$currentTable, $currentModel] = $currentModel->addRelationshipJoin(
                $query,
                $relation,
                $currentTable
            );
        }

        if (!empty($query->getQuery()->groups)) {
            // If the query already has a group by, we  must also add the last table in this groupby
            $query->groupBy("{$currentTable}.{$currentModel->getKeyName()}");
        }

        // Make sure we only select relevant columns. By default, eloquent selects *, which may result in
        // conflicts during joins.
        $allColumns = "{$this->model->getTable()}.*";
        if (!in_array($allColumns, $query->getQuery()->columns ?: [])) {
            $query->addSelect($allColumns);
        }

        if ($currentModel->isCustomColumn($column)) {
            $currentModel->addCustomColumn($query, $column, $currentTable);
            return $query->orderBy($column, $direction);
        } else {
            return $query->orderBy("$currentTable.$column", $direction);
        }
    }

    public static function saveRelations($model, $data)
    {
        static::saveItems($model, $data);
        static::saveCollections($model, $data);
    }

    protected static function saveItems($model, $data)
    {
        $skipObjectRelation = [];
        foreach (
            array_diff(array_keys($data), $model->getFillable())
            as $field
        ) {
            if ($field === "id") {
                continue;
            }

            if (preg_match('/_id$/', $field)) {
                $relationName = str_replace("_id", "", $field);

                if (in_array($relationName, $skipObjectRelation)) {
                    continue;
                }

                if (in_array($relationName, $model->items)) {
                    $newAssoc = $data[$field];

                    $skipObjectRelation[] = $relationName;

                    $camelRelationName = Str::camel($relationName);
                    $relation = $model->{$camelRelationName}();

                    if (is_a($relation, BelongsTo::class)) {
                        if ($newAssoc) {
                            $relation->associate($data[$field]);
                        } else {
                            $relation->dissociate();
                        }
                    } else {
                        if ($newAssoc) {
                            $newData = ["id" => $newAssoc];
                            static::saveRelatedItem(
                                $model,
                                $relationName,
                                $newData
                            );
                        } else {
                            static::deleteRelatedItem($model, $relationName);
                        }
                    }
                }
            } elseif (
                in_array($field, $model->items) &&
                is_array($data[$field])
            ) {
                $camelField = Str::camel($field);
                $related = static::saveRelatedItem(
                    $model,
                    $field,
                    $data[$field]
                );
                $relation = $model->{$camelField}();

                if (
                    $model->{$camelField} &&
                    $model->{$camelField}->id !== $related->id
                ) {
                    if (is_a($relation, HasOne::class)) {
                        $model->{$camelField}[
                            $model->{$camelField}()->getForeignKeyName()
                        ] = null;
                        $model->{$camelField}->save();
                    } else {
                        $relation->dissociate();
                    }
                }

                foreach (array_keys($related->morphOnes) as $morphOne) {
                    static::savePolymorphicRelation(
                        $related,
                        $morphOne,
                        $data[$field]
                    );
                }

                foreach (array_keys($related->morphManys) as $morphMany) {
                    static::saveMorphManyRelation(
                        $related,
                        $morphMany,
                        $data[$field]
                    );
                }

                if (is_a($relation, HasOne::class)) {
                    $related->{$relation->getForeignKeyName()} = $model->id;
                    $related->save();
                }

                if (is_a($relation, BelongsTo::class)) {
                    $relation->associate($related);
                }
            } elseif (
                in_array($field, array_keys($model->morphOnes)) &&
                array_key_exists($field, $data)
            ) {
                if (is_array($data[$field]) && !!$data[$field]) {
                    static::savePolymorphicRelation($model, $field, $data);
                } else {
                    static::deletePolymorphicRelation($model, $field);
                }
            }
        }
    }

    protected static function saveCollections($model, $data)
    {
        foreach (
            array_diff(array_keys($data), $model->getFillable())
            as $field
        ) {
            if (
                $field === "id" ||
                !array_key_exists($field, $data) ||
                !is_array($data[$field]) ||
                !in_array(
                    $field,
                    array_merge(
                        $model->collections,
                        array_keys($model->morphManys)
                    )
                )
            ) {
                continue;
            }

            $relation = $model->{Str::camel($field)}();

            if (is_a($relation, HasManyThrough::class)) {
                continue;
            }

            $ids = [];

            if (method_exists($relation, "getPivotClass")) {
                $pivotClass = $relation->getPivotClass();
                $pivot = new $pivotClass();
                $pivotAttributes = $pivot->getFillable();

                $isExtendedPivot = static::isBaseModel($pivot);
                if ($isExtendedPivot) {
                    $pivotItems = array_merge(
                        $pivot->items,
                        array_keys($pivot->morphOnes)
                    );
                    $pivotCollections = array_merge(
                        $pivot->collections,
                        array_keys($pivot->morphManys)
                    );
                } else {
                    $pivotItems = [];
                    $pivotCollections = [];
                }

                foreach ($data[$field] as $element) {
                    $pivotData = [];
                    $pivotItemData = [];

                    foreach ($pivotAttributes as $pivotAttribute) {
                        if (!array_key_exists($pivotAttribute, $element)) {
                            continue;
                        }

                        $pivotData[$pivotAttribute] = $element[$pivotAttribute];
                        unset($element[$pivotAttribute]);
                    }

                    foreach ($pivotItems as $pivotItem) {
                        if (!array_key_exists($pivotItem, $element)) {
                            continue;
                        }

                        $pivotItemData[$pivotItem] = $element[$pivotItem];
                        unset($element[$pivotItem]);
                    }

                    if (array_key_exists("id", $element) && $element["id"]) {
                        $sync = [];
                        $sync[$element["id"]] = $pivotData;
                        $relation->syncWithoutDetaching($sync);

                        $targetPivot = $model
                            ->{Str::camel($field)}()
                            ->find($element["id"])->pivot;

                        foreach ($pivotItems as $pivotItem) {
                            static::savePolymorphicRelation(
                                $targetPivot,
                                $pivotItem,
                                $pivotItemData
                            );
                        }

                        foreach ($pivotCollections as $pivotCollection) {
                            if (isset($element[$pivotCollection])) {
                                static::saveMorphManyRelation(
                                    $targetPivot,
                                    $pivotCollection,
                                    $element
                                );
                            }
                        }

                        $ids[] = $element["id"];
                    }
                }
            } else {
                foreach ($data[$field] as $element) {
                    if (array_key_exists("id", $element) && $element["id"]) {
                        $ids[] = $element["id"];
                    }
                }
            }

            $relatedClass = $relation->getRelated();

            if ($relatedClass->readOnly) {
                continue;
            }

            if (is_a($relation, MorphMany::class)) {
                static::saveMorphManyRelation($model, $field, $data);
            } elseif (is_a($relation, HasMany::class)) {
                $newItems = [];
                foreach ($data[$field] as $element) {
                    if (array_key_exists("id", $element) && $element["id"]) {
                        $existingItem = $relatedClass->find($element["id"]);
                        $existingItem->fill($element);
                        $newItems[] = $existingItem;
                    } else {
                        $newItem = new $relatedClass();
                        $newItem->fill($element);
                        $newItems[] = $newItem;
                    }
                }

                $existingIds = $relation
                    ->get()
                    ->pluck("id")
                    ->toArray();
                $removedIds = array_diff($existingIds, $ids);

                if (!empty($removedIds)) {
                    $relation
                        ->getRelated()
                        ->whereIn("id", $removedIds)
                        ->delete();
                }

                $relation->saveMany($newItems);
            } else {
                $relation->sync($ids);
            }
        }
    }

    /**
     * Transforms an array of key-value filter params into a nested structure,
     * removing any reserved params.
     *
     * @see RestRepository::parseFilter()
     */
    protected function parseFilters(array $filterParams): array
    {
        $filters = [];
        foreach ($filterParams as $filterKey => $filterValue) {
            if (in_array($filterKey, $this->reserved)) {
                continue;
            }

            $this->parseFilter($filterKey, $filterValue, $filters);
        }
        return $filters;
    }

    /**
     * Transforms a key-value filter param array into a nested array structure.
     * For example
     *
     * From:
     *
     * [
     *   "a.b.c.d" => 3,
     *   "a.b.c.e" => 4,
     *   "a.b.f.g" => 5,
     * ]
     *
     * to this:
     * [
     *   a => [
     *     b => [
     *       c => [
     *         d => 3,
     *         e => 4,
     *       ],
     *       f => [
     *         g => 5,
     *       ],
     *     ]
     *   ]
     * ]
     *
     * @param int|string $filterKey a possibly nested filter key (e.g. a.b.c)
     * @param mixed $filterValue the value for the filter
     * @param array $filters the mutable filter array. It is built through the recursive calls.
     */
    private function parseFilter(
        int|string $filterKey,
        mixed $filterValue,
        array &$filters
    ) {
        // Todo change this into a loop
        if (str_contains($filterKey, ".")) {
            [$filterKey, $rest] = explode(".", $filterKey, 2);
            if (!isset($filters[$filterKey])) {
                $filters[$filterKey] = [];
            }

            $this->parseFilter($rest, $filterValue, $filters[$filterKey]);
        } else {
            $filters[$filterKey] = $filterValue;
        }
    }

    /**
     * Applies filters on the query. This is called recursively once for every
     * relation the filters depend on.
     *
     * @param array $filters nested array of filters, where child arrays
     *      indicate filtering on a relation's property.
     * @param mixed $model model currently being filtered.
     * @param Builder $query a query or subquery for the current model to apply
     *      the filter on
     * @param mixed $userId the current users id
     * @return Builder
     */
    protected function applyFilters(
        array $filters,
        mixed $model,
        Builder &$query,
        User $user
    ): Builder {
        $relationFilters = [];
        $directFilter = [];
        $customColumnFilters = [];

        foreach ($filters as $filterKey => $filterValue) {
            if (is_array($filterValue)) {
                $relationFilters[$filterKey] = $filterValue;
            } elseif ($model->isCustomColumn($filterKey)) {
                $customColumnFilters[$filterKey] = $filterValue;
            } else {
                $directFilter[$filterKey] = $filterValue;
            }
        }

        // We recurse on every relation that is filtered
        foreach ($relationFilters as $filterKey => $filterValue) {
            $negative = $filterKey[0] === "!";
            $paramName = str_replace("!", "", $filterKey);

            $camelRelation = Str::camel($paramName);
            $targetRelation = $model->{$camelRelation}();
            $targetModel = $targetRelation->getRelated();

            if ($negative) {
                $query->whereDoesntHave(
                    $camelRelation,
                    fn(Builder $q) => $this->applyFilters(
                        $filterValue,
                        $targetModel,
                        $q,
                        $user
                    )
                );
            } else {
                $query->whereHas(
                    $camelRelation,
                    fn($q) => $this->applyFilters(
                        $filterValue,
                        $targetModel,
                        $q,
                        $user
                    )
                );
            }
        }

        // Add model's property filters
        foreach ($directFilter as $filterKey => $filterValue) {
            $this->applyFilter($model, $filterKey, $filterValue, $query, $user);
        }

        if (!empty($customColumnFilters)) {
            $query->withCustomColumns(array_keys($customColumnFilters));
            // To add a where clause on an aliased column, we need to wrap the current query in a sub query.
            $this->wrapQuery($query, $model);
            foreach ($customColumnFilters as $filterKey => $filterValue) {
                $this->applyFilter(
                    $model,
                    $filterKey,
                    $filterValue,
                    $query,
                    $user
                );
            }
        }

        return $query;
    }

    protected function applyFilter(
        $model,
        $param,
        $value,
        Builder &$query,
        User $user
    ): Builder {
        $negative = $param[0] === "!";
        $paramName = str_replace("!", "", $param);

        $camelParamName = Str::camel($paramName);
        if (method_exists($model, "scope$camelParamName")) {
            return $query->{$camelParamName}($value, $negative);
        }

        if (in_array($paramName, $model->items)) {
            if ($negative) {
                return $query->doesntHave($camelParamName);
            }

            return $query->whereHas($camelParamName);
        }

        $scopedParam = "{$model->getTable()}.$paramName";

        if ($paramName === "id" && $value === "me") {
            $value = $user->id;
        }

        if (
            $user->cant("listArchived", $model::class) &&
            ($paramName === "deleted_at" ||
                $paramName === "is_deleted" ||
                $paramName === "with_deleted")
        ) {
            return $query;
        }

        if ($paramName === "is_deleted") {
            if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                return $query->onlyTrashed();
            } else {
                return $query->withoutTrashed();
            }
        }

        if ($paramName === "with_deleted") {
            if (filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                return $query->withTrashed();
            } else {
                return $query->withoutTrashed();
            }
        }

        if ($paramName === "deleted_at" && !!$value) {
            $query->withTrashed();
        }

        // If a type is defined for this filter, use the query
        // language, otherwise fallback to default Laravel filtering
        $filterType = array_get($model::$filterTypes, $paramName);
        if (is_array($filterType)) {
            $filterType = "enum";
        }
        if (!$filterType && $paramName === "id") {
            $filterType = "number";
        }

        switch ($filterType) {
            case "enum":
                $values = explode(",", $value);
                return $this->applyWhereInFilter(
                    $values,
                    $negative,
                    $scopedParam,
                    $query
                );
            case "boolean":
                return $query->where(
                    $scopedParam,
                    $negative ? "!=" : "=",
                    filter_var($value, FILTER_VALIDATE_BOOLEAN) || $value === ""
                );
            case "number":
                if (is_array($value)) {
                    $value = implode(",", $value);
                }

                if ($value === "") {
                    return $query;
                }

                if (str_contains($value, ":")) {
                    $matches = [];
                    $regex = "(\d*)?";
                    preg_match("/^$regex:$regex$/", $value, $matches);

                    $start = $end = null;
                    if (isset($matches[1])) {
                        $start = $matches[1];
                    }
                    if (isset($matches[2])) {
                        $end = $matches[2];
                    }
                    $range = [$start, $end];

                    return $this->parseRangeFilter(
                        $scopedParam,
                        $range,
                        $query
                    );
                }

                $values = array_map(
                    "intval",
                    array_map(
                        "trim",
                        array_filter(explode(",", $value), function ($i) {
                            return $i !== "";
                        })
                    )
                );

                return $this->applyWhereInFilter(
                    $values,
                    $negative,
                    $scopedParam,
                    $query
                );
            case "text":
                if (
                    $model->getConnection()->getConfig()["driver"] === "pgsql"
                ) {
                    // Use prepared statement to search.
                    return $query->whereRaw(
                        "unaccent($scopedParam) ILIKE unaccent(?)",
                        ["%" . $value . "%"]
                    );
                } else {
                    return $query->where($scopedParam, "LIKE", "%$value%");
                }

            case "date":
                return $this->applyDateRangeFilter(
                    $scopedParam,
                    $value,
                    $query
                );

            default:
                if ($negative) {
                    return $query->where($scopedParam, "!=", $value);
                }

                return $query->where($scopedParam, $value);
        }
    }

    protected function applyWithFromQuery($fields, &$query)
    {
        $relations = array_merge(
            $this->model->items,
            $this->model->collections,
            array_keys($this->model->morphOnes)
        );

        $withRelations = array_intersect(array_keys($fields), $relations);
        return $query->with(array_map(Str::camel(...), $withRelations));
    }

    protected static function deletePolymorphicRelation(&$model, $field)
    {
        if ($currentRelation = $model->{$field}()->first()) {
            $currentRelation->delete();
        }

        return $currentRelation;
    }

    protected static function saveMorphManyRelation(&$model, $field, &$data)
    {
        if (!array_key_exists($field, $data)) {
            // No data for the current relationship
            return;
        }

        $relation = $model->{Str::camel($field)}();

        $relatedModel = $relation->getRelated();

        $ids = [];
        $newItems = [];
        foreach ($data[$field] as $element) {
            if (array_key_exists("id", $element) && $element["id"]) {
                $existingItem = $relatedModel->find($element["id"]);
                $existingItem->fill($element);
                $newItems[] = $existingItem;
                $ids[] = $element["id"];
            } else {
                $newItem = new $relatedModel();
                $newItem->fill($element);
                $newItems[] = $newItem;
            }
        }

        $itemsToDelete = $relation->whereNotIn("id", $ids)->get();

        foreach ($itemsToDelete as $itemToDelete) {
            $itemToDelete->delete();
        }

        $relation->saveMany($newItems);
    }

    protected static function savePolymorphicRelation(&$model, $field, &$data)
    {
        if (!array_key_exists($field, $data)) {
            return $model->{$field};
        }

        if (!$data[$field]) {
            if ($model->{$field}) {
                $model->{$field}->delete();
            }

            return null;
        }

        $relation = $model->{$field}();
        $related = $relation->getRelated();
        $existingRelation = $relation->getResults();

        $modelToSave = $related->newInstance();

        if (isset($data[$field]["id"])) {
            $id = $data[$field]["id"];
            $modelToSave = $related->findOrFail($id);
            if ($existingRelation && $existingRelation->id !== $id) {
                //delete existing relation.
                $existingRelation->delete();
            }
        }

        $modelToSave->fill($data[$field])->save();
        $relation->save($modelToSave);

        return $related;
    }

    protected static function deleteRelatedItem(&$model, $field)
    {
        $relation = $model->{$field}();
        $related = $model->{$field};

        if (!$related) {
            return $related;
        }

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = null;
        }

        $related->save();

        return $related;
    }

    protected static function saveRelatedItem(&$model, $field, &$data)
    {
        $camelField = Str::camel($field);
        $relation = $model->{$camelField}();

        if (isset($data["id"])) {
            // if the ID is provided, we want to update and associate an existing model.
            $related = $relation->getRelated()->find($data["id"]);
        } else {
            // if not, we update the existing item.
            $related = $relation->getResults();
        }

        // If the existing model is not found, in either case we create a new one
        if (!$related) {
            $related = $relation->getRelated()->newInstance();

            if (is_a($relation, HasOne::class)) {
                $related->{$relation->getForeignKeyName()} = $model->id;
            } elseif (is_a($relation, BelongsTo::class)) {
                // We cannot use $relation->associate($related) since that
                // would set the $model's owner key based on the related's foreign key.
                // Essentially the inverse of what we would like, since our model does exist
                // and the related is new.
                $related->{$relation->getForeignKeyName()} =
                    $model->{$relation->getOwnerKeyName()};
            }
        }

        if ($related->readOnly) {
            return $related;
        }

        $relatedData = array_intersect_key(
            $data,
            array_flip($related->getFillable())
        );

        $related->fill($relatedData);
        $related->save();

        return $related;
    }

    protected function parseRangeFilter($scopedParam, $range, &$query): Builder
    {
        [$start, $end] = array_map(function ($r) {
            if ($r === "" || $r === null || !is_numeric($r)) {
                return null;
            }

            return $r;
        }, $range);

        if ($start === null && $end === null) {
            return $query;
        } elseif ($start === null) {
            return $query->where($scopedParam, "<=", $end);
        } elseif ($end === null) {
            return $query->where($scopedParam, ">=", $start);
        }

        return $query->whereBetween($scopedParam, [$start, $end]);
    }

    // PHP's default query string parser replaces . by _
    // which would break our query language
    public static function parseQueryString($data)
    {
        $data = preg_replace_callback(
            "/(?:^|(?<=&))[^=[]+/",
            function ($match) {
                return bin2hex(urldecode($match[0]));
            },
            $data
        );

        parse_str($data, $values);

        return array_combine(
            array_map("hex2bin", array_keys($values)),
            $values
        );
    }

    public static function getCount($query)
    {
        if (str_contains($query->toSql(), "group by")) {
            $total = self::wrapQueryForRealTotal($query);
        } else {
            try {
                $total = $query->count();
            } catch (\Illuminate\Database\QueryException) {
                $total = self::wrapQueryForRealTotal($query);
            }
        }

        return $total;
    }

    // FIXME Virtual columns can mess with count
    protected static function wrapQueryForRealTotal($query)
    {
        $subquery = self::queryToRawSql($query);

        return \DB::select(
            \DB::raw("SELECT COUNT(*) AS cnt FROM ($subquery) AS query")
        )[0]->cnt;
    }

    protected static function isBaseModel($class, $autoload = true)
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return in_array("App\Models\BaseModelTrait", $traits);
    }

    public static function applyWhereInFilter(
        $values,
        $negative,
        $scopedParam,
        &$query
    ): Builder {
        if ($negative) {
            return $query->whereNotIn($scopedParam, $values);
        }

        return $query->whereIn($scopedParam, $values);
    }

    /*
      @param scopedParam
        Database column name prepended by table name if necessary.

      @param paramName
        Name of the parameter to filter with.

      @param value
        Right-open interval : [, ) containing ISO-8601 timestamps separated by @.

        Examples:
           Bounded interval:
            2021-06-01T14:00:00Z@2021-07-01T08:00:00Z
          Left-bounded interval
            2021-06-01T14:00:00Z
            2021-06-01T14:00:00Z@
          Right-bounded interval
            @2021-07-01T08:00:00Z
          Unbounded interval
            @
            Empty string.

      @param query
        Query object to apply filter to.

      @param aggregate
        Boolean indicating that the query is an aggregate that will switch
        "where" conditions to "having" conditions.
    */
    public static function applyDateRangeFilter(
        $scopedParam,
        $value,
        Builder &$query
    ): Builder {
        // Don't filter if value is empty.
        if ($value === "") {
            return $query;
        }

        $intervalRegex = "(?<start>[0-9TZ\.:-]*)@{0,1}(?<end>[0-9TZ\.:-]*)";

        $timestampRegex =
            "(?<year>[0-9]{4})" .
            "-(?<month>[0-9]{2})" .
            "-(?<day>[0-9]{2})" .
            "T(?<hour>[0-9]{2})" .
            ":(?<minute>[0-9]{2})" .
            ":(?<second>[0-9]{2})" .
            "\.{0,1}(?<millisecond>[0-9]*)" .
            "(?<timezone>Z)";

        $intervalMatches = [];
        $timestampMatches = [];

        if (preg_match("/^$intervalRegex?$/", $value, $intervalMatches)) {
            // Apply left boundary if exists
            if ($intervalMatches["start"]) {
                if (
                    preg_match(
                        "/^$timestampRegex?$/",
                        $intervalMatches["start"],
                        $timestampMatches
                    )
                ) {
                    $start = Carbon::parse(
                        $intervalMatches["start"]
                    )->setTimezone(config("app.timezone"));
                    // Match found, then add constraint.
                    $query->where($scopedParam, ">=", $start);
                } else {
                    throw new \Exception("Malformed timestamp.");
                }
            }

            // Apply right boundary if exists
            if ($intervalMatches["end"]) {
                if (
                    preg_match(
                        "/^$timestampRegex?$/",
                        $intervalMatches["end"],
                        $timestampMatches
                    )
                ) {
                    $end = Carbon::parse($intervalMatches["end"])->setTimezone(
                        config("app.timezone")
                    );
                    // Match found, then add constraint.
                    $query->where($scopedParam, "<", $end);
                } else {
                    throw new \Exception("Malformed timestamp.");
                }
            }

            // '@' open interval
            if (!$intervalMatches["start"] && !$intervalMatches["end"]) {
                $query->whereNotNull($scopedParam);
            }
        } else {
            $query->whereNotNull($scopedParam);
        }

        return $query;
    }

    protected static function queryToRawSql($query)
    {
        $sql = $query->toSql();

        $bindings = array_map(function ($p) {
            $escapedP = pg_escape_string($p);
            return "'$escapedP'";
        }, $query->getBindings());

        $sql = str_replace("%", "~!~!~", $sql);
        $sql = str_replace("?", "%s", $sql);
        $subquery = vsprintf($sql, $bindings);
        $subquery = str_replace("~!~!~", "%", $subquery);

        return $subquery;
    }

    /**
     * Wraps the query in a subquery with the same bindings (will get resolved
     * intothe same model) and the same name. This enables filtering on
     * custom columns.
     */
    private function wrapQuery(Builder $query, mixed $model): void
    {
        $query->setQuery(
            \DB::table(
                // Remove global scopes here, they will apply on the outer query still
                \DB::raw(
                    "({$query->clone()->withoutGlobalScopes()->toSql()}) AS {$model->getTable()}"
                )
            )->mergeBindings($query->getQuery())
        );
    }

    public function buildBaseQuery(
        array $params,
        User $user,
        array $fields
    ): Builder {
        /** @var Builder $query */
        $query = $this->model->newQuery();

        if (method_exists($this->model, "scopeAccessibleBy")) {
            $query = $query->accessibleBy($user);
        }

        if (isset($params["for"]) && method_exists($this->model, "scopeFor")) {
            $query = $query->for($params["for"], $user);
        }

        if ($fields) {
            $this->applyWithFromQuery($fields, $query);
            $query->withCustomColumns(array_keys($fields));
        }

        return $query;
    }

    public function buildGetQuery(
        array $params,
        User $user,
        array $fields
    ): Builder {
        $query = $this->buildBaseQuery($params, $user, $fields);
        // The following condition is a small improvement to an edge case where we both filter and order by
        // different custom columns which depend on a join. Filtering (which happens first) would wrap
        // the base query so that the custom columns could be filtered on, which would make the joined
        // table unaccessible when trying to add the orderBy. This line prevents an identical second join
        // from being added at the orderBy step, by selecting preemptively the required column into the
        // base query.
        if (isset($params["order"])) {
            $query->withCustomColumns(
                $this->getOrderByFields($params["order"])
            );
        }

        $query = $this->applyFilters(
            $this->parseFilters($params),
            $this->model,
            $query,
            $user
        );

        if (isset($params["q"]) && method_exists($this->model, "scopeSearch")) {
            $query = $query->search($params["q"]);
        }

        if (isset($params["order"])) {
            $query = $this->orderBy($query, $params["order"]);
        }

        return $query;
    }
}
