<?php

namespace App\Rules;

use GeoJson\Geometry\MultiPolygon;
use Illuminate\Contracts\Validation\Rule;

/*
  This rule only valid GeoJson MultiPolygon.
*/

class IsGeoJsonMultiPolygon implements Rule
{
    public function passes($attribute, $value)
    {
        // Accept nulls
        if (null === $value) {
            return true;
        }

        if (is_a($value, MultiPolygon::class)) {
            return true;
        }

        if (!is_array($value)) {
            return false;
        }

        // Accept an empty array.
        if (empty($value)) {
            return true;
        }

        // Accept either an array of Polygons or an object with an array of polygons in
        // the coordinates property.
        $coordinates = $value["coordinates"] ?? $value;

        try {
            // Constructor validates structure
            new MultiPolygon($coordinates);
        } catch (\InvalidArgumentException | \TypeError) {
            return false;
        }

        return true;
    }

    public function message()
    {
        return "Le MultiPolygone est invalide. Voir la spécification ici: https://datatracker.ietf.org/doc/html/rfc7946#section-3.1.7.";
    }
}
