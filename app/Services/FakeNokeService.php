<?php

namespace App\Services;

use App\Models\Padlock;
use App\Models\User;
use Exception;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

/**
 * This imitates NokeService, without sending requests to the noke API. One major difference,
 * is that the Noke Groups we return are always empty of users since this fake doesn't preserve
 * state between requests. For Noke users, locks and groups, it assumes the current DB state is
 * perfectly synced: for example, we won't return Noke Users if no User has a noke_id.
 *
 * @see NokeService
 */
class FakeNokeService implements NokeServiceInterface
{
    private array $groups = [];
    private int $lastGroupId = 0;
    private array $users = [];
    private array $locks = [];

    public function __construct($initialize = true)
    {
        if ($initialize) {
            // To test the user, locks or group sync/creation commands locally, we can comment
            // each ofthose lines
            $this->initUsers();
            $this->initLocks();
            $this->initGroups();
        }
    }

    public function initLocks()
    {
        // Initialize the fake locks list with DB padlocks
        foreach (Padlock::all() as $item) {
            $newLock = (object) [
                "id" => $item->external_id,
                "name" => $item->name,
                "macAddress" => $item->mac_address,
            ];
            $this->locks[$newLock->id] = $newLock;
        }
    }

    public function initGroups()
    {
        // Initialize the fake locks list with DB padlocks
        foreach (Padlock::all() as $item) {
            $newGroup = $this->constructNokeGroup(
                "API $item->mac_address",
                $item->external_id
            );
            $this->groups[$newGroup->id] = $newGroup;
        }
    }

    public function initUsers()
    {
        foreach (User::whereNotNull("noke_id")->get() as $user) {
            $newNokeUser = $this->constructNokeUser($user);
            $this->users[$newNokeUser->id] = $newNokeUser;
        }
    }

    public function createGroup($groupName, $lockExternalId)
    {
        Log::info("FakeNoke: Request to create group $groupName");

        $group = $this->constructNokeGroup($groupName, $lockExternalId);

        $this->groups[$group->id] = $group;

        return $group;
    }

    public function findUserById($nokeId): object
    {
        Log::info("Fake Noke: request to find noke user ID $nokeId");

        $user = $this->users[$nokeId] ?? null;

        if (!$user) {
            throw new Exception("Could not find the noke user {$nokeId}");
        }

        return $user;
    }

    public function createUser(User $user)
    {
        Log::info("Fake Noke: Request to create user ID $user->id");

        if (!!$user->noke_id) {
            Log::info("User $user->id already has noke user.");
            return null;
        }

        $nokeUser = $this->constructNokeUser($user);

        $this->users[$nokeUser->id] = $nokeUser;

        $user->noke_id = $nokeUser->id;
        $user->save();

        return $nokeUser;
    }

    public function fetchGroups()
    {
        return array_values($this->groups);
    }

    public function fetchLocks()
    {
        return array_values($this->locks);
    }

    public function fetchUsers()
    {
        return array_values($this->users);
    }

    public function getGroupProfile($id): object
    {
        Log::info("Fake Noke: request to find noke group ID $id");

        $group = $this->groups[$id] ?? null;

        if (!$group) {
            throw new Exception("Could not find the noke group {$id}");
        }

        return $group;
    }

    public function updateGroup($data)
    {
        Log::info("Fake Noke: Request to update group '$data->id'");

        $this->groups[$data->id] = $data;
    }

    public function updateUser($data)
    {
        Log::info("Fake Noke: Request to update user '$data->id'");

        $this->users[$data->id] = $data;
    }

    private function constructNokeGroup($groupName, $lockExternalId): object
    {
        $groupId = ++$this->lastGroupId;
        return (object) [
            "id" => $groupId,
            "name" => $groupName,
            "groupType" => "online",
            "lockIds" => [intval($lockExternalId)],
            "userIds" => [intval(config("services.noke.api_user_id"))],
            "schedule" => [
                [
                    "startDate" => "2020-05-01T00:00:00-04:00",
                    "endDate" => "2030-05-01T23:59:59-04:00",
                    "expiration" => "2030-05-01T23:59:59-04:00",
                    "repeatType" => "none",
                    "dayOfWeek" => "",
                    "name" => strval(time()),
                ],
            ],
        ];
    }

    private function constructNokeUser(User $user): object
    {
        return (object) [
            // FOR ease of testing,
            "id" => random_int(1, 999999999),
            "city" => "",
            "company" => "",
            "mobilePhone" => $user->is_smart_phone ? $user->phone : "",
            "name" => $user->full_name,
            "notifyEmail" => 1,
            "notifySMS" => $user->is_smart_phone ? 1 : 0,
            "permissions" => ["app_flag"],
            "phoneCountryCode" => "1",
            "state" => "",
            "streetAddress" => "",
            "title" => "",
            "username" => $user->email,
            "zip" => "",
        ];
    }
}
