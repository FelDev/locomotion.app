<?php

namespace App\Services;

use Illuminate\Support\Facades\Facade;

class GeocoderService extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return "geocoder";
    }
}
