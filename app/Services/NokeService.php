<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class NokeService implements NokeServiceInterface
{
    private $baseUrl = "https://v1-api-nokepro.appspot.com";
    private $token;
    private array $nokeUserCache = [];

    public function __construct(private Client $client)
    {
        if (app()->environment() !== "production") {
            return;
        }

        $this->token = Cache::get("noke:token");

        if (!$this->token) {
            $this->resetToken();
        }
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function createGroup($groupName, $lockExternalId)
    {
        $url = "{$this->baseUrl}/group/create/";
        Log::info("[NokeService] Request to $url for group $groupName");

        if (app()->environment() !== "production") {
            return null;
        }

        $response = $this->client->post($url, [
            "json" => [
                "name" => $groupName,
                "groupType" => "online",
                "lockIds" => [intval($lockExternalId)],
                "userIds" => [intval(config("services.noke.api_user_id"))],
                "schedule" => [
                    [
                        "startDate" => "2020-05-01T00:00:00-04:00",
                        "endDate" => "2030-05-01T23:59:59-04:00",
                        "expiration" => "2030-05-01T23:59:59-04:00",
                        "repeatType" => "none",
                        "dayOfWeek" => "",
                        "name" => strval(time()),
                    ],
                ],
            ],
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            throw new \Exception(
                "[NokeService] Could not create a noke group: {$response->getBody()}"
            );
        }

        return $result->data;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function findUserById($nokeId): object
    {
        $url = "{$this->baseUrl}/user/profile/";
        Log::info("[NokeService] Request to $url for noke user ID $nokeId");

        $response = $this->client->post($url, [
            "json" => [
                "id" => $nokeId,
            ],
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            throw new Exception(
                "[NokeService] Could not find the noke user {$nokeId}: {$response->getBody()}"
            );
        }

        return $result->data;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function createUser(User $user)
    {
        if (!!$user->noke_id) {
            Log::info("[NokeService] User $user->id already has noke user.");
            return null;
        }

        $url = "{$this->baseUrl}/user/create/";
        Log::info("[NokeService] Request to $url for user ID $user->id");

        if (app()->environment() !== "production") {
            return null;
        }

        $data = [
            "city" => "",
            "company" => "",
            "mobilePhone" => $user->is_smart_phone ? $user->phone : "",
            "name" => $user->full_name,
            "notifyEmail" => 1,
            "notifySMS" => $user->is_smart_phone ? 1 : 0,
            "permissions" => ["app_flag"],
            "phoneCountryCode" => "1",
            "state" => "",
            "streetAddress" => "",
            "title" => "",
            "username" => $user->email,
            "zip" => "",
        ];

        $response = $this->client->post($url, [
            "json" => $data,
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            $nokeUser = null;
            if ($result->errorCode == 14) {
                // Noke says User account with this email already exists, but our DB doesn't have
                // this users noke_id. We attempt to re-sync this here.
                $users = $this->fetchUsers();
                $nokeUser = array_first(
                    $users,
                    fn($u) => $u->username === $user->email
                );
            }
            if (!$nokeUser) {
                throw new Exception(
                    "[NokeService] Failed creating noke user: {$response->getBody()}"
                );
            }
        } else {
            $nokeUser = $result->data;
        }

        Log::info(
            "[NokeService] Assigning $nokeUser->id noke_id to user $user->id"
        );
        $user->noke_id = $nokeUser->id;
        $user->save();

        return $nokeUser;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function fetchGroups()
    {
        $page = 0;
        $groups = [];
        do {
            $page += 1;

            $url = "{$this->baseUrl}/group/get/all/";
            Log::info("Request to $url with page $page");

            $groupsResponse = $this->client->post($url, [
                "json" => [
                    "page" => $page,
                ],
                "headers" => [
                    "Accept" => "application/json",
                    "Authorization" => "Bearer $this->token",
                ],
            ]);
            $groupsResult = json_decode($groupsResponse->getBody());

            if (!isset($groupsResult->data->groups)) {
                throw new Exception(
                    "[NokeService] Could not fetch a noke groups: {$groupsResponse->getBody()}"
                );
            }

            $groups = array_merge($groupsResult->data->groups, $groups);
            $maxPage = intval($groupsResult->data->pageCount);
        } while ($maxPage > $page);

        return $groups;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function fetchLocks()
    {
        $url = "{$this->baseUrl}/lock/get/list/";
        Log::info("[NokeService] Request to $url");

        $locksResponse = $this->client->post($url, [
            "json" => [
                "page" => 1,
            ],
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $locksResult = json_decode($locksResponse->getBody());

        if (!isset($locksResult->data)) {
            throw new Exception(
                "[NokeService] Could not fetch a noke locks: {$locksResponse->getBody()}"
            );
        }

        return $locksResult->data;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function fetchUsers()
    {
        if ($this->nokeUserCache) {
            return $this->nokeUserCache;
        }

        $url = "{$this->baseUrl}/user/get/list/";
        if (app()->environment() !== "production") {
            Log::info(
                "[NokeService] In production: would make a request to $url to get all noke users."
            );
            return [];
        }

        Log::info("[NokeService] Request to $url");

        $usersResponse = $this->client->post($url, [
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);
        $usersResults = json_decode($usersResponse->getBody());

        if (!isset($usersResults->data)) {
            throw new Exception(
                "[NokeService] Could not fetch a noke locks: {$usersResponse->getBody()}"
            );
        }

        $this->nokeUserCache = $usersResults->data;

        return $usersResults->data;
    }

    /**
     * @throws GuzzleException
     * @throws Exception
     */
    public function getGroupProfile($id): object
    {
        $url = "{$this->baseUrl}/group/profile/";
        Log::info("[NokeService] Request to $url");

        $response = $this->client->post($url, [
            "json" => [
                "id" => $id,
            ],
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            throw new Exception(
                "[NokeService] Could not fetch the noke group {$id}: {$response->getBody()}"
            );
        }
        return $result->data;
    }

    public function updateGroup($data)
    {
        $url = "{$this->baseUrl}/group/edit/";
        Log::info("[NokeService] Request to $url");

        if (app()->environment() !== "production") {
            return null;
        }
        $response = $this->client->post($url, [
            "json" => $data,
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            throw new \Exception(
                "[NokeService] Could not update the noke group {$data->id}: {$response->getBody()}"
            );
        }

        return $result;
    }

    public function updateUser($data)
    {
        $url = "{$this->baseUrl}/user/edit/";
        Log::info("[NokeService] Request to $url");

        if (app()->environment() !== "production") {
            return null;
        }

        $response = $this->client->post($url, [
            "json" => $data,
            "headers" => [
                "Accept" => "application/json",
                "Authorization" => "Bearer $this->token",
            ],
        ]);

        $result = json_decode($response->getBody());

        if (isset($result->result) && $result->result === "failure") {
            throw new \Exception(
                "[NokeService] Could not update the noke user {$data->id}: {$response->getBody()}"
            );
        }

        return $result;
    }

    private function resetToken()
    {
        if (app()->environment() != "production") {
            return;
        }

        $url = "{$this->baseUrl}/company/web/login/";
        Log::info("[NokeService] Request to $url");

        $loginResponse = $this->client->post($url, [
            "json" => [
                "username" => config("services.noke.username"),
                "password" => config("services.noke.password"),
            ],
        ]);

        $loginResult = json_decode($loginResponse->getBody());

        if ($loginResult->result === "failure") {
            throw new \Exception("[NokeService] login error");
        }

        $this->token = $loginResult->token;

        // Noke tokens seem to reset at midgnight GMT, so we cache to either the end of day, or for 15 minutes.
        $cacheTime = min(Carbon::now(0)->secondsUntilEndOfDay(), 3600 / 4);
        if ($cacheTime > 0) {
            Cache::set("noke:token", $this->token, $cacheTime);
        } else {
            // If it's midnight, do not set a token for an hour, since the token will reset the next second.
            Cache::forget("noke:token");
        }
    }
}
