<?php

namespace App\Services;

use App\Models\User;

interface NokeServiceInterface
{
    public function fetchGroups();

    public function fetchLocks();

    public function fetchUsers();

    public function createGroup($groupName, $lockExternalId);

    public function createUser(User $user);

    public function findUserById($nokeId);

    public function getGroupProfile($id);

    public function updateGroup($data);

    public function updateUser($data);
}
