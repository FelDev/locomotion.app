<?php

namespace App\Transformers;

use App\Http\Resources\ArchivedLoanableResource;
use App\Models\Loan;
use Auth;

class LoanableTransformer extends Transformer
{
    public function transform($options = [])
    {
        $user = Auth::user();
        if ($user) {
            /** @var Loan $loan */
            $loan = $this->getFirstAncestor(Loan::class);
            $this->item->handleInstructionVisibility($user, $loan);
        }

        $output = parent::transform($options);

        if ($this->item->trashed()) {
            return new ArchivedLoanableResource($this->item);
        }

        if ($user && $user->isAdmin()) {
            return $output;
        }

        unset($output["padlock"]);

        return $output;
    }
}
