<?php

namespace App\Transformers;

use App\Http\Resources\ArchivedUserResource;
use App\Models\Community;
use App\Models\Coowner;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\User;
use Auth;

class UserTransformer extends Transformer
{
    protected array $pivots = [Community::class];

    public function authorize($output, $options)
    {
        $user = Auth::user();

        // If the user is...
        if ($user) {
            // ...a global admin or itself
            if ($user->isAdmin() || $user->id === $this->item->id) {
                // ...display everything
                return $output;
            }
            // ...a community admin
            if ($user->isAdminOfCommunityFor($this->item->id)) {
                $adminOfCommunityFields = [
                    "id",
                    "avatar",
                    "available_loanable_types",
                    "communities",
                    "date_of_birth",
                    "address",
                    "postal_code",
                    "phone",
                    "is_smart_phone",
                    "other_phone",
                    "description",
                    "email",
                    "full_name",
                    "last_name",
                    "loanables",
                    "loans",
                    "name",
                    "owner",
                    "residency_proof",
                    "identity_proof",
                    "is_proof_invalid",
                ];

                return $this->filterKeys($output, $adminOfCommunityFields);
            }
        }

        $publicFields = [
            "id",
            "name",
            "last_name",
            "full_name",
            "avatar",
            "description",
            "owner",
        ];

        /** @var Loanable $loanable */
        $loanable = $this->getFirstAncestor(Loanable::class);
        // If loanable is present in the ancestry, we're transforming an owner or coowner.
        // Show contact info only if user is another (co-)owner;
        if ($loanable && $loanable->isOwnerOrCoowner($user)) {
            $publicFields[] = "phone";
            $publicFields[] = "communities";
            return $this->filterKeys($output, $publicFields);
        }

        /** @var Loan $loan */
        $loan = $this->getFirstAncestor(Loan::class);
        if ($loan && $this->shouldShowLoanUserPhone($user, $loan)) {
            $publicFields[] = "phone";
        }

        return $this->filterKeys($output, $publicFields);
    }

    public function transform($options = [])
    {
        if ($this->item->trashed()) {
            return new ArchivedUserResource($this->item);
        }

        $output = parent::transform($options);

        if ($this->shouldIncludeRelation("borrower", $this->item, $options)) {
            $output["borrower"] = $this->item->borrower ?: new \stdClass();
        }

        if (isset($output["balance"])) {
            // Approximation but more convenient for display
            $output["balance"] = floatval($output["balance"]);
        }

        return $output;
    }

    private function shouldShowLoanUserPhone(User $seeingUser, Loan $loan): bool
    {
        /** @var User $shownUser */
        $shownUser = $this->item;

        if ($shownUser->is($loan->borrower->user)) {
            return $loan->loanable->isOwnerOrCoowner($seeingUser);
        }

        if ($loan->loanable->isOwnerOrCoowner($shownUser)) {
            if ($loan->loanable->isOwnerOrCoowner($seeingUser)) {
                return true;
            }

            if ($seeingUser->is($loan->borrower->user)) {
                return ($this->hasAncestor(Owner::class) &&
                    $loan->loanable->show_owner_as_contact) ||
                    $this->getFirstAncestor(Coowner::class)?->show_as_contact;
            }
        }

        return false;
    }
}
