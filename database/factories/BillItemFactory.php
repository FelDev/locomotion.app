<?php
namespace Database\Factories;
use App\Models\BillItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class BillItemFactory extends Factory
{
    protected $model = BillItem::class;

    public function definition(): array
    {
        $amount = $this->faker->numberBetween(0, 300_000);

        return [
            "label" => $this->faker->word,
            "amount" => $amount,
            "item_date" => $this->faker->date(),
            "taxes_tps" => $amount * 0.05,
            "taxes_tvq" => $amount * 0.09975,
            "amount_type" => $this->faker->randomElement(["debit", "credit"]),
            "item_type" => $this->faker->word(),
        ];
    }
}
