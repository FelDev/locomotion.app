<?php
namespace Database\Factories;
use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    protected $model = Car::class;

    public function definition(): array
    {
        return [
            "brand" => $this->faker->word,
            "engine" => $this->faker->randomElement([
                "fuel",
                "diesel",
                "electric",
                "hybrid",
            ]),
            "has_informed_insurer" => $this->faker->boolean,
            "insurer" => $this->faker->word,
            "is_value_over_fifty_thousand" => $this->faker->boolean,
            "value_category" => $this->faker->randomElement([
                "lte50k",
                "lte70k",
                "lte100k",
            ]),
            "model" => $this->faker->sentence,
            "papers_location" => $this->faker->randomElement([
                "in_the_car",
                "to_request_with_car",
            ]),
            "plate_number" => $this->faker->shuffle("9F29J2"),
            "pricing_category" => $this->faker->randomElement([
                "small",
                "large",
            ]),
            "transmission_mode" => $this->faker->randomElement([
                "automatic",
                "manual",
            ]),
            "year_of_circulation" => $this->faker->year(),
        ];
    }
}
