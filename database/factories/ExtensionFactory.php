<?php
namespace Database\Factories;
use App\Models\Extension;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExtensionFactory extends Factory
{
    protected $model = Extension::class;
    public function definition()
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
            "new_duration" => $this->faker->randomNumber(),
            "comments_on_extension" => $this->faker->paragraph,
            "contested_at" => null,
            "comments_on_contestation" => null,
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }

    public function rejected()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "rejected",
        ]);
    }
}
