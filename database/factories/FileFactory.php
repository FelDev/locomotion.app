<?php
namespace Database\Factories;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;
use Storage;

class FileFactory extends Factory
{
    protected $model = File::class;

    public function definition(): array
    {
        $filename = $this->faker->word . "." . $this->faker->fileExtension;
        return [
            "path" => $this->faker->word,
            "filename" => $filename,
            "original_filename" => $filename,
            "filesize" => $this->faker->randomNumber(),
        ];
    }

    public function configure(): FileFactory
    {
        return $this->afterMaking(function (File $file) {
            Storage::put($file->full_path, "fakedata");
        });
    }
}
