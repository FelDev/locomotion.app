<?php
namespace Database\Factories;
use App\Models\Handover;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class HandoverFactory extends Factory
{
    protected $model = Handover::class;
    public function definition(): array
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
            "mileage_end" => $this->faker->numberBetween(0, 300_000),
            "comments_by_borrower" => $this->faker->sentence,
            "comments_by_owner" => $this->faker->sentence,
            "purchases_amount" => 0,
            "contested_at" => null,
            "comments_on_contestation" => null,
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
            "mileage_end" => null,
            "comments_by_borrower" => null,
            "comments_by_owner" => null,
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }
}
