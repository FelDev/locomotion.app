<?php
namespace Database\Factories;
use App\Models\Intention;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class IntentionFactory extends Factory
{
    protected $model = Intention::class;

    public function definition()
    {
        return [
            "executed_at" => Carbon::now(),
            "status" => $this->faker->randomElement([
                "in_process",
                "canceled",
                "completed",
            ]),
        ];
    }

    public function completed()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "completed",
        ]);
    }

    public function inProcess()
    {
        return $this->state([
            "executed_at" => null,
            "status" => "in_process",
        ]);
    }

    public function canceled()
    {
        return $this->state([
            "executed_at" => Carbon::now(),
            "status" => "canceled",
        ]);
    }
}
