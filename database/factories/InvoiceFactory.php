<?php
namespace Database\Factories;
use App\Models\BillItem;
use App\Models\Invoice;
use App\Models\PaymentMethod;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class InvoiceFactory extends Factory
{
    protected $model = Invoice::class;

    public function definition(): array
    {
        return [
            "period" => Carbon::now()
                ->locale("fr_FR")
                ->isoFormat("LLLL"),
            "paid_at" => Carbon::now(),
        ];
    }

    public function withPaymentMethod(): InvoiceFactory
    {
        return $this->has(PaymentMethod::factory());
    }

    public function withUser(): InvoiceFactory
    {
        return $this->has(User::factory());
    }

    public function withItemCount($count): InvoiceFactory
    {
        return $this->has(
            BillItem::factory()
                ->count($count)
                ->state(function (array $attributes, Invoice $invoice) {
                    return ["invoice_id" => $invoice->id];
                })
        );
    }
}
