<?php

namespace Database\Factories;

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Extension;
use App\Models\Handover;
use App\Models\Intention;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Payment;
use App\Models\PrePayment;
use App\Models\Takeover;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoanFactory extends Factory
{
    protected $model = Loan::class;

    public function definition(): array
    {
        return [
            "departure_at" => Carbon::now(),
            // Over 15 minutes.
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3),
            "reason" => $this->faker->text,
            "message_for_owner" => $this->faker->paragraph,
            "platform_tip" => $this->faker->randomNumber(1),
            "loanable_id" => Loanable::factory(),
            "borrower_id" => Borrower::factory(),
        ];
    }

    public function configure(): LoanFactory
    {
        return $this->afterMaking(function ($loan) {
            if (!$loan->community_id) {
                $loan->community_id =
                    $loan->loanable->owner->user->main_community->id;
            }
        })->afterCreating(function (Loan $loan) {
            $loan->refresh();
        });
    }

    public function withCompletedIntention(): LoanFactory
    {
        return $this->has(Intention::factory()->completed());
    }

    public function withInProcessIntention(): LoanFactory
    {
        return $this->has(Intention::factory()->inProcess());
    }

    public function withInProcessTakeover(): LoanFactory
    {
        return $this->withCompletedPrePayment()->has(
            Takeover::factory()->inProcess()
        );
    }

    public function withCompletedTakeover($mileage = null): LoanFactory
    {
        if (!$mileage) {
            $mileage = $this->faker->randomNumber(5);
        }

        return $this->withCompletedPrePayment()->has(
            Takeover::factory([
                "mileage_beginning" => $mileage,
            ])->completed()
        );
    }

    public function withContestedTakeover(): LoanFactory
    {
        return $this->withCompletedPrePayment()->has(
            Takeover::factory()->canceled()
        );
    }

    public function withInProcessHandover(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Handover::factory()->inProcess()
        );
    }

    public function withCompletedHandover(
        int $mileageBeginning = null,
        int $mileageEnd = null
    ): LoanFactory {
        $mileageEnd = $mileageEnd ?? $this->faker->numberBetween(1000, 300_000);
        $mileageBeginning =
            $mileageBeginning ??
            $this->faker->numberBetween($mileageEnd - 500, $mileageEnd);

        return $this->withCompletedTakeover($mileageBeginning)->has(
            Handover::factory([
                "mileage_end" => $mileageEnd,
            ])->completed()
        );
    }

    public function withContestedHandover(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Handover::factory()->canceled()
        );
    }

    public function withCompletedPrePayment(): LoanFactory
    {
        return $this->withCompletedIntention()->has(
            PrePayment::factory()->completed()
        );
    }

    public function withInProcessPrePayment(): LoanFactory
    {
        return $this->withCompletedIntention()->has(
            PrePayment::factory()->inProcess()
        );
    }

    public function withCanceledPrePayment(): LoanFactory
    {
        return $this->withCompletedIntention()->has(
            PrePayment::factory()->canceled()
        );
    }

    public function withInProcessExtension(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Extension::factory([
                "new_duration" => 120,
            ])->inProcess()
        );
    }

    public function withCompletedExtension(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Extension::factory(["new_duration" => 120])->completed()
        );
    }

    public function withRejectedExtension(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Extension::factory()->rejected()
        );
    }

    public function withCanceledExtension(): LoanFactory
    {
        return $this->withCompletedTakeover()->has(
            Extension::factory()->canceled()
        );
    }

    public function withInProcessPayment(): LoanFactory
    {
        return $this->withCompletedHandover()->has(
            Payment::factory()->inProcess()
        );
    }

    public function withCompletedPayment(): LoanFactory
    {
        return $this->withCompletedHandover()->has(
            Payment::factory()->completed()
        );
    }
}
