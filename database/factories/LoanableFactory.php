<?php

namespace Database\Factories;

use App\Enums\LoanableTypes;
use App\Models\Community;
use App\Models\Coowner;
use App\Models\Image;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Padlock;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;

class LoanableFactory extends Factory
{
    protected $model = Loanable::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "type" => LoanableTypes::Bike,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "availability_mode" => "always",
            "owner_id" => Owner::factory(),
        ];
    }

    public function configure(): LoanableFactory
    {
        return $this->createDetails();
    }

    private function getFactory(LoanableTypes $type): Factory
    {
        return $type->getLoanableModel()::factory();
    }

    private function createDetails(array $attributes = []): LoanableFactory
    {
        return $this->afterCreating(function (Loanable $loanable) use (
            $attributes
        ) {
            $factory = $this->getFactory($loanable->type);

            if (!$loanable->details) {
                $factory->for($loanable)->create($attributes);
                $loanable->load("details");
            } else {
                $loanable->details->fill($attributes)->save();
            }
        });
    }

    public function withBike(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Bike,
        ])->createDetails($attributes);
    }

    public function withTrailer(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Trailer,
        ])->createDetails($attributes);
    }

    public function withCar(array $attributes = []): LoanableFactory
    {
        return $this->state([
            "type" => LoanableTypes::Car,
        ])->createDetails($attributes);
    }

    public function withCommunity(): LoanableFactory
    {
        return $this->has(Community::factory()->withDefaultFreePricing());
    }

    public function withPadlock(): LoanableFactory
    {
        return $this->has(Padlock::factory());
    }

    public function withCoowner(UserFactory|User $user = null): LoanableFactory
    {
        if ($user) {
            return $this->has(Coowner::factory(["user_id" => $user]));
        }
        return $this->has(Coowner::factory());
    }

    public function withImage(): LoanableFactory
    {
        return $this->afterCreating(function (Loanable $loanable) {
            // Without events to avoid triggering Image move
            Image::withoutEvents(
                fn() => $loanable->image()->save(
                    Image::factory()->create([
                        "imageable_type" => Loanable::class,
                        "imageable_id" => $loanable->id,
                        "field" => "image",
                    ])
                )
            );
            $loanable->save();
        });
    }
}
