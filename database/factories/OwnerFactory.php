<?php
namespace Database\Factories;
use App\Models\Owner;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class OwnerFactory extends Factory
{
    protected $model = Owner::class;

    public function definition(): array
    {
        return [
            "submitted_at" => Carbon::now(),
            "approved_at" => Carbon::now(),
            "user_id" => User::factory()->withCommunity(),
        ];
    }
}
