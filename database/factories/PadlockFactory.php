<?php
namespace Database\Factories;
use App\Models\Padlock;
use Illuminate\Database\Eloquent\Factories\Factory;

class PadlockFactory extends Factory
{
    protected $model = Padlock::class;
    public function definition(): array
    {
        return [
            "name" => $this->faker->words(3, true),
            "mac_address" => $this->faker->macAddress,
            "external_id" => $this->faker->asciify("*********"),
        ];
    }
}
