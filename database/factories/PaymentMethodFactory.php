<?php
namespace Database\Factories;
use App\Models\PaymentMethod;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentMethodFactory extends Factory
{
    protected $model = PaymentMethod::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "external_id" => $this->faker->sentence,
            "four_last_digits" => $this->faker->randomNumber(4, true),
            "credit_card_type" => $this->faker->creditCardType,
        ];
    }
}
