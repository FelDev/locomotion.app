<?php
namespace Database\Factories;
use App\Models\Pricing;
use Illuminate\Database\Eloquent\Factories\Factory;

class PricingFactory extends Factory
{
    protected $model = Pricing::class;

    public function definition(): array
    {
        return [
            "name" => $this->faker->name,
            "rule" => "10",
            "object_type" => $this->faker->randomElement([
                "App\Models\Bike",
                "App\Models\Car",
                "App\Models\Trailer",
            ]),
        ];
    }
}
