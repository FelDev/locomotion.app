<?php
namespace Database\Factories;
use App\Models\Trailer;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrailerFactory extends Factory
{
    protected $model = Trailer::class;

    public function definition(): array
    {
        return [
            "maximum_charge" => $this->faker->numerify("# kg"),
            "dimensions" => $this->faker->sentence(4),
        ];
    }
}
