<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleAndDisplayFlagToCoowners extends Migration
{
    public function up()
    {
        Schema::table("coowners", function (Blueprint $table) {
            $table->boolean("show_as_contact")->default(false);
            $table->string("title")->nullable();
        });
    }

    public function down()
    {
        Schema::table("coowners", function (Blueprint $table) {
            $table->dropColumn("show_as_contact");
            $table->dropColumn("title");
        });
    }
}
