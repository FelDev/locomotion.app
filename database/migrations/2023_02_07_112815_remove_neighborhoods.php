<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        DB::table("communities")
            ->where("type", "=", "neighborhood")
            ->update(["type" => "private"]);

        Schema::table("communities", function (Blueprint $table) {
            $table
                ->enum("type", ["private", "borough"])
                ->default("private")
                ->change();

            $table->dropColumn("parent_id");
        });

        \DB::statement("DROP MATERIALIZED VIEW loanables");
        \DB::statement(
            <<<SQL
CREATE MATERIALIZED VIEW loanables
(id, type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id, community_id, created_at, updated_at, deleted_at) AS
    SELECT id, 'car' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id, community_id, created_at, updated_at, deleted_at FROM cars
UNION
    SELECT id, 'bike' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id, community_id, created_at, updated_at, deleted_at FROM bikes
UNION
    SELECT id, 'trailer' AS type, name, position, location_description, comments, instructions, is_self_service, availability_mode, availability_json, owner_id, community_id, created_at, updated_at, deleted_at FROM trailers
SQL
        );
        \DB::statement(
            <<<SQL
CREATE UNIQUE INDEX loanables_index
ON loanables (id, type);
SQL
        );

        foreach (["cars", "trailers", "bikes"] as $type) {
            Schema::table($type, function (Blueprint $table) {
                $table->dropColumn("share_with_parent_communities");
            });
        }
    }

    public function down()
    {
        Schema::table("communities", function (Blueprint $table) {
            $table
                ->enum("type", ["private", "neighborhood", "borough"])
                ->default("neighborhood")
                ->change();

            $table->unsignedBigInteger("parent_id")->nullable();

            $table
                ->foreign("parent_id")
                ->references("id")
                ->on("communities")
                ->onDelete("restrict");
        });

        foreach (["cars", "trailers", "bikes"] as $type) {
            Schema::table($type, function (Blueprint $table) {
                $table
                    ->boolean("share_with_parent_communities")
                    ->default(false);
            });
        }

        \DB::statement("DROP MATERIALIZED VIEW loanables");
        \DB::statement(
            <<<SQL
CREATE MATERIALIZED VIEW loanables
(id, type, name, position, location_description, comments, instructions, availability_mode, availability_json, owner_id, community_id, share_with_parent_communities, created_at, updated_at, deleted_at) AS
    SELECT id, 'car' AS type, name, position, location_description, comments, instructions, availability_mode, availability_json, owner_id, community_id, share_with_parent_communities, created_at, updated_at, deleted_at FROM cars
UNION
    SELECT id, 'bike' AS type, name, position, location_description, comments, instructions, availability_mode, availability_json, owner_id, community_id, share_with_parent_communities, created_at, updated_at, deleted_at FROM bikes
UNION
    SELECT id, 'trailer' AS type, name, position, location_description, comments, instructions, availability_mode, availability_json, owner_id, community_id, share_with_parent_communities, created_at, updated_at, deleted_at FROM trailers
SQL
        );
        \DB::statement(
            <<<SQL
CREATE UNIQUE INDEX loanables_index
ON loanables (id, type);
SQL
        );
    }
};
