<?php

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Handover;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\Takeover;
use App\Models\Trailer;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration {
    const currentMorphMap = [
        "loanable" => Loanable::class,
        "car" => Car::class,
        "bike" => Bike::class,
        "trailer" => Trailer::class,

        "user" => User::class,
        "communityUser" => CommunityUser::class,
        "borrower" => Borrower::class,

        "handover" => Handover::class,
        "takeover" => Takeover::class,
    ];
    public function up()
    {
        foreach (self::currentMorphMap as $type => $class) {
            DB::table("images")
                ->where("imageable_type", $class)
                ->update(["imageable_type" => $type]);

            DB::table("files")
                ->where("fileable_type", $class)
                ->update(["fileable_type" => $type]);

            DB::table("taggables")
                ->where("taggable_type", $class)
                ->update(["taggable_type" => $type]);
        }
    }

    public function down()
    {
        foreach (self::currentMorphMap as $type => $class) {
            DB::table("images")
                ->where("imageable_type", $type)
                ->update(["imageable_type" => $class]);

            DB::table("files")
                ->where("fileable_type", $type)
                ->update(["fileable_type" => $class]);

            DB::table("taggables")
                ->where("taggable_type", $type)
                ->update(["taggable_type" => $class]);
        }
    }
};
