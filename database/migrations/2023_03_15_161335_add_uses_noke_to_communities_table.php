<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsesNokeToCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->boolean("uses_noke")->default(false);
        });

        // Set to true for existing communities, but defaults to 0 false for future communities.
        DB::table("communities")->update(["uses_noke" => true]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("communities", function (Blueprint $table) {
            $table->dropColumn("uses_noke");
        });
    }
}
