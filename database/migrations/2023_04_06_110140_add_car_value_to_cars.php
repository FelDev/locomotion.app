<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCarValueToCars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cars", function (Blueprint $table) {
            $table
                ->enum("value_category", ["lte50k", "lte70k", "lte100k"])
                ->nullable();
        });

        \DB::statement(
            "UPDATE cars SET value_category = 'lte50k' WHERE is_value_over_fifty_thousand = false"
        );
        \DB::statement(
            "UPDATE cars SET value_category = 'lte70k' WHERE is_value_over_fifty_thousand = true"
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("cars", function (Blueprint $table) {
            $table->dropColumn("value_category");
        });
    }
}
