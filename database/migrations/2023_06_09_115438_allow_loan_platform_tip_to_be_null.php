<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AllowLoanPlatformTipToBeNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table
                ->decimal("platform_tip", 8, 2)
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table("loans", function (Blueprint $table) {
            $table->decimal("platform_tip", 8, 2)->default(0);
        });
    }
}
