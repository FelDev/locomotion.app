<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->string("noke_id")->nullable();
        });

        $nokeUsers = Noke::fetchUsers();

        foreach ($nokeUsers as $nokeUser) {
            // Noke's username is email address.
            $user = User::where("email", $nokeUser->username)->first();
            if ($user) {
                $user->noke_id = $nokeUser->id;
                $user->save();
            }
        }
    }

    public function down(): void
    {
        Schema::table("users", function (Blueprint $table) {
            $table->dropColumn("noke_id");
        });
    }
};
