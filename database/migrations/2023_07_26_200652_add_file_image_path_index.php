<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table("images", function (Blueprint $table) {
            $table->index("path");
        });

        Schema::table("files", function (Blueprint $table) {
            $table->index("path");
        });
    }

    public function down(): void
    {
        Schema::table("images", function (Blueprint $table) {
            $table->dropIndex("images_path_index");
        });

        Schema::table("files", function (Blueprint $table) {
            $table->dropIndex("files_path_index");
        });
    }
};
