<?php

namespace Database\Seeders;
use App\Models\Bike;
use App\Models\Loanable;
use Illuminate\Database\Seeder;

class BikesTableSeeder extends Seeder
{
    public function run()
    {
        // Start bikes at 1
        $bikes = [
            [
                "id" => 1,
                "type" => "bike",
                "name" => "Vélo Solon sans communauté",
                "position" => "45.54371 -73.627796",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonpetitepatrie@locomotion.app
                "owner_id" => 3,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 1,
                    "model" => "Vélo",
                    "bike_type" => "regular",
                    "size" => "big",
                ],
            ],
            [
                "id" => 2,
                "type" => "bike",
                "name" => "Vélo Solon Ahuntsic",
                "position" => "45.562652 -73.653695",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonahuntsic@locomotion.app
                "owner_id" => 2,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 2,
                    "model" => "Vélo",
                    "bike_type" => "regular",
                    "size" => "big",
                ],
            ],
            [
                "id" => 3,
                "type" => "bike",
                "name" => "Vélo Solon Petite-Patrie",
                "position" => "45.540 -73.600",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "always",
                // solonpetitepatrie@locomotion.app
                "owner_id" => 3,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 3,
                    "model" => "Vélo",
                    "bike_type" => "regular",
                    "size" => "big",
                ],
            ],
            [
                "id" => 101,
                "type" => "bike",
                "name" => "Vélo de Propriétaire Petite-Patrie sur demande",
                "position" => "45.535 -73.595",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available":true,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                // proprietairepetitepatrie@locomotion.app
                "owner_id" => 6,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => false,
                "details" => [
                    "id" => 101,
                    "model" => "Vélo",
                    "bike_type" => "regular",
                    "size" => "big",
                ],
            ],
            [
                "id" => 102,
                "type" => "bike",
                "name" => "Vélo de Propriétaire Petite-Patrie en libre service",
                "position" => "45.540 -73.595",
                "location_description" => "",
                "comments" => "",
                "instructions" => "",
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
  {
    "available":true,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
                ,
                // proprietairepetitepatrie@locomotion.app
                "owner_id" => 6,
                "created_at" => "2020-05-01 13:57:14",
                "is_self_service" => true,
                "details" => [
                    "id" => 102,
                    "model" => "Vélo",
                    "bike_type" => "regular",
                    "size" => "big",
                ],
            ],
        ];

        foreach ($bikes as $loanable) {
            $bike = $loanable["details"];
            unset($loanable["details"]);
            Loanable::create($loanable);
            Bike::create($bike);
        }

        \DB::statement(
            "SELECT setval('loanables_id_seq'::regclass, (SELECT MAX(id) FROM loanables) + 1)"
        );
    }
}
