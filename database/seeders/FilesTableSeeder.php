<?php

namespace Database\Seeders;
use App\Models\File;
use App\Models\Pivots\CommunityUser;
use Illuminate\Database\Seeder;

class FilesTableSeeder extends Seeder
{
    public function run()
    {
        $files = [
            [
                "id" => 1,
                "fileable_type" => "user",
                "fileable_id" => 6, // Emprunteur Petite Patrie
                "path" => "/seeds/1",
                "filename" => "preuve_de_résidence.png",
                "original_filename" => "preuve de résidence.png",
                "field" => "residency_proof",
                "filesize" => 3817,
            ],
        ];

        foreach ($files as $file) {
            if (!File::where("id", $file["id"])->exists()) {
                File::create($file);
            } else {
                File::where("id", $file["id"])->update($file);
            }
        }

        \DB::statement(
            "SELECT setval('files_id_seq'::regclass, (SELECT MAX(id) FROM images) + 1)"
        );
    }
}
