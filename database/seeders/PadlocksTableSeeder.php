<?php

namespace Database\Seeders;
use App\Models\Padlock;
use Illuminate\Database\Seeder;

class PadlocksTableSeeder extends Seeder
{
    public function run()
    {
        $nUnaffectedPadlocks = 2;

        // Bikes
        Padlock::factory()->create([
            "loanable_id" => 1,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 2,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 3,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 102,
        ]);

        // Trailers
        Padlock::factory()->create([
            "loanable_id" => 2001,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 2002,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 2003,
        ]);
        Padlock::factory()->create([
            "loanable_id" => 2102,
        ]);

        for ($p = 0; $p < $nUnaffectedPadlocks; ++$p) {
            Padlock::factory()->create();
        }

        \DB::statement(
            "SELECT setval('padlocks_id_seq'::regclass, (SELECT MAX(id) FROM padlocks) + 1)"
        );
    }
}
