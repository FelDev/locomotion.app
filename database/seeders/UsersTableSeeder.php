<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    public const AHUNTSIC_COMMUNITY_ADMIN_USER_SEED_ID = 8;
    public const RPP_COMMUNITY_ADMIN_USER_SEED_ID = 9;

    public function run()
    {
        $solon = [
            "password" => "locomotion",
            "date_of_birth" => "1965-06-04",
            "address" => "6450 Christophe-Colomb, Montréal",
            "address_position" => [45.537573, -73.601417],
            "postal_code" => "H2S 2G7",
            "phone" => "555 555-5555",
            "accept_conditions" => true,
        ];

        $users = [
            "soutien@locomotion.app" => array_merge($solon, [
                "id" => 1,
                "role" => "admin",
                "name" => "Soutien Locomotion",
            ]),
            "solonahuntsic@locomotion.app" => array_merge($solon, [
                "id" => 2,
                "role" => "admin",
                "name" => "Solon",
                "last_name" => "Ahuntsic",
                "description" => "Propriétaire de la flotte dans Ahuntsic.",
            ]),
            "solonpetitepatrie@locomotion.app" => array_merge($solon, [
                "id" => 3,
                "role" => "admin",
                "name" => "Solon",
                "last_name" => "Petite-Patrie",
                "description" =>
                    "Propriétaire de la flotte dans La Petite-Patrie.",
            ]),
            "adminahuntsic@locomotion.app" => array_merge($solon, [
                "id" => self::AHUNTSIC_COMMUNITY_ADMIN_USER_SEED_ID,
                "name" => "Admin",
                "last_name" => "Ahuntsic",
                "description" => "Administrateur de communauté dans Ahuntsic.",
            ]),
            "proprietaireahuntsic@locomotion.app" => array_merge($solon, [
                "id" => 4,
                "name" => "Propriétaire",
                "last_name" => "Ahuntsic",
                "description" => "Salut tout le monde :)",
            ]),
            "emprunteurahuntsic@locomotion.app" => array_merge($solon, [
                "id" => 5,
                "name" => "Emprunteur",
                "last_name" => "Ahuntsic",
            ]),
            "adminpetitepatrie@locomotion.app" => array_merge($solon, [
                "id" => self::RPP_COMMUNITY_ADMIN_USER_SEED_ID,
                "name" => "Admin",
                "last_name" => "Petite-Patrie",
                "description" =>
                    "Administrateur de communauté dans La Petite-Patrie.",
            ]),
            "proprietairepetitepatrie@locomotion.app" => array_merge($solon, [
                "id" => 6,
                "name" => "Propriétaire",
                "last_name" => "Petite-Patrie",
                "description" => "Salut tout le monde :)",
            ]),
            "emprunteurpetitepatrie@locomotion.app" => array_merge($solon, [
                "id" => 7,
                "name" => "Emprunteur",
                "last_name" => "Petite-Patrie",
                "description" => "Salut tout le monde :)",
            ]),
        ];

        // Community memberships
        $memberships = [
            "soutien@locomotion.app" => [],
            "solonahuntsic@locomotion.app" => [
                CommunitiesTableSeeder::AHUNTSIC_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
            "solonpetitepatrie@locomotion.app" => [
                CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
            "adminahuntsic@locomotion.app" => [
                CommunitiesTableSeeder::AHUNTSIC_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                    "role" => "admin",
                ],
            ],
            "proprietaireahuntsic@locomotion.app" => [
                CommunitiesTableSeeder::AHUNTSIC_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
            "emprunteurahuntsic@locomotion.app" => [
                CommunitiesTableSeeder::AHUNTSIC_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
            "adminpetitepatrie@locomotion.app" => [
                CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                    "role" => "admin",
                ],
            ],
            "proprietairepetitepatrie@locomotion.app" => [
                CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
            "emprunteurpetitepatrie@locomotion.app" => [
                CommunitiesTableSeeder::RPP_COMMUNITY_SEED_ID => [
                    "approved_at" => new \DateTime(),
                ],
            ],
        ];

        foreach ($users as $email => $data) {
            $data = array_merge($data, [
                "email" => $email,
                "password" => Hash::make(
                    array_get($data, "password", "password")
                ),
            ]);

            if (!User::where("email", $email)->exists()) {
                User::create($data);
            } else {
                User::where("email", $email)->update($data);
            }
        }

        foreach ($memberships as $email => $communities) {
            $user = User::where("email", $email)->first();

            User::withoutEvents(function () use ($user, $communities) {
                $user->communities()->sync($communities);
            });
        }

        \DB::statement(
            "SELECT setval('users_id_seq'::regclass, (SELECT MAX(id) FROM users) + 1)"
        );
    }
}
