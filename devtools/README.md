## Outils de développement

Ce répertoire contient quelques outils pour simplifier le développement.

### Linux: locomotion.bashrc

Une liste d'alias a ajouter à vôtre terminal. Pour les installer, ajoutez la ligne suivante à votre [configuration de _shell_](https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html) (`.bashrc`, `.zshrc`).

```bash
source path/to/project/devtools/locomotion.bashrc
```

**Alias**

Consultez le fichier `locomotion.bashrc` pour avoir la liste complète.

Quelques examples:

-   `dcep [commande]` : exécute la [commande] dans le conteneur docker avec le serveur php.
-   `locoseed` : _seed_ la base de données avec des données par défaut.
-   `locomigr` : exécute les migrations de la base de donnée.
-   `locotest` : exécute les tests unitaires et d'intégration
-   `locopretty` : exécute prettier sur tous les fichiers

### Windows: locomotion.ps1

Prérequis: Powershell 7+.

Vous pouvez ajouter un alias à ce script dans votre [$PROFILE](https://devblogs.microsoft.com/scripting/powertip-find-all-powershell-profile-locations/). Exemple:

```
function Loco-Dev {
    cd "C:\Users\YourName\src\gitlab\Solon-Collectif\locomotion.app"
    devtools\locomotion.ps1 @args
}
Set-Alias loco Loco-Dev
```

Vous pouvez afficher la liste des commandes en l'appelant:

```
C:\Users\YourName> loco
Usage: devtools\locomoation.ps1 <commande>

Commandes disponibles:
  up:                 Pars tous les composants de dev (back, front, bd).
  back:test:          Éxécute les tests unitaire et intégration PHP.
  back:test:cover:    Éxécute les tests unitaire et intégration PHP avec résultats de couverture.
  back:test:detail:   Éxécute les tests unitaire et intégration PHP avec affichage détaillé.
  back:pretty:        Éxécute prettier sur tous les fichiers.
  front:test:         Éxécute les tests unitaires Jest.
  front:test:cover:   Éxécute les tests unitaire Jest avec résultats de couverture.
  db:migrate:         Éxécute les migrations de la base de donnée de dev.
  db:test:migrate:    Éxécute les migrations de la base de donnée de test.
  db:seed:            Initialise la base de données avec des données par défaut.
  db:wipe:            Ré-initialise la base de données.
  git:                Ouvre la page de locomotion.app sur Gitlab.
  git:issues:         Ouvre la page des issues sur Gitlab.
  git:mrs:            Ouvre la page des merge requests sur Gitlab.
  git:tags:           Ouvre la page des tags sur Gitlab.
  git:new-issue:      Crée une nouvelle issue sur Gitlab.
```
