Param
(
    [parameter(mandatory=$false, position=0)][string]$cmd,
    [parameter(mandatory=$false, position=1, ValueFromRemainingArguments=$true)]$remaining
)

Set-Variable -Name isPowerShell7OrMore ((Get-Host).Version.Major -ge 7)
if (!$isPowerShell7OrMore) {
    echo "You must have PowerShell v7+ to use this script. Install at https://docs.microsoft.com/fr-ca/powershell/scripting/install/installing-powershell-core-on-windows."
    exit
}

function Migrate-Backend-Test-Db() {
    docker-compose exec -e DB_DATABASE=locomotion_test -e XDEBUG_MODE=off php php artisan migrate
}

switch($cmd) {
    "up" {
        docker-compose up --build
    }
    "back:test" {
        docker-compose exec -e XDEBUG_MODE=off php php artisan test --parallel @remaining
    }
    "back:test:cover" {
        Migrate-Backend-Test-Db && docker-compose exec -e DB_DATABASE=locomotion_test -e XDBEBUG_MODE=coverage php ./vendor/bin/phpunit -dmemory_limit=-1 --coverage-html=coverage-html
    }
    "back:test:detail" {
        docker-compose exec -e DB_DATABASE=locomotion_test php php artisan test @remaining
    }
    "back:pretty" {
        docker-compose exec php npx prettier --write .
    }
    "front:test" {
        docker-compose exec vue npm run test @remaining
    }
    "front:test:cover" {
        docker-compose exec vue npm run test -- --coverage --collectCoverageFrom="./src/**"
    }
    "db:migrate" {
        docker-compose exec -e XDEBUG_MODE=off php php artisan migrate
    }
    "db:test:migrate" {
        Migrate-Backend-Test-Db
    }
    "db:seed" {
        docker-compose exec -e XDEBUG_MODE=off php php artisan migrate:fresh --seed
    }
    "db:wipe" {
        docker-compose exec -e XDEBUG_MODE=off php php artisan db:wipe
    }
    "git" {
        Start-Process https://gitlab.com/solon-collectif/locomotion.app/
    }
    "git:issues" {
        Start-Process https://gitlab.com/solon-collectif/locomotion.app/-/issues
    }
    "git:mrs" {
        Start-Process https://gitlab.com/solon-collectif/locomotion.app/-/merge_requests
    }
    "git:tags" {
        Start-Process https://gitlab.com/solon-collectif/locomotion.app/-/tags
    }
    "git:new-issue" {
        Start-Process https://gitlab.com/solon-collectif/locomotion.app/-/issues/new
    }
    default {
        echo "Usage: devtools\locomoation.ps1 <commande>"
        echo ""
        echo "Commandes disponibles:"
        echo "  up:                 Pars tous les composants de dev (back, front, bd)."
        echo "  back:test:          Éxécute les tests unitaire et intégration PHP."
        echo "  back:test:cover:    Éxécute les tests unitaire et intégration PHP avec résultats de couverture."
        echo "  back:test:detail:   Éxécute les tests unitaire et intégration PHP avec affichage détaillé."
        echo "  back:pretty:        Éxécute prettier sur tous les fichiers."
        echo "  front:test:         Éxécute les tests unitaires Jest."
        echo "  front:test:cover:   Éxécute les tests unitaire Jest avec résultats de couverture."
        echo "  db:migrate:         Éxécute les migrations de la base de donnée de dev."
        echo "  db:test:migrate:    Éxécute les migrations de la base de donnée de test."
        echo "  db:seed:            Initialise la base de données avec des données par défaut."
        echo "  db:wipe:            Ré-initialise la base de données."
        echo "  git:                Ouvre la page de locomotion.app sur Gitlab."
        echo "  git:issues:         Ouvre la page des issues sur Gitlab."
        echo "  git:mrs:            Ouvre la page des merge requests sur Gitlab."
        echo "  git:tags:           Ouvre la page des tags sur Gitlab."
        echo "  git:new-issue:      Crée une nouvelle issue sur Gitlab."
    }
}
