import { mount } from "@vue/test-utils";
import PaginatedTable, { FieldDef } from "@/components/shared/PaginatedTable.vue";
import AdminPagination from "@/components/Admin/Pagination.vue";
import store from "@/store";
import { filters } from "../../helpers";
import BootstrapVue, { BootstrapVueIcons } from "bootstrap-vue";
import axios from "axios";
import VueAxios from "vue-axios";
import Vue from "vue";
import FilterInput from "@/components/shared/FilterInput.vue";
import dayjs from "@/helpers/dayjs";

Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);
Vue.use(VueAxios, axios);
const clock = Vue.observable({ second: dayjs() });
Object.defineProperties(Vue.prototype, {
  $dayjs: {
    get() {
      return dayjs;
    },
  },
  $second: {
    get() {
      return clock.second;
    },
    set(value) {
      clock.second = value;
    },
  },
});
Vue.dayjs = dayjs;
Object.keys(filters).forEach((f) => Vue.filter(f, filters[f]));

class MockIntersectionObserver {
  static observers = [];

  constructor(callback) {
    this.callback = callback;
  }

  observe() {
    MockIntersectionObserver.observers.push(this);
  }
  unobserve() {
    MockIntersectionObserver.observers = MockIntersectionObserver.observers.filter(
      (o) => o !== this
    );
  }

  static trigger() {
    MockIntersectionObserver.observers.forEach((observers) => {
      observers.callback([{ isIntersecting: true }]);
    });
  }
}
describe("PaginatedTable", () => {
  window.IntersectionObserver = MockIntersectionObserver;

  let wrapper;

  beforeEach(() => {
    store.commit("user", { role: "admin" });
    // Clear store
    store.commit("paginatedTables/updateListState", { listId: "user-table" });

    wrapper = mount(PaginatedTable, {
      store,
      mocks: { $t: (msg) => msg },
      propsData: {
        id: "user-table",
        endpoint: "users",
        fieldDefs: [
          new FieldDef("id", "ID", "id"),
          new FieldDef("displayed_field", "Displayed field"),
          new FieldDef("hidden_field", "Hidden field", "text", { show: false }),
          new FieldDef("no_filter", "No filter field", "text", { show: false, filter: false }),
          new FieldDef("date_field", "Date field", "date", { show: false }),
          new FieldDef("number_field", "Number field", "number", { show: false }),
          new FieldDef("unsortable_field", "Unsortable field", "text", { sortable: false }),
        ],
      },
    });
  });

  it("should not be initially loaded", () => {
    expect(wrapper.find("table").exists()).toBe(false);
  });

  describe("after being observed", () => {
    beforeEach(() => {
      axios.get = jest.fn(async () => {
        return { data: { data: [{ id: 2 }, { id: 1 }], total: 2 } };
      });
      window.IntersectionObserver.trigger();
    });

    it("should fetch data and render a table", () => {
      expect(axios.get).toHaveBeenCalledWith("users", {
        params: {
          fields:
            "id,displayed_field,hidden_field,no_filter,date_field,number_field,unsortable_field",
          order: "-id",
          page: 1,
          per_page: 10,
        },
      });
      expect(wrapper.find("table").exists()).toBe(true);
    });

    it("should use labels for all visible column headers", () => {
      const colHeaders = wrapper.findAll("table th");
      expect(colHeaders.length).toBe(4);
      expect(colHeaders.at(0).text()).toBe("ID");
      expect(colHeaders.at(1).text()).toBe("Displayed field");
      expect(colHeaders.at(2).text()).toBe("Unsortable field");
      expect(colHeaders.at(3).text()).toBe("Actions");
    });

    it("should have the right number of rows", async function () {
      await new Promise((resolve) => setTimeout(resolve, 50));
      expect(wrapper.findAll("table tbody tr").length).toBe(2);
    });

    it("should not display pagination with less than ten items", () => {
      expect(wrapper.findComponent(AdminPagination).exists()).toBe(false);
    });

    it("shows a Generate CSV button by default", () => {
      expect(wrapper.find(".generate-csv-button").exists()).toBe(true);
    });

    it("shows all the filterable fields when toggled", async () => {
      await wrapper.find(".toggle-filters-button").trigger("click");
      let filters = wrapper.findAllComponents(FilterInput);
      expect(filters.length).toBe(6);
      expect(filters.at(0).props()).toEqual({ name: "hidden_field", type: "text" });
      expect(filters.at(1).props()).toEqual({ name: "date_field", type: "date" });
      expect(filters.at(2).props()).toEqual({ name: "number_field", type: "number" });
      expect(filters.at(3).props()).toEqual({ name: "id", type: "id" });
      expect(filters.at(4).props()).toEqual({ name: "displayed_field", type: "text" });
      expect(filters.at(5).props()).toEqual({ name: "unsortable_field", type: "text" });
    });

    it("should call the api whenever a filter is changed after debouncing", async () => {
      await new Promise((resolve) => setTimeout(resolve, 50));
      await wrapper.find(".toggle-filters-button").trigger("click");
      await wrapper.findAll("th input").at(0).setValue(5);
      expect(axios.get).toHaveBeenCalledTimes(1);
      // we have to wait for the debounce
      await new Promise((resolve) => setTimeout(resolve, 500));
      expect(axios.get).toHaveBeenCalledTimes(2);
      expect(axios.get).toHaveBeenLastCalledWith("users", {
        params: {
          fields:
            "id,displayed_field,hidden_field,no_filter,date_field,number_field,unsortable_field",
          order: "-id",
          id: "5",
          page: 1,
          per_page: 10,
        },
      });
    });

    it("should only show sort arrows on sortable columns", () => {
      let sortableHeaders = wrapper.findAll(".b-table-sort-icon-left");
      expect(sortableHeaders.length).toBe(2);
      expect(sortableHeaders.at(0).text()).toBe("ID");
      expect(sortableHeaders.at(1).text()).toBe("Displayed field");
    });

    it("should call the api whenever a sort direction is changed", async () => {
      expect(axios.get).toHaveBeenCalledTimes(1);
      await wrapper.findAll(".b-table-sort-icon-left .header-label").at(1).trigger("click");
      await new Promise((resolve) => setTimeout(resolve, 20));
      expect(axios.get).toHaveBeenCalledTimes(2);
      expect(axios.get).toHaveBeenLastCalledWith("users", {
        params: {
          fields:
            "id,displayed_field,hidden_field,no_filter,date_field,number_field,unsortable_field",
          order: "displayed_field",
          page: 1,
          per_page: 10,
        },
      });
    });
  });

  describe("with more than 10 results", () => {
    beforeEach(() => {
      axios.get = jest.fn(async () => {
        return { data: { data: new Array(20), total: 20 } };
      });
      window.IntersectionObserver.trigger();
    });

    it("should display the pagination", async () => {
      expect(wrapper.findComponent(AdminPagination).exists()).toBe(true);
    });

    it("should call the api whenever the pagination is changed", async () => {
      await wrapper.findAll(".admin-pagination button").at(1).trigger("click");
      await new Promise((resolve) => setTimeout(resolve, 10));
      expect(axios.get).toHaveBeenLastCalledWith("users", {
        params: {
          fields:
            "id,displayed_field,hidden_field,no_filter,date_field,number_field,unsortable_field",
          order: "-id",
          page: 2,
          per_page: 10,
        },
      });
    });
  });
});
