export default {
  fr: {
    fund: "Approvisionner",
    claim: "Réclamer",
    claim_tooltip: "Un minimum de 10$ est requis pour réclamer son solde.",
    fund_popover: "Approvisionnez votre compte pour économiser sur les frais de transaction.",
  },
};
