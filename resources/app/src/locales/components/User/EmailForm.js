export default {
  fr: {
    current_password: "Mot de passe actuel",
    new_email: "Nouveau courriel",
    new_email_repeat: "Nouveau courriel (confirmation)",
    submit: "Mettre à jour",
  },
};
