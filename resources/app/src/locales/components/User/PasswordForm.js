export default {
  fr: {
    current_password: "Mot de passe actuel",
    new_password: "Nouveau mot de passe",
    new_password_length: "Minimum 8 caractères",
    new_password_repeat: "Nouveau mot de passe (confirmation)",
    submit: "Mettre à jour",
  },
};
