import AdminFilters from "@/locales/components/Admin/Filters";
import DashboardBalance from "@/locales/components/Dashboard/Balance";
import LoginBox from "@/locales/components/Login/Box";
import RegisterForm from "@/locales/components/Register/RegisterForm";
import UserEmailForm from "@/locales/components/User/EmailForm";
import UserPasswordForm from "@/locales/components/User/PasswordForm";
import Dashboard from "@/locales/views/Dashboard";
import PasswordRequest from "@/locales/views/Password/Request";
import PasswordReset from "@/locales/views/Password/Reset";
import AccountProfile from "@/locales/views/Profile/Account";
import bikes from "@/locales/models/bikes";
import borrowers from "@/locales/models/borrowers";
import cars from "@/locales/models/cars";
import communities from "@/locales/models/communities";
import faq from "@/locales/faq";
import invoices from "@/locales/models/invoices";
import loans from "@/locales/models/loans";
import loanables from "@/locales/models/loanables";
import padlocks from "@/locales/models/padlocks";
import paymentMethods from "@/locales/models/paymentMethods";
import trailers from "@/locales/models/trailers";
import users from "@/locales/models/users";
import Loan from "@/locales/views/Loan";

export default {
  bikes: bikes.fr,
  borrowers: borrowers.fr,
  cars: cars.fr,
  communities: communities.fr,
  components: {
    admin: {
      filters: AdminFilters.fr,
    },
    dashboard: {
      balance: DashboardBalance.fr,
    },
    login: {
      box: LoginBox.fr,
    },
    register: {
      register_form: RegisterForm.fr,
    },
    user: {
      email_form: UserEmailForm.fr,
      password_form: UserPasswordForm.fr,
    },
  },
  views: {
    dashboard: Dashboard.fr,
    profile: {
      account: AccountProfile.fr,
    },
    password: {
      request: PasswordRequest.fr,
      reset: PasswordReset.fr,
    },
    loan: Loan.fr,
  },
  faq: faq.fr,
  forms: {
    actions: "actions",
    view: "afficher",
    archive: "archiver",
    details: "détails",
    save: "enregistrer",
    optional: "facultatif",
    edit: "modifier",
    new: "nouveau",
    reset: "réinitialiser",
    restore: "restaurer",
    reinstate: "rétablir",
    remove: "retirer",
    delete: "supprimer",
  },
  invoices: invoices.fr,
  lists: {
    // Generic label for list ids.
    id: "ID",
  },
  loans: loans.fr,
  loanables: loanables.fr,
  locales: {
    en: "English",
    fr: "Français",
  },
  padlocks: padlocks.fr,
  paymentMethods: paymentMethods.fr,
  pricings: {
    types: {
      car: "voiture",
      bike: "vélo",
      trailer: "remorque",
      generic: "générique",
    },
  },
  profile: {
    titles: {
      account: "Mon compte",
      borrower: "Mon dossier de conduite",
      communities: "Mes voisinages",
      residency_proof: "Ma preuve de résidence",
      invoice: "Ma facture",
      invoices: "Mes factures",
      loans: "Historique d'emprunts",
      loanable: "Mon véhicule",
      loanables: "Mes véhicules",
      payment_method: "Mon mode de paiement",
      payment_methods: "Mes modes de paiement",
      profile: "Mon profil LocoMotion",
    },
  },
  titles: {
    account: "compte",
    admin: "administration",
    borrower: "dossier de conduite",
    communities: "voisinages",
    communities_overview: "voisinages",
    community: "voisinage",
    dashboard: "tableau de bord",
    faq: "foire aux questions",
    find_vehicle: "réserver un véhicule",
    insurance: "assurances Desjardins",
    invoice: "facture",
    invoices: "factures",
    loan: "emprunt",
    loans: "emprunts",
    loanable: "véhicule",
    loanables: "véhicules",
    login: "se connecter",
    padlock: "Cadenas",
    padlocks: "Cadenas",
    password: "Mot de passe",
    password_request: "Réinitialisation de mot de passe",
    password_reset: "Réinitialisation de mot de passe",
    payment_method: "mode de paiement",
    payment_methods: "modes de paiement",
    privacy: "conditions générales d'utilisation",
    profile: "profil",
    register: "s'inscrire",
    register_map: "rejoindre mes voisin-e-s",
    user: "membre",
    users: "membres",
  },
  trailers: trailers.fr,
  users: users.fr,
};
