import bikes from "./bikes";
import cars from "./cars";
import trailers from "./trailers";
import owners from "./owners";

export default {
  en: {
    fields: {},
  },
  fr: {
    new: {
      bike: "Nouveau vélo",
      car: "Nouvelle auto",
      trailer: "Nouvelle remorque",
      null: "Nouveau véhicule",
    },
    bike_types: {
      cargo: "cargo",
      electric: "electric",
      fixed_wheel: "roue fixe",
      regular: "régulier",
    },
    create_loanable: "créer un véhicule",
    selected_loanables:
      "aucun véhicule sélectionné | 1 véhicule sélectionné | {count} véhicules sélectionnés",
    descriptions: {
      comments: "Quoi savoir sur ce véhicule avant de faire l'emprunt? (vu par tout le monde)",
      image:
        "L'image de votre véhicule s'affichera dans un ratio d'aspect de 16 par 10. Assurez-vous qu'il est bien visible dans l'aperçu ici.",
      instructions:
        "Quoi savoir sur l'accès ou l'utilisation de ce véhicule? (vu par l'emprunteur seulement)",
      location_description:
        "Généralement, votre véhicule se trouve où? Cliquez sur la carte " +
        "pour définir sa position.",
      name:
        "Merci de nommer votre véhicule pour en informer votre voisinage. Le nom n'a pas " +
        "besoin d'être compliqué. Allez-y au plus simple... ou au plus drôle!",
      is_self_service:
        "Mon véhicule sera accessible en mode libre service, c'est-à-dire que" +
        " les demandes d'emprunts seront automatiquement acceptées.",
    },
    engines: cars.fr.engines,
    fields: {
      ...bikes.fr.fields,
      ...cars.fr.fields,
      ...trailers.fr.fields,
      comments: "information sur le véhicule",
      deleted_at: "supprimé",
      image: "photo du véhicule",
      instructions: "instructions",
      location_description: "précisions sur l'emplacement",
      name: "nom",
      owner: owners.fr.fields,
      owner_id: "propriétaire",
      padlock_id: "cadenas",
      position: "position géographique",
      type: "type de véhicule",
      types: {
        bike: "vélo",
        car: "auto",
        null: "n'importe quel type",
        trailer: "remorque",
      },
      is_deleted: "afficher les véhicules archivés",
      is_self_service: "Véhicule en libre service",
    },
    types_with_article: {
      bike: "le vélo",
      car: "l'auto",
      trailer: "la remorque",
    },
    model: {
      singular: "véhicule",
    },
    model_name: "véhicule | véhicules",
    papers_locations: {
      in_the_car: "dans l'auto",
      to_request_with_car: "à récupérer avec la clé",
    },
    placeholders: {
      comments:
        "ex.: SVP ne pas fumer dans mon auto. Merci beaucoup!\n" +
        "J'ai un siège pour bébé et un support pour les vélos en arrière.",
      instructions:
        "ex.: Indications pour récupérer les clés.\n" +
        "J'apprécierais que le siège de bébé soit replacé, s'il y a lieu.\n" +
        "Le code du cadenas pour accéder à la cour est le 1234.",
      location_description: "ex.: Généralement dans la ruelle, textez-moi pour plus de précisions.",
      name: "ex.: la LocoMobile bleue",
      padlock_id: "",
    },
    sizes: {
      big: "grand",
      medium: "moyen",
      small: "petit",
      kid: "enfant",
    },
    transmission_modes: cars.fr.transmission_modes,
    loanable: "véhicule | véhicule | véhicules",
    resource_people: "Personnes responsables",
  },
};
