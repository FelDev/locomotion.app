export default {
  fr: {
    welcome_text: "Bienvenue {name},",
    welcome_description: "Vous êtes {approvedUserCount} voisin-e-s à {community}.",
  },
  en: {
    welcome_text: "Welcome {name}!",
  },
};
