export default {
  fr: {
    self_service_free_after_use:
      "À {0} le {1}, {2} redeviendra automatiquement disponible pour les autres participant\u2011e\u2011s sur la plateforme\xa0: prolongez votre emprunt avec le bouton «\xa0Signaler un retard\xa0» pour le garder plus longtemps.",
  },
};
