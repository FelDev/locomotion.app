import { extractErrors } from "@/helpers";
import Vue from "vue";

export default {
  beforeRouteLeave(to, from, next) {
    this.$store.commit(`${this.slug}/cancelRequests`);
    this.$store.commit(`${this.slug}/item`, null);
    this.$store.commit(`${this.slug}/initialItem`, null);

    next();
  },
  beforeRouteEnter(to, from, next) {
    next((vm) => {
      vm.loadItem();
    });
  },
  beforeRouteUpdate(to, from, next) {
    // Handle navigating to the same page with a different ID
    if (to.params.id != from.params.id && from.params.id !== "new") {
      // vue-router doesn't seem to update the route props automatically.
      this.id = to.params.id;
      this.itemLoaded = false;
      this.loadItem();
      next();
    }

    next();
  },
  data() {
    return {
      itemLoaded: false,
    };
  },
  computed: {
    changed() {
      // shallow clone
      const initialItem = {
        ...this.initialItem,
      };
      const currentItem = {
        ...this.item,
      };

      // remove ignored fields set on the component using this mixin
      for (const ignoreField of this.ignoreFields ?? []) {
        initialItem[ignoreField] = null;
        currentItem[ignoreField] = null;
      }

      return JSON.stringify(initialItem) !== JSON.stringify(currentItem);
    },
    context() {
      return this.$store.state[this.slug];
    },
    error() {
      return this.context.error;
    },
    form() {
      return this.context.form || this.$route.meta.form;
    },
    initialItem() {
      return this.$store.getters[`${this.slug}/initialItem`];
    },
    initialItemJson() {
      return this.$store.getters[`${this.slug}/initialItemJson`];
    },
    item: {
      get() {
        return this.context.item;
      },
      set(item) {
        this.$store.commit(`${this.slug}/item`, item);
      },
    },
    loading() {
      return this.context.loading;
    },
    params() {
      return this.$route.meta.params;
    },
    parentPath() {
      const parentPathParts = this.$route.path.split("/").filter((p) => !!p);
      parentPathParts.pop();
      return `/${parentPathParts.join("/")}`;
    },
    slug() {
      return this.$route.meta.slug;
    },
  },
  methods: {
    async destroy() {
      await this.$store.dispatch("paymentMethods/destroy", this.item.id);
      this.onDestroy();
    },
    onDestroy() {
      this.$router.push(this.parentPath);
    },
    onCreated() {
      this.$router.replace(this.$route.fullPath.replace("new", this.item.id));
    },
    async loadItem() {
      const { dispatch } = this.$store;

      if (this.$route.name === "Login") {
        // Redirected to login page, do nothing
        return;
      }

      try {
        if (
          !this.skipLoadItem ||
          (typeof this.skipLoadItem === "function" && !this.skipLoadItem())
        ) {
          if (this.id === "new") {
            await dispatch(`${this.slug}/loadEmpty`);
          } else {
            await dispatch(`${this.slug}/retrieveOne`, {
              id: this.id,
              params: this.params,
            });
          }
        }
        this.itemLoaded = true;

        if (this.itemLoadedCallback && typeof this.itemLoadedCallback === "function") {
          await this.itemLoadedCallback();
        }
      } catch (e) {
        if (e.request) {
          switch (e.request.status) {
            case 404:
              this.$router.push(`/${this.parentPath}`);
              break;
            default:
              throw e;
          }
        }

        throw e;
      }
    },
    reset() {
      this.$store.commit(`${this.slug}/resetItem`);
    },
    async submit(handleErrors = true) {
      const savingMessage = this.savingToast;
      try {
        if (!this.item.id) {
          await this.$store.dispatch(`${this.slug}/createItem`, this.params);
          this.onCreated();
        } else {
          // remove ignored fields set on the component using this mixin
          await this.$store.dispatch(`${this.slug}/updateItem`, {
            params: this.params,
            fieldsToRemove: this.ignoreFields ?? [],
          });
        }

        if (typeof this.afterSubmit === "function") {
          this.afterSubmit();
        }

        this.$store.commit(
          "addNotification",
          savingMessage ?? {
            title: "Changements sauvegardés!",
            variant: "success",
          }
        );
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        if (e.request && handleErrors) {
          switch (e.request.status) {
            case 422:
              this.$store.commit("addNotification", {
                content: extractErrors(e.response.data).join(", "),
                title: "Erreur de sauvegarde",
                variant: "danger",
                type: "form",
              });
              break;
            default:
              throw e;
          }
        }

        throw e;
      }
    },
  },
  props: {
    id: {
      type: String,
      required: true,
    },
  },
  watch: {
    error(newError) {
      let variant;
      let title;
      let content;

      switch (newError.response.status) {
        case 400:
          content = "Erreur dans la requête.";
          title = "Mauvaise requête";
          variant = "warning";
          break;
        case 403:
          if (newError.response.data?.message) {
            content = newError.response.data.message;
            title = "Action interdite";
          } else {
            content = "Vous n'avez pas les permissions nécessaires pour effectuer cette action.";
            title = "Permissions insuffisantes";
          }
          variant = "warning";
          break;
        case 404:
          content = "La page que vous avez demandée est introuvable.";
          title = "Page introuvable";
          variant = "warning";
          break;
        case 500:
          content = "Une erreur système s'est produite.";
          title = "Erreur fatale";
          variant = "danger";
          break;
        default:
          return;
      }

      this.$store.commit("addNotification", {
        content,
        title,
        variant,
        type: this.slug,
      });
    },
    item: {
      deep: true,
      handler(newValue, oldValue) {
        if (oldValue === null && newValue) {
          if (this.$route.hash) {
            setTimeout(() => {
              const el = document.getElementById(this.$route.hash.substring(1));
              if (el) {
                this.$scrollTo(el);
              }
            }, 100);
          }
        }
        this.$store.commit(`${this.slug}/item`, this.item);
      },
    },
  },
};
