export default {
  computed: {
    hasActiveExtensions() {
      if (!this.item.id) {
        return false;
      }

      // an active extension has the status "in_process"
      return this.item.extensions.reduce(
        (acc, i) => acc || !i.id || i.status === "in_process",
        false
      );
    },
    hasActiveIncidents() {
      if (!this.item.id) {
        return false;
      }

      return this.item.incidents.reduce((acc, i) => acc || i.status !== "completed", false);
    },
    loanIsContested() {
      const { handover, takeover } = this.item;
      return (
        (handover && handover.status === "canceled") || (takeover && takeover.status === "canceled")
      );
    },
    loanIsCompleted() {
      return this.item.status === "completed";
    },
    loanableIsSelfService() {
      return this.item.loanable.is_self_service;
    },
    loanIsCanceled() {
      return this.item.status === "canceled";
    },
    userIsOwner() {
      if (!this.item.loanable.owner) {
        return false;
      }

      return this.user.id === this.item.loanable.owner.user.id;
    },
    userIsBorrower() {
      return this.user.id === this.item.borrower.user.id;
    },
    currentStep() {
      if (!this.item.id) {
        return "creation";
      }
      if (this.loanIsCompleted) {
        return "completed";
      }
      if (this.loanIsCanceled) {
        return "canceled";
      }
      if (this.hasActiveIncidents) {
        return "incident";
      }
      if (this.item.takeover?.status === "canceled") {
        return "takeover";
      }
      if (this.item.handover?.status === "canceled") {
        return "handover";
      }
      if (this.hasActiveExtensions) {
        return "extension";
      }

      const inProcessSteps = ["intention", "pre_payment", "takeover", "handover", "payment"].filter(
        (step) => this.item[step]?.status === "in_process"
      );

      return inProcessSteps[0] ?? "creation";
    },
  },
  methods: {
    addExtension() {
      const handover = this.item.handover;

      if (handover) {
        let newDuration = this.item.actual_duration_in_minutes + 15;
        const inTenMinutes = this.$dayjs().add(10, "minute");

        // if return is (almost) in the past, add 15 minutes to current time as new return time.
        if (inTenMinutes.isAfter(this.item.actual_return_at, "minute")) {
          newDuration = this.$dayjs()
            .add(15, "minute")
            .diff(this.$dayjs(this.item.departure_at), "minute");
        }

        this.item.extensions.push({
          status: "in_process",
          new_duration: newDuration,
          comments_on_extension: "",
          loan_id: this.item.id,
        });
      }

      setTimeout(() => {
        const el = document.getElementById("loan-extension-new");
        this.$scrollTo(el);
      }, 10);
    },
    addIncident(type) {
      this.item.incidents.push({
        status: "in_process",
        incident_type: type,
        loan_id: this.item.id,
      });

      setTimeout(() => {
        const el = document.getElementById("loan-incident-new");
        this.$scrollTo(el);
      }, 10);
    },
    async cancelLoan() {
      await this.$store.dispatch("loans/cancel", this.item.id);
      await this.loadItemAndUser();
    },
    async loadItemAndUser() {
      await Promise.all([
        this.loadItem(),
        this.$store.dispatch("loadUser"),
        this.$store.dispatch("dashboard/loadLoans"),
      ]);
    },
    hasReachedStep(step) {
      const { id, intention, pre_payment, takeover, handover, payment } = this.item;

      switch (step) {
        case "creation":
          return !!id;
        case "intention":
          return intention && !!intention.executed_at;
        case "pre_payment":
          return pre_payment && !!pre_payment.executed_at;
        case "takeover":
          return takeover && !!takeover.executed_at;
        case "handover":
          return handover && !!handover.executed_at;
        case "payment":
          return payment && !!payment.executed_at;
        default:
          return false;
      }
    },
    isCurrentStep(step) {
      return this.currentStep === step;
    },
    /*
      This method determines whether a loan step should be displayed.

      Visible steps should only depend on the loanable, who's involved in the
      loan and the loan itself.
      They should not depend on who the current user is.
    */
    displayStep(step) {
      switch (step) {
        case "intention":
          // Intention is required if loanable is not self-service
          // As of now, it is required even if the borrower is the owner.
          // This is likely to be reviewed.
          return !this.loanableIsSelfService;

        case "pre_payment":
          // Pre-payment should always be displayed. This is where we ask for financial contributions.
          return true;
        case "takeover":
          return !this.item.loanable.is_self_service;
        case "handover":
          return true;
        case "payment":
          return true;

        default:
          return false;
      }
    },
  },
};
