import Admin from "@/views/Admin.vue";
import AdminCommunities from "@/views/admin/Communities.vue";
import AdminCommunity from "@/views/admin/Community.vue";
import AdminDashboard from "@/views/admin/Dashboard.vue";
import AdminInvoice from "@/views/admin/Invoice.vue";
import AdminInvoices from "@/views/admin/Invoices.vue";
import AdminLoan from "@/views/admin/Loan.vue";
import AdminLoanable from "@/views/admin/Loanable.vue";
import AdminLoanables from "@/views/admin/Loanables.vue";
import AdminLoans from "@/views/admin/Loans.vue";
import AdminPadlock from "@/views/admin/Padlock.vue";
import AdminPadlocks from "@/views/admin/Padlocks.vue";
import AdminUser from "@/views/admin/User.vue";
import AdminUsers from "@/views/admin/Users.vue";

export default [
  {
    path: "/admin",
    component: Admin,
    meta: {
      auth: true,
      title: "titles.admin",
    },
    children: [
      {
        path: "dashboard",
        component: AdminDashboard,
        meta: {
          auth: true,
          title: "titles.dashboard",
        },
      },
      {
        path: "communities",
        component: AdminCommunities,
        meta: {
          auth: true,
          title: "titles.communities",
        },
      },
      {
        path: "communities/:id",
        component: AdminCommunity,
        props: true,
        meta: {
          auth: true,
          slug: "communities",
          params: {
            fields: ["*", "pricings.*", "allowed_loanable_types.*"].join(","),
            for: "edit",
          },
          data: {
            loanableTypes: {
              loadTypes: {},
            },
          },
        },
      },
      {
        path: "loanables",
        component: AdminLoanables,
        meta: {
          auth: true,
          title: "titles.loanables",
        },
      },
      {
        path: "loanables/:id",
        component: AdminLoanable,
        props: true,
        meta: {
          auth: true,
          slug: "loanables",
          params: {
            fields:
              "*,owner.user.full_name,owner.user.name,owner.user.last_name,owner.user.communities.name,owner.user.available_loanable_types," +
              "owner.user.communities.community.name," +
              "padlock.name,details.*,details.report.*,image.*," +
              "coowners.id,coowners.title,coowners.show_as_contact,coowners.user.full_name," +
              "coowners.user.id,coowners.user.avatar,coowners.user.phone",
            with_deleted: true,
          },
          title: "titles.loanable",
        },
      },
      {
        path: "users",
        component: AdminUsers,
        meta: {
          auth: true,
          title: "titles.users",
        },
      },
      {
        path: "users/:id",
        component: AdminUser,
        props: true,
        meta: {
          auth: true,
          slug: "users",
          params: {
            fields:
              "*,owner.*,borrower.*,borrower.gaa.*,borrower.insurance.*,borrower.saaq.*,avatar.*",
          },
          title: "titles.user",
        },
      },
      {
        path: "invoices",
        component: AdminInvoices,
        meta: {
          auth: true,
          title: "titles.invoices",
        },
      },
      {
        path: "invoices/:id",
        component: AdminInvoice,
        props: true,
        meta: {
          auth: true,
          slug: "invoices",
          params: {
            fields: "*,bill_items.*,user.*",
          },
          title: "titles.invoice",
          data: {
            users: {
              retrieveOne: {
                conditional({ route }) {
                  return !!route && !!route.query && !!route.query.user_id;
                },
                params: {
                  fields: "full_name,address,postal_code",
                },
                id({ route: { query } }) {
                  return query.user_id;
                },
              },
            },
          },
        },
      },
      {
        path: "loans",
        component: AdminLoans,
        meta: {
          auth: true,
          title: "titles.loans",
        },
      },
      {
        path: "loans/:id",
        component: AdminLoan,
        props: true,
        meta: {
          auth: true,
          slug: "loans",
          title: "titles.loan",
        },
      },
      {
        path: "padlocks",
        component: AdminPadlocks,
        meta: {
          auth: true,
          title: "titles.padlocks",
        },
      },
      {
        path: "padlocks/:id",
        component: AdminPadlock,
        props: true,
        meta: {
          auth: true,
          slug: "padlocks",
          params: {
            fields: "*,loanable.name",
          },
          title: "titles.loan",
        },
      },
    ],
  },
];
