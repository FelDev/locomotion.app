import Vue from "vue";
import merge from "deepmerge";
import { extractErrors } from "@/helpers";
import axios from "axios";

export default function RestModule(slug, initialState, actions = {}, mutations = {}) {
  return {
    namespaced: true,
    state: {
      cancelToken: null,
      data: [],
      deleted: null,
      empty: null,
      error: null,
      exportFields: ["id"],
      exportNotFields: [],
      form: null,
      // Used in forms to determine whether a field has changed or to reset it's content.
      initialItemJson: "",
      item: null,
      lastLoadedAt: null,
      lastPage: 1,
      loaded: false,
      loading: false,
      search: [],
      lastSearchQuery: "",
      slug,
      total: undefined,
      ...initialState,
    },
    getters: {
      initialItem(state) {
        return JSON.parse(state.initialItemJson);
      },
      initialItemJson(state) {
        return state.initialItemJson;
      },
    },
    mutations: {
      addData(state, data) {
        state.data.push(...data);
      },
      cancelRequests(state) {
        if (state.cancelToken) {
          state.cancelToken.cancel(`${state.slug} canceled`);
          state.cancelToken = null;
          state.loading = false;
        }
      },
      cancelToken(state, cancelToken) {
        if (cancelToken && state.cancelToken) {
          state.cancelToken.cancel(`${state.slug} canceled`);
        }
        state.cancelToken = cancelToken;
      },
      data(state, data) {
        state.data = data;
      },
      deleted(state, deleted) {
        state.deleted = deleted;
      },
      empty(state, empty) {
        state.empty = empty;
      },
      error(state, error) {
        state.error = error;
      },
      form(state, form) {
        state.form = form;
      },
      // SETTER : Sets the item as a whole
      item(state, item) {
        state.item = item;
      },
      initialItem(state, item) {
        state.initialItemJson = JSON.stringify(item);
      },
      lastLoadedAt(state, lastLoadedAt) {
        state.lastLoadedAt = lastLoadedAt;
      },
      lastPage(state, lastPage) {
        state.lastPage = lastPage;
      },
      loaded(state, loaded) {
        state.loaded = loaded;
      },
      loading(state, loading) {
        state.loading = loading;
      },
      // SETTER : Deep merges the partial in the item
      mergeItem(state, partial) {
        state.item = merge(state.item, partial);
      },
      // SETTER : Shallow merges the partial in the item
      patchItem(state, partial) {
        state.item = {
          ...state.item,
          ...partial,
        };
      },
      patchInitialItem(state, partial) {
        state.initialItemJson = JSON.stringify({
          ...JSON.parse(state.initialItemJson),
          ...partial,
        });
      },
      resetItem(state) {
        state.item = JSON.parse(state.initialItemJson);
      },
      search(state, search) {
        state.search = search;
      },
      total(state, total) {
        state.total = total;
      },
      ...mutations,
    },
    actions: {
      async sendRequest({ commit }, requestConfig) {
        const { CancelToken } = Vue.axios;
        const cancelToken = CancelToken.source();

        try {
          commit("cancelToken", cancelToken);
          const response = await axios({
            ...requestConfig,
            cancelToken: cancelToken.token,
          });

          commit("cancelToken", null);
          return response;
        } catch (e) {
          if (Vue.axios.isCancel(e)) {
            throw e;
          }
          commit("cancelToken", null);

          const { request, response } = e;
          commit("error", { request, response });

          if (request?.status === 422) {
            commit(
              "addNotification",
              {
                content: extractErrors(response.data).join(", "),
                title: "Erreur de validation",
                variant: "danger",
              },
              { root: true }
            );
          } else if (request?.status === 500) {
            commit(
              "addNotification",
              {
                content: "Une erreur système s'est produite.",
                title: "Erreur",
                variant: "danger",
              },
              { root: true }
            );
          }
          throw e;
        }
      },
      async loadEmpty({ commit, dispatch, state }) {
        commit("loaded", false);

        await dispatch("options");

        commit("item", JSON.parse(JSON.stringify(state.empty)));
        commit("initialItem", state.item);

        commit("loaded", true);
      },
      async search({ commit, state, dispatch }, { q, params }) {
        const {
          data: { data },
        } = await dispatch("sendRequest", {
          method: "get",
          url: `/${state.slug}`,
          params: {
            ...params,
            q,
          },
        });
        commit("search", data);
      },
      async createItem({ dispatch, state, commit }, params) {
        const data = state.item;
        commit("loaded", false);
        commit("loading", true);

        try {
          const { data: item } = await dispatch("sendRequest", {
            method: "post",
            url: `/${state.slug}`,
            data,
            params: {
              ...params,
            },
          });

          commit("item", item);
          commit("initialItem", item);
          commit("loading", false);
        } catch (e) {
          if (!Vue.axios.isCancel(e)) {
            commit("loading", false);
          }
          throw e;
        }
      },
      async options({ state, commit }) {
        if (state.form === null || state.empty === null) {
          const {
            data: { item: empty, form },
          } = await Vue.axios.options(`/${state.slug}`);

          commit("empty", empty);
          commit("form", form);
        }
      },
      async retrieveOne({ dispatch, commit, state }, { params, id }) {
        commit("loaded", false);
        commit("loading", true);

        try {
          await dispatch("options");

          const { data } = await dispatch("sendRequest", {
            method: "get",
            url: `/${state.slug}/${id}`,
            params: {
              ...params,
            },
          });

          commit("item", data);
          commit("initialItem", data);

          commit("loaded", true);
          commit("loading", false);
        } catch (e) {
          if (!Vue.axios.isCancel(e)) {
            commit("loading", false);
          }
          throw e;
        }
      },
      async retrieve({ dispatch, state, commit }, params) {
        commit("loaded", false);

        await dispatch("options");
        const {
          data: { data, total, last_page },
        } = await dispatch("sendRequest", {
          method: "get",
          url: `/${state.slug}`,
          params,
        });

        commit("data", data);
        commit("total", total);
        commit("lastPage", last_page);
        commit("lastLoadedAt", Date.now());

        commit("loaded", true);
      },
      async updateItem({ dispatch, state }, { params, fieldsToRemove }) {
        const toUpdate = state.item;
        if (fieldsToRemove) {
          for (const field of fieldsToRemove) {
            delete toUpdate[field];
          }
        }
        await dispatch("update", { id: state.item.id, data: toUpdate, params });
      },
      async update({ commit, state, dispatch }, { id, data, params }) {
        commit("loaded", false);
        commit("loading", true);

        try {
          const response = await dispatch("sendRequest", {
            method: "put",
            url: `/${state.slug}/${id}`,
            data,
            params: {
              ...params,
            },
          });

          commit("item", response.data);
          commit("initialItem", response.data);

          commit("loaded", true);
          commit("loading", false);
        } catch (e) {
          if (!Vue.axios.isCancel(e)) {
            commit("loading", false);
          }
          throw e;
        }
      },
      async destroy({ commit, state, dispatch }, id) {
        const { data } = await dispatch("sendRequest", {
          method: "delete",
          url: `/${state.slug}/${id}`,
        });

        commit("deleted", data);
      },
      async restore({ dispatch, state }, { id, params }) {
        await dispatch("sendRequest", {
          method: "put",
          url: `/${state.slug}/${id}/restore`,
          params,
        });
      },
      ...actions,
    },
  };
}
