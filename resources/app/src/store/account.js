import Vue from "vue";
import { extractErrors } from "../helpers";
import router from "@/router";

export default {
  namespaced: true,
  state: {
    data: null,
    error: null,
    loaded: false,
    transactionId: 1,
  },
  mutations: {
    data(state, data) {
      state.data = data;
    },
    error(state, error) {
      state.error = error;
    },
    transactionId(state, transactionId) {
      state.transactionId = transactionId;
    },
    loaded(state, loaded) {
      state.loaded = loaded;
    },
    cancelToken(state, cancelToken) {
      state.cancelToken = cancelToken;
    },
  },
  actions: {
    async buyCredit({ commit, dispatch, state }, { amount, paymentMethodId }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();
      commit("loaded", false);

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.put(
          "/auth/user/balance",
          {
            amount,
            payment_method_id: paymentMethodId,
            transaction_id: state.transactionId,
          },
          { cancelToken: cancelToken.token }
        );

        commit("data", data);
        commit("loaded", true);
        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        commit(
          "addNotification",
          {
            content: extractErrors(e.response.data).join(", "),
            title: "Problème lors de la transaction",
            variant: "danger",
            type: "extension",
          },
          { root: true }
        );
        commit("error", e.response.data);
        throw e;
      }
    },
    async claimCredit({ commit }) {
      commit("loaded", false);
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.put("/auth/user/claim", null, {
          cancelToken: cancelToken.token,
        });
        commit("data", data);

        commit("loaded", true);
        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        commit("error", e.response.data);

        throw e;
      }
    },
    async mandate({ commit, dispatch }, { mandatedUserId }) {
      const response = await Vue.axios(`/auth/password/mandate/${mandatedUserId}`);
      await commit("token", response.data, { root: true });
      await dispatch("loadUser", {}, { root: true });

      router.push("/app");
    },
  },
};
