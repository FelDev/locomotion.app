import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import merge from "deepmerge";

import account from "./account";
import passwordModule from "./password";
import stats from "./stats";

import bikes from "./models/bikes";
import borrowers from "./models/borrowers";
import cars from "./models/cars";
import communities from "./models/communities";
import files from "./models/files";
import images from "./models/images";
import invoiceItems from "./models/invoiceItems";
import invoices from "./models/invoices";
import loans from "./models/loans";
import loanables from "./models/loanables";
import loanableTypes from "./models/loanableTypes";
import padlocks from "./models/padlocks";
import paymentMethods from "./models/paymentMethods";
import trailers from "./models/trailers";
import owners from "./models/owners";
import users from "./models/users";
import communityUsers from "./models/communityUsers";

import adminDashboard from "./pages/admin/dashboard";
import CommunityMap from "./pages/community/map";
import LoanableSearch from "./pages/loanable/search";
import Dashboard from "./pages/dashboard";
import Login from "./pages/login";
import Register from "./pages/register";
import RegisterIntent from "./pages/register/intent";
import paginatedTables from "./paginatedTables";

Vue.use(Vuex);

/*
  Store is divided into modules:
    https://vuex.vuejs.org/guide/modules.html

  Root state is described in initialState below.
*/
const modules = {
  // General modules
  account,
  password: passwordModule,
  stats,

  // Rest modules: containing data for each entity.
  bikes,
  borrowers,
  cars,
  communities,
  communityUsers,
  files,
  images,
  invoices,
  invoiceItems,
  loans,
  loanables,
  loanableTypes,
  owners,
  padlocks,
  paymentMethods,
  trailers,
  users,

  // Page modules
  "community.map": CommunityMap,
  "loanable.search": LoanableSearch,
  dashboard: Dashboard,
  login: Login,
  register: Register,
  "register.intent": RegisterIntent,
  adminDashboard,
  paginatedTables: paginatedTables,
};

const getInitialState = () => ({
  // Initial root state.
  // Initial state of modules is set in each module individually.
  loaded: false,
  loading: false,
  loansLoaded: false,
  loanablesLoaded: false,
  hasMoreLoanables: false,
  notifications: [],
  user: null,
  token: null,
  refreshToken: null,
  seenVersions: [],
});

const loadUserFields = [
  "*",
  "avatar.*",
  "residency_proof.*",
  "identity_proof.*",
  "borrower.*",
  "communities.*",
  "owner.*",
  "payment_methods.*",
].join(",");

const actions = {
  async loadUser({ commit, dispatch, state }) {
    commit("loading", true);

    try {
      const { data: user } = await Vue.axios.get("/auth/user", {
        params: {
          fields: loadUserFields,
        },
      });

      const newUser = {
        ...user,
        loanables: state.user?.loanables ?? [],
        loans: state.user?.loans ?? [],
      };

      commit("user", newUser);
      commit("account/transactionId", user.transaction_id + 1);
    } catch (e) {
      commit("loading", false);
      if (e.request?.status === 401) {
        dispatch("logout");
      }
      throw e;
    }

    commit("loaded", true);
    commit("loading", false);
  },
  async login({ commit, dispatch, state }, { email, password }) {
    try {
      commit("loading", true);

      const { data } = await Vue.axios.post("/auth/login", {
        email,
        password,
        rememberMe: state.login.rememberMe,
      });

      commit("token", data.access_token);
      commit("refreshToken", data.refresh_token);

      if (!state.login.rememberMe) {
        commit("login/email", "");
      }
    } catch (e) {
      commit("loading", false);
      throw e;
    }

    await dispatch("loadUser");
  },
  async register({ commit, dispatch, state }, { email, password }) {
    try {
      commit("loading", true);
      const { data } = await Vue.axios.post("/auth/register", {
        email,
        password,
      });

      commit("token", data.access_token);
      commit("refreshToken", data.refresh_token);
    } catch (e) {
      commit("loading", false);
      throw e;
    }

    if (!state.login.rememberMe) {
      commit("login/email", "");
    }

    await dispatch("loadUser");
  },
  logout({ commit }) {
    commit("reset");
    commit("dashboard/reset");
  },
};

const mutations = {
  addNotification(state, notification) {
    state.notifications.push(notification);
  },
  removeNotification(state, notification) {
    const index = state.notifications.indexOf(notification);

    if (index > -1) {
      state.notifications.splice(index, 1);
    }
  },
  loaded(state, value) {
    state.loaded = value;
  },
  loading(state, value) {
    state.loading = value;
  },
  mergeUser(state, user) {
    state.user = merge(state.user, user);
  },
  refreshToken(state, refreshToken) {
    state.refreshToken = refreshToken;
  },
  seeVersion(state, version) {
    state.seenVersions.push(version);
  },
  unseeVersion(state, version) {
    const index = state.seenVersions.indexOf(version);
    if (index > -1) {
      state.seenVersions.splice(index, 1);
    }
  },
  token(state, token) {
    state.token = token;
  },
  user(state, user) {
    state.user = user;
  },
  reset(state) {
    Object.assign(state, getInitialState());
  },
};

const vuexPersist = new VuexPersist({
  key: "locomotion",
  storage: window.localStorage,
  /*
     State reducer. Reduces state to only those values you want to save.
     By default, saves entire state.
     https://github.com/championswimmer/vuex-persist#constructor-parameters--
   */
  reducer: (state) => ({
    // Root state
    user: state.user,
    token: state.token,
    refreshToken: state.refreshToken,
    seenVersions: state.seenVersions,

    // General modules
    stats: state.stats,

    // Rest modules
    loans: {
      item: state.loans.item,
    },

    // Page modules
    "loanable.search": {
      ...state["loanable.search"],
      center: null,
    },
    login: {
      email: state.login.email,
      rememberMe: state.login.rememberMe,
    },
    "register.intent": state["register.intent"],

    paginatedTables: state.paginatedTables,
  }),
});

export default new Vuex.Store({
  modules,
  state: getInitialState(),
  mutations,
  actions,
  plugins: [vuexPersist.plugin],
});
