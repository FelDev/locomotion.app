import Vue from "vue";

let actions = {
  async loadTypes({ commit }) {
    try {
      const { data } = await Vue.axios.get("/loanableTypes");
      commit("setTypes", data);
    } catch (e) {
      commit("finishLoading");
      throw e;
    }
  },
};
let mutations = {
  setTypes(state, types) {
    state.types = types;
    state.loaded = true;
  },
  startLoading(state) {
    state.loaded = false;
    state.loading = true;
  },
  finishLoading(state) {
    state.loading = false;
  },
};
export default {
  namespaced: true,
  state: {
    types: [],
    loaded: false,
    loading: false,
  },
  actions,
  mutations,
};
