import Vue from "vue";

import { extractErrors } from "@/helpers";

import RestModule from "../RestModule";

export default new RestModule(
  "loanables",
  {},
  {
    async disable({ commit, dispatch, state }, id) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data: deleted } = await Vue.axios.delete(`/${state.slug}/${id}`, {
          cancelToken: cancelToken.token,
        });

        commit("deleted", deleted);

        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);

        const { request, response } = e;
        commit("error", { request, response });

        throw e;
      }
    },
    reset({ commit, state }) {
      const newData = state.data.map((d) => ({
        ...d,
        available: null,
        insurance: null,
        price: null,
        pricing: null,
        tested: false,
      }));

      commit("data", newData);
    },
    async testAll({ commit, state }, { loan }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.get(`/loanables/search`, {
          params: { ...loan },
          cancelToken: cancelToken.token,
        });

        const availableLoanable = new Set(data);

        const newData = state.data.map((d) => {
          const isAvailable = availableLoanable.has(d.id);
          return {
            ...d,
            available: isAvailable,
            tested: true,
          };
        });

        commit("data", newData);
        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        const { request, response } = e;
        if (request) {
          switch (request.status) {
            case 422:
              commit(
                "addNotification",
                {
                  content: extractErrors(response.data).join(", "),
                  title: "Erreur de validation",
                  variant: "danger",
                  type: "extension",
                },
                { root: true }
              );
              return;
            default:
              break;
          }
        }
        commit("error", { request, response });
        commit("cancelToken", null);

        throw e;
      }
    },
    async list({ commit, state }, { types }) {
      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();
      commit("loaded", false);
      commit("loading", true);
      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.get(`/loanables/list`, {
          params: { types },
          cancelToken: cancelToken.token,
        });

        commit("data", data);
        commit("loaded", true);
        commit("loading", false);
        commit("cancelToken", null);
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        const { request, response } = e;
        if (request) {
          switch (request.status) {
            case 422:
              commit(
                "addNotification",
                {
                  content: extractErrors(response.data).join(", "),
                  title: "Erreur de validation",
                  variant: "danger",
                  type: "extension",
                },
                { root: true }
              );
              return;
            default:
              break;
          }
        }
        commit("error", { request, response });
        commit("loading", false);
        commit("cancelToken", null);

        throw e;
      }
    },
    async addCoowner({ commit, state }, { loanable, user }) {
      try {
        const { data } = await Vue.axios.put(`/loanables/${loanable.id}/coowners`, {
          user_id: user.id,
        });
        const coowners = [
          ...(state.item.coowners ?? []),
          {
            ...data,
            loanable,
            user,
          },
        ];

        commit("patchItem", { coowners });
        commit("patchInitialItem", { coowners });
        commit(
          "addNotification",
          {
            title: "Modification réussie",
            content: `Le-a copropriétaire a bien été ajouté-e.`,
            variant: "success",
          },
          { root: true }
        );
      } catch (e) {
        commit(
          "addNotification",
          {
            content: extractErrors(e.response?.data).join(", "),
            title: "Erreur lors de l'ajout de co-propriétaire",
            variant: "danger",
          },
          { root: true }
        );
        throw e;
      }
    },
    async removeCoowner({ commit, state }, { coownerId }) {
      try {
        await Vue.axios.delete(`/coowners/${coownerId}`);
        const coowners = [...state.item.coowners.filter((c) => c.id !== coownerId)];
        commit("patchItem", { coowners });
        commit("patchInitialItem", { coowners });
        commit(
          "addNotification",
          {
            title: "Modification réussie",
            content: `Le-a copropriétaire a bien été retiré-e.`,
            variant: "success",
          },
          { root: true }
        );
      } catch (e) {
        commit(
          "addNotification",
          {
            content: extractErrors(e.response?.data).join(", "),
            title: "Erreur lors de la suppression de co-propriétaire",
            variant: "danger",
          },
          { root: true }
        );
        throw e;
      }
    },
  }
);
