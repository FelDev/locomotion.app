import Vue from "vue";

import RestModule from "../RestModule";

export default new RestModule(
  "loans",
  {
    evaluatingPrice: false,
    exportFields: [
      "id",
      "status",
      "departure_at",
      "duration_in_minutes",
      "estimated_distance",
      "actual_distance",
      "reason",
      "borrower.user.id",
      "borrower.user.name",
      "borrower.user.last_name",
      "loanable.id",
      "loanable.owner.user.id",
      "loanable.owner.user.name",
      "loanable.owner.user.last_name",
      "intention.status",
      "handover.purchases_amount",
      "final_price",
      "final_insurance",
      "final_platform_tip",
      "calendar_days",
      "canceled_at",
    ],
  },
  {
    async cancel({ dispatch }, loanId) {
      await dispatch("sendRequest", {
        method: "put",
        url: `/loans/${loanId}/cancel`,
      });
    },
    async resume({ dispatch }, { id, params }) {
      await dispatch("update", { id, data: { status: "in_process", canceled_at: null }, params });
    },
    async createAction({ dispatch }, action) {
      await dispatch("sendRequest", {
        method: "post",
        url: `/loans/${action.loan_id}/actions`,
        data: action,
      });
    },
    async test({ commit, state }, params) {
      const distance = state.item.handover?.mileage_end
        ? state.item.handover.mileage_end - state.item.takeover.mileage_beginning
        : state.item.estimated_distance;

      const mergedParams = {
        departure_at: state.item.departure_at,
        duration_in_minutes: state.item.duration_in_minutes,
        estimated_distance: distance,
        loanable_id: state.item.loanable?.id,
        ...params,
      };

      if (state.item.community?.id) {
        mergedParams.community_id = state.item.community.id;
      }

      if (state.item.id) {
        mergedParams.loan_id = state.item.id;
      }

      // Avoid sending invalid requests.
      if (mergedParams.estimated_distance < 0 || mergedParams.duration_in_minutes < 15) {
        return;
      }

      const { CancelToken } = Vue.axios;
      const cancelToken = CancelToken.source();
      commit("setEvaluatingPrice", true);

      try {
        commit("cancelToken", cancelToken);
        const { data } = await Vue.axios.get(`/loanables/${mergedParams.loanable_id}/test`, {
          params: mergedParams,
          cancelToken: cancelToken.token,
        });

        commit("mergeItem", {
          estimated_insurance: data.insurance,
          estimated_price: data.price,
          loanable: {
            ...data,
          },
        });

        commit("cancelToken", null);
        commit("setEvaluatingPrice", false);
        return data;
      } catch (e) {
        if (Vue.axios.isCancel(e)) {
          return;
        }
        commit("cancelToken", null);
        commit("setEvaluatingPrice", false);
        if (!e.message || e.message !== "loans canceled test") {
          throw e;
        }
      }
    },
    async isAvailable({ dispatch, commit }, loanId) {
      const response = await dispatch("sendRequest", {
        method: "get",
        url: `/loans/${loanId}/isavailable`,
        data: { loanId },
      });

      if (response) {
        commit("patchItem", {
          isAvailable: response.data.isAvailable,
        });
      }
    },
    async rejectExtension({ dispatch }, action) {
      await dispatch("sendRequest", {
        method: "put",
        url: `/loans/${action.loan_id}/extensions/${action.id}/reject`,
        data: action,
      });
    },
    async validate({ commit, dispatch }, { loan, user }) {
      const response = await dispatch("sendRequest", {
        method: "put",
        url: `/loans/${loan.id}/validate`,
      });

      if (response) {
        const { data } = response;

        if (user.id === loan.borrower.user.id) {
          commit("patchItem", {
            borrower_validated_at: data,
          });
        }
        if (user.id === loan.loanable.owner.id) {
          commit("patchItem", {
            owner_validated_at: data,
          });
        }
      }
    },
  },
  {
    setEvaluatingPrice(state, value) {
      state.evaluatingPrice = value;
    },
  }
);
