import Vue from "vue";
import dayjs from "@/helpers/dayjs";

const getInitialState = () => ({
  loans: {
    future: {
      loans: [],
      total: 0,
    },
    started: {
      loans: [],
      total: 0,
    },
    contested: {
      loans: [],
      total: 0,
    },
    waiting: {
      loans: [],
      total: 0,
    },
    completed: {
      loans: [],
      total: 0,
    },
    need_approval: {
      loans: [],
      total: 0,
    },
  },
  loanables: {
    owned: {
      loanables: [],
      total: 0,
    },
    coowned: {
      loanables: [],
      total: 0,
    },
  },
  loaded: false,
  loansLoaded: false,
  loansLoading: false,
  loanablesLoaded: false,
  loanablesLoading: false,
  hasMoreLoanables: false,
  loansLastLoadTime: null,
  loanablesLastLoadTime: null,
});

const maxLoanableCount = 5;

const actions = {
  async reload({ dispatch }) {
    dispatch("loadLoans");
    dispatch("loadLoanables");
  },
  async loadLoans({ state, commit }) {
    if (state.loansLoading) {
      return;
    }

    commit("loadLoans");
    try {
      const { data: loans } = await Vue.axios.get("/loans/dashboard");
      commit("loansLoaded", loans);
    } catch (e) {
      commit("errorLoadingLoans");
      commit(
        "addNotification",
        {
          title: "Erreur de chargement des emprunts",
          variant: "danger",
        },
        { root: true }
      );
      throw e;
    }
  },
  async loadLoanables({ state, commit }) {
    if (state.loanablesLoading) {
      return;
    }
    commit("loadLoanables");
    try {
      const { data: loanables } = await Vue.axios.get("/loanables/dashboard");
      commit("loanablesLoaded", loanables);
    } catch (e) {
      commit("errorLoadingLoanables");

      commit(
        "addNotification",
        {
          title: "Erreur de chargement des véhicules",
          variant: "danger",
        },
        { root: true }
      );

      throw e;
    }
  },
};

const mutations = {
  loadLoans(state) {
    state.loansLoaded = false;
    state.loansLoading = true;
  },
  loadLoanables(state) {
    state.loanablesLoading = true;
    state.loanablesLoaded = false;
  },
  loansLoaded(state, loans) {
    state.loans = loans;
    state.loansLoaded = true;
    state.loansLoading = false;
    state.loansLastLoadTime = dayjs();
  },
  loanablesLoaded(state, loanables) {
    state.loanablesLoaded = true;
    state.loanables = loanables;
    state.hasMoreLoanables = loanables.total > maxLoanableCount;
    state.loanablesLoading = false;
    state.loanablesLastLoadTime = dayjs();
  },
  errorLoadingLoans(state) {
    state.loansLoading = false;
  },
  errorLoadingLoanables(state) {
    state.loanablesLoading = false;
  },
  setLoanables(state, loanables) {
    state.loanables = loanables;
  },
  reset(state) {
    Object.assign(state, getInitialState());
  },
};

export default {
  namespaced: true,
  state: getInitialState(),
  actions,
  mutations,
};
