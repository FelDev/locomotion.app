import dayjs from "dayjs";

export default {
  namespaced: true,
  state: {
    center: null,
    selectedLoanableTypes: [],
    lastSearchDate: null,
    lastSearchDuration: null,
  },
  mutations: {
    center(state, center) {
      state.center = center;
    },
    selectedLoanableTypes(state, selectedLoanableTypes) {
      state.selectedLoanableTypes = selectedLoanableTypes;
    },
    lastSearchDate(state, date) {
      if (dayjs(date).isValid()) {
        state.lastSearchDate = date;
      } else {
        state.lastSearchDate = null;
      }
    },
    lastSearchDuration(state, duration) {
      if (duration && duration > 15) {
        state.lastSearchDuration = duration;
      } else {
        state.lastSearchDuration = null;
      }
    },
  },
};
