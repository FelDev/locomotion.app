<?php

return [
    "action" => [
        "must_be_ongoing" => "L'étape :action doit être en cours.",
        "must_be_completed" => "L'étape :action doit être complétée.",
        "must_be_after_departure" =>
            "L'étape :action ne peut pas être complétée avant l'heure du départ.",
    ],
    "borrower" => [
        "must_be_active" => "L'emprunteur ne doit pas être archivé.",
    ],
    "loan" => [
        "must_be_ongoing" => "L'emprunt doit être en cours.",
        "must_not_be_ongoing_with_cost" =>
            "L'emprunt payant ne doit pas être débuté.",
        "must_be_canceled" => "L'emprunt doit être annulé.",
        "must_be_contested" => "L'emprunt doit être contesté.",
        "must_not_have_open_incident" =>
            "L'emprunt ne doit pas déjà avoir un incident en cours.",
    ],
    "loanable" => [
        "must_be_active" => "Le véhicule ne doit pas être archivé.",
    ],
    "owner" => [
        "must_be_active" => "Le propriétaire ne doit pas être archivé.",
    ],
    "payment" => [
        "must_not_be_completed_with_funds" =>
            "L'emprunt ne doit pas avoir un paiement complété.",
        "borrower_cant_pay" =>
            "L'emprunteur-se n'a pas assez de fonds dans son solde pour payer présentement.",
        "loan_needs_validation" =>
            "L'emprunt doit être validé avant d'être payé.",
    ],
];
