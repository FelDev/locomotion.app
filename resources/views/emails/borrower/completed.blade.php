@extends('emails.layouts.main') @section('content')

<p
    style="
        text-align: center;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
        margin-bottom: 32px;
    "
>
    Bonjour {{ $user->name }},
</p>

<p
    style="
        text-align: center;
        margin: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Vous avez bien complété votre dossier de conduite. Un-e membre de l'équipe
    va l'examiner. Vous serez informé lorsque votre dossier de conduite sera
    approuvé.
</p>

@endsection
