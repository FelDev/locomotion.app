@extends('emails.layouts.main') @section('content')
<p
    style="
        text-align: center;
        margin: 0;
        font-weight: 390;
        font-size: 17px;
        line-height: 24px;
        color: #343a40;
    "
>
    Votre export de données est complété et
    <a href="{{$url}}" target="_blank"> est disponible ici</a>.
</p>

@endsection
