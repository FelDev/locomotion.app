@extends('emails.layouts.main')

@section('content')
Bonjour {{ $receiver->name }},

{{ $caller->name }} a contesté les données entrées lors de retour du
véhicule sur son emprunt de {{ $loan->loanable->name }} qui commençait
à {{ $loan->departure_at }}.

@if (!!$handover->comments_on_contestation)
"
{{ $handover->comments_on_contestation }}
"
@endif

Un membre de l'équipe LocoMotion a été notifié et sera appelé à arbitrer la résolution du
problème.

Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

        - L'équipe LocoMotion
@endsection
