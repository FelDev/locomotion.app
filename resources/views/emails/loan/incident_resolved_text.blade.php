@extends('emails.layouts.main_text')

@section('content')
    Bonjour {{ $recipient->name }},

    L'incident rapporté lors de l'emprunt de {{ $loan->loanable->name }} qui commençait à {{ $loan->departure_at }} a été résolu.

Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

            - L'équipe LocoMotion
@endsection
