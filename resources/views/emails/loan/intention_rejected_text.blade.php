@extends('emails.layouts.main_text')

@section('content')
Bonjour {{ $borrower->user->name }},

{{ $owner->user->name }} a refusé votre demande d'emprunt de {{ $loan->loanable->name }}
à partir de {{ $loan->departure_at }}.

{{ $intention->message_for_borrower }}

Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

            - L'équipe LocoMotion
@endsection
