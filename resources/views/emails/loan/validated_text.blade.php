@extends('emails.layouts.main_text') @section('content')
    Bonjour {{ $recipient->name }},

@if($isBorrower)
    {{ $validator->name }} a accepté les données de la fin de votre emprunt de
    {{$loan->loanable->name}}.

    Vous pouvez maintenant valider les données et payer {{
    $loan->loanable->owner->user->name }}. Si des données sont incorrectes, vous
    pouvez contester pour qu'un membre de l'équipe LocoMotion contacte les
    participant-e-s et ajuste les données.

    Si vous ne validez ni ne contestez les données de l'emprunt, celui-ci
    sera validé et payé automatiquement depuis votre solde 48 heures après la
    fin de la réservation.
@else
    {{ $validator->name }} a accepté les données à la fin de l'emprunt du
    véhicule {{ $loan->loanable->name }} et a authorisé le paiement.

    Vous pouvez maintenant accepter les fonds en validant les données. Si
    des données sont incorrectes, vous pouvez contester pour qu'un membre de
    l'équipe LocoMotion contacte les participant-e-s et ajuste les données.

    Si vous ne validez ni ne contestez les données de l'emprunt, celui-ci
    sera validé et payé automatiquement 48 heures après la fin de la
    réservation.
@endif

    Voir l'emprunt [{{ env('FRONTEND_URL') . '/loans/' . $loan->id }}]

@endsection
