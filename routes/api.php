<?php

use App\Http\Controllers\CoownerController;
use App\Http\Controllers\UserController;
use App\Http\RouteHelper;

Route::prefix("v1")->group(function () {
    Route::get("/", "StaticController@blank");
    Route::get("/status", "StaticController@status");
    Route::get("/stats", "StaticController@stats");

    Route::post("auth/login", "AuthController@login");
    Route::post("auth/register", "AuthController@register");
    Route::post("auth/password/request", "AuthController@passwordRequest");
    Route::post("auth/password/reset", "AuthController@passwordReset");
    Route::get(
        "auth/password/mandate/{userId}",
        "AuthController@mandate"
    )->middleware("auth:api");

    Route::middleware(["auth:api", "transaction"])->group(function () {
        Route::prefix("auth")->group(function () {
            Route::get("user", "AuthController@retrieveUser");
            Route::put("user", "AuthController@updateUser");
            Route::get("user/balance", "AuthController@getUserBalance");
            Route::put("user/balance", "AuthController@addToUserBalance");
            Route::put("user/claim", "AuthController@claimUserBalance");
            Route::put("logout", "AuthController@logout");
        });

        Route::post("acceptConditions", [
            UserController::class,
            "acceptConditions",
        ]);

        foreach (
            [
                "community",
                "file",
                "image",
                "invoice",
                "loan",
                "loanable",
                "padlock",
                "payment_method",
                "user",
            ]
            as $entity
        ) {
            RouteHelper::resource($entity);
        }

        Route::get("invoiceitems", "InvoiceItemController@index")->name(
            "invoiceitems.index"
        );

        Route::get("users/{me}", "UserController@retrieve")->where("me", "me");

        Route::put("users/send/{type}", "UserController@sendEmail");
        Route::put(
            "users/add_to_community/{communityId}",
            "UserController@addAllToCommunity"
        );

        RouteHelper::retrieve("borrower");
        RouteHelper::index("borrower");
        RouteHelper::retrieve("owner");
        RouteHelper::index("owner");

        Route::post("users/{user_id}/email", "UserController@updateEmail");
        Route::post(
            "users/{user_id}/password",
            "UserController@updatePassword"
        );
        Route::get(
            "users/{user_id}/communities",
            "UserController@indexCommunities"
        );
        Route::get(
            "users/{user_id}/communities/{community_id}",
            "UserController@retrieveCommunity"
        );
        Route::put(
            "users/{user_id}/communities/{community_id}",
            "UserController@createCommunityUser"
        );
        Route::delete(
            "users/{user_id}/communities/{community_id}",
            "UserController@deleteCommunityUser"
        );

        Route::get(
            "/users/{user_id}/borrower",
            "UserController@retrieveBorrower"
        );
        Route::put(
            "/users/{user_id}/borrower/submit",
            "UserController@submitBorrower"
        );
        Route::put(
            "/users/{user_id}/borrower/approve",
            "UserController@approveBorrower"
        );
        Route::put(
            "/users/{user_id}/borrower/suspend",
            "UserController@suspendBorrower"
        );
        Route::delete(
            "/users/{user_id}/borrower/suspend",
            "UserController@unsuspendBorrower"
        );
        Route::put(
            "/users/{user_id}/proof/refuse",
            "UserController@markProofInvalid"
        );

        Route::get("/communityUsers", "CommunityUserController@index");
        Route::put(
            "/communityUsers/{communityUser_id}",
            "CommunityUserController@updateCommunityUser"
        );

        RouteHelper::subresource("community", "user");

        Route::get(
            "/loans/{loan_id}/borrower",
            "LoanController@retrieveBorrower"
        );
        Route::put("/loans/{loan_id}/cancel", "LoanController@cancel");

        Route::post("/loans/{loan_id}/actions", "LoanController@createAction");
        Route::put(
            "/loans/{loan_id}/intention/complete",
            "IntentionController@complete"
        );
        Route::put(
            "/loans/{loan_id}/intention/cancel",
            "IntentionController@cancel"
        );
        Route::put(
            "/loans/{loan_id}/prepayment/complete",
            "PrePaymentController@complete"
        );
        Route::put(
            "/loans/{loan_id}/takeover/complete",
            "TakeoverController@complete"
        );
        Route::put(
            "/loans/{loan_id}/takeover/contest",
            "TakeoverController@contest"
        );
        Route::put(
            "/loans/{loan_id}/handover/complete",
            "HandoverController@complete"
        );
        Route::put(
            "/loans/{loan_id}/handover/contest",
            "HandoverController@contest"
        );
        Route::put(
            "/loans/{loan_id}/extensions/{extension_id}/complete",
            "ExtensionController@complete"
        );
        Route::put(
            "/loans/{loan_id}/extensions/{extension_id}/cancel",
            "ExtensionController@cancel"
        );
        Route::put(
            "/loans/{loan_id}/extensions/{extension_id}/reject",
            "ExtensionController@reject"
        );
        Route::put(
            "/loans/{loan_id}/incidents/{incident_id}/complete",
            "IncidentController@complete"
        );
        Route::put(
            "/loans/{loan_id}/payment/complete",
            "PaymentController@complete"
        );
        Route::get(
            "/loans/{loan_id}/isavailable",
            "LoanController@isAvailable"
        );
        Route::put(
            "/loans/{loan_id}/validate",
            "LoanController@validateInformation"
        );

        Route::put(
            "/pricings/{id}/evaluate",
            "PricingController@evaluate"
        )->name("pricings.evaluate");

        Route::get("/loanables/{id}/test", "LoanableController@test")->where(
            "id",
            "[0-9]+"
        );
        Route::get("/loanables/search", "LoanableController@search")->name(
            "loanables.search"
        );
        Route::get("/loanables/list", "LoanableController@list")->name(
            "loanables.list"
        );

        Route::get(
            "/loanables/{loanable_id}/availability",
            "LoanableController@availability"
        )->where("loanable_id", "[0-9]+");
        Route::get(
            "/loanables/{loanable_id}/events",
            "LoanableController@events"
        )->where("loanable_id", "[0-9]+");
        Route::get(
            "/loanables/{loanable_id}/loans/{loan_id}/next",
            "LoanableController@retrieveNextLoan"
        )
            ->where("loanable_id", "[0-9]+")
            ->where("loan_id", "[0-9]+");
        Route::get(
            "/loanables/{loanable_id}/loans/unavailable",
            "LoanableController@loansDuringUnavailabilities"
        )->where("loanable_id", "[0-9]+");

        Route::put("/loanables/{loanable_id}/coowners", [
            CoownerController::class,
            "create",
        ])->where("loanable_id", "[0-9]+");

        Route::get("/loanableTypes", "GetLoanableTypesController");

        Route::delete("/coowners/{coowner_id}", [
            CoownerController::class,
            "delete",
        ])->where("coowner_id", "[0-9]+");

        Route::put("/coowners/{coowner_id}", [
            CoownerController::class,
            "update",
        ])->where("coowner_id", "[0-9]+");

        RouteHelper::subresource("loanable", "loan", null, [
            "create",
            "update",
            "destroy",
            "restore",
        ]);

        Route::get("/loans/dashboard", "LoanController@dashboard");
        Route::get("/loanables/dashboard", "LoanableController@dashboard");

        Route::get("/batch/{batchId}", function (string $batchId) {
            return Bus::findBatch($batchId);
        })->can("view-batch-status");
    });

    Route::get(
        "/scheduler/noke/sync/locks/{appKey}",
        "SchedulerController@nokeSyncLocks"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/noke/sync/users/{appKey}",
        "SchedulerController@nokeSyncUsers"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/noke/sync/loans/{appKey}",
        "SchedulerController@nokeSyncLoans"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/actions/complete/{appKey}",
        "SchedulerController@actionsComplete"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/email/loan/upcoming/{appKey}",
        "SchedulerController@emailLoanUpcoming"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/email/loan/prepayement/{appKey}",
        "SchedulerController@emailPrePayement"
    )->where("appKey", ".*");

    Route::get(
        "/scheduler/files/list/missing/{appKey}",
        "SchedulerController@filesListMissing"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/files/clean/db/{appKey}",
        "SchedulerController@filesCleanDB"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/files/clean/storage/{appKey}",
        "SchedulerController@filesCleanStorage"
    )->where("appKey", ".*");
    Route::get(
        "/scheduler/files/update/location/{appKey}",
        "SchedulerController@filesUpdateLocation"
    )->where("appKey", ".*");

    Route::get("/{any?}", "StaticController@notFound")->where("any", ".*");
});
