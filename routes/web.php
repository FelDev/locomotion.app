<?php

Route::get("/auth/callback", "AuthController@callback");
Route::get("/auth/google", "AuthController@google");

Route::get("/password/reset", "StaticController@app")->name("password.reset");
Route::get("/status", "StaticController@status")->name("status");

// Email test routes
if (env("ENABLE_TEST_ROUTES", false)) {
    Route::get("/test/email/{name}", "TestEmailController@show")->where(
        "name",
        ".*"
    );
}

Route::get("/storage/{any?}", "StaticController@storage")
    ->where("any", ".*")
    ->name("app");

Route::get("/images/{any?}", "StaticController@images")
    ->where("any", ".*")
    ->middleware("auth.query");
Route::get("/files/{any?}", "StaticController@files")
    ->where("any", ".*")
    ->middleware("auth.query");
