<?php

namespace Tests\Integration;

use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

abstract class ActionTestCase extends TestCase
{
    protected function buildLoan($upTo = null): Loan
    {
        $community = Community::factory()->create();
        Pricing::factory()->create([
            "rule" => "0",
            "community_id" => $community->id,
            "object_type" => "App\Models\Car",
        ]);

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withCommunity($community)
            ->create();
        $owner = Owner::factory()->create([
            "user_id" => User::factory()->withCommunity($community),
        ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "always",
            ]);

        $loanFactory = Loan::factory([
            "borrower_id" => $borrowerUser->borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
        ]);

        return match ($upTo) {
            "intention" => $loanFactory->withInProcessIntention()->create(),
            "pre_payment" => $loanFactory->withInProcessPrePayment()->create(),
            "takeover" => $loanFactory->withInProcessTakeover()->create(),
            "handover" => $loanFactory->withInProcessHandover()->create(),
            default => $loanFactory->withInProcessPayment()->create(),
        };
    }
}
