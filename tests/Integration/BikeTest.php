<?php

namespace Tests\Integration;

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Payment;
use App\Models\PrePayment;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class BikeTest extends TestCase
{
    private static $getBikeResponseStructure = [
        "id",
        "name",
        "details" => ["bike_type", "model", "size"],
        "position",
        "location_description",
        "instructions",
        "comments",
    ];

    public function testBikeCreationValidation()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $validData = [
            "name" => $this->faker->name,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "owner_id" => $owner->id,
            "type" => "bike",
            "details" => [
                "model" => $this->faker->sentence,
                "bike_type" => $this->faker->randomElement([
                    "regular",
                    "electric",
                    "fixed_wheel",
                ]),
                "size" => $this->faker->randomElement([
                    "big",
                    "medium",
                    "small",
                    "kid",
                ]),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        $missingModelData = $validData;
        unset($missingModelData["details"]["model"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingModelData);
        $response->assertStatus(422);

        $missingType = $validData;
        unset($missingType["details"]["bike_type"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingType);
        $response->assertStatus(422);

        $invalidBikeTypeData = $validData;
        $invalidBikeTypeData["details"]["bike_type"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidBikeTypeData
        );
        $response->assertStatus(422);

        $missingSize = $validData;
        unset($missingSize["details"]["size"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingSize);
        $response->assertStatus(422);

        $invalidSizeData = $validData;
        $invalidSizeData["details"]["size"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $invalidSizeData);
        $response->assertStatus(422);
    }

    public function testCreateBikes()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $data = [
            "name" => $this->faker->name,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "owner_id" => $owner->id,
            "details" => [
                "model" => $this->faker->sentence,
                "bike_type" => $this->faker->randomElement([
                    "regular",
                    "electric",
                    "fixed_wheel",
                ]),
                "size" => $this->faker->randomElement([
                    "big",
                    "medium",
                    "small",
                    "kid",
                ]),
            ],
            "type" => "bike",
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getBikeResponseStructure);
    }

    public function testShowBikes()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $bike = Loanable::factory()
            ->withBike()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getBikeResponseStructure);
    }

    public function testUpdateBikes()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $bike = Loanable::factory()
            ->withBike()
            ->create(["owner_id" => $owner->id]);

        $data = [
            "name" => $this->faker->name,
            "details" => [
                "model" => "TERN",
            ],
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$bike->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteBikes()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $bike = Loanable::factory()
            ->withBike()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");
        $response->assertStatus(404);
    }

    public function testDeleteBikesWithActiveLoan()
    {
        // No active loan
        $loan = $this->buildLoan();
        $bike = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$bike->id");
        $response->assertStatus(404);

        // Prepaid (active) loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $bike = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        // Only completed loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $payment = Payment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $bike = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$bike->id");
        $response->assertStatus(200);
    }

    public function testListBikes()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $bikes = Loanable::factory(2)
            ->withBike()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", route("loanables.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getBikeResponseStructure
                )
            );
    }

    protected function buildLoan(): Loan
    {
        return Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => Loanable::factory()->withBike(),
            ]);
    }
}
