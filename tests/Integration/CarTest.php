<?php

namespace Tests\Integration;

use App\Models\Borrower;
use App\Models\Car;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Payment;
use App\Models\PrePayment;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use MStaack\LaravelPostgis\Geometries\Point;
use Tests\TestCase;

class CarTest extends TestCase
{
    private static $carResponseStructure = [
        "type",
        "comments",
        "instructions",
        "location_description",
        "name",
        "position",
        "details" => [
            "brand",
            "engine",
            "model",
            "has_informed_insurer",
            "insurer",
            "value_category",
            "papers_location",
            "plate_number",
            "transmission_mode",
            "year_of_circulation",
        ],
    ];

    public function testCarCreationValidation()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $validData = [
            "type" => "car",
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "location_description" => $this->faker->sentence,

            "name" => $this->faker->name,
            "owner_id" => $owner->id,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "details" => [
                "engine" => $this->faker->randomElement([
                    "fuel",
                    "diesel",
                    "electric",
                    "hybrid",
                ]),
                "brand" => $this->faker->word,
                "has_informed_insurer" => true,
                "insurer" => $this->faker->word,
                "value_category" => $this->faker->randomElement([
                    "lte50k",
                    "lte70k",
                    "lte100k",
                ]),
                "model" => $this->faker->sentence,
                "papers_location" => $this->faker->randomElement([
                    "in_the_car",
                    "to_request_with_car",
                ]),
                "plate_number" => $this->faker->shuffle("9F29J2"),
                "pricing_category" => "large",
                "transmission_mode" => $this->faker->randomElement([
                    "automatic",
                    "manual",
                ]),
                "year_of_circulation" => $this->faker->year($max = "now"),
            ],
        ];
        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        $missingBrand = $validData;
        unset($missingBrand["details"]["brand"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingBrand);
        $response->assertStatus(422);

        $missingEngine = $validData;
        unset($missingEngine["details"]["engine"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingEngine);
        $response->assertStatus(422);

        $wrongEngineData = $validData;
        $wrongEngineData["details"]["engine"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $wrongEngineData);
        $response->assertStatus(422);

        $missingInsurer = $validData;
        unset($missingInsurer["details"]["insurer"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingInsurer);
        $response->assertStatus(422);

        $missingModel = $validData;
        unset($missingModel["details"]["model"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingModel);
        $response->assertStatus(422);

        $missingPapersLocation = $validData;
        unset($missingPapersLocation["details"]["papers_location"]);
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $missingPapersLocation
        );
        $response->assertStatus(422);

        $invalidPapersLocation = $validData;
        $invalidPapersLocation["details"]["papers_location"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidPapersLocation
        );
        $response->assertStatus(422);

        $missingPlateNumber = $validData;
        unset($missingPlateNumber["details"]["plate_number"]);
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $missingPlateNumber
        );
        $response->assertStatus(422);

        $missingPricingCategory = $validData;
        unset($missingPricingCategory["details"]["pricing_category"]);
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $missingPricingCategory
        );
        $response->assertStatus(422);

        $invalidPricingCategory = $validData;
        $invalidPricingCategory["details"]["pricing_category"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidPricingCategory
        );
        $response->assertStatus(422);

        $missingTransmissionMode = $validData;
        unset($missingTransmissionMode["details"]["transmission_mode"]);
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $missingTransmissionMode
        );
        $response->assertStatus(422);

        $invalidTransmissionMode = $validData;
        $invalidTransmissionMode["details"]["transmission_mode"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidTransmissionMode
        );
        $response->assertStatus(422);

        $missingYearOfCirculation = $validData;
        unset($missingYearOfCirculation["details"]["year_of_circulation"]);
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $missingYearOfCirculation
        );
        $response->assertStatus(422);

        $invalidYearOfCirculation = $validData;
        $invalidYearOfCirculation["details"]["year_of_circulation"] = "wrong";
        $response = $this->json(
            "POST",
            "/api/v1/loanables",
            $invalidYearOfCirculation
        );
        $response->assertStatus(422);
    }

    public function testCreateCars()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $data = [
            "type" => "car",
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "location_description" => $this->faker->sentence,

            "name" => $this->faker->name,
            "owner_id" => $owner->id,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "details" => [
                "engine" => $this->faker->randomElement([
                    "fuel",
                    "diesel",
                    "electric",
                    "hybrid",
                ]),
                "brand" => $this->faker->word,
                "has_informed_insurer" => true,
                "insurer" => $this->faker->word,
                "value_category" => $this->faker->randomElement([
                    "lte50k",
                    "lte70k",
                    "lte100k",
                ]),
                "model" => $this->faker->sentence,
                "papers_location" => $this->faker->randomElement([
                    "in_the_car",
                    "to_request_with_car",
                ]),
                "plate_number" => $this->faker->shuffle("9F29J2"),
                "pricing_category" => "large",
                "transmission_mode" => $this->faker->randomElement([
                    "automatic",
                    "manual",
                ]),
                "year_of_circulation" => $this->faker->year($max = "now"),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$carResponseStructure)
            ->assertJsonPath("details.pricing_category", "large");
    }

    public function testShowCars()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $car = Loanable::factory()
            ->withCar()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", "/api/v1/loanables/$car->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$carResponseStructure);
    }

    public function testUpdateCars()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $car = Loanable::factory()
            ->withCar()
            ->create(["owner_id" => $owner->id]);
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$car->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteCars()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $car = Loanable::factory()
            ->withCar()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$car->id");
        $response->assertStatus(404);
    }

    public function testDeleteCarsWithActiveLoan()
    {
        // No active loan
        $loan = $this->buildLoan();
        $car = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$car->id");
        $response->assertStatus(404);

        // Prepaid (active) loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $car = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        // Only completed loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $payment = Payment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $car = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$car->id");
        $response->assertStatus(200);
    }

    public function testListCars()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        Loanable::factory(2)
            ->withCar()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", route("loanables.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(static::$carResponseStructure)
            );
    }

    protected function buildLoan(): Loan
    {
        return Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => Loanable::factory()->withCar(),
            ]);
    }
}
