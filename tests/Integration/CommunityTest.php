<?php

namespace Tests\Integration;

use App\Models\Community;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class CommunityTest extends TestCase
{
    private static $getCommunitiesResponseStructure = [
        "current_page",
        "data",
        "first_page_url",
        "from",
        "last_page",
        "last_page_url",
        "next_page_url",
        "path",
        "per_page",
        "prev_page_url",
        "to",
        "total",
    ];

    private static $getUserResponseStructure = [
        "id",
        "name",
        "email",
        "email_verified_at",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
    ];

    private static $getCommunityResponseStructure = [
        "id",
        "name",
        "description",
        "area",
        "created_at",
        "updated_at",
        "deleted_at",
        "type",
        "center",
    ];

    public function testOrderCommunitiesById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);
    }

    public function testOrderCommunitiesByName()
    {
        $data = [
            "order" => "name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);
    }

    public function testOrderCommunitiesByType()
    {
        $data = [
            "order" => "type",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);
    }

    public function testOrderCommunitiesByApprovedUsersCount()
    {
        $community1 = Community::factory()->create();
        $community2 = Community::factory()->create();

        $user1 = User::factory()->create();
        $user2 = User::factory()->create();
        $user3 = User::factory()->create();

        $user1
            ->communities()
            ->attach($community2, ["approved_at" => new Carbon()]);
        $user2
            ->communities()
            ->attach($community2, ["approved_at" => new Carbon()]);
        $user3
            ->communities()
            ->attach($community1, ["approved_at" => new Carbon()]);

        $data = [
            "order" => "approved_users_count",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type,approved_users_count",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJson([
                "data" => [
                    ["id" => $community1->id],
                    ["id" => $community2->id],
                ],
            ])
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);

        $data["order"] = "-approved_users_count";
        $response = $this->json("GET", route("communities.index"), $data);
        $response->assertStatus(200)->assertJson([
            "data" => [["id" => $community2->id], ["id" => $community1->id]],
        ]);
    }

    public function testFilterCommunitiesById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "id" => "4",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);
    }

    public function testFilterCommunitiesByName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "name" => "Patrie",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getCommunitiesResponseStructure);
    }

    public function testSearchCommunities()
    {
        Community::factory()->create([
            "name" => "Community name",
        ]);
        Community::factory()->create([
            "name" => "Other name",
        ]);

        $data = [
            "q" => "nity NA",
        ];
        $response = $this->json(
            "GET",
            route("communities.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testCreateCommunities()
    {
        $data = [
            "name" => $this->faker->name,
            "description" => $this->faker->sentence,
            "area" => null,
        ];

        $response = $this->json("POST", "/api/v1/communities/", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "name",
                "description",
                "area",
                "updated_at",
                "created_at",
            ]);
    }

    public function testCreateCommunitiesWithNullName()
    {
        $data = [
            "name" => null,
            "description" => $this->faker->sentence,
            "area" => null,
        ];

        $response = $this->json("POST", "/api/v1/communities/", $data);
        $response->assertStatus(422);
    }

    public function testCreateCommunitiesEmptyArea()
    {
        // Inspecting API call showed empty area to be an
        // empty array, not null which used to cause a
        // problem.
        $data = [
            "area" => [],

            "chat_group_url" => "",
            "description" => "Test quartier",
            "long_description" => "<p>Blabla</p>",
            "name" => "Quartier",
            "pricings" => [
                ["name" => "Test", "object_type" => null, "rule" => "0"],
            ],
            "type" => "borough",
        ];

        $response = $this->json("POST", "/api/v1/communities/", $data);
        $response->assertStatus(201);
    }

    public function testCreateCommunities_WithInvalidStartingGuideURL()
    {
        // This is to make sure the URL validation rule will kick in.
        $data = [
            "name" => $this->faker->name,
            "description" => $this->faker->sentence,
            "starting_guide_url" => "https://locomotion.app",
        ];

        $response = $this->json("POST", "/api/v1/communities/", $data);
        $response->assertStatus(201);

        $data = [
            "name" => $this->faker->name,
            "description" => $this->faker->sentence,
            "starting_guide_url" => "invalid url",
        ];

        $response = $this->json("POST", "/api/v1/communities/", $data);
        $response->assertStatus(422);
    }

    public function testShowCommunities()
    {
        $community = Community::factory()->create();

        $response = $this->json(
            "GET",
            "/api/v1/communities/$community->id",
            []
        );
        $response->assertStatus(200);
    }

    public function testUpdateCommunities()
    {
        $community = Community::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json(
            "PUT",
            "/api/v1/communities/$community->id",
            $data
        );
        $response->assertStatus(200)->assertJson($data);
    }

    public function testUpdateCommunitiesByAdminOfCommunity()
    {
        $community = Community::factory()->create();

        $this->user->role = "";
        $this->user->save();

        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json(
            "PUT",
            route("communities.update", $community->id),
            $data
        );
        $response->assertStatus(403);

        $communities = [
            $community->id => [
                "role" => "admin",
            ],
        ];
        $this->user->communities()->sync($communities);

        $response = $this->json(
            "PUT",
            route("communities.update", $community->id),
            $data
        );
        $response->assertStatus(200);
    }

    public function testUpdateCommunitiesByNonAdmin()
    {
        $this->user->role = "";
        $community = Community::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json(
            "PUT",
            route("communities.update", $community->id),
            $data
        );

        $response->assertStatus(403);
    }

    public function testDeleteCommunities()
    {
        $community = Community::factory()->create();

        $response = $this->json(
            "DELETE",
            route("communities.destroy", $community->id)
        );

        $response->assertStatus(200);
    }

    public function testDeleteCommunitiesByNonAdmin()
    {
        $this->user->role = "";
        $community = Community::factory()->create();

        $response = $this->json(
            "DELETE",
            route("communities.destroy", $community->id)
        );

        $response->assertStatus(403);
    }

    public function testListCommunities()
    {
        $communities = Community::factory(2)
            ->create()
            ->map(
                fn($post) => $post->only(["id", "name", "description", "area"])
            );

        $response = $this->json("GET", route("communities.index"));
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->buildCollectionStructure([
                    "id",
                    "name",
                    "description",
                    "area",
                ])
            );
    }

    public function testShowCommunitiesUsers()
    {
        $community = Community::factory()->create();
        User::factory()
            ->withCommunity($community)
            ->create();
        // User from no community should not be returned
        User::factory()->create();

        $data = [
            "communities.id" => $community->id,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getUserResponseStructure
                )
            )
            ->assertJsonCount(1, "data");
    }
}
