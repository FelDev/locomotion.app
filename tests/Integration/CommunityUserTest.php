<?php

use App\Models\Community;
use App\Models\User;
use Tests\TestCase;

class CommunityUserTest extends TestCase
{
    public function testApprovingCommunityUser_createsNokeUser()
    {
        $community = Community::factory()->create(["uses_noke" => true]);
        $user = User::factory()->create();

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        $assertion = Noke::shouldReceive("createUser")
            ->with(Mockery::on(fn($argument) => $user->is($argument)))
            ->once();

        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => \Carbon\Carbon::now(),
            ]
        );

        $response->assertStatus(200);

        $assertion->verify();
    }

    public function testApprovingCommunityUser_doesntCreateNokeUserIfAlreadyExists()
    {
        $community = Community::factory()->create(["uses_noke" => true]);
        $user = User::factory()->create(["noke_id" => 13]);

        $user->communities()->attach($community);

        $communityUser = $user->communities[0]->pivot;

        $assertion = Noke::shouldReceive("createUser")
            ->with(Mockery::on(fn($argument) => $user->is($argument)))
            ->never();

        $response = $this->json(
            "PUT",
            "/api/v1/communityUsers/$communityUser->id",
            [
                "approved_at" => \Carbon\Carbon::now(),
            ]
        );

        $response->assertStatus(200);

        $assertion->verify();
    }
}
