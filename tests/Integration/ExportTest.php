<?php

use App\Exports\BaseExport;
use App\Exports\LoanableExport;
use App\Exports\tests\SmallBatchExport;
use App\Jobs\AppendExportToFile;
use App\Jobs\NotifyUserExportCompleted;
use App\Models\Community;
use App\Models\Loanable;
use App\Models\User;
use Tests\TestCase;

class ExportTest extends TestCase
{
    public function testExport_defaultsToOrderById()
    {
        Storage::fake();
        $userData = [
            [
                "id" => 12,
                "name" => "foo",
            ],
            [
                "id" => 13,
                "name" => "bar",
            ],
            [
                "id" => 14,
                "name" => "baz",
            ],
            [
                "id" => 15,
                "name" => "quux",
            ],
            [
                "id" => 16,
                "name" => "quuz",
            ],
        ];

        $shuffledUserData = $userData;
        shuffle($shuffledUserData);

        User::factory()
            ->sequence(...$shuffledUserData)
            ->count(5)
            ->create();

        $response = $this->get("/api/v1/users?fields=id,name&id=12:16", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            $userData
        );
    }

    public function testExport_respectsOrder()
    {
        Storage::fake();
        $userData = [
            [
                "id" => 16,
                "name" => "a",
            ],
            [
                "id" => 15,
                "name" => "b",
            ],
            [
                "id" => 14,
                "name" => "c",
            ],
            [
                "id" => 13,
                "name" => "d",
            ],
            [
                "id" => 12,
                "name" => "e",
            ],
        ];

        $shuffledUserData = $userData;
        shuffle($shuffledUserData);

        User::factory()
            ->sequence(...$shuffledUserData)
            ->count(5)
            ->create();

        $response = $this->get(
            "/api/v1/users?fields=id,name&id=12:16&order=name",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            $userData
        );
    }

    public function testExport_serializesObjectValues()
    {
        Storage::fake();
        $community = Community::factory()->create([
            "id" => 5,
            "area" => [
                "type" => "MultiPolygon",
                "coordinates" => [[[[1, 2], [3, 4], [5, 6], [1, 2]]]],
            ],
        ]);

        self::assertIsObject($community->area);

        $response = $this->get("/api/v1/communities?fields=area&id=5", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["area"],
            [
                [
                    '{"type":"MultiPolygon","coordinates":[[[[1,2],[3,4],[5,6],[1,2]]]]}',
                ],
            ]
        );
    }

    public function testExport_serializesArrayValues()
    {
        Storage::fake();
        $loanable = Loanable::factory()->create([
            "id" => 5,
            "position" => [2, 3],
        ]);

        Loanable::$export = BaseExport::class;

        self::assertIsArray($loanable->position);

        $response = $this->get("/api/v1/loanables?fields=position&id=5", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["position"],
            [["[2,3]"]]
        );
    }

    public function testExport_enumeratesRelationColumns()
    {
        Storage::fake();
        $user = User::factory()->create([
            "id" => 5,
        ]);
        $communityA = Community::factory()->create(["name" => "CommunityA"]);
        $communityB = Community::factory()->create(["name" => "CommunityB"]);
        $user->communities()->attach($communityA);
        $user->communities()->attach($communityB);

        User::factory()
            ->create([
                "id" => 6,
            ])
            ->communities()
            ->attach($communityB);

        $response = $this->get("/api/v1/users?fields=communities.name&id=5,6", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["communities.0.name", "communities.1.name"],
            [["CommunityA", "CommunityB"], ["CommunityB", ""]]
        );
    }

    public function testExport_fromColumns()
    {
        Storage::fake();
        Loanable::$export = LoanableExport::class;
        $loanable = Loanable::factory()->create([
            "id" => 5,
        ]);

        self::assertIsArray($loanable->position);

        $response = $this->get("/api/v1/loanables?id=5", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            [
                "id",
                "name",
                "type",
                "comments",
                "instructions",
                "location_description",
                "position",
                "owner.id",
                "owner.user.id",
                "owner.user.name",
                "owner.user.last_name",
                "owner.user.communities.0.id",
                "owner.user.communities.0.name",
                "car_insurer",
            ],
            [
                [
                    $loanable->id,
                    $loanable->name,
                    $loanable->type->value,
                    $loanable->comments,
                    $loanable->instructions,
                    $loanable->location_description,
                    json_encode($loanable->position),
                    $loanable->owner->id,
                    $loanable->owner->user->id,
                    $loanable->owner->user->name,
                    $loanable->owner->user->last_name,
                    $loanable->owner->user->main_community->id,
                    $loanable->owner->user->main_community->name,
                    $loanable->car_insurer,
                ],
            ]
        );
    }

    public function testExport_appendsPivotFields()
    {
        Storage::fake();
        $user = User::factory()->create([
            "id" => 5,
        ]);
        $communityA = Community::factory()->create(["name" => "CommunityA"]);
        $user->communities()->attach($communityA, [
            "approved_at" => "2023-05-05T12:12:12.000000Z",
        ]);

        $response = $this->get(
            // Accessing the community name from the pivot user->communityUser->community relation,
            // for testing purposes.
            "/api/v1/users?fields=communities.community.name,communities.approved_at&id=5",
            [
                "accept" => "text/csv",
            ]
        );

        $response->assertStatus(200)->assertJson([
            "queued" => false,
        ]);

        self::assertCsvMatches(
            $response->json("file"),
            ["communities.0.community.name", "communities.0.approved_at"],
            [["CommunityA", "2023-05-05T12:12:12.000000Z"]]
        );
    }

    public function testExport_dispatchesToQueueForLargeExports()
    {
        Storage::fake();
        User::factory()
            ->count(10)
            ->create();

        // SmallBatchExport creates batches with 6 items
        User::$export = SmallBatchExport::class;
        Queue::fake();
        $response = $this->get("/api/v1/users?fields=id", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => true,
        ]);

        Queue::assertPushedWithChain(
            AppendExportToFile::class,
            [AppendExportToFile::class, NotifyUserExportCompleted::class],
            fn(AppendExportToFile $export) => $export->getJobNumber() === 0
        );
    }

    public function testQueuedExport_hasExpectedRows()
    {
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();

        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;
        $response = $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => true,
        ]);

        // The queue is sync in tests, so it should have all run
        self::assertCsvMatches(
            $response->json("file"),
            ["id", "name"],
            [
                [0, "c_0"],
                [1, "c_1"],
                [2, "c_2"],
                [3, "c_3"],
                [4, "c_4"],
                [5, "c_5"],
                [6, "c_6"],
                [7, "c_7"],
                [8, "c_8"],
                [9, "c_9"],
            ]
        );
    }

    public function testExport_cleansUpTemporaryFile()
    {
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();

        $filename = "";
        $assertion = File::partialMock()
            ->shouldReceive("delete")
            ->twice()
            ->with(Mockery::capture($filename))
            ->passthru();

        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;
        $response = $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ]);

        $response->assertStatus(200)->assertJson([
            "queued" => true,
        ]);

        self::assertStringStartsWith(
            storage_path("framework/exports/"),
            $filename
        );
        $assertion->verify();
        self::assertFileDoesNotExist($filename);
    }

    public function testExport_cleansUpTemporaryFileOnError()
    {
        Storage::fake();
        Community::factory()
            ->count(10)
            ->sequence(
                fn($sequence) => [
                    "id" => $sequence->index,
                    "name" => "c_$sequence->index",
                ]
            )
            ->create();
        // SmallBatchExport creates batches with 6 items
        Community::$export = SmallBatchExport::class;

        $filename = "";
        $assertion = File::partialMock()
            ->shouldReceive("delete")
            ->once()
            ->with(Mockery::capture($filename))
            ->passthru();

        Storage::shouldReceive("putFileAs")->andReturn(false);
        $this->get("/api/v1/communities?fields=id,name", [
            "accept" => "text/csv",
        ])->assertStatus(500);

        self::assertStringStartsWith(
            storage_path("framework/exports/"),
            $filename
        );
        $assertion->verify();
        self::assertFileDoesNotExist($filename);
    }

    private static function assertCsvMatches(
        $fileUrl,
        array $headers,
        array $expectedValues
    ) {
        $filePath = str_after($fileUrl, "http://localhost:8000/");
        Storage::assertExists($filePath);
        $data = Storage::get($filePath);

        // reading line by line
        $stream = fopen("php://memory", "r+");
        fwrite($stream, $data);
        rewind($stream);
        self::assertCsvLine($stream, $headers);
        foreach ($expectedValues as $expectedValue) {
            self::assertCsvLine($stream, array_values($expectedValue));
        }
        self::assertEndOfCsv($stream);

        fclose($stream);
    }

    private static function assertCsvLine($stream, array $values): void
    {
        $csvLine = fgetcsv($stream);
        self::assertNotFalse($csvLine);
        self::assertEquals($values, $csvLine);
    }

    private static function assertEndOfCsv($stream): void
    {
        self::assertFalse(fgetcsv($stream));
    }
}
