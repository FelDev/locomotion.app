<?php

use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class FileTest extends TestCase
{
    public function testUploadFile()
    {
        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $response->assertJsonStructure([
            "path",
            "original_filename",
            "filename",
            "field",
            "filesize",
            "updated_at",
            "created_at",
            "id",
            "url",
        ]);

        $response->assertJson([
            "field" => "residency_proof",
            "original_filename" => "foo.png",
            "path" => "/files/tmp/{$this->user->id}",
        ]);

        // New filename is some temporary string
        $filename = $response->json("filename");
        self::assertNotEquals("foo.png", $filename);
        self::assertStringEndsWith(".png", $filename);
    }

    public function testUploadFile_failsIfNotStored()
    {
        Storage::shouldReceive("putFileAs")
            ->withArgs([
                "/files/tmp/{$this->user->id}",
                Mockery::any(),
                Mockery::any(),
            ])
            ->once()
            ->andReturn(false);

        $response = $this->post("/api/v1/files", [
            "field" => "residency_proof",
            "residency_proof" => UploadedFile::fake()->create("foo.png"),
        ]);

        $response->assertStatus(500);
    }
}
