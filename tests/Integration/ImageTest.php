<?php

use Illuminate\Http\UploadedFile;
use Tests\TestCase;

class ImageTest extends TestCase
{
    public function testUploadFile()
    {
        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);

        $response->assertJsonStructure([
            "path",
            "original_filename",
            "filename",
            "field",
            "filesize",
            "updated_at",
            "created_at",
            "id",
            "url",
            "height",
            "width",
            "sizes" => ["original", "thumbnail"],
        ]);

        $response->assertJson([
            "field" => "avatar",
            "original_filename" => "foo.png",
            "filename" => "avatar.png",
        ]);

        // New path starts ends with uniquely named folder
        $path = $response->json("path");
        self::assertStringStartsWith("/images/tmp/{$this->user->id}/", $path);
        self::assertNotEquals("/images/tmp/{$this->user->id}/", $path);
    }

    public function testUploadImage_failsIfNotStored()
    {
        Storage::shouldReceive("put")
            ->once()
            ->andReturn(false);

        $response = $this->post("/api/v1/images", [
            "field" => "avatar",
            "avatar" => UploadedFile::fake()->image("foo.png"),
        ]);

        $response->assertStatus(500);
    }
}
