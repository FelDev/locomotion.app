<?php

namespace Tests\Integration;

use App\Mail\LoanIncidentCreated;
use App\Mail\LoanIncidentReviewable;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\User;

class IncidentTest extends ActionTestCase
{
    public function testIncidentCreation_SendsEmailsToAdminsAndOwners()
    {
        \Mail::fake();

        $ourCommunity = Community::factory()->create();
        $coowner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $owner = User::factory()
            ->withCommunity($ourCommunity)
            ->create();
        $loan = Loan::factory()->create([
            "borrower_id" => Borrower::factory([
                "user_id" => User::factory()->withCommunity($ourCommunity),
            ]),
            "loanable_id" => Loanable::factory()
                ->withCoowner($coowner)
                ->create(["owner_id" => Owner::factory(["user_id" => $owner])]),
        ]);

        $communityAdmin = User::factory()
            ->adminOfCommunity($ourCommunity)
            ->create();
        $globalAdmin = User::factory()->create(["role" => "admin"]);
        $otherCommunityAdmin = User::factory()
            ->adminOfCommunity(Community::factory())
            ->create();

        $response = $this->json("POST", "/api/v1/loans/$loan->id/actions", [
            "type" => "incident",
            "incident_type" => "accident",
            "comments_on_incident" => "Un gros soucis",
        ]);

        $response->assertStatus(201);

        \Mail::assertQueued(
            LoanIncidentCreated::class,
            fn(LoanIncidentCreated $mail) => $mail->hasTo($owner->email)
        );
        \Mail::assertQueued(
            LoanIncidentCreated::class,
            fn(LoanIncidentCreated $mail) => $mail->hasTo($coowner->email)
        );
        \Mail::assertQueued(
            LoanIncidentReviewable::class,
            fn(LoanIncidentReviewable $mail) => $mail->hasTo(
                $globalAdmin->email
            )
        );
        \Mail::assertQueued(
            LoanIncidentReviewable::class,
            fn(LoanIncidentReviewable $mail) => $mail->hasTo(
                $communityAdmin->email
            )
        );
        \Mail::assertNotQueued(
            LoanIncidentReviewable::class,
            fn(LoanIncidentReviewable $mail) => $mail->hasTo(
                $otherCommunityAdmin->email
            )
        );
    }
}
