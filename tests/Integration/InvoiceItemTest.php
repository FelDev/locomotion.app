<?php

namespace Tests\Integration;

use App\Models\Invoice;
use Tests\TestCase;

class InvoiceItemTest extends TestCase
{
    public function testListInvoiceItems()
    {
        $invoices = Invoice::factory()
            ->withUser()
            ->withItemCount(1)
            ->create([
                "user_id" => $this->user->id,
            ]);

        $response = $this->json("GET", route("invoiceitems.index"));

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                "*" => [
                    "id",
                    "invoice_id",
                    "item_type",
                    "label",
                    "amount",
                    "taxes_tps",
                    "taxes_tvq",
                    "amount_type",
                    "meta",
                ],
            ],
        ]);
    }
}
