<?php

namespace Tests\Integration;

use App\Mail\InvoicePaid;
use App\Mail\Loan\LoanCompleted;
use App\Mail\Loan\Updated;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Extension;
use App\Models\Intention;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\User;
use Carbon\Carbon;
use Database\Factories\CommunityFactory;
use Database\Factories\LoanFactory;
use DateTime;
use Mail;
use Tests\TestCase;

class LoanTest extends TestCase
{
    private static array $getLoanResponseStructure = [
        "id",
        "departure_at",
        "duration_in_minutes",
        "estimated_distance",
        "reason",
        "message_for_owner",
        "final_price",
        "platform_tip",
        "final_insurance",
        "final_platform_tip",
        "canceled_at",
        "final_purchases_amount",
        "status",
        "actual_return_at",
        "owner_validated_at",
        "borrower_validated_at",
        "actual_duration_in_minutes",
        "actual_insurance",
        "actual_price",
        "calendar_days",
        "is_contested",
        "is_free",
        "needs_validation",
        "total_actual_cost",
        "total_final_cost",
    ];

    public function testOrderLoansById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower.user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByDepartureAt()
    {
        $data = [
            "order" => "departure_at",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower.user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByBorrowerFullName()
    {
        $data = [
            "order" => "borrower.user.full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower.user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByOwnerFullName()
    {
        $data = [
            "order" => "loanable.owner.user.full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower.user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderLoansByCommunityName()
    {
        $data = [
            "order" => "community.name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "*,borrower.user.full_name,loanable.owner.user.full_name,community.name",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoanByDepartureAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "departure_at" => "@",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCalendarDays()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "calendar_days" => "3",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanableType()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.type" => "bike",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByOwnerFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.owner.user.full_name" => "David",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByBorrowerFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "borrower.user.full_name" => "Georges",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByIncidentStatus()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "incidents.status" => "completed",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByTakeoverStatus()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "takeover.status" => "in_process",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanStatus()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "status" => "completed",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCommunityId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "community.id" => "9",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByCommunityName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "community.name" => "Patrie",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanableId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.id" => "1",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterLoansByLoanableName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.name" => "Vélo",
        ];
        $response = $this->json("GET", route("loans.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testCreateLoans()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
            "community_id" => $community->id,
        ];

        $response = $this->json("POST", "/api/v1/loans", $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure);
    }

    public function testCreateLoan_CannotCreateZeroMinuteLoan()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => 0,
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
            "community_id" => $community->id,
        ];

        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "duration_in_minutes" => [
                    "La durée de l'emprunt doit être supérieure ou égale à 15 minutes.",
                ],
            ],
        ]);
    }

    public function testCreateLoanOnApprovedCommunityOnly()
    {
        $approvedCommunity = Community::factory()->create();
        $suspendedCommunity = Community::factory()->create();
        $justRegisteredCommunity = Community::factory()->create();

        $user = User::factory()->create();
        $user->communities()->attach($approvedCommunity->id, [
            "approved_at" => new DateTime(),
        ]);
        $user->communities()->attach($suspendedCommunity->id, [
            "approved_at" => new DateTime(),
            "suspended_at" => new DateTime(),
        ]);
        $user->communities()->attach($justRegisteredCommunity->id);

        $this->actAs($user);

        $borrower = Borrower::factory()->create(["user_id" => $user->id]);

        $this->user->communities()->attach($approvedCommunity->id, [
            "approved_at" => new DateTime(),
        ]);
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $departure = new Carbon();
        $baseData = [
            "duration_in_minutes" => 60,
            "estimated_distance" => $this->faker->randomNumber(4),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
        ];

        // Try creating a loan on...
        // 1. an approved community
        // 2. a suspended community
        // 3. a new community
        $approvedData = array_merge($baseData, [
            "community_id" => $approvedCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);
        $suspendedData = array_merge($baseData, [
            "community_id" => $suspendedCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);
        $justRegisteredData = array_merge($baseData, [
            "community_id" => $justRegisteredCommunity->id,
            "departure_at" => $departure->add(2, "hour")->toDateTimeString(),
        ]);

        $this->json("POST", "/api/v1/loans", $approvedData)->assertStatus(201);
        $this->json("POST", "/api/v1/loans", $suspendedData)->assertStatus(422);
        $this->json("POST", "/api/v1/loans", $justRegisteredData)->assertStatus(
            422
        );

        // Approve previously suspended or not approved communities
        $user->communities()->updateExistingPivot($suspendedCommunity->id, [
            "approved_at" => new DateTime(),
            "suspended_at" => null,
        ]);
        $user
            ->communities()
            ->updateExistingPivot($justRegisteredCommunity->id, [
                "approved_at" => new DateTime(),
            ]);

        $this->json("POST", "/api/v1/loans", $suspendedData)->assertStatus(201);
        $this->json("POST", "/api/v1/loans", $justRegisteredData)->assertStatus(
            201
        );
    }

    public function testShowLoans()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $loan = Loan::factory()->create(["borrower_id" => $borrower->id]);

        $response = $this->json("GET", "/api/v1/loans/$loan->id");

        $response
            ->assertStatus(200)
            ->assertJson(["id" => $loan->id])
            ->assertJsonStructure(static::$getLoanResponseStructure);
    }

    public function testUpdateLoanTime_resetsApproval()
    {
        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
            ]);
        $data = [
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
        ];

        $this->actAs($loan->borrower->user);
        Mail::fake();
        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals("in_process", $loan->intention->status);
        Mail::assertQueued(
            Updated::class,
            fn(Updated $mail) => $mail->hasTo($loanable->owner->user->email)
        );
        Mail::assertQueued(
            Updated::class,
            fn(Updated $mail) => $mail->hasTo(
                $loanable->coowners[0]->user->email
            )
        );
    }

    public function testUpdateSelfServiceLoan_isApprovedAutomaticallyIfAvailable()
    {
        $loanable = Loanable::factory()->create([
            "is_self_service" => true,
        ]);
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
            ]);
        $data = [
            "duration_in_minutes" => $this->faker->randomDigitNotZero() * 15,
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals("completed", $loan->intention->status);
    }

    public function testUpdateLoan_resetsPrepaymentIfNotFree()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $this->user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "is_self_service" => true,
        ]);
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->withCompletedPrePayment()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 60,
            ]);
        $data = [
            "duration_in_minutes" => 120,
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals("in_process", $loan->prePayment->status);
        self::assertNull($loan->takeover);
    }

    public function testUpdateLoan_DoesNotResetPrepaymentIfChangingPlatformTip()
    {
        $community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $this->user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "is_self_service" => true,
        ]);
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->withCompletedPrePayment()
            ->withCompletedTakeover()
            ->withCompletedHandover()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $loanable->id,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 60,
            ]);
        $data = [
            "platform_tip" => 12,
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(200)->assertJson($data);
        $loan->refresh();
        self::assertEquals("completed", $loan->prePayment->status);
        self::assertEquals("completed", $loan->takeover->status);
        self::assertEquals("completed", $loan->handover->status);
        self::assertEquals("in_process", $loan->payment->status);
    }

    public function testUpdateLoan_isForbiddenIfUnavailable()
    {
        $loanable = Loanable::factory()->create();
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->addWeek(),
                "duration_in_minutes" => 45,
            ]);

        $otherLoan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()
                    ->addWeek()
                    ->addHour(),
            ]);

        $data = [
            "duration_in_minutes" => 360, // 6 hours, which would overlap the other loan
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "loanable_id" => [
                    "Le véhicule n'est pas disponible sur cette période.",
                ],
            ],
        ]);
    }

    public function testUpdateLoan_isForbiddenIfAfterLoanDeparture()
    {
        $loanable = Loanable::factory()->create();
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->subHour(),
                "duration_in_minutes" => 120,
            ]);

        $data = [
            "duration_in_minutes" => 360, // 6 hours, which would overlap the other loan
        ];

        $response = $this->json("PUT", "/api/v1/loans/$loan->id", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "departure_at" => [
                    "L'heure du début de la réservation doit être dans le futur.",
                ],
            ],
        ]);
    }

    public function testDeleteLoans()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $loan = Loan::factory()->create(["borrower_id" => $borrower->id]);

        $response = $this->json("DELETE", "/api/v1/loans/$loan->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loans/$loan->id");
        $response->assertStatus(404);
    }

    public function testListLoans()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        Loan::factory(2)
            ->create(["borrower_id" => $borrower->id])
            ->map(fn($loan) => $loan->only(static::$getLoanResponseStructure));

        $response = $this->json("GET", route("loans.index"));

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getLoanResponseStructure
                )
            );
    }

    public function testShowLoansBorrower()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $loan = Loan::factory()->create(["borrower_id" => $borrower->id]);

        $response = $this->json("GET", "/api/v1/loans/$loan->id/borrower");

        $response->assertStatus(200)->assertJson(["id" => $borrower->id]);
    }

    public function testCannotCreateConcurrentLoans()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);
        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $departure = new Carbon();
        $departure->setSeconds(0);
        $departure->setMilliseconds(0);

        $data = [
            "departure_at" => $departure->toDateTimeString(),
            "duration_in_minutes" => 60,
            "estimated_distance" => 0,
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
            "community_id" => $community->id,
        ];

        // First loan
        $response = $this->json(
            "POST",
            "/api/v1/loans?fields=*,intention.*",
            $data
        );

        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getLoanResponseStructure);

        $loanId = $response->json()["id"];
        $intention = $response->json()["intention"];

        // Confirm intention on first loan
        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loanId/intention/complete",
            array_merge($intention, ["status" => "completed"])
        );
        $response->assertStatus(200);

        // Exactly the same time: overlap
        $response = $this->json("POST", "/api/v1/loans", $data);

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "loanable_id" => [
                    "Le véhicule n'est pas disponible sur cette période.",
                ],
            ],
        ]);

        // 1 hour from 30 minutes later: overlap
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->add(30, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "loanable_id" => [
                    "Le véhicule n'est pas disponible sur cette période.",
                ],
            ],
        ]);

        // 1 hour from 1 hour later: OK
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->add(60, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(201);

        // 1 hour from 30 minutes earlier: overlap
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->subtract(30, "minutes")
                    ->toDateTimeString(),
            ])
        );

        $response->assertStatus(422)->assertJson([
            "errors" => [
                "loanable_id" => [
                    "Le véhicule n'est pas disponible sur cette période.",
                ],
            ],
        ]);

        // 30 minutes from 30 minutes earlier: OK
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->copy()
                    ->subtract(30, "minutes")
                    ->toDateTimeString(),
                "duration_in_minutes" => 30,
            ])
        );

        $response->assertStatus(201);
    }

    public function testCreateLoansOnlyBuildsOneIntention()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3, true),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
            "community_id" => $community->id,
        ];

        $response = $this->json(
            "POST",
            "/api/v1/loans?fields=*,intention.*,pre_payment.*,takeover.*,handover.*,incidents.*,extensions.*,payment.*",
            $data
        );

        $response->assertStatus(201)->assertJson([
            "intention" => [
                "status" => "in_process",
            ],
        ]);
        $response->assertJsonMissing([
            "pre_payment" => [],
        ]);
    }

    public function testCreateWithSelfServiceLoanableIsAutomaticallyAccepted()
    {
        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "is_self_service" => true,
        ]);

        $data = [
            "departure_at" => now()->toDateTimeString(),
            "duration_in_minutes" => $this->faker->randomNumber(3, true),
            "estimated_distance" => $this->faker->randomNumber(3, true),
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
            "estimated_price" => 1,
            "estimated_insurance" => 1,
            "platform_tip" => 1,
            "message_for_owner" => "",
            "reason" => "salut",
            "community_id" => $community->id,
        ];

        $response = $this->json(
            "POST",
            "/api/v1/loans?fields=*,intention.*,pre_payment.*,takeover.*,handover.*,incidents.*,extensions.*,payment.*",
            $data
        );

        $response->assertStatus(201)->assertJson([
            "intention" => [
                "status" => "completed",
            ],
            "pre_payment" => [
                "status" => "in_process",
            ],
        ]);
        $response->assertJsonMissing([
            "takeover" => [],
        ]);
    }

    // Basic case: the actual_duration_in_minutes of a loan is its intended duration
    public function testLoanActualDurationInMinutesBase()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "duration_in_minutes" => 60,
            ]);

        $this->assertEquals(60, $loan->actual_duration_in_minutes);
        $this->json("GET", "/api/v1/loans/$loan->id")->assertJson([
            "actual_duration_in_minutes" => 60,
        ]);
    }

    // Extended case: the actual_duration_in_minutes is its largest extension duration
    public function testLoanActualDurationInMinutesWithExtension()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "duration_in_minutes" => 60,
            ]);

        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
            ])
        );

        // Reload loan from database before testing
        $loan->refresh();

        $this->assertEquals(120, $loan->actual_duration_in_minutes);
        $this->json("GET", "/api/v1/loans/$loan->id")->assertJson([
            "actual_duration_in_minutes" => 120,
        ]);
    }

    // Extended case with multiple extensions:
    // same as before, but only the largest completed (approved) extension is considered
    public function testLoanActualDurationInMinutesWithMultipleExtensions()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "duration_in_minutes" => 60,
            ]);

        // Only completed extensions are considered
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
            ])
        );

        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 360,
                "status" => "completed",
            ])
        );

        // In process or canceled extensions are ignored
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 240,
                "status" => "in_process",
            ])
        );
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 480,
                "status" => "canceled",
            ])
        );

        // Reload loan from database before testing
        $loan->refresh();

        $this->assertEquals(360, $loan->actual_duration_in_minutes);
        $this->json("GET", "/api/v1/loans/$loan->id")->assertJson([
            "actual_duration_in_minutes" => 360,
        ]);
    }

    // Paid case: if the loan is paid (payment step is completed),
    // the actual_duration_in_minutes becomes the time when it was paid if it's
    // smaller than the extended or regular case, as previously tested
    public function testLoanActualDurationInMinutesWhenPaid()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            //            ->has(Payment::factory())
            ->create([
                "duration_in_minutes" => 60,
            ]);

        // The loan was completed earlier, so it's assumed the vehicle
        // becomes available earlier
        $payment = $loan->payment()->first();
        $payment->executed_at = Carbon::now()->add(30, "minutes");
        $payment->save();

        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
            ])
        );

        // In process or canceled extensions are ignored
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 240,
                "status" => "canceled",
            ])
        );

        // Reload loan from database before testing
        $loan->refresh();

        $this->assertEquals(30, $loan->actual_duration_in_minutes);
        $this->json("GET", "/api/v1/loans/$loan->id")->assertJson([
            "actual_duration_in_minutes" => 30,
        ]);
    }

    // If the loan was paid before it's departure time, then the actual duration is expected to be 0.
    public function testLoanActualDurationInMinutesWhenPaidInThePast()
    {
        // Loan starts 270 minutes ago and was paid 30 minutes before it started.
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "departure_at" => Carbon::now()->sub(270, "minutes"),
                "duration_in_minutes" => 60,
            ]);

        // The loan was completed earlier, so it's assumed the vehicle
        // becomes available earlier
        $payment = $loan->payment()->first();
        $payment->executed_at = Carbon::now()->sub(300, "minutes");
        $payment->save();

        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
            ])
        );

        // In process or canceled extensions are ignored
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 240,
                "status" => "canceled",
            ])
        );

        // Reload loan from database before testing
        $loan->refresh();

        $this->assertEquals(0, $loan->actual_duration_in_minutes);
        $this->json("GET", "/api/v1/loans/$loan->id")->assertJson([
            "actual_duration_in_minutes" => 0,
        ]);
    }

    public function testRetrieveApprovedLoan_showsInstructions()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()->create();

        $borrowerUser = User::factory()->create();
        $borrowerUser
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);

        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "instructions" => "test",
        ]);

        $this->actAs($borrower->user);
        $response = $this->json("GET", route("loans.index"));

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);

        $loan = Loan::factory()->create([
            "borrower_id" => $borrower->id,
            "loanable_id" => $loanable->id,
        ]);
        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id?fields=*,loanable.*"
        );

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);

        $loan->intention()->save(
            Intention::factory()->make([
                "executed_at" => Carbon::now(),
                "status" => "completed",
            ])
        );

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id?fields=*,loanable.*"
        );

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testLoanDashboard()
    {
        $this->withoutEvents();

        $borrower = Borrower::factory()->create();
        $owner = Owner::factory()->create([
            "user_id" => $borrower->user_id,
        ]);

        // Need of approval from $owner
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);
        Loan::factory(2)
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $loanable->id,
            ]);
        // $borrower waiting for approval
        $otherOwner = Owner::factory()->create();
        $otherLoanable = Loanable::factory()->create([
            "owner_id" => $otherOwner->id,
        ]);
        Loan::factory(1)
            ->withInProcessIntention()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $otherLoanable->id,
            ]);
        // Starting in the future
        Loan::factory(2)
            ->withCompletedIntention()
            ->create([
                "departure_at" => Carbon::now()->addHour(),
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        // Starting in the future with completed takeovers (if borrower clicks too fast)
        Loan::factory(3)
            ->withCompletedTakeover()
            ->create([
                "departure_at" => Carbon::now()->addHour(),
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        // Started
        Loan::factory(4)
            ->withCompletedTakeover()
            ->create([
                "departure_at" => Carbon::now()->subMinutes(5),
                "duration_in_minutes" => 60,
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        // Started without completed takeover
        Loan::factory(3)
            ->withInProcessTakeover()
            ->create([
                "departure_at" => Carbon::now()->subMinutes(5),
                "duration_in_minutes" => 60,
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        // Contested
        Loan::factory(1)
            ->withContestedTakeover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        Loan::factory(2)
            ->withContestedHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        Loan::factory(3)
            ->withContestedHandover()
            ->create([
                "departure_at" => Carbon::now()->addHour(),
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);
        // recently_completed
        Loan::factory(7)
            ->withCompletedPayment()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ]);

        $this->actAs($borrower->user);
        $response = $this->json("get", "/api/v1/loans/dashboard");

        $response->assertJsonCount(5, "started.loans");
        $response->assertJsonPath("started.total", 7);
        $response->assertJsonCount(5, "contested.loans");
        $response->assertJsonPath("contested.total", 6);
        $response->assertJsonCount(1, "waiting.loans");
        $response->assertJsonPath("waiting.total", 1);
        $response->assertJsonCount(2, "need_approval.loans");
        $response->assertJsonPath("need_approval.total", 2);
        $response->assertJsonCount(5, "future.loans");
        $response->assertJsonPath("future.total", 5);
        $response->assertJsonCount(5, "completed.loans");
        $response->assertJsonPath("completed.total", 7);
    }

    public function testLoanDashboard_adminSeesOnlyAsBorrowerOrOwner()
    {
        $this->withoutEvents();

        $borrower = Borrower::factory()->create();
        $owner = Owner::factory()->create();

        $admin = User::factory()
            ->withCommunity()
            ->create(["role" => "admin"]);
        $adminAsOwner = Owner::factory()->create([
            "user_id" => $admin->id,
        ]);
        $adminAsBorrower = Borrower::factory()->create([
            "user_id" => $admin->id,
        ]);

        $borrowedLoanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);
        $ownedLoanable = Loanable::factory()->create([
            "owner_id" => $adminAsOwner->id,
        ]);
        $otherLoanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $hiddenLoan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $otherLoanable->id,
            ]);
        $loanAsBorrower = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "borrower_id" => $adminAsBorrower->id,
                "loanable_id" => $borrowedLoanable->id,
            ]);
        $loanAsOwner = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $ownedLoanable->id,
            ]);

        $this->actAs($admin);
        $response = $this->json("get", "/api/v1/loans/dashboard");

        $response->assertJson([
            "waiting" => [
                "loans" => [["id" => $loanAsBorrower->id]],
                "total" => 1,
            ],
            "need_approval" => [
                "loans" => [["id" => $loanAsOwner->id]],
                "total" => 1,
            ],
        ]);
        $response->assertJsonCount(0, "started.loans");
        $response->assertJsonCount(0, "contested.loans");
        $response->assertJsonCount(0, "future.loans");
        $response->assertJsonCount(0, "completed.loans");
    }

    public function testCompleteLoan_sendsAndMails()
    {
        // Bike with owner
        $bike = Loanable::factory(["is_self_service" => false])
            ->withCoowner()
            ->create();

        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $bike->id,
                "platform_tip" => 10,
            ]);

        $loan->borrower->user->balance = $loan->total_actual_cost + 100;
        $loan->borrower->user->save();

        Mail::fake();

        $response = $this->json(
            "put",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
            ]
        );

        $response->assertStatus(200);
        Mail::assertQueued(
            InvoicePaid::class,
            fn($mail) => $mail->hasTo($loan->borrower->user->email)
        );
        Mail::assertQueued(
            InvoicePaid::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner->user->email)
        );
        Mail::assertQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->borrower->user->email)
        );
        Mail::assertQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner->user->email)
        );
        Mail::assertQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->loanable->coowners[0]->user->email)
        );
    }

    public function testCompleteSelfServiceLoan_sendsMailOnlyToBorrower()
    {
        // Bike without owner
        $bike = Loanable::factory()
            ->withCoowner()
            ->create(["is_self_service" => true]);

        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $bike->id,
            ]);
        $loan->borrower->user->balance = $loan->total_actual_cost + 100;
        $loan->borrower->user->save();
        $loan->refresh();

        Mail::fake();

        $response = $this->json(
            "put",
            "/api/v1/loans/$loan->id/payment/complete",
            [
                "type" => "payment",
            ]
        );

        $response->assertStatus(200);
        Mail::assertQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->borrower->user->email)
        );
        Mail::assertNotQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->loanable->owner->user->email)
        );
        Mail::assertNotQueued(
            LoanCompleted::class,
            fn($mail) => $mail->hasTo($loan->loanable->coowners[0]->user->email)
        );
    }

    function testCancelLoan_failsWhenDisallowed()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefault10DollarsPricing(),
            Loan::factory()->withCompletedTakeover(),
            [
                "departure_at" => Carbon::now()->subMinutes(30),
            ]
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(403)->assertJson([
            "message" => "L'emprunt payant ne doit pas être débuté.",
        ]);
    }

    function testCancelLoan_succeedsAsAdmin()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefault10DollarsPricing(),
            Loan::factory()->withCompletedTakeover(),
            [
                "departure_at" => Carbon::now()->subMinutes(30),
            ]
        );

        // $this->user is admin
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testCancelLoan_succeedsWhenNoTakeover()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefault10DollarsPricing(),
            Loan::factory()->withCompletedIntention(),
            [
                "departure_at" => Carbon::now()->subMinutes(30),
            ]
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testCancelLoan_succeedsWhenInTheFuture()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefault10DollarsPricing(),
            Loan::factory()->withCompletedTakeover(),
            [
                "departure_at" => Carbon::now()->addMinutes(30),
            ]
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testCancelLoan_succeedsWhenFree()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedTakeover(),
            [
                "departure_at" => Carbon::now()->subMinutes(30),
            ]
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/cancel");
        $response->assertStatus(200);
    }

    function testValidateLoan_succeedsForOwner()
    {
        $this->withoutEvents();
        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedHandover()
        );

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->owner_validated_at);
        self::assertEquals(
            new Carbon($loan->owner_validated_at),
            new Carbon($response->content())
        );
    }

    function testValidateLoan_doesntOverwriteBorrowerInitialValidation()
    {
        $this->withoutEvents();

        $twentyMinutesAgo = Carbon::now(0)
            ->subMinutes(20)
            ->milli(0);

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedHandover(),
            [
                "borrower_validated_at" => $twentyMinutesAgo,
            ]
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            $twentyMinutesAgo,
            new Carbon($loan->borrower_validated_at)
        );
        self::assertEquals($twentyMinutesAgo, new Carbon($response->content()));
    }

    function testValidateLoan_doesntOverwriteOwnerInitialValidation()
    {
        $this->withoutEvents();

        $twentyMinutesAgo = Carbon::now(0)
            ->subMinutes(20)
            ->milli(0);

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedHandover(),
            [
                "owner_validated_at" => $twentyMinutesAgo,
            ]
        );

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertEquals(
            $twentyMinutesAgo,
            new Carbon($loan->owner_validated_at)
        );
        self::assertEquals($twentyMinutesAgo, new Carbon($response->content()));
    }

    function testValidateLoan_succeedsForBorrower()
    {
        $this->withoutEvents();
        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedHandover()
        );

        $this->actAs($loan->borrower->user);
        $response = $this->json("PUT", "api/v1/loans/$loan->id/validate");
        $response->assertStatus(200);
        $loan->refresh();
        self::assertNotNull($loan->borrower_validated_at);
        self::assertEquals(
            new Carbon($loan->borrower_validated_at),
            new Carbon($response->content())
        );
    }

    function testContestHandover_resetsValidation()
    {
        $this->withoutEvents();

        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedHandover(),
            [
                "borrower_validated_at" => new Carbon(),
                "owner_validated_at" => new Carbon(),
            ]
        );

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);

        $response = $this->json(
            "PUT",
            "api/v1/loans/$loan->id/handover/contest",
            [
                "type" => "handover",
            ]
        );
        $response->assertStatus(200);

        $loan->refresh();
        self::assertNull($loan->owner_validated_at);
        self::assertNull($loan->borrwer_validated_at);
    }

    function testContestTakeover_resetsValidation()
    {
        $this->withoutEvents();
        $loan = self::createLoanInCommunity(
            Community::factory()->withDefaultFreePricing(),
            Loan::factory()->withCompletedTakeover(),
            [
                "borrower_validated_at" => new Carbon(),
                "owner_validated_at" => new Carbon(),
            ]
        );

        self::assertNotNull($loan->borrower_validated_at);
        self::assertNotNull($loan->owner_validated_at);

        $response = $this->json(
            "PUT",
            "api/v1/loans/$loan->id/takeover/contest",
            [
                "type" => "takeover",
            ]
        );
        $response->assertStatus(200);

        $loan->refresh();
        self::assertNull($loan->owner_validated_at);
        self::assertNull($loan->borrwer_validated_at);
    }

    /**
     * Creates a full loan structure (with a borrower, a loanable and an owner) in a new community.
     */
    public static function createLoanInCommunity(
        CommunityFactory $communityFactory,
        LoanFactory $loanFactory,
        array $params = []
    ): Loan {
        $community = $communityFactory->create();

        $borrowerUser = User::factory()->create();
        $borrowerUser
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => new DateTime()]);
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);

        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "instructions" => "test",
        ]);

        return $loanFactory->create(
            array_merge($params, [
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
            ])
        );
    }
}
