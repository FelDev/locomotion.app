<?php

namespace Tests\Integration;

use App\Mail\CoownerAddedMail;
use App\Mail\CoownerRemovedMail;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Intention;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Padlock;
use App\Models\Pricing;
use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Database\Factories\UserFactory;
use Illuminate\Mail\Mailable;
use Mail;
use Tests\TestCase;

class LoanableTest extends TestCase
{
    private static $getLoanablesResponseStructure = [
        "current_page",
        "data",
        "first_page_url",
        "from",
        "last_page",
        "last_page_url",
        "next_page_url",
        "path",
        "per_page",
        "prev_page_url",
        "to",
        "total",
    ];

    public function testLoanableCreationValidation()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $validData = [
            "name" => $this->faker->name,
            "owner_id" => $owner->id,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "type" => "trailer",
            "details" => [
                "dimensions" => "",
                "maximum_charge" => "",
            ],
        ];
        $response = $this->json("POST", "/api/v1/loanables", $validData);
        $response->assertStatus(201);

        $missingType = $validData;
        unset($missingType["type"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingType);
        $response->assertStatus(422);

        $wrongType = $validData;
        $wrongType["type"] = "wrong";
        $response = $this->json("POST", "/api/v1/loanables", $wrongType);
        $response->assertStatus(422);

        $missingOwner = $validData;
        unset($missingOwner["owner_id"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingOwner);
        $response->assertStatus(422);

        $missingDetails = $validData;
        unset($missingDetails["details"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingDetails);
        $response->assertStatus(422);

        $missingPosition = $validData;
        unset($missingPosition["position"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingPosition);
        $response->assertStatus(422);

        $missingName = $validData;
        unset($missingName["name"]);
        $response = $this->json("POST", "/api/v1/loanables", $missingName);
        $response->assertStatus(422);

        // Valid with  owner.id instead of owner_id
        $withFullOwner = $validData;
        unset($withFullOwner["owner_id"]);
        $withFullOwner["owner"] = ["id" => $owner->id];
        $response = $this->json("POST", "/api/v1/loanables", $withFullOwner);
        $response->assertStatus(201);

        // with owner.name but no owner.id or owner_id should fail.
        $withWrongOwner = $validData;
        unset($withWrongOwner["owner_id"]);
        $withWrongOwner["owner"] = ["name" => $owner->name];
        $response = $this->json("POST", "/api/v1/loanables", $withWrongOwner);
        $response->assertStatus(422);
    }

    public function testAvailability_AvailabilityModeNever_ResponseModeAvailable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "never",
            "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-12 13:15:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 8.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "available",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "data" => ["available" => true],
            ],
            // Loan from 13:15 to 21:45
            [
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 13:15:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-12 21:45:00",
                "end" => "2022-10-12 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "data" => ["available" => true],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeAlways_ResponseModeAvailable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "always",
            "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        // Loan that overlaps availability rules.
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-12 09:00:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 13.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "available",
            ]
        );

        $response->assertStatus(200)->assertExactJson([
            [
                "start" => "2022-10-10 00:00:00",
                "end" => "2022-10-10 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 12:00:00",
                "end" => "2022-10-10 13:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-10 22:00:00",
                "end" => "2022-10-11 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-11 00:00:00",
                "end" => "2022-10-11 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 12:00:00",
                "end" => "2022-10-11 17:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-11 22:00:00",
                "end" => "2022-10-12 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-12 00:00:00",
                "end" => "2022-10-12 09:00:00",
                "data" => ["available" => true],
            ],
            // Loan from 09:00 to 22:30
            [
                "start" => "2022-10-12 22:30:00",
                "end" => "2022-10-13 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-13 00:00:00",
                "end" => "2022-10-13 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 12:00:00",
                "end" => "2022-10-13 17:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-13 22:00:00",
                "end" => "2022-10-14 00:00:00",
                "data" => ["available" => true],
            ],

            [
                "start" => "2022-10-14 00:00:00",
                "end" => "2022-10-14 10:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 12:00:00",
                "end" => "2022-10-14 13:00:00",
                "data" => ["available" => true],
            ],
            [
                "start" => "2022-10-14 22:00:00",
                "end" => "2022-10-15 00:00:00",
                "data" => ["available" => true],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeNever_ResponseModeUnavailable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "never",
            "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-12 13:15:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 8.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "unavailable",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-10 00:00:00",
                "end" => "2022-10-10 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 12:00:00",
                "end" => "2022-10-10 13:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 22:00:00",
                "end" => "2022-10-11 00:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-11 00:00:00",
                "end" => "2022-10-11 10:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-11 12:00:00",
                "end" => "2022-10-11 17:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-11 22:00:00",
                "end" => "2022-10-12 00:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 00:00:00",
                "end" => "2022-10-12 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 12:00:00",
                "end" => "2022-10-12 13:00:00",
                "data" => ["available" => false],
            ],
            // Loan
            [
                "start" => "2022-10-12 13:15:00",
                "end" => "2022-10-12 21:45:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 22:00:00",
                "end" => "2022-10-13 00:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-13 00:00:00",
                "end" => "2022-10-13 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 12:00:00",
                "end" => "2022-10-13 17:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 22:00:00",
                "end" => "2022-10-14 00:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-14 00:00:00",
                "end" => "2022-10-14 10:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 12:00:00",
                "end" => "2022-10-14 13:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 22:00:00",
                "end" => "2022-10-15 00:00:00",
                "data" => ["available" => false],
            ],
        ]);
    }

    public function testAvailability_AvailabilityModeAlways_ResponseModeUnavailable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "always",
            "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-11 12:15:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 4.5 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/availability",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "responseMode" => "unavailable",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "data" => ["available" => false],
            ],
            // Loan
            [
                "start" => "2022-10-11 12:15:00",
                "end" => "2022-10-11 16:45:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 22:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "data" => ["available" => false],
            ],
            [
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 22:00:00",
                "data" => ["available" => false],
            ],

            [
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "data" => ["available" => false],
            ],
        ]);
    }

    public function testEvents_AvailabilityModeNever()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "never",
            "availability_json" => <<<JSON
[
  {
    "available": true,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": true,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": true,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": true,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);
        $loanLessThanOneDay = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-11 11:15:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 10 * 60,
            ]);

        $loanMoreThanOneDay = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-12 22:45:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 3 * 24 * 60,
            ]);

        $loanMoreThanOneMonth = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-09-30 00:00:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 44 * 24 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/events",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                // Set order to get the results in a predictable order.
                "order" => "type,start,end",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 17:00:00",
                "end" => "2022-10-10 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 17:00:00",
                "end" => "2022-10-12 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 17:00:00",
                "end" => "2022-10-14 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => true],
            ],
            // Loans
            [
                "type" => "loan",
                "start" => "2022-09-30 00:00:00",
                "end" => "2022-11-13 00:00:00",
                "uri" => "/loans/{$loanMoreThanOneMonth->id}",
                "data" => ["status" => "in_process"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-11 11:15:00",
                "end" => "2022-10-11 21:15:00",
                "uri" => "/loans/{$loanLessThanOneDay->id}",
                "data" => ["status" => "in_process"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-12 22:45:00",
                "end" => "2022-10-15 22:45:00",
                "uri" => "/loans/{$loanMoreThanOneDay->id}",
                "data" => ["status" => "in_process"],
            ],
        ]);
    }

    public function testEvents_AvailabilityModeAlways()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()->create([
            "availability_mode" => "always",
            "availability_json" => <<<JSON
[
  {
    "available": false,
    "type": "weekdays",
    "scope": ["MO","TU","TH","WE","FR"],
    "period": "17:00-22:00"
  },{
    "available": false,
    "type": "weekdays",
    "scope": ["SA","SU"],
    "period":"00:00-24:00"
  },{
    "available": false,
    "type": "dates",
    "scope": ["2022-10-10","2022-10-12","2022-10-14"],
    "period": "13:00-17:00"
  },{
    "available": false,
    "type": "dateRange",
    "scope": ["2022-10-10","2022-10-14"],
    "period": "10:00-12:00"
  }
]
JSON
            ,
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $loanLessThanOneDay = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-11 11:15:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 10 * 60,
            ]);

        $loanMoreThanOneDay = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-10-12 22:45:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 3 * 24 * 60,
            ]);

        $loanMoreThanOneMonth = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "departure_at" => (new Carbon("2022-09-30 00:00:00"))->format(
                    "Y-m-d H:i:s"
                ),
                "duration_in_minutes" => 44 * 24 * 60,
            ]);

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/events",
            [
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-16 00:00:00",
                // Set order to get the results in a predictable order.
                "order" => "type,start,end",
            ]
        );

        $response->assertStatus(200)->assertSimilarJson([
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-09 00:00:00",
                "end" => "2022-10-10 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 10:00:00",
                "end" => "2022-10-10 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 13:00:00",
                "end" => "2022-10-10 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-10 17:00:00",
                "end" => "2022-10-10 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 10:00:00",
                "end" => "2022-10-11 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-11 17:00:00",
                "end" => "2022-10-11 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 10:00:00",
                "end" => "2022-10-12 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 13:00:00",
                "end" => "2022-10-12 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-12 17:00:00",
                "end" => "2022-10-12 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 10:00:00",
                "end" => "2022-10-13 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-13 17:00:00",
                "end" => "2022-10-13 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dateRange", "scope": ["2022-10-10","2022-10-14"], "period": "10:00-12:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 10:00:00",
                "end" => "2022-10-14 12:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "dates", "scope": ["2022-10-10","2022-10-12","2022-10-14"], "period": "13:00-17:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 13:00:00",
                "end" => "2022-10-14 17:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["MO","TU","TH","WE","FR"], "period": "17:00-22:00"
                "type" => "availability_rule",
                "start" => "2022-10-14 17:00:00",
                "end" => "2022-10-14 22:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            [
                // "type": "weekdays", "scope": ["SA","SU"], "period":"00:00-24:00"
                "type" => "availability_rule",
                "start" => "2022-10-15 00:00:00",
                "end" => "2022-10-16 00:00:00",
                "uri" => "/loanables/{$loanable->id}",
                "data" => ["available" => false],
            ],
            // Loans
            [
                "type" => "loan",
                "start" => "2022-09-30 00:00:00",
                "end" => "2022-11-13 00:00:00",
                "uri" => "/loans/{$loanMoreThanOneMonth->id}",
                "data" => ["status" => "in_process"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-11 11:15:00",
                "end" => "2022-10-11 21:15:00",
                "uri" => "/loans/{$loanLessThanOneDay->id}",
                "data" => ["status" => "in_process"],
            ],
            [
                "type" => "loan",
                "start" => "2022-10-12 22:45:00",
                "end" => "2022-10-15 22:45:00",
                "uri" => "/loans/{$loanMoreThanOneDay->id}",
                "data" => ["status" => "in_process"],
            ],
        ]);
    }

    public function testListLoanablesValidation()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        // Complete valid request
        $this->json("GET", route("loanables.list"))->assertStatus(200);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car",
            ])
        )->assertStatus(200);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,bike,trailer",
            ])
        )->assertStatus(200);
        $this->json("GET", route("loanables.list", ["types" => "couch"]))
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "types" => [
                        "Les types demandés sont invalides. Options possibles: bike,trailer,car.",
                    ],
                ],
            ]);
    }

    public function testListLoanables_doesntReturnUnaccessibleLoanables()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()->create();
        $otherCommunity = Community::factory()->create();

        $borrowerUser = User::factory()->create();
        $borrowerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $otherCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()->create([
            "owner_id" => $owner->id,
            "availability_mode" => "never",
        ]);
        Loanable::factory()
            ->withBike()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
            ]);
        Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
            ]);

        $this->actAs($borrowerUser);
        $this->json(
            "GET",
            route("loanables.list", [
                "types" => "car,trailer,bike",
            ])
        )
            ->assertStatus(200)
            ->assertExactJson([]);
    }

    public function testListLoanables_returnsDetailedCars()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "always",
            ]);

        $this->json("GET", route("loanables.list", ["types" => "car"]))
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    "id",
                    "type",
                    "name",
                    "position_google",
                    "is_self_service",
                    "comments",
                    "owner" => [
                        "user" => ["id", "full_name", "avatar"],
                    ],
                    "image" => [],
                    "details" => [
                        "brand",
                        "engine",
                        "transmission_mode",
                        "year_of_circulation",
                        "papers_location",
                        "model",
                    ],
                ],
            ])
            ->assertJsonCount(1);
    }

    public function testListLoanables_returnsDetailedBikes()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()->create([
            "owner_id" => $owner->id,
            "availability_mode" => "always",
        ]);

        $this->json("GET", route("loanables.list", ["types" => "bike"]))
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    "id",
                    "type",
                    "name",
                    "position_google",
                    "is_self_service",
                    "comments",
                    "owner" => [
                        "user" => ["id", "full_name", "avatar"],
                    ],
                    "image" => [],
                    "details" => ["bike_type", "model", "size"],
                ],
            ])
            ->assertJsonCount(1);
    }

    public function testListLoanables_returnsDetailedTrailers()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "always",
            ]);

        $this->json("GET", route("loanables.list", ["types" => "trailer"]))
            ->assertStatus(200)
            ->assertJsonStructure([
                [
                    "id",
                    "type",
                    "name",
                    "position_google",
                    "is_self_service",
                    "comments",
                    "owner" => [
                        "user" => ["id", "full_name", "avatar"],
                    ],
                    "image" => [],
                    "details" => ["maximum_charge", "dimensions"],
                ],
            ])
            ->assertJsonCount(1);
    }

    public function testListLoanables_hidesUnavailableLoanables()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
            ]);
        Loanable::factory()->create([
            "owner_id" => $owner->id,
            "availability_mode" => "never",
        ]);
        Loanable::factory()
            ->withBike()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
            ]);

        $this->json("GET", route("loanables.list"))
            ->assertStatus(200)
            ->assertExactJson([]);
    }

    public function testListLoanables_showsPartiallyAvailableLoanables()
    {
        // Avoid triggering emails
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
            ]);
        Loanable::factory()->create([
            "owner_id" => $owner->id,
            "availability_mode" => "never",
            "availability_json" =>
                '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
        ]);
        Loanable::factory()
            ->withBike()
            ->create([
                "owner_id" => $owner->id,
                "availability_mode" => "never",
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["SU"],"period":"00:00-24:00"}]',
            ]);

        $this->json("GET", "/api/v1/loanables/list")
            ->assertStatus(200)
            ->assertJsonCount(3);
    }

    public function testSearchLoanablesValidation()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()->create([
            "availability_mode" => "always",
            "owner_id" => $owner->id,
        ]);

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])->assertStatus(200);

        // Departure missing
        $this->json("GET", route("loanables.search"), [
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "departure_at" => ["validation.required"],
                ],
            ]);

        // Duration 0
        $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "duration_in_minutes" => ["validation.min.numeric"],
                ],
            ]);
    }

    public function testSearchLoanables_findsLoanable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        $carToFind = Loanable::factory()->create([
            "availability_mode" => "always",
            "owner_id" => $owner->id,
        ]);

        $borrowerUser = User::factory()->create();
        Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        // Non-overlapping loan after
        $loanAfter = Loan::factory()->create([
            "loanable_id" => $carToFind->id,
            "departure_at" => Carbon::now()
                ->addDay()
                ->format("Y-m-d H:i:s"),
        ]);
        // Non-overlapping loan before
        $loanBefore = Loan::factory()->create([
            "loanable_id" => $carToFind->id,
            "departure_at" => Carbon::now()
                ->subDay()
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
        ]);
        Intention::factory()->create([
            "status" => "completed",
            "loan_id" => $loanAfter->id,
        ]);
        Intention::factory()->create([
            "status" => "completed",
            "loan_id" => $loanBefore->id,
        ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertJson([$carToFind->id]);
    }

    public function testSearchLoanables_ignoresLoanableFromOtherCommunity()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $otherCommunity = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $borrowerUser = User::factory()->create();
        Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $otherCommunity->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        Loanable::factory()->create([
            "availability_mode" => "always",
            "owner_id" => $owner->id,
        ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_ignoresLoanableWithOverlappingLoans()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();

        $borrowerUser = User::factory()->create();
        Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        $carToIgnore = Loanable::factory()->create([
            "availability_mode" => "always",
            "owner_id" => $owner->id,
        ]);
        $loan = Loan::factory()->create([
            "loanable_id" => $carToIgnore->id,
            "departure_at" => Carbon::now()
                ->subMinutes(5)
                ->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
        ]);
        Intention::factory()->create([
            "status" => "completed",
            "loan_id" => $loan->id,
        ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_ignoresLoanableWhenUnavailable()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $borrowerUser = User::factory()->create();
        Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);
        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        Loanable::factory()->create([
            "availability_mode" => "never",
            "owner_id" => $owner->id,
        ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $response->assertStatus(200)->assertJson([]);
    }

    public function testSearchLoanables_estimatesWithPricing()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $community = Community::factory()->create();
        $this->user->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $borrowerUser = User::factory()->create();
        Borrower::factory()->create([
            "user_id" => $borrowerUser->id,
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($borrowerUser);
        $borrowerUser->communities()->attach($community->id, [
            "approved_at" => Carbon::now(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $carToFind = Loanable::factory()
            ->withCar()
            ->create([
                "availability_mode" => "always",
                "owner_id" => $owner->id,
            ]);

        $bikeToFind = Loanable::factory()->create([
            "availability_mode" => "always",
            "owner_id" => $owner->id,
        ]);

        $trailerToFind = Loanable::factory()
            ->withTrailer()
            ->create([
                "availability_mode" => "always",
                "owner_id" => $owner->id,
            ]);

        Pricing::factory()->create([
            "community_id" => $community->id,
            "rule" => "[10,3]",
            "object_type" => "car",
            "name" => "car pricing name",
        ]);
        Pricing::factory()->create([
            "community_id" => $community->id,
            "rule" => "7",
            "object_type" => "bike",
            "name" => "bike pricing name",
        ]);
        Pricing::factory()->create([
            "community_id" => $community->id,
            "rule" => "0",
            "object_type" => null,
            "name" => "default pricing",
        ]);

        $response = $this->json("GET", route("loanables.search"), [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ]);

        $this->assertJsonUnordered($response->json(), [
            $carToFind->id,
            $bikeToFind->id,
            $trailerToFind->id,
        ]);
    }

    public function testOrderLoanablesById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testOrderLoanablesByName()
    {
        $data = [
            "order" => "name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testOrderLoanablesByType()
    {
        $data = [
            "order" => "type",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testOrderLoanablesByOwnerFullName()
    {
        $data = [
            "order" => "owner.user.full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" =>
                "id,name,type,owner.id,owner.user.full_name,owner.user.id,deleted_at",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "id" => "4",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "name" => "Vélo",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByType()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,type",
            "type" => "car",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testFilterLoanablesByDeletedAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "deleted_at" => "@",
        ];
        $response = $this->json("GET", route("loanables.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getLoanablesResponseStructure);
    }

    public function testSearchLoanables()
    {
        Loanable::factory()->create([
            "name" => "Loanable name",
        ]);
        Loanable::factory()->create([
            "name" => "Other name",
        ]);

        $data = [
            "q" => "able NA",
        ];
        $response = $this->json(
            "GET",
            route("loanables.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testRetrieveNextLoan()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();

        $borrower = Borrower::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $community = Community::factory()->create();

        $user = User::factory()->create();
        $user
            ->communities()
            ->attach($community->id, ["approved_at" => new \DateTime()]);

        $owner = Owner::factory()->create(["user_id" => $user->id]);
        $loanable = Loanable::factory()->create(["owner_id" => $owner->id]);

        $departure = new Carbon();

        $data = Loan::factory()
            ->make([
                "duration_in_minutes" => 30,
                "borrower_id" => $borrower->id,
                "loanable_id" => $loanable->id,
                "community_id" => $community->id,
            ])
            ->toArray();

        // Create a loan that departs in 1 hour and lasts 30 minutes.
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->add(1, "hour")
                    ->toDateTimeString(),
            ])
        );
        $response->assertStatus(201);
        $nextLoanId = $response->json()["id"];

        // Create a loan that departs in 2 hours and lasts 30 minutes.
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->add(2, "hour")
                    ->toDateTimeString(),
            ])
        );
        $response->assertStatus(201);
        $nextNextLoanId = $response->json()["id"];

        // Create a loan that departed 4 hours ago and lasts 30 minutes.
        $response = $this->json(
            "POST",
            "/api/v1/loans",
            array_merge($data, [
                "departure_at" => $departure
                    ->subtract(4, "hour")
                    ->toDateTimeString(),
            ])
        );
        $response->assertStatus(201);
        $currentLoanId = $response->json()["id"];

        $now = new Carbon();

        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/loans",
            [
                "order" => "departure_at",
                "departure_at" => $now->toISOString() . "@",
                "!id" => $currentLoanId,
                "per_page" => 1,
            ]
        );
        $response->assertStatus(200)->assertJson([
            "data" => [
                [
                    "id" => $nextLoanId,
                ],
            ],
        ]);

        // Shortcut request
        $response = $this->json(
            "GET",
            "/api/v1/loanables/{$loanable->id}/loans/{$nextLoanId}/next"
        );
        $response->assertStatus(200)->assertJson([
            "id" => $nextNextLoanId,
        ]);
    }

    public function testRetrieveLoanableForOwner_showsInstructions()
    {
        $ownerUser = User::factory()->create();
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);

        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "instructions" => "test",
        ]);

        $this->actAs($ownerUser);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testRetrieveLoanableForAdmin_showsInstructions()
    {
        $ownerUser = User::factory()->create();
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "instructions" => "test",
        ]);

        $admin = User::factory()->create(["role" => "admin"]);

        $this->actAs($admin);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonFragment([
            "instructions" => "test",
        ]);
    }

    public function testRetrieveLoanable_hidesInstructions()
    {
        $this->withoutEvents();

        $community = Community::factory()->create();

        $ownerUser = User::factory()->create();
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
            "instructions" => "test",
        ]);

        $otherUser = User::factory()->create();

        // Other user has access to loanable but not to instructions
        $otherUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $this->actAs($otherUser);
        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}");

        $response->assertJsonMissing([
            "instructions" => "test",
        ]);
    }

    public function testLoanableTestEndpointValidation()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])->assertStatus(200);

        // Departure missing
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "departure_at" => ["validation.required"],
                ],
            ]);

        // Community missing: OK
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])->assertStatus(200);

        // Loanable missing
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "loanable_id" => ["validation.required"],
                ],
            ]);

        // Duration 0
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "duration_in_minutes" => ["validation.min.numeric"],
                ],
            ]);

        // Estimated distance negative
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 0,
            "estimated_distance" => -1,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(422)
            ->assertJson([
                "errors" => [
                    "estimated_distance" => ["validation.min.numeric"],
                ],
            ]);
    }

    public function testLoanableTestEndpointValidation_WithNullPricing()
    {
        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();

        $community = Community::factory()->create();
        $this->user->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);
        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $this->setTestLocale();

        // Complete valid request
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            "community_id" => $community->id,
        ])->assertStatus(200);
    }

    public function testLoanableTestAvailability()
    {
        $this->withoutEvents();

        $loanable = Loanable::factory()->create();

        $borrower = Borrower::factory()->create([
            "user_id" => User::factory()->withCommunity(
                $loanable->owner->user->main_community
            ),
        ]);

        $this->actAs($borrower->user);
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => true]);
    }

    public function testLoanableTestAvailability_whenUnavailable()
    {
        $this->withoutEvents();

        $loanable = Loanable::factory()->create();

        $borrower = Borrower::factory()->create([
            "user_id" => User::factory()->withCommunity(
                $loanable->owner->user->main_community
            ),
        ]);

        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->subHour(),
                "duration_in_minutes" => 120,
            ]);

        $this->actAs($borrower->user);
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => false]);
    }

    public function testLoanableTestAvailability_ignoresLoan()
    {
        $this->withoutEvents();

        $loanable = Loanable::factory()->create();

        $borrower = Borrower::factory()->create([
            "user_id" => User::factory()->withCommunity(
                $loanable->owner->user->main_community
            ),
        ]);

        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $loanable,
                "departure_at" => Carbon::now()->subHour(),
                "duration_in_minutes" => 120,
            ]);

        $this->actAs($borrower->user);
        $this->json("GET", "/api/v1/loanables/{$loanable->id}/test", [
            "departure_at" => Carbon::now()->format("Y-m-d H:i:s"),
            "duration_in_minutes" => 20,
            "estimated_distance" => 0,
            "loanable_id" => $loanable->id,
            // This loan_id is ignored when computing availability
            "loan_id" => $loan->id,
        ])
            ->assertStatus(200)
            ->assertJson(["available" => true]);
    }

    public function testUpdateOwner_forbiddenForOwner()
    {
        $this->withoutEvents();

        $owner = Owner::factory()->create();
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);
        $otherOwner = Owner::factory()->create();

        $this->actAs($otherOwner->user);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_id" => $otherOwner->id,
        ])->assertStatus(403);

        $this->actAs($owner->user);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_id" => $otherOwner->id,
        ])->assertStatus(403);
    }

    public function testUpdateOwner_allowedForGlobalAdmin()
    {
        $this->withoutEvents();

        $loanable = Loanable::factory()->create();
        $admin = User::factory()->create(["role" => "admin"]);
        $otherOwner = Owner::factory()->create();

        $this->actAs($admin);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_id" => $otherOwner->id,
        ])->assertStatus(200);
    }

    public function testUpdateOwner_allowedForCommunityAdmin()
    {
        $this->withoutEvents();

        $community = Community::factory()->create();
        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => Carbon::now()]);
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);
        $communityAdmin = User::factory()->create();
        $communityAdmin->communities()->attach($community->id, [
            "role" => "admin",
            "approved_at" => Carbon::now(),
        ]);
        $otherOwner = Owner::factory()->create();
        $otherOwner->user->communities()->attach($community->id, [
            "role" => "admin",
            "approved_at" => Carbon::now(),
        ]);

        $this->actAs($communityAdmin);
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "type" => "bike",
            "id" => $loanable->id,
            "owner_id" => $otherOwner->id,
        ])->assertStatus(200);
    }

    public function testAddCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);

        $this->actAs($ownerUser);
        Mail::fake();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}/coowners", [
            "user_id" => $coOwnerUser->id,
        ])->assertStatus(201);
        $loanable->refresh();

        Mail::assertQueued(CoownerAddedMail::class, function ($mail) use (
            $coOwnerUser
        ) {
            return $mail->hasTo($coOwnerUser->email);
        });

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "coowners.*",
        ]);
        $response->assertJson([
            "coowners" => [
                [
                    "user_id" => $coOwnerUser->id,
                    "loanable_id" => $loanable->id,
                ],
            ],
        ]);
    }

    public function testAddCoowner_failsWithNoSharedCommunities()
    {
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);

        $this->actAs($ownerUser);

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}/coowners", [
            "user_id" => $coOwnerUser->id,
        ])->assertStatus(422);
        $loanable->refresh();

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "coowners.*",
        ]);
        $response->assertJson([
            "coowners" => [],
        ]);
    }

    public function testAddCoowner_failsForSelf()
    {
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);

        $this->actAs($coOwnerUser);

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}/coowners", [
            "user_id" => $coOwnerUser->id,
        ])->assertStatus(403);
        $loanable->refresh();
    }

    public function testRemoveCoowner()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);
        $coowner = $loanable->addCoowner($coOwnerUser->id);

        Mail::fake();
        $this->actAs($ownerUser);
        $this->json("DELETE", "/api/v1/coowners/{$coowner->id}")->assertStatus(
            200
        );
        $loanable->refresh();
        Mail::assertQueued(CoownerRemovedMail::class, function (
            Mailable $mailable
        ) use ($coowner) {
            return $mailable->hasTo($coowner->user->email);
        });

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "coowners.*",
        ]);
        $response->assertJsonCount(0, "coowners");
    }

    public function testRemoveCoowner_succeedsForSelf()
    {
        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);
        $coowner = $loanable->addCoowner($coOwnerUser->id);

        Mail::fake();
        $this->actAs($coOwnerUser);
        $this->json("DELETE", "/api/v1/coowners/{$coowner->id}")->assertStatus(
            200
        );
        $loanable->refresh();
        // Doesn't send emails for self
        Mail::assertNothingSent();

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "coowners.*",
        ]);
        $response->assertJsonCount(0, "coowners");
    }

    public function testRemoveCoowner_failsForDifferentCoowner()
    {
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $coOwnerUser = User::factory()->create();
        $coOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $otherCoOwnerUser = User::factory()->create();
        $otherCoOwnerUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);
        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        $loanable = Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);
        $coowner = $loanable->addCoowner($coOwnerUser->id);
        $otherCoowner = $loanable->addCoowner($otherCoOwnerUser->id);

        $this->actAs($coOwnerUser);
        $this->json(
            "DELETE",
            "/api/v1/coowners/{$otherCoowner->id}"
        )->assertStatus(403);
    }

    public function testRemoveCoowner_failsForNonCoowner()
    {
        $this->withoutEvents();

        $community = Community::factory()
            ->withDefaultFreePricing()
            ->create();
        $randomUser = User::factory()->create();
        $randomUser->communities()->attach($community->id, [
            "approved_at" => new \DateTime(),
        ]);

        $ownerUser = User::factory()->create();
        $ownerUser->communities()->sync([
            $community->id => [
                "approved_at" => Carbon::now(),
            ],
        ]);

        $owner = Owner::factory()->create([
            "user_id" => $ownerUser->id,
        ]);

        Loanable::factory()
            ->withTrailer()
            ->create([
                "owner_id" => $owner->id,
            ]);

        $this->actAs($ownerUser);
        $this->json("DELETE", "/api/v1/coowners/{$owner->id}")->assertStatus(
            404
        );
    }

    public function testAddPadlock()
    {
        $this->withoutEvents();

        $admin = User::factory()->create(["role" => "admin"]);
        $this->actAs($admin);

        $loanable = Loanable::factory()->create();
        $padlock = Padlock::factory()->create();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock" => $padlock,
        ])->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "padlock.*",
        ]);

        $response->assertJson([
            "id" => $loanable->id,
            "padlock" => [
                "id" => $padlock->id,
            ],
        ]);
    }

    public function testUpdatePadlock()
    {
        $this->withoutEvents();

        $admin = User::factory()->create(["role" => "admin"]);
        $this->actAs($admin);

        $loanable = Loanable::factory()
            ->withPadlock()
            ->create();

        $newPadlock = Padlock::factory()->create();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock" => $newPadlock,
        ])->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "padlock.*",
        ]);

        $response->assertJson([
            "id" => $loanable->id,
            "padlock" => [
                "id" => $newPadlock->id,
            ],
        ]);
    }

    public function testRemovePadlock()
    {
        $this->withoutEvents();

        $admin = User::factory()->create(["role" => "admin"]);
        $this->actAs($admin);

        $loanable = Loanable::factory()
            ->withPadlock()
            ->create();

        $padlock = Padlock::factory()->create();

        // Removing requires setting padlock_id to null, which is not the same
        // as changing the padlock where we set the padlock directly.
        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock_id" => null,
        ])->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "padlock.*",
        ]);

        $response->assertJson([
            "id" => $loanable->id,
            "padlock" => null,
        ]);
    }

    public function testUpdatePadlock_allowedForGlobalAdmin()
    {
        $this->withoutEvents();

        $admin = User::factory()->create(["role" => "admin"]);
        $this->actAs($admin);

        $loanable = Loanable::factory()->create();
        $padlock = Padlock::factory()->create();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock" => $padlock,
        ])->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/{$loanable->id}", [
            "fields" => "padlock.*",
        ]);

        $response->assertJson([
            "id" => $loanable->id,
            "padlock" => [
                "id" => $padlock->id,
            ],
        ]);
    }

    public function testUpdatePadlock_forbiddenForCommunityAdmin()
    {
        $this->withoutEvents();

        $community = Community::factory()->create();

        $communityAdmin = User::factory()->create();
        $communityAdmin->communities()->attach($community->id, [
            "role" => "admin",
            "approved_at" => Carbon::now(),
        ]);
        $this->actAs($communityAdmin);

        // Owner must be in the same community.
        $ownerUser = User::factory()->create();
        $ownerUser
            ->communities()
            ->attach($community->id, ["approved_at" => Carbon::now()]);
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);

        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $padlock = Padlock::factory()->create();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock" => $padlock,
        ])->assertStatus(403);
    }

    public function testUpdatePadlock_forbiddenForOwner()
    {
        $this->withoutEvents();

        $ownerUser = User::factory()->create();
        $owner = Owner::factory()->create(["user_id" => $ownerUser->id]);

        $this->actAs($ownerUser);

        $loanable = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $padlock = Padlock::factory()->create();

        $this->json("PUT", "/api/v1/loanables/{$loanable->id}", [
            "padlock" => $padlock,
        ])->assertStatus(403);
    }

    public function testDashboard()
    {
        $owner = Owner::factory()->create();

        $trailer = Loanable::factory()
            ->withTrailer()
            ->withImage()
            ->create([
                "owner_id" => $owner->id,
            ]);
        $bike = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);
        $car = Loanable::factory()->create([
            "owner_id" => $owner->id,
        ]);

        $coOwnedTrailer = Loanable::factory()
            ->withTrailer()
            ->withImage()
            ->create();
        $coOwnedTrailer->addCoowner($owner->user->id);

        $this->actAs($owner->user);
        $response = $this->json("GET", "/api/v1/loanables/dashboard");

        $response->assertStatus(200);
        $this->assertJsonUnordered($response->json(), [
            "owned" => [
                "loanables" => [
                    [
                        "id" => $trailer->id,
                        "image" => [
                            "path" => $trailer->image->path,
                        ],
                    ],
                    [
                        "id" => $bike->id,
                    ],
                    [
                        "id" => $car->id,
                    ],
                ],
                "total" => 3,
            ],
            "coowned" => [
                "loanables" => [
                    [
                        "id" => $coOwnedTrailer->id,
                        "image" => [
                            "path" => $coOwnedTrailer->image->path,
                        ],
                    ],
                ],
                "total" => 1,
            ],
        ]);
    }

    public function testGetUnavailableLoans_onlyReturnsUnavailableLoans()
    {
        $now = CarbonImmutable::now();
        $loanable = Loanable::factory()->create();
        // Available loan for loanable
        Loan::factory()->create([
            "departure_at" => $now->addMinute(),
            "duration_in_minutes" => 30,
            "loanable_id" => $loanable->id,
        ]);
        // unavailable loan for other loanable
        Loan::factory()->create([
            "departure_at" => $now->addDay(),
            "duration_in_minutes" => 30,
        ]);
        $unavailableLoan = Loan::factory()->create([
            "departure_at" => $now->addDay(),
            "duration_in_minutes" => 30,
            "loanable_id" => $loanable->id,
        ]);

        // With a 'only avaialble today' rule
        $response = $this->json(
            "GET",
            "api/v1/loanables/$loanable->id/loans/unavailable",
            [
                "availability_mode" => "never",
                "availability_json" => <<<JSON
[
    { 
        "available":  true,
        "type":  "dates",
        "scope":  ["{$now->format("Y-m-d")}"],
        "period": "00:00-24:00"
    }
]
JSON
            ,
            ]
        );

        $response->assertJsonCount(1);
        $response->assertJson([
            [
                "id" => $unavailableLoan->id,
            ],
        ]);
    }

    public function testDelete_mustNotHaveOngoingLoansAsOwner()
    {
        $loanable = Loanable::factory()->create();
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $response = $this->json("DELETE", "/api/v1/loanables/$loanable->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        $loan->cancel();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/loanables/$loanable->id");

        $response->assertStatus(200);
    }

    public function testRestore_removesAvailabilities()
    {
        $loanable = Loanable::factory()->create([
            "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            "availability_mode" => "always",
        ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertEquals("never", $loanable->availability_mode);
        self::assertEquals("[]", $loanable->availability_json);
    }

    public function testRestore_canRestoreAvailabilities()
    {
        $loanable = Loanable::factory()->create([
            "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            "availability_mode" => "always",
        ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore?restore_availability=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertEquals("always", $loanable->availability_mode);
        self::assertEquals(
            <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            $loanable->availability_json
        );
    }
}
