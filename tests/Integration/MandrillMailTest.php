<?php

use App\Mail\Loan\LoanCompleted;
use App\Mail\Registration\Approved;
use App\Mail\Registration\Submitted;
use App\Mail\Transport\MandrillTransport;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\User;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use Tests\TestCase;

class MandrillMailTest extends TestCase
{
    private array $guzzleHistory;

    public function setUpFakeMandrillTransport(
        array $config = ["secret" => "fake-key"],
        array $guzzleResponses = [new \GuzzleHttp\Psr7\Response()]
    ): void {
        $this->withoutEvents();

        $mock = new MockHandler($guzzleResponses);
        $this->guzzleHistory = [];
        $history = Middleware::history($this->guzzleHistory);
        $handler = HandlerStack::create($mock);
        $handler->push($history);
        $client = new Client(["handler" => $handler]);

        $transport = new MandrillTransport($config, $client);

        Mail::mailer("mandrill")->setSymfonyTransport($transport);
    }

    public function testMailMandrill_LoanCompleted()
    {
        $this->setUpFakeMandrillTransport();

        $borrower = User::factory()
            ->withBorrower()
            ->create();
        $ownerUser = User::factory()
            ->withOwner()
            ->withCommunity()
            ->create();
        $loanable = Loanable::factory()
            ->withBike()
            ->create([
                "owner_id" => $ownerUser->owner,
            ]);

        $loan = Loan::factory()->create([
            "borrower_id" => $borrower->borrower,
            "loanable_id" => $loanable,
            "duration_in_minutes" => 185,
        ]);
        Mail::mailer("mandrill")->send(
            // Mandrill mailable
            (new LoanCompleted($borrower, $loan, false))->to(
                $borrower->email,
                $borrower->name
            )
        );

        /** @var \GuzzleHttp\Psr7\Request $request */
        self::assertCount(1, $this->guzzleHistory);

        $request = $this->guzzleHistory[0]["request"];
        self::assertJsonUnordered(
            [
                "key" => "fake-key",
                "template_name" => "loan-completed",
                "template_content" => [
                    [
                        "name" => "main",
                        "content" => " ",
                    ],
                ],
                "message" => [
                    "subject" => "Emprunt Complété!",
                    "from_email" => "soutien@locomotion.app",
                    "from_name" => "LocoMotion",
                    "to" => [
                        [
                            "email" => "$borrower->email",
                            "name" => "$borrower->name",
                            "type" => "to",
                        ],
                    ],
                    "global_merge_vars" => [
                        [
                            "name" => "FNAME",
                            "content" => "$borrower->name",
                        ],
                        [
                            "name" => "email",
                            "content" => "$borrower->email",
                        ],
                        ["name" => "isOwner", "content" => ""],
                        [
                            "name" => "loanableName",
                            "content" => "$loanable->name",
                        ],
                        [
                            "name" => "borrowerName",
                            "content" => "$borrower->name",
                        ],
                        [
                            "name" => "duration",
                            "content" => "3h 5m",
                        ],
                        [
                            "name" => "departureAt",
                            "content" => "$loan->departure_at",
                        ],
                        ["name" => "loanableType", "content" => "Vélo"],
                    ],
                    "tags" => [
                        "locomotion",
                        "locomotion_template",
                        "locomotion_template_loan-completed",
                    ],
                    "auto_text" => true,
                ],
            ],
            json_decode($request->getBody()->getContents(), true)
        );
    }

    public function testMailMandrill_UserSubmitted()
    {
        $this->setUpFakeMandrillTransport();

        $user = User::factory()
            ->withBorrower()
            ->create();

        $admin = User::factory(["role" => "admin"])->create();

        Mail::mailer("mandrill")->send(
            // Mandrill mailable
            (new Submitted($user))->to($admin->email, $admin->name)
        );

        /** @var \GuzzleHttp\Psr7\Request $request */
        self::assertCount(1, $this->guzzleHistory);

        $request = $this->guzzleHistory[0]["request"];
        self::assertJsonUnordered(
            [
                "key" => "fake-key",
                "template_name" => "registration-submitted",
                "template_content" => [
                    [
                        "name" => "main",
                        "content" => " ",
                    ],
                ],
                "message" => [
                    "subject" =>
                        "Bienvenue dans LocoMotion, vous y êtes presque!",
                    "from_email" => "soutien@locomotion.app",
                    "from_name" => "LocoMotion",
                    "to" => [
                        [
                            "email" => "$admin->email",
                            "name" => "$admin->name",
                            "type" => "to",
                        ],
                    ],
                    "global_merge_vars" => [
                        [
                            "name" => "FNAME",
                            "content" => $user->name,
                        ],
                        [
                            "name" => "full_name",
                            "content" => $user->full_name,
                        ],
                        [
                            "name" => "last_name",
                            "content" => $user->last_name,
                        ],
                        [
                            "name" => "title",
                            "content" =>
                                "Bienvenue dans LocoMotion, vous y êtes presque!",
                        ],
                    ],
                    "tags" => [
                        "locomotion",
                        "locomotion_template",
                        "locomotion_template_registration-submitted",
                    ],
                    "auto_text" => true,
                ],
            ],
            json_decode($request->getBody()->getContents(), true)
        );
    }

    public function testMailMandrill_Approved()
    {
        $this->setUpFakeMandrillTransport();

        $community = Community::factory([
            "starting_guide_url" => "www.startingguide.url",
            "name" => "Foo Community",
        ]);
        $user = User::factory()
            ->withCommunity($community)
            ->create();

        Mail::mailer("mandrill")->send(
            (new Approved($user->main_community->pivot))->to(
                $user->email,
                $user->name
            )
        );

        /** @var \GuzzleHttp\Psr7\Request $request */
        self::assertCount(1, $this->guzzleHistory);

        $request = $this->guzzleHistory[0]["request"];
        self::assertJsonUnordered(
            [
                "key" => "fake-key",
                "template_name" => "registration-approved",
                "template_content" => [
                    [
                        "name" => "main",
                        "content" => " ",
                    ],
                ],
                "message" => [
                    "subject" => "Bienvenue dans LocoMotion, c'est parti!",
                    "from_email" => "soutien@locomotion.app",
                    "from_name" => "LocoMotion",
                    "to" => [
                        [
                            "email" => "$user->email",
                            "name" => "$user->name",
                            "type" => "to",
                        ],
                    ],
                    "global_merge_vars" => [
                        [
                            "name" => "FNAME",
                            "content" => $user->name,
                        ],
                        [
                            "name" => "NEIGHBOURHOOD",
                            "content" => "Foo Community",
                        ],
                        [
                            "name" => "full_name",
                            "content" => $user->full_name,
                        ],
                        [
                            "name" => "last_name",
                            "content" => $user->last_name,
                        ],
                        [
                            "name" => "title",
                            "content" =>
                                "Bienvenue dans LocoMotion, c'est parti!",
                        ],
                        [
                            "name" => "starting_guide_url",
                            "content" => "www.startingguide.url",
                        ],
                    ],
                    "tags" => [
                        "locomotion",
                        "locomotion_template",
                        "locomotion_template_registration-approved",
                    ],
                    "auto_text" => true,
                ],
            ],
            json_decode($request->getBody()->getContents(), true)
        );
    }

    public function testMailMandrill_logsWhenKeyNotPresentInTesting()
    {
        $this->setUpFakeMandrillTransport([
            // no key in config
        ]);
        $user = User::factory()
            ->withCommunity()
            ->create();

        $logAssertion = Log::shouldReceive("info")
            ->with(
                "Would have sent 'Bienvenue dans LocoMotion, c'est parti!' mandrill message to $user->email with template registration-approved"
            )
            ->once();

        Log::shouldReceive("info")->with(
            "OK Sending App\Mail\Registration\Approved to $user->email"
        );

        Mail::mailer("mandrill")->send(
            (new Approved($user->main_community->pivot))->to(
                $user->email,
                $user->name
            )
        );

        $logAssertion->verify();
    }

    public function testMailMandrill_logsErrorWhenKeyNotPresentInProduction()
    {
        $this->setUpFakeMandrillTransport([
            // no key in config
        ]);
        $user = User::factory()
            ->withCommunity()
            ->create();

        App::shouldReceive("environment")
            ->once()
            ->andReturn("production");

        $logAssertion = Log::shouldReceive("error")
            ->with("Cannot send email, Mandrill key missing.")
            ->once();

        Log::shouldReceive("info")
            ->with(
                "Would have sent 'Bienvenue dans LocoMotion, c'est parti!' mandrill message to $user->email with template registration-approved"
            )
            ->once();
        Log::shouldReceive("info")->with(
            "OK Sending App\Mail\Registration\Approved to $user->email"
        );

        Mail::mailer("mandrill")->send(
            (new Approved($user->main_community->pivot))->to(
                $user->email,
                $user->name
            )
        );

        $logAssertion->verify();
    }

    public function testMailMandrill_throwsWhenMandrillErrors()
    {
        $this->setUpFakeMandrillTransport(
            [
                "secret" => "fake_key",
            ],
            [new \GuzzleHttp\Psr7\Response(500)]
        );
        $user = User::factory()
            ->withCommunity()
            ->create();

        $this->expectException(\GuzzleHttp\Exception\ServerException::class);

        Mail::mailer("mandrill")->send(
            (new Approved($user->main_community->pivot))->to(
                $user->email,
                $user->name
            )
        );
    }
}
