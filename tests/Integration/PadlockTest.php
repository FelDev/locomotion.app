<?php

namespace Tests\Integration;

use App\Models\Padlock;
use Tests\TestCase;

class PadlockTest extends TestCase
{
    private static $getPadlocksResponseStructure = [
        "current_page",
        "data",
        "first_page_url",
        "from",
        "last_page",
        "last_page_url",
        "next_page_url",
        "path",
        "per_page",
        "prev_page_url",
        "to",
        "total",
    ];

    public function testShowPadlocks()
    {
        $padlock = Padlock::factory()->create();

        $response = $this->json(
            "GET",
            route("padlocks.retrieve", $padlock->id)
        );

        $response->assertStatus(200)->assertJson($padlock->toArray());
    }

    public function testOrderPadlocksById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" => "*,loanable.name",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testOrderPadlocksByName()
    {
        $data = [
            "order" => "name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "*,loanable.name",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testOrderPadlocksByMacAddress()
    {
        $data = [
            "order" => "mac_address",
            "page" => 1,
            "per_page" => 10,
            "fields" => "*,loanable.name",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testOrderPadlocksByLoanableName()
    {
        $data = [
            "order" => "loanable.name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "*,loanable.name",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testFilterPadlocksByExternalId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "external_id" => "20345",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testFilterPadlocksByName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "name" => "Cadenas",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testFilterPadlocksByLoanableId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.id" => "4",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testFilterPadlocksByLoanableName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "loanable.name" => "Auto",
        ];
        $response = $this->json("GET", route("padlocks.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getPadlocksResponseStructure);
    }

    public function testSearchPadlocks()
    {
        Padlock::factory()->create([
            "name" => "Padlock name",
        ]);
        Padlock::factory()->create([
            "name" => "Other name",
        ]);

        $data = [
            "q" => "lock NA",
        ];
        $response = $this->json(
            "GET",
            route("padlocks.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testUpdatePadlocks()
    {
        $padlock = Padlock::factory()->create();
        $data = [
            "mac_address" => $this->faker->macAddress,
        ];
        $response = $this->json(
            "GET",
            route("padlocks.retrieve", $padlock->id)
        )->assertJson(["mac_address" => $padlock->mac_address]);

        $response = $this->json(
            "PUT",
            route("padlocks.update", $padlock->id),
            $data
        );

        $response->assertStatus(200)->assertJson($data);

        $response = $this->json(
            "GET",
            route("padlocks.retrieve", $padlock->id)
        )->assertJson($data);
    }

    public function testListPadlocks()
    {
        $padlocks = Padlock::factory(2)
            ->create()
            ->map(function ($padlock) {
                return $padlock->only(["id", "mac_address"]);
            });

        $response = $this->json(
            "GET",
            route("padlocks.index", [
                "order" => "id",
            ])
        );

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                "*" => ["id", "mac_address"],
            ],
        ]);

        $this->assertJsonUnordered($response->json(), [
            "data" => $padlocks->toArray(),
        ]);
    }
}
