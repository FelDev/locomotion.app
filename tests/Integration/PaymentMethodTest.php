<?php

namespace Tests\Integration;

use App\Models\PaymentMethod;
use Stripe;
use Tests\TestCase;

class PaymentMethodTest extends TestCase
{
    public static $responseStructure = [
        "name",
        "external_id",
        "four_last_digits",
        "credit_card_type",
        "user_id",
        "is_default",
        "id",
    ];

    public function testCreatePaymentMethods()
    {
        $data = [
            "name" => $this->faker->name,
            "external_id" => $this->faker->sentence,
            "four_last_digits" => $this->faker->randomNumber(
                $nbDigits = 4,
                $strict = true
            ),
            "credit_card_type" => $this->faker->creditCardType,
            "user_id" => $this->user->id,
        ];

        $response = $this->json("POST", "/api/v1/payment_methods", $data);

        $response->assertStatus(201)->assertJson($data);
    }

    public function testCreateCreditCardPaymentMethods()
    {
        $data = [
            "name" => $this->faker->name,
            "external_id" => "card_test",
            "four_last_digits" => $this->faker->randomNumber(
                $nbDigits = 4,
                $strict = true
            ),
            "credit_card_type" => $this->faker->creditCardType,
            "user_id" => $this->user->id,
        ];

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->with($this->user)
            ->andReturn(
                (object) [
                    "id" => "cus_test",
                ]
            );

        Stripe::shouldReceive("createCardBySourceId")
            ->once()
            ->with("cus_test", "card_test")
            ->andReturn((object) ["id" => "card_test", "country" => "CA"]);

        $response = $this->json("POST", "/api/v1/payment_methods", $data);

        $response->assertStatus(201)->assertJson($data);
    }

    public function testShowPaymentMethods()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $response = $this->json(
            "GET",
            "/api/v1/payment_methods/$paymentMethod->id"
        );

        $response
            ->assertStatus(200)
            ->assertJson($paymentMethod->only(static::$responseStructure));
    }

    public function testUpdatePaymentMethods()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json(
            "PUT",
            "/api/v1/payment_methods/$paymentMethod->id",
            $data
        );

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeletePaymentMethods()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
        ]);

        $response = $this->json(
            "DELETE",
            "/api/v1/payment_methods/$paymentMethod->id"
        );

        $response
            ->assertStatus(200)
            ->assertJson($paymentMethod->only(static::$responseStructure));
    }

    public function testDeleteCreditCardPaymentMethods()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
            "external_id" => "card_test",
        ]);

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->with($this->user)
            ->andReturn((object) ["id" => "cus_test"]);

        Stripe::shouldReceive("deleteSource")
            ->once()
            ->with("cus_test", "card_test");

        $response = $this->json(
            "DELETE",
            "/api/v1/payment_methods/$paymentMethod->id"
        );

        $response
            ->assertStatus(200)
            ->assertJson($paymentMethod->only(static::$responseStructure));
    }

    public function testListPaymentMethods()
    {
        $paymentMethods = PaymentMethod::factory(2)
            ->create([
                "user_id" => $this->user->id,
            ])
            ->map(function ($paymentMethod) {
                return $paymentMethod->only(static::$responseStructure);
            });

        $response = $this->json(
            "GET",
            route("payment_methods.index", [
                "order" => "id",
            ])
        );

        $response->assertStatus(200)->assertJsonStructure([
            "data" => [
                "*" => static::$responseStructure,
            ],
        ]);

        $this->assertJsonUnordered($response->json(), [
            "data" => $paymentMethods->toArray(),
        ]);
    }
}
