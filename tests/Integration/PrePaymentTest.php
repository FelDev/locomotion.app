<?php

namespace Tests\Integration;

use Carbon\Carbon;
use function PHPUnit\Framework\assertEquals;

class PrePaymentTest extends ActionTestCase
{
    public function testCreatePrePaymentsWithActionsFlow()
    {
        $loan = $this->buildLoan("intention");

        $intention = $loan->intention;

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/intention/complete",
            [
                "type" => "intention",
            ]
        );
        $response->assertStatus(200);

        $prePayment = $loan->refresh()->prePayment;
        $this->assertNotNull($prePayment);
        $this->assertEquals("in_process", $prePayment->status);
    }

    public function testCompletePrePayments()
    {
        $loan = $this->buildLoan("pre_payment");

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $prePayment = $loan->prePayment;

        $this->assertNotNull($prePayment);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/prepayment/complete",
            [
                "type" => "pre_payment",
            ]
        );
        $response->assertStatus(200);

        $prePayment->refresh();
        assertEquals("completed", $prePayment->status);
        assertEquals($executedAtDate, $prePayment->executed_at);

        $loan = $loan->fresh();
        $takeover = $loan->takeover;
        $this->assertNotNull($takeover);
    }

    public function testCompletePrePayments_WithPlatformTip()
    {
        $loan = $this->buildLoan("pre_payment");

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $prePayment = $loan->prePayment;

        $this->assertNotNull($prePayment);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/prepayment/complete",
            [
                "platform_tip" => 2.0,
            ]
        );
        $response->assertStatus(200);

        $prePayment->refresh();
        assertEquals("completed", $prePayment->status);
        assertEquals($executedAtDate, $prePayment->executed_at);

        $loan = $loan->fresh();
        $takeover = $loan->takeover;
        $this->assertNotNull($takeover);

        $this->assertEquals(2.0, $loan->platform_tip);
    }
}
