<?php

namespace Tests\Integration;

use Carbon\Carbon;

class TakeoverTest extends ActionTestCase
{
    public function testCreateTakeoversWithActionsFlow()
    {
        $loan = $this->buildLoan("takeover");

        $takeover = $loan->takeover;
        $this->assertNotNull($takeover);
        $this->assertEquals("in_process", $takeover->status);
    }

    public function testCompleteTakeovers()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $loan = $this->buildLoan("takeover");

        $takeover = $loan->takeover;

        $this->assertNotNull($takeover);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/takeover/complete",
            [
                "type" => "takeover",
                "mileage_beginning" => 0,
            ]
        );
        $response->assertStatus(200);

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id/actions/$takeover->id"
        );

        $takeover->refresh();
        self::assertEquals("completed", $takeover->status);
        self::assertEquals($executedAtDate, $takeover->executed_at);
    }

    public function testCompleteTakeovers_failsIfBeforeLoanDeparture()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $loan = $this->buildLoan("takeover");

        $executedAt = Carbon::now()->subMinutes(15);
        Carbon::setTestNow($executedAt);

        $takeover = $loan->takeover;
        $this->assertNotNull($takeover);

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/takeover/complete",
            [
                "type" => "takeover",
                "mileage_beginning" => 0,
            ]
        );
        $response->assertStatus(403);

        $response = $this->json(
            "GET",
            "/api/v1/loans/$loan->id/actions/$takeover->id"
        );

        $takeover->refresh();
        $this->assertEquals("in_process", $takeover->status);
        $this->assertNull($takeover->executed_at);
    }
    public function testContestTakeover()
    {
        // Reset test time now.
        Carbon::setTestNow();

        $executedAtDate = Carbon::now()->format("Y-m-d h:m:s");
        Carbon::setTestNow($executedAtDate);

        $loan = $this->buildLoan("takeover");

        $takeover = $loan->takeover;
        $takeover->complete();
        $takeover->save();

        $response = $this->json(
            "PUT",
            "/api/v1/loans/$loan->id/takeover/contest",
            [
                "type" => "takeover",
            ]
        );
        $response->assertStatus(200);

        $takeover->refresh();
        self::assertEquals("canceled", $takeover->status);
        self::assertEquals($executedAtDate, $takeover->executed_at);
    }
}
