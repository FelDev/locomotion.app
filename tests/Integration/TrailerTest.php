<?php

namespace Tests\Integration;

use App\Models\Borrower;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Payment;
use App\Models\PrePayment;
use App\Models\Pricing;
use App\Models\Trailer;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class TrailerTest extends TestCase
{
    private static $getTrailerResponseStructure = [
        "id",
        "type",
        "name",
        "comments",
        "instructions",
        "location_description",
        "position",
        "details" => ["maximum_charge", "dimensions"],
    ];

    public function testCreateTrailers()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $data = [
            "name" => $this->faker->name,
            "owner_id" => $owner->id,
            "position" => [$this->faker->latitude, $this->faker->longitude],
            "location_description" => $this->faker->sentence,
            "comments" => $this->faker->paragraph,
            "instructions" => $this->faker->paragraph,
            "type" => "trailer",
            "details" => [
                "maximum_charge" => $this->faker->numberBetween(1000, 9000),
                "dimensions" => $this->faker->sentence(2),
            ],
        ];

        $response = $this->json("POST", "/api/v1/loanables", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure(static::$getTrailerResponseStructure);
    }

    public function testShowTrailers()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $trailer = Loanable::factory()
            ->withTrailer()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");

        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$getTrailerResponseStructure);
    }

    public function testUpdateTrailers()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $trailer = Loanable::factory()
            ->withTrailer()
            ->create(["owner_id" => $owner->id]);
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/loanables/$trailer->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testDeleteTrailers()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        $trailer = Loanable::factory()
            ->withTrailer()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(404);
    }

    public function testDeleteTrailersWithActiveLoan()
    {
        // No active loan
        $loan = $this->buildLoan();
        $trailer = $loan->loanable;

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);

        $response = $this->json("GET", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(404);

        // Prepaid (active) loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $trailer = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["Le véhicule ne doit pas avoir d'emprunts en cours."],
            ],
        ]);

        // Only completed loan
        $loan = $this->buildLoan();
        $prePayment = PrePayment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $payment = Payment::factory()->create([
            "loan_id" => $loan->id,
            "status" => "completed",
        ]);
        $trailer = $loan->loanable;
        $loan = $loan->fresh();

        $response = $this->json("DELETE", "/api/v1/loanables/$trailer->id");
        $response->assertStatus(200);
    }
    public function testListTrailers()
    {
        $owner = Owner::factory()->create(["user_id" => $this->user->id]);
        Loanable::factory(2)
            ->withTrailer()
            ->create(["owner_id" => $owner->id]);

        $response = $this->json("GET", "/api/v1/loanables");

        $response
            ->assertStatus(200)
            ->assertJson(["total" => 2])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getTrailerResponseStructure
                )
            );
    }

    protected function buildLoan(): Loan
    {
        return Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => Loanable::factory()->withTrailer(),
            ]);
    }
}
