<?php

namespace Tests\Integration;

use App\Events\RegistrationSubmittedEvent;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Coowner;
use App\Models\File;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\PaymentMethod;
use App\Models\User;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Str;
use Illuminate\Testing\Assert;
use Mockery;
use Noke;
use Stripe;
use Tests\TestCase;

class UserTest extends TestCase
{
    private static $userResponseStructure = [
        "id",
        "name",
        "email",
        "email_verified_at",
        "description",
        "date_of_birth",
        "address",
        "postal_code",
        "phone",
        "is_smart_phone",
        "other_phone",
    ];

    private static $getCommunityResponseStructure = [
        "id",
        "name",
        "description",
        "area",
        "created_at",
        "updated_at",
        "deleted_at",
        "type",
        "center",
    ];

    public function testOrderUsersById()
    {
        $data = [
            "order" => "id",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByFullName()
    {
        $data = [
            "order" => "full_name",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testOrderUsersByEmail()
    {
        $data = [
            "order" => "email",
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersById()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "id" => "3",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCreatedAt()
    {
        // Lower bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Lower and upper bounds
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "2020-11-10T01:23:45Z@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Upper bound only
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@2020-11-12T01:23:45Z",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        // Degenerate case when bounds are removed
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "created_at" => "@",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByFullName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "full_name" => "Ariane",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByIsDeleted()
    {
        // Zero integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => 0,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $notDeactivatedResponseContent = $response->baseResponse->getContent();

        // Positive integer
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => 1,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        $deactivatedResponseContent = $response->baseResponse->getContent();

        // Boolean false
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => false,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $notDeactivatedResponseContent,
            $response->baseResponse->getContent()
        );

        // Boolean true
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "is_deleted" => true,
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);

        Assert::assertJsonStringEqualsJsonString(
            $deactivatedResponseContent,
            $response->baseResponse->getContent()
        );
    }

    public function testFilterUsersByCommunityId()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.id" => "3",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testFilterUsersByCommunityName()
    {
        $data = [
            "page" => 1,
            "per_page" => 10,
            "fields" => "id,name,last_name,full_name,email",
            "communities.name" => "Patrie",
        ];
        $response = $this->json("GET", route("users.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(TestCase::$collectionResponseStructure);
    }

    public function testSearchUsers()
    {
        User::factory()->create([
            "name" => "First name",
        ]);
        User::factory()->create([
            "name" => "Other name",
        ]);

        $data = [
            "q" => "rst NA",
        ];
        $response = $this->json(
            "GET",
            route("users.index"),
            $data
        )->assertStatus(200);

        $this->assertCount(1, $response->json()["data"]);
    }

    public function testCreateUsers()
    {
        $data = [
            "accept_conditions" => true,
            "name" => $this->faker->name,
            "last_name" => $this->faker->name,
            "email" => $this->faker->unique()->safeEmail,
            "password" =>
                '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            "description" => null,
            "date_of_birth" => null,
            "address" => "",
            "postal_code" => "",
            "phone" => "",
            "is_smart_phone" => false,
            "other_phone" => "",
            "remember_token" => Str::random(10),
        ];

        $response = $this->json("POST", "/api/v1/users", $data);
        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "accept_conditions",
                "created_at",
                "updated_at",
            ]);
    }

    public function testCreateUsersWithSimilarEmails()
    {
        $user = User::factory()
            ->make()
            ->toArray();
        $user["password"] = "12354123124";

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(201);

        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);

        // Case insensitivity
        $user["email"] =
            strtoupper($user["email"][0]) . substr($user["email"], 1);
        $response = $this->json("POST", "/api/v1/users", $user);
        $response->assertStatus(422);
    }

    public function testShowUsers()
    {
        $user = User::factory()->create();
        $data = [];

        $response = $this->json("GET", "/api/v1/users/$user->id", $data);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                "id",
                "name",
                "last_name",
                "email",
                "email_verified_at",
                "description",
                "date_of_birth",
                "address",
                "postal_code",
                "phone",
                "is_smart_phone",
                "other_phone",
                "remember_token",
                "created_at",
                "updated_at",
                "role",
                "full_name",
            ]);
    }

    public function testUpdateUsers()
    {
        $user = User::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $this->actAs($user);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $response->assertStatus(200)->assertJson($data);
    }

    public function testUpdateUsersWithNonexistentId()
    {
        $user = User::factory()->create();
        $data = [
            "name" => $this->faker->name,
        ];

        $response = $this->json("PUT", "/api/v1/users/208084082084", $data);

        $response->assertStatus(404);
    }

    public function testDeleteUser_AuthorizedAsGlobalAdmin()
    {
        $userToDeactivate = User::factory()->create();

        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );
        $response->assertStatus(200);
    }

    public function testDeleteUser_UnauthorizedAsRegularUser()
    {
        $normalUser = User::factory()->create();
        $this->actAs($normalUser);

        $userToDeactivate = User::factory()->create();
        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );

        $response->assertStatus(403);
    }

    public function testDeleteUser_UnauthorizedAsAdminOfCommunity()
    {
        $community = Community::factory()->create();

        $communityAdmin = User::factory()->create();
        $communityAdmin->communities()->attach($community->id, [
            "role" => "admin",
        ]);
        $this->actAs($communityAdmin);

        $userToDeactivate = User::factory()->create();
        $userToDeactivate->communities()->attach($community->id, [
            "role" => "regular",
        ]);
        $response = $this->json(
            "DELETE",
            "/api/v1/users/$userToDeactivate->id"
        );

        $response->assertStatus(403);
    }

    public function testDeleteUsers_failsWithNonexistentId()
    {
        $response = $this->json("DELETE", "/api/v1/users/0280398420384");

        $response->assertStatus(404);
    }

    public function testDeleteUsers_mustNotHaveOngoingLoansAsOwner()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $owner = Owner::factory()->create([
            "user_id" => $user,
        ]);
        $loanable = Loanable::factory()->create([
            "owner_id" => $owner,
        ]);
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                [
                    "L'utilisateur ne doit pas avoir d'emprunts en cours en tant que propriétaire ou emprunteur.",
                ],
            ],
        ]);

        $loan->cancel();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(200);
    }

    public function testDeleteUsers_mustNotHaveOngoingLoansAsBorrower()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $borrower = Borrower::factory()->create([
            "user_id" => $user,
        ]);
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $borrower,
            ]);

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                [
                    "L'utilisateur ne doit pas avoir d'emprunts en cours en tant que propriétaire ou emprunteur.",
                ],
            ],
        ]);

        $loan->cancel();
        $loan->save();
        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(200);
    }

    public function testDeleteUsers_mustNotHaveBalance()
    {
        $user = User::factory([
            "balance" => 10.0,
        ])->create();

        $response = $this->json("DELETE", "/api/v1/users/$user->id");

        $response->assertStatus(422)->assertJson([
            "errors" => [
                ["L'utilisateur ne doit pas avoir un solde dans son compte."],
            ],
        ]);
    }

    public function testRestoreUser_doesntRestoreLoanablesByDefault()
    {
        $loanable = Loanable::factory()->create();

        $loanable->owner->user->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$loanable->owner->user->id}/restore"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNotNull($loanable->deleted_at);
    }

    public function testRestoreUser_removesLoanableAvailabilities()
    {
        $loanable = Loanable::factory()->create([
            "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            "availability_mode" => "always",
        ]);

        $loanable->owner->user->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/users/{$loanable->owner->user->id}/restore?restore_loanables=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNull($loanable->deleted_at);
        self::assertEquals("never", $loanable->availability_mode);
        self::assertEquals("[]", $loanable->availability_json);
    }

    public function testRestoreUser_canRestoreLoanableAvailabilities()
    {
        $loanable = Loanable::factory()->create([
            "availability_json" => <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            "availability_mode" => "always",
        ]);

        $loanable->delete();

        $response = $this->json(
            "PUT",
            "/api/v1/loanables/$loanable->id/restore?restore_loanables=true&restore_availability=true"
        );

        $response->assertStatus(200);
        $loanable->refresh();
        self::assertNull($loanable->deleted_at);
        self::assertEquals("always", $loanable->availability_mode);
        self::assertEquals(
            <<<JSON
[
  {
    "available":false,
    "type":"weekdays",
    "scope":["MO","TU","TH","WE","FR"],
    "period":"00:00-24:00"
  }
]
JSON
            ,
            $loanable->availability_json
        );
    }

    public function testListUsers()
    {
        $users = User::factory(2)
            ->create()
            ->map(function ($user) {
                return $user->only(static::$userResponseStructure);
            });

        $response = $this->json("GET", route("users.index"));

        $response
            ->assertStatus(200)
            ->assertJsonStructure(
                $this->buildCollectionStructure(static::$userResponseStructure)
            );
    }

    public function testAssociateUserToCommunity()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();

        $response = $this->json(
            "PUT",
            "/api/v1/users/$user->id/communities/$community->id"
        );
        $response->assertStatus(200);
    }

    public function testUpdateUserWithCommunity()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
    }

    public function testUpdateUserWithResidencyProof_triggersRegistrationSubmitted()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $proof = File::factory()->create([
            "field" => "residency_proof",
        ]);

        $data = [
            "residency_proof" => [["id" => $proof->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }
    public function testUpdateUserWithProofs_onlyTriggersRegistrationSubmittedWhenAllProofsPresent()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create([
            "requires_identity_proof" => true,
        ]);
        $user->communities()->save($community);

        $residencyProof = File::factory()->create([
            "field" => "residency_proof",
        ]);

        $data = [
            "residency_proof" => [["id" => $residencyProof->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);

        $identityProof = File::factory()->create([
            "field" => "identity_proof",
        ]);
        $data = [
            "residency_proof" => [["id" => $residencyProof->id]],
            "identity_proof" => [["id" => $identityProof->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithResidencyProof_resetsProofInvalid()
    {
        $user = User::factory()->create([
            "is_proof_invalid" => true,
        ]);
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $proof = File::factory()->create([
            "field" => "residency_proof",
        ]);

        $data = [
            "residency_proof" => [["id" => $proof->id]],
        ];

        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        $user->refresh();
        self::assertFalse($user->is_proof_invalid);
    }

    public function testUpdateUserWithSameResidencyProof_doesntTriggerRegistrationSubmitted()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $proof = File::factory()->create([
            "field" => "residency_proof",
        ]);
        $user->residencyProof()->save($proof);

        $data = ["residency_proof" => [["id" => $proof->id]]];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testUpdateUserWithoutResidencyProof_doesntTriggerRegistrationSubmitted()
    {
        $user = User::factory()->create();
        $community = Community::factory()->create();
        $user->communities()->save($community);

        $data = [
            "communities" => [["id" => $community->id]],
        ];

        Event::fake([RegistrationSubmittedEvent::class]);
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);
        $response
            ->assertStatus(200)
            ->assertJsonStructure(static::$userResponseStructure);
        Event::assertNotDispatched(RegistrationSubmittedEvent::class);
    }

    public function testListUsersCommunities()
    {
        $user = User::factory()->create();
        $communities = Community::factory(2)->create();

        $data = [
            "communities" => [["id" => $communities[0]->id]],
        ];
        $response = $this->json("PUT", "/api/v1/users/$user->id", $data);

        $data = [
            "users.id" => $user->id,
        ];
        $response = $this->json("GET", route("communities.index"), $data);
        $response
            ->assertStatus(200)
            ->assertJson(["total" => 1])
            ->assertJsonStructure(
                $this->buildCollectionStructure(
                    static::$getCommunityResponseStructure
                )
            );
    }

    public function testUsersUpdateEmailEndpointUpdatesNokeEmail()
    {
        $originalEmail = "test@gmail.com";
        $user = User::factory()->create([
            "email" => $originalEmail,
            "role" => "admin",
            "noke_id" => 1234,
        ]);

        $newEmail = "different@hotmail.com";

        Noke::shouldReceive("findUserById")
            ->withArgs(fn($a) => $a === 1234)
            ->andReturns(
                (object) [
                    "username" => $originalEmail,
                ]
            )
            ->once();

        Noke::shouldReceive("updateUser")
            ->withArgs(function ($arg) use ($newEmail) {
                return $arg->username === $newEmail;
            })
            ->once();

        $this->json("POST", "/api/v1/users/$user->id/email", [
            "email" => $newEmail,
        ])->assertStatus(200);
    }

    public function testUsersUpdateEndpointUpdatesNokeEmail()
    {
        $originalEmail = "test@gmail.com";
        $user = User::factory()->create([
            "email" => $originalEmail,
            "noke_id" => 1234,
        ]);

        $newEmail = "different@hotmail.com";

        Noke::shouldReceive("findUserById")
            ->withArgs(fn($a) => $a === 1234)
            ->andReturns(
                (object) [
                    "username" => $originalEmail,
                ]
            )
            ->once();

        Noke::shouldReceive("updateUser")
            ->withArgs(function ($arg) use ($newEmail) {
                return $arg->username === $newEmail;
            })
            ->once();

        $this->json(
            "PUT",
            "/api/v1/users/$user->id",
            array_merge($user->toArray(), [
                "email" => $newEmail,
            ])
        )->assertStatus(200);
    }

    public function testAddToBalanceEndpoint()
    {
        $paymentMethod = PaymentMethod::factory()->create([
            "user_id" => $this->user->id,
            "external_id" => "stripe source id",
        ]);

        Stripe::shouldReceive("getUserCustomer")
            ->once()
            ->withArgs(function ($arg) {
                return $arg->id === $this->user->id;
            })
            ->andReturn((object) ["id" => "cus_test"]);

        Stripe::shouldReceive("computeAmountWithFee")
            ->once()
            ->with(10, Mockery::any())
            ->andReturn(10.5);

        Stripe::shouldReceive("createCharge")
            ->once()
            ->with(
                1050,
                "cus_test",
                "Ajout au compte LocoMotion: 10,00$ + 0,50$ (frais)",
                "stripe source id"
            )
            ->andReturn(
                (object) [
                    "id" => "ch_3N1fAGMh7B5SwAr3Q2hQpeQH",
                    "object" => "charge",
                    "amount" => 1050,
                ]
            );

        $response = $this->json("PUT", "/api/v1/auth/user/balance", [
            "transaction_id" => 1,
            "amount" => 10,
            "payment_method_id" => $paymentMethod->id,
        ]);
        $response->assertStatus(200)->assertSee("10");
    }

    public function testPhoneNotVisibleToOtherUser()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();
        $otherUser = User::factory()
            ->withCommunity($user->main_community)
            ->create();

        $this->actAs($user);
        $response = $this->json("GET", "api/v1/users/{$otherUser->id}");

        $response->assertStatus(200)->assertJsonMissing(["phone"]);
    }

    public function testPhoneVisibleToAdmin()
    {
        $user = User::factory()
            ->withCommunity()
            ->create();

        $response = $this->json("GET", "api/v1/users/{$user->id}");

        $response->assertStatus(200)->assertJsonStructure(["phone"]);
    }

    public function testLoanableOwnerPhoneNotVisibleToRandomUser()
    {
        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();
        $otherUser = User::factory()->create();

        $this->actAs($otherUser);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,owner.user.phone"
        );

        $response->assertStatus(404);
    }

    public function testLoanableOwnerPhoneVisibleToCoowner()
    {
        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();

        $this->actAs($loanable->coowners[0]->user);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,owner.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "owner" => [
                "user" => ["phone"],
            ],
        ]);
    }

    public function testLoanableCoOwnerPhoneVisibleToOwner()
    {
        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();

        $this->actAs($loanable->owner->user);
        $response = $this->json(
            "GET",
            "api/v1/loanables/{$loanable->id}?fields=*,coowners.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "coowners" => [
                [
                    "user" => ["phone"],
                ],
            ],
        ]);
    }

    public function testBorrowerPhoneVisibleToLoanableOwners()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory()->withCoowner(),
        ])->create();

        $this->actAs($loan->loanable->owner->user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,borrower.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "borrower" => [
                "user" => ["phone"],
            ],
        ]);

        $this->actAs($loan->loanable->coowners[0]->user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,borrower.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "borrower" => [
                "user" => ["phone"],
            ],
        ]);
    }

    public function testLoanableOwnersPhoneVisibleToBorrower()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory([
                "show_owner_as_contact" => true,
            ])->has(Coowner::factory(["show_as_contact" => true])),
        ])->create();

        $this->actAs($loan->borrower->user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,loanable.owner.user.phone,loanable.coowners.user.phone"
        );

        $response->assertStatus(200)->assertJsonStructure([
            "loanable" => [
                "owner" => [
                    "user" => ["phone"],
                ],
                "coowners" => [["user" => ["phone"]]],
            ],
        ]);
    }

    public function testLoanableOwnersPhoneNotVisibleToBorrower_ifDesired()
    {
        $loan = Loan::factory([
            "loanable_id" => Loanable::factory([
                "show_owner_as_contact" => false,
            ])->has(Coowner::factory(["show_as_contact" => false])),
        ])->create();

        $this->actAs($loan->borrower->user);
        $response = $this->json(
            "GET",
            "api/v1/loans/{$loan->id}?fields=*,loanable.owner.user.phone,loanable.coowners.user.phone"
        );

        $response->assertStatus(200);
        $response->assertJsonMissing([
            "loanable" => [
                "owner" => [
                    "user" => ["phone"],
                ],
            ],
        ]);
        $response->assertJsonMissing([
            "loanable" => [
                "coowners" => [["user" => ["phone"]]],
            ],
        ]);
    }
}
