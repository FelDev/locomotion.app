<?php

namespace Tests;

use Illuminate\Support\Arr;
use PHPUnit\Framework\Constraint\Constraint;
use SebastianBergmann\Comparator\ComparisonFailure;

class TraversableIsSimilar extends Constraint
{
    private $expected;

    public function __construct($value)
    {
        $this->expected = $value;
    }

    public function evaluate(
        $other,
        string $description = "",
        bool $returnResult = false
    ): ?bool {
        $success = $this->isArraySimilar($other, $this->expected);

        if ($returnResult) {
            return $success;
        }

        if (!$success) {
            $f = new ComparisonFailure(
                $this->expected,
                $other,
                $this->exporter()->export($this->expected),
                $this->exporter()->export($other)
            );

            $this->fail($other, $description, $f);
        }

        return null;
    }

    /**
     * Checks that the $expected partial array is present in $actual, regardless of non-associative
     * array order at any depth.
     *
     * @param $actual
     * @param $expected
     * @return bool
     */
    public function isArraySimilar($actual, $expected): bool
    {
        if (!is_array($actual) || !is_array($expected)) {
            return $expected == $actual;
        }

        if (!Arr::isAssoc($expected)) {
            // check that a similar path object exists in actual for every item.
            foreach ($expected as $expectedItem) {
                if (!$this->arrayContainsSimilar($actual, $expectedItem)) {
                    return false;
                }
            }
        } else {
            // check for every key
            foreach ($expected as $key => $expectedValue) {
                if (
                    !isset($actual[$key]) ||
                    !$this->isArraySimilar($actual[$key], $expectedValue)
                ) {
                    return false;
                }
            }
        }

        return true;
    }

    protected function arrayContainsSimilar(array &$actual, $expectedItem): bool
    {
        foreach ($actual as $actualItemKey => $actualItem) {
            if ($this->isArraySimilar($actualItem, $expectedItem)) {
                unset($actual[$actualItemKey]);
                return true;
            }
        }
        return false;
    }

    public function toString(): string
    {
        return "is similar to " . $this->exporter()->export($this->expected);
    }

    protected function failureDescription($other): string
    {
        return "two arrays are similar";
    }
}
