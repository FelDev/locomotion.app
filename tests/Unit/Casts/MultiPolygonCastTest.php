<?php

namespace Tests\Unit\Casts;

use App\Casts\MultiPolygonCast;
use MStaack\LaravelPostgis\Geometries\LineString;
use MStaack\LaravelPostgis\Geometries\MultiPolygon;
use MStaack\LaravelPostgis\Geometries\Point;
use MStaack\LaravelPostgis\Geometries\Polygon;
use Tests\TestCase;

class MultiPolygonCastTest extends TestCase
{
    public function testSetEmpty()
    {
        $cast = new MultiPolygonCast();

        $this->assertNull($cast->set(null, "polygon", null, []));
        $this->assertNull($cast->set(null, "polygon", [], []));
    }

    public function testSetFromArray()
    {
        $cast = new MultiPolygonCast();

        $coordinates = [
            [
                [
                    // GeoGSON format is Lng, Lat
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ];

        $polygon = $cast->set(null, "polygon", $coordinates, []);
        $this->assertInstanceOf(MultiPolygon::class, $polygon);

        $this->assertEquals(
            -33.456789,
            $polygon
                ->getPolygons()[0]
                ->getLineStrings()[0]
                ->getPoints()[0]
                ->getLat()
        );
    }

    public function testSetFromGeoJson()
    {
        $cast = new MultiPolygonCast();

        $coordinates = new \GeoJson\Geometry\MultiPolygon([
            [
                [
                    [55.234567, -33.456789],
                    [56.789012, -34.56789],
                    [54.321098, -32.109876],
                    [55.234567, -33.456789],
                ],
            ],
        ]);

        $polygon = $cast->set(null, "polygon", $coordinates, []);
        $this->assertInstanceOf(MultiPolygon::class, $polygon);

        $this->assertEquals(
            -33.456789,
            $polygon
                ->getPolygons()[0]
                ->getLineStrings()[0]
                ->getPoints()[0]
                ->getLat()
        );
    }

    public function testSetString()
    {
        $cast = new MultiPolygonCast();

        $this->assertEquals("", $cast->set(null, "polygon", "", []));

        $this->assertEquals(
            "string",
            $cast->set(null, "polygon", "string", [])
        );
    }

    public function testSetInvalid()
    {
        $this->expectException(\Exception::class);

        $cast = new MultiPolygonCast();

        $cast->set(null, "polygon", -18, []);
    }

    public function testGetEmpty()
    {
        $cast = new MultiPolygonCast();

        $this->assertNull($cast->get(null, "polygon", null, []));
        $this->assertNull($cast->get(null, "polygon", [], []));
    }

    public function testGetTriangle()
    {
        $cast = new MultiPolygonCast();

        $polygon = new MultiPolygon([
            new Polygon([
                new LineString([
                    new Point(55.234567, -33.456789),
                    new Point(56.789012, -34.56789),
                    new Point(54.321098, -32.109876),
                    new Point(55.234567, -33.456789),
                ]),
            ]),
        ]);

        $expected_coordinates = new \GeoJson\Geometry\MultiPolygon([
            [
                [
                    [-33.456789, 55.234567],
                    [-34.56789, 56.789012],
                    [-32.109876, 54.321098],
                    [-33.456789, 55.234567],
                ],
            ],
        ]);

        $coordinates = $cast->get(null, "polygon", $polygon, []);
        $this->assertEquals($expected_coordinates, $coordinates);
    }
}
