<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\ActionsComplete as ActionsCompleteCommand;
use App\Models\Extension;
use App\Models\Handover;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Payment;
use App\Models\User;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class ActionsCompleteTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // Act as random user without privileges
        $this->actAs(User::factory()->create());
    }

    public function testGetActiveLoansScheduledToReturnBefore()
    {
        $expectedLoanIds = [];

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->withPaidCommunity()
            ->create();

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
            ]);

        $unpaidCompletedLoanEndingMoreThan48HoursAgo = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "community_id" => $loanable->community_id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "loanable_id" => $loanable->id,
                "platform_tip" => 0,
            ])
            ->refresh();
        $expectedLoanIds[] = $unpaidCompletedLoanEndingMoreThan48HoursAgo->id;

        $unpaidCompletedLoanEndingLessThan48HoursAgo = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "community_id" => $loanable->community_id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "loanable_id" => $loanable->id,
                "platform_tip" => 0,
            ])
            ->refresh();

        $unpaidCompletedLoanEndingLessThan48HoursAgoCanceled = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "community_id" => $loanable->community_id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "loanable_id" => $loanable->id,
                "platform_tip" => 0,
                "canceled_at" => $twoDaysAgo->subMinutes(30),
            ])
            ->refresh();

        $unpaidCompletedLoanEndingMoreThan48HoursAgoExtended = Loan::factory()
            ->withInProcessPayment()
            ->withCompletedExtension()
            ->create([
                "borrower_id" => $borrowerUser->borrower->id,
                "community_id" => $loanable->community_id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "loanable_id" => $loanable->id,
                "platform_tip" => 0,
            ])
            ->refresh();

        $scheduledLoans = ActionsCompleteCommand::getActiveLoansScheduledToReturnBefore(
            $twoDaysAgo
        );

        $scheduledLoanIds = [];
        foreach ($scheduledLoans as $loan) {
            $scheduledLoanIds[] = $loan->id;
        }

        // Assert equals, order not important.
        $this->assertEqualsCanonicalizing($expectedLoanIds, $scheduledLoanIds);
    }

    public function testIntentionInProcess_LoanNotExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->intention ? $loan->intention->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and intention must remain in process
        $this->assertEquals(
            "in_process",
            $loan->intention ? $loan->intention->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testIntentionInProcess_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->intention ? $loan->intention->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Intention remains in process, but loan must be canceled.
        $this->assertEquals(
            "in_process",
            $loan->intention ? $loan->intention->status : ""
        );
        $this->assertEquals("canceled", $loan->status);
    }

    public function testPrepaymentInProcess_LoanExpired()
    {
        $loanableIsSelfService = true;
        $loanCost = 5;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 0,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessPrePayment()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->prepayment ? $loan->prepayment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Prepayment remains in process, but loan must be canceled.
        $this->assertEquals(
            "in_process",
            $loan->prepayment ? $loan->prepayment->status : ""
        );
        $this->assertEquals("canceled", $loan->status);
    }

    public function testTakeoverInProcess_LoanNotExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and takeover must remain in process
        $this->assertEquals(
            "in_process",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testTakeoverInProcessNotSelfService_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Takeover remains in process, but loan must be canceled.
        $this->assertEquals(
            "in_process",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals("canceled", $loan->status);
    }

    public function testHandoverInProcess_LoanNotExpired_BalanceSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and handover must remain in process
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testHandoverInProcess_LoanExpired_BalanceSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->has(Owner::factory())
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        $loan->handover->executed_at = null;

        // Run the command
        // At the moment, the command only completes one step at a time. Call
        // it twice so as to complete handover and payment.
        $this->artisan("actions:complete")->assertExitCode(0);
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan, handover and payment must be completed
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "completed",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("completed", $loan->status);
    }

    public function testHandoverInProcess_LoanExpired_BalanceNotSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 30;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        $loan->handover->purchases_amount = 0;
        $loan->handover->save();

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Handover must be completed
        // Loan and payment must remain in_process
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testHandoverInProcess_ExtensionAccepted_NotExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                // Loan would be expired if it was not for the extension.
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Create extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 70,
                "status" => "completed",
                "executed_at" => CarbonImmutable::now(),
            ])
        );

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        $this->assertCount(1, $loan->extensions);
        foreach ($loan->extensions as $extension) {
            $this->assertEquals("completed", $extension->status);
        }

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and handover must remain in process
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testHandoverInProcess_ExtensionAccepted_Expired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 30,
                "platform_tip" => $loanCost,
            ]);

        // Create extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 50,
                "status" => "completed",
                "executed_at" => CarbonImmutable::now(),
            ])
        );

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        $this->assertCount(1, $loan->extensions);
        foreach ($loan->extensions as $extension) {
            $this->assertEquals("completed", $extension->status);
        }

        // Run the command
        // At the moment, the command only completes one step at a time. Call
        // it twice so as to complete handover and payment.
        $this->artisan("actions:complete")->assertExitCode(0);
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan, handover and payment must be completed
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "completed",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("completed", $loan->status);
    }

    public function testHandoverInProcess_TakeoverContested_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withContestedTakeover()
            ->has(Handover::factory()->inProcess())
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "canceled",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Takeover remains canceled. Handover and loan remain in process.
        $this->assertEquals(
            "canceled",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testPaymentInProcess_LoanNotExpired_BalanceSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 70,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and payment must remain in process
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testPaymentInProcess_LoanExpired_BalanceSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and payment must be completed
        $this->assertEquals(
            "completed",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("completed", $loan->status);
    }

    public function testPaymentInProcess_LoanExpired_BalanceNotSufficient()
    {
        $loanableIsSelfService = false;
        $loanCost = 30;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Expenses must be 0 so as to ensure a positive value for total_actual_cost.
        $loan->handover->purchases_amount = 0;
        $loan->handover->save();

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Loan and payment must remain in process
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testExtensionInProcess_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $loanable->community_id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Create extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 70,
                "status" => "in_process",
                "executed_at" => CarbonImmutable::now(),
            ])
        );

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "in_process",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        $this->assertCount(1, $loan->extensions);
        foreach ($loan->extensions as $extension) {
            $this->assertEquals("in_process", $extension->status);
        }

        // Run the command
        // At the moment, the command only completes one step at a time. Call
        // it twice so as to complete handover and payment.
        $this->artisan("actions:complete")->assertExitCode(0);
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Extension was not accepted before initial loan expiration and thus
        // must be canceled.
        $this->assertCount(1, $loan->extensions);
        foreach ($loan->extensions as $extension) {
            $this->assertEquals("canceled", $extension->status);
        }

        // Loan, handover and payment must be completed
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "completed",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("completed", $loan->status);
    }

    public function testPaymentInProcess_TakeoverContested_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withContestedTakeover()
            ->has(Handover::factory()->completed())
            ->has(Payment::factory()->inProcess())
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "canceled",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Things remain intact.
        $this->assertEquals(
            "canceled",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "completed",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }

    public function testPaymentInProcess_HandoverContested_LoanExpired()
    {
        $loanableIsSelfService = false;
        $loanCost = 0;

        $twoDaysAgo = CarbonImmutable::now()->sub(48, "hours");
        $moreThanTwoDaysAgo = CarbonImmutable::now()->sub(54, "hours");

        $ownerUser = User::factory()
            ->withOwner()
            ->withPaidCommunity()
            ->create();

        $borrowerUser = User::factory()
            ->withBorrower()
            ->create([
                "balance" => 15,
            ]);

        $loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $ownerUser->owner->id,
                "is_self_service" => $loanableIsSelfService,
            ]);

        CarbonImmutable::setTestNow($moreThanTwoDaysAgo);

        $loan = Loan::factory()
            ->withContestedHandover()
            ->has(Payment::factory()->inProcess())
            ->create([
                "loanable_id" => $loanable->id,
                "community_id" => $ownerUser->communities[0]->id,
                "borrower_id" => $borrowerUser->borrower->id,
                "departure_at" => $twoDaysAgo->subMinutes(60),
                "duration_in_minutes" => 50,
                "platform_tip" => $loanCost,
            ]);

        // Setup is finished, set back test time to now.
        CarbonImmutable::setTestNow();

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Validate preconditions
        $this->assertEquals(
            "completed",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "canceled",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);

        // Run the command
        $this->artisan("actions:complete")->assertExitCode(0);

        // Ensure we fetch loan back from the database
        $loan->refresh();

        // Things remain intact.
        $this->assertEquals(
            "completed",
            $loan->takeover ? $loan->takeover->status : ""
        );
        $this->assertEquals(
            "canceled",
            $loan->handover ? $loan->handover->status : ""
        );
        $this->assertEquals(
            "in_process",
            $loan->payment ? $loan->payment->status : ""
        );
        $this->assertEquals("in_process", $loan->status);
    }
}
