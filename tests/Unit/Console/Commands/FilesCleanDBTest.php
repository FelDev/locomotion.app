<?php

namespace Console\Commands;

use App\Models\File;
use App\Models\Image;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;

class FilesCleanDBTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // We flush events (delete, save) which move files around since we don't care about
        // those in the current test and we would need to mock the Storage:: calls.
        Image::flushEventListeners();
        File::flushEventListeners();
    }

    public function testClean_deletesUnreferencedFilesOlderThanADay()
    {
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "fileable_id" => User::factory()->create(),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "fileable_type" => "user",
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "fileable_type" => "user",
            "field" => "proof",
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "fileable_type" => "user",
            "fileable_id" => User::factory()->create(),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "field" => "proof",
            "fileable_id" => User::factory()->create(),
        ]);

        self::assertCount(6, File::all());

        $this->artisan("files:clean:db");

        self::assertCount(0, File::all());
    }

    public function testClean_keepsUnreferencedFilesFromTheLastDay()
    {
        File::factory()->create([
            "updated_at" => Carbon::now(),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now(),
            "fileable_id" => User::factory()->create(),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now(),
            "fileable_type" => "user",
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now(),
            "fileable_type" => "user",
            "field" => "proof",
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now(),
            "fileable_type" => "user",
            "fileable_id" => User::factory()->create(),
        ]);
        File::factory()->create([
            "updated_at" => Carbon::now(),
            "field" => "proof",
            "fileable_id" => User::factory()->create(),
        ]);

        self::assertCount(6, File::all());

        $this->artisan("files:clean:db");

        self::assertCount(6, File::all());
    }

    public function testClean_keepsReferencedFiles()
    {
        $file = File::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
        ]);
        $user = User::factory()->create();

        $user->identityProof()->save($file);

        self::assertCount(1, File::all());
        $this->artisan("files:clean:db");
        self::assertCount(1, File::all());
    }

    public function testClean_deletesUnreferencedImagesOlderThanADay()
    {
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "imageable_id" => User::factory()->create(),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "imageable_type" => "user",
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "imageable_type" => "user",
            "field" => "avatar",
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "imageable_type" => "user",
            "imageable_id" => User::factory()->create(),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
            "field" => "avatar",
            "imageable_id" => User::factory()->create(),
        ]);

        self::assertCount(6, Image::all());

        $this->artisan("files:clean:db");

        self::assertCount(0, Image::all());
    }

    public function testClean_keepsUnreferencedImagesFromTheLastDay()
    {
        Image::factory()->create([
            "updated_at" => Carbon::now(),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now(),
            "imageable_id" => User::factory()->create(),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now(),
            "imageable_type" => "user",
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now(),
            "imageable_type" => "user",
            "field" => "avatar",
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now(),
            "imageable_type" => "user",
            "imageable_id" => User::factory()->create(),
        ]);
        Image::factory()->create([
            "updated_at" => Carbon::now(),
            "field" => "avatar",
            "imageable_id" => User::factory()->create(),
        ]);

        self::assertCount(6, Image::all());

        $this->artisan("files:clean:db");

        self::assertCount(6, Image::all());
    }

    public function testClean_keepsReferencedImages()
    {
        $image = Image::factory()->create([
            "updated_at" => Carbon::now()->subDays(2),
        ]);
        $user = User::factory()->create();

        $user->avatar()->save($image);

        self::assertCount(1, Image::all());
        $this->artisan("files:clean:db");
        self::assertCount(1, Image::all());
    }
}
