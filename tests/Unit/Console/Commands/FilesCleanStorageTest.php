<?php

namespace Console\Commands;

use App\Models\File;
use App\Models\Image;
use Storage;
use Tests\TestCase;

class FilesCleanStorageTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        // We flush events (delete, save) which move files around since we don't care about
        // those in the current test and we would need to mock the Storage:: calls.
        Image::flushEventListeners();
        File::flushEventListeners();
    }

    public function testClean_removesUnreferencedStorageDirectory()
    {
        Storage::put("files/users/user1/foo.txt", "foo");
        Storage::put("files/users/user2/foo.txt", "foo");
        Storage::put("images/borrowers/borrower1/foo.txt", "foo");
        Storage::put("images/borrowers/borrower2/foo.txt", "foo");
        Storage::put("storage/somefolder/foo.txt", "foo");
        Storage::put("storage/exports/myexport.txt", "foo");

        $this->artisan("files:clean:storage");

        self::assertFalse(Storage::exists("files/users/user1/foo.txt"));
        self::assertFalse(Storage::exists("files/users/user2/foo.txt"));
        self::assertFalse(
            Storage::exists("images/borrowers/borrower1/foo.txt")
        );
        self::assertFalse(Storage::exists("storage/somefolder/foo.txt"));
        self::assertTrue(Storage::exists("storage/exports/myexport.txt"));
    }

    public function testClean_preservesReferencedStorageDirectory()
    {
        Storage::put("images/users/user1/foo.txt", "foo");
        Storage::put("files/users/user2/foo.txt", "foo");
        Storage::put("images/borrowers/borrower1/foo.txt", "foo");
        Storage::put("files/borrowers/borrower2/foo.txt", "foo");

        File::factory()
            ->sequence(
                ["path" => "files/users/user2", "fileable_type" => "users"],
                [
                    "path" => "files/borrowers/borrower2",
                    "fileable_type" => "borrowers",
                ]
            )
            ->count(2)
            ->create();

        Image::factory()
            ->sequence(
                ["path" => "images/users/user1", "imageable_type" => "users"],
                [
                    "path" => "images/borrowers/borrower1",
                    "imageable_type" => "borrowers",
                ]
            )
            ->count(2)
            ->create();

        $this->artisan("files:clean:storage");

        self::assertTrue(Storage::exists("images/users/user1/foo.txt"));
        self::assertTrue(Storage::exists("files/users/user2/foo.txt"));
        self::assertTrue(Storage::exists("images/borrowers/borrower1/foo.txt"));
        self::assertTrue(Storage::exists("files/borrowers/borrower2/foo.txt"));
    }

    public function testClean_deletesUnreferencedFiles()
    {
        Storage::put("files/users/user1/foo.txt", "foo");
        Storage::put("files/users/user1/bar.txt", "foo");
        Storage::put("images/borrowers/borrower1/foo.txt", "foo");
        Storage::put("images/borrowers/borrower1/bar.txt", "foo");

        File::factory()->create([
            "path" => "files/users/user1",
            "fileable_type" => "users",
            "filename" => "foo.txt",
        ]);

        Image::factory()->create([
            "path" => "images/borrowers/borrower1",
            "imageable_type" => "borrowers",
            "filename" => "foo.txt",
        ]);

        $this->artisan("files:clean:storage", ["--checkfiles" => true]);

        self::assertTrue(Storage::exists("files/users/user1/foo.txt"));
        self::assertFalse(Storage::exists("files/users/user1/bar.txt"));

        self::assertTrue(Storage::exists("images/borrowers/borrower1/foo.txt"));
        // Only affects /files paths.
        self::assertTrue(Storage::exists("images/borrowers/borrower1/bar.txt"));
    }
}
