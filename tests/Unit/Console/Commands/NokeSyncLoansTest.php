<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\NokeSyncLoans as NokeSyncLoansCommand;
use App\Models\Loan;
use App\Models\Loanable;
use Carbon\CarbonImmutable;
use Tests\TestCase;

class NokeSyncLoansTest extends TestCase
{
    public function testGetLoansFromPadlockMacQuery()
    {
        $query = NokeSyncLoansCommand::getBorrowersForLoansWithPadlockMac(
            "0D:34:F2:3E:0F:2F"
        );

        $query->get();

        // Assert that we ended up here.
        $this->assertTrue(true);
    }

    public function testGetLoansFromPadlockMacTakesCancelationIntoAccount()
    {
        $bikeWithPadlock = Loanable::factory()
            ->withPadlock()
            ->create();

        $now = CarbonImmutable::now();
        CarbonImmutable::setTestNow($now);

        // Loan starts in more than 15 minutes, should not have access.
        $loanStartingLater = Loan::factory()
            ->withCompletedPrePayment()
            ->create([
                "departure_at" => $now->addMinutes(20),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        // Loan starts in less than 15 minutes, should have access.
        $loanStartingSoon = Loan::factory()
            ->withCompletedPrePayment()
            ->create([
                "departure_at" => $now->addMinutes(10),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        // Loan in process.
        $loanInProcess = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "departure_at" => $now->subMinutes(20),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        // Loan ended less than 15 minutes ago, should have access.
        $loanEndedRecently = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "departure_at" => $now->subMinutes(70),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        // Loan ended more than 15 minutes ago, should not have access.
        $loanEndedEarlier = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "departure_at" => $now->subMinutes(80),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        // Cancelled loans should never grant access.
        $canceledLoan = Loan::factory()
            ->withCompletedPrePayment()
            ->create([
                "canceled_at" => $now->addMinutes(10),
                "departure_at" => $now->addMinutes(10),
                "duration_in_minutes" => 60,
                "loanable_id" => $bikeWithPadlock,
            ]);

        $query = NokeSyncLoansCommand::getBorrowersForLoansWithPadlockMac(
            $bikeWithPadlock->padlock->mac_address
        );

        $borrowers = $query->get();

        $testBorrowersId = [];
        foreach ($borrowers as $borrower) {
            $testBorrowersId[] = $borrower->id;
        }

        $this->assertEqualsCanonicalizing(
            [
                $loanStartingSoon->borrower->user->id,
                $loanInProcess->borrower->user->id,
                $loanEndedRecently->borrower->user->id,
            ],
            $testBorrowersId
        );
    }
}
