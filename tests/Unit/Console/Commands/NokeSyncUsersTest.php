<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\NokeSyncUsers as NokeSyncUsersCommand;
use App\Facades\Noke;
use App\Models\Community;
use App\Models\User;
use App\Services\NokeService;
use Tests\TestCase;

class NokeSyncUsersTest extends TestCase
{
    public function testNokeSyncUsersCommand_getLocalUsers()
    {
        $communityUsingNoke = Community::factory()->create([
            "uses_noke" => true,
        ]);
        $communityNotUsingNoke = Community::factory()->create([
            "uses_noke" => false,
        ]);

        $userWithoutCommunity = User::factory()
            ->withBorrower()
            ->create([]);

        $userNotApprovedWithNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityUsingNoke, [])
            ->create([]);
        $userNotApprovedWithoutNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityNotUsingNoke, [])
            ->create([]);

        $userApprovedWithNoke = User::factory()
            ->hasAttached($communityUsingNoke, [
                "approved_at" => new \DateTime(),
            ])
            ->withBorrower()
            ->create([]);
        $userApprovedWithoutNoke = User::factory()
            ->withBorrower()
            ->hasAttached($communityNotUsingNoke, [
                "approved_at" => new \DateTime(),
            ])
            ->create([]);

        $users = NokeSyncUsersCommand::getLocalUsers();

        $this->assertEquals(1, $users->count());

        $this->assertTrue(
            $users->contains(function ($user, $key) use (
                $userApprovedWithNoke
            ) {
                return $user->id == $userApprovedWithNoke->id;
            })
        );
    }

    public function testNokeSyncUsers_onlyCreatesIfNecessary()
    {
        // Noke service as if nothing is synced
        Noke::forTest();
        // Events would create the Noke user once the user is created
        $this->withoutEvents();

        $communityUsingNoke = Community::factory()->create([
            "uses_noke" => true,
        ]);

        $userWithNokeAccount = User::factory()
            ->withBorrower()
            ->withCommunity($communityUsingNoke)
            ->create([
                "noke_id" => 1234,
            ]);

        $userWithoutNokeAccount = User::factory()
            ->withBorrower()
            ->withCommunity($communityUsingNoke)
            ->create([
                "email" => "unsynced@noke.user",
            ]);

        self::assertEmpty(Noke::fetchUsers());

        $this->artisan("noke:sync:users");

        // Since no Noke users existed, and a single one was created, we should only expect to see
        // the new one
        $nokeUsers = Noke::fetchUsers();
        self::assertCount(1, $nokeUsers);
        self::assertEquals("unsynced@noke.user", $nokeUsers[0]->username);
    }
}
