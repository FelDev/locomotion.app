<?php

namespace Helpers;

use App\Helpers\Path;
use Tests\TestCase;

class PathTest extends TestCase
{
    public function testArrayDiffAbsolute()
    {
        self::assertEquals(
            ["/path1", "/path3", "/path5/foo"],
            array_values(
                Path::arrayDiffAbsolute(
                    [
                        "/path1",
                        "/path2",
                        "path3",
                        "path4",
                        "path5/foo",
                        "path6/foo",
                    ],
                    ["path2", "/path4"],
                    ["/path6/foo"]
                )
            )
        );
    }

    public function testJoin()
    {
        self::assertEquals("/foo/bar", Path::join("/foo/", "/bar"));
        self::assertEquals("/foo/bar", Path::join("/foo", "/bar"));
        self::assertEquals("/foo/bar", Path::join("/foo/", "bar"));
        self::assertEquals("/foo/bar", Path::join("/foo", "bar"));
    }

    public function testBuild()
    {
        self::assertEquals("foo/bar/baz", Path::build("foo", "bar", "baz"));
    }

    public function testRoot()
    {
        self::assertEquals("foo", Path::root("/foo/bar"));
        self::assertEquals("foo", Path::root("foo/bar"));
        self::assertEquals("", Path::root("foo"));
    }

    public function testIsSameAbsolute()
    {
        self::assertTrue(Path::isSameAbsolute("foo/bar", "/foo/bar"));
        self::assertTrue(Path::isSameAbsolute("/foo/bar", "/foo/bar"));
        self::assertTrue(Path::isSameAbsolute("foo/bar", "foo/bar"));
        self::assertFalse(Path::isSameAbsolute("/foo/quux", "/foo/bar"));
    }

    public function testAsAbsolute()
    {
        self::assertEquals("/foo/bar", Path::asAbsolute("foo/bar"));
        self::assertEquals("/foo/bar", Path::asAbsolute("/foo/bar"));
    }
}
