<?php

namespace Helpers;

use App\Enums\FileMoveResult;
use App\Helpers\StorageHelper;
use Storage;

class StorageHelperTest extends \Tests\TestCase
{
    public function testCopy_doesntCopyToSameFile()
    {
        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::copy("foo/bar", "/foo/bar")
        );
        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::copy("/foo/bar", "/foo/bar")
        );
    }

    public function testCopy_doesntOverrideExisting()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::copy("foo/bar", "/foo/bar/baz")
        );
    }

    public function testCopy_doesCopyToNewLocation()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::copied,
            StorageHelper::copy("foo/bar", "/foo/bar/baz")
        );
    }

    public function testCopy_fallsBackToManualCopy()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("foo/bar")
            ->andReturn("filedata")
            ->once();
        Storage::shouldReceive("put")
            ->with("/foo/bar/baz", "filedata")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::copied,
            StorageHelper::copy("foo/bar", "/foo/bar/baz")
        );
    }

    public function testCopy_failsIfFallBackGetFails()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("foo/bar")
            ->andReturn(null)
            ->once();

        self::assertEquals(
            FileMoveResult::failed,
            StorageHelper::copy("foo/bar", "/foo/bar/baz")
        );
    }

    public function testCopy_failsIfFallBackPutFails()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("foo/bar")
            ->andReturn("filedata")
            ->once();
        Storage::shouldReceive("put")
            ->with("/foo/bar/baz", "filedata")
            ->andReturn(false)
            ->once();

        self::assertEquals(
            FileMoveResult::failed,
            StorageHelper::copy("foo/bar", "/foo/bar/baz")
        );
    }

    public function testMove_doesntCopyToSameFile()
    {
        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::move("foo/bar", "/foo/bar")
        );
        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::move("/foo/bar", "/foo/bar")
        );
    }

    public function testMove_doesntOverrideExisting()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("exists")
            ->with("foo/bar")
            ->andReturn(false)
            ->once();

        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );
    }

    public function testMove_deletesSourceIfNeedBe()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("exists")
            ->with("foo/bar")
            ->andReturn(true)
            ->once();

        $deleteAssertion = Storage::shouldReceive("delete")
            ->with("foo/bar")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::moved,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );

        $deleteAssertion->verify();
    }

    public function testMove_warnsIfSourceNotDeleted()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("exists")
            ->with("foo/bar")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("foo/bar")
            ->andReturn(false)
            ->once();

        \Log::spy();
        self::assertEquals(
            FileMoveResult::copied,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );
        \Log::shouldHaveReceived("warning")->once();
    }

    public function testMove_doesMoveToNewLocation()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("foo/bar")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::moved,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );
    }

    public function testMove_fallsBackToManualCopy()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("foo/bar")
            ->andReturn("filedata")
            ->once();
        Storage::shouldReceive("put")
            ->with("/foo/bar/baz", "filedata")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("foo/bar")
            ->andReturn(true)
            ->once();

        self::assertEquals(
            FileMoveResult::moved,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );
    }

    public function testMove_failsIfFallBackFails()
    {
        Storage::shouldReceive("exists")
            ->with("/foo/bar/baz")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("foo/bar", "/foo/bar/baz")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("foo/bar")
            ->andReturn("filedata")
            ->once();
        Storage::shouldReceive("put")
            ->with("/foo/bar/baz", "filedata")
            ->andReturn(false)
            ->once();

        self::assertEquals(
            FileMoveResult::failed,
            StorageHelper::move("foo/bar", "/foo/bar/baz")
        );
    }

    public function testMoveDirectoryFiles_doesntMoveToSameDirectoryFiles()
    {
        self::assertEquals(
            FileMoveResult::unchanged,
            StorageHelper::moveDirectoryFiles(
                "foo/bar",
                "/foo/bar",
                "quux",
                "quux"
            )
        );
    }

    public function testMoveDirectoryFiles()
    {
        Storage::fake();
        Storage::put("/foo/bar/afile.txt", "a");
        Storage::put("/foo/bar/foo_afile.txt", "foo_a");
        Storage::put("/foo/bar/bar_afile.txt", "bar_a");

        $result = StorageHelper::moveDirectoryFiles(
            "/foo/bar",
            "/baz/bar",
            "afile",
            "bfile"
        );

        self::assertEquals(FileMoveResult::moved, $result);

        self::assertFalse(Storage::exists("/foo/bar/afile.txt"));
        self::assertFalse(Storage::exists("/foo/bar/foo_afile.txt"));
        self::assertFalse(Storage::exists("/foo/bar/bar_afile.txt"));

        self::assertTrue(Storage::exists("/baz/bar/bfile.txt"));
        self::assertTrue(Storage::exists("/baz/bar/foo_bfile.txt"));
        self::assertTrue(Storage::exists("/baz/bar/bar_bfile.txt"));
    }

    public function testMoveDirectoryFiles_doesNotOverwrite()
    {
        Storage::fake();
        Storage::put("/foo/bar/afile.txt", "a");
        Storage::put("/foo/bar/foo_afile.txt", "foo_a");
        Storage::put("/foo/bar/bar_afile.txt", "bar_a");

        Storage::put("/baz/bar/bar_bfile.txt", "bar_a_original");

        $result = StorageHelper::moveDirectoryFiles(
            "/foo/bar",
            "/baz/bar",
            "afile",
            "bfile"
        );

        self::assertEquals(FileMoveResult::moved, $result);

        self::assertFalse(Storage::exists("/foo/bar/afile.txt"));
        self::assertFalse(Storage::exists("/foo/bar/foo_afile.txt"));
        self::assertFalse(Storage::exists("/foo/bar/bar_afile.txt"));

        self::assertTrue(Storage::exists("/baz/bar/bfile.txt"));
        self::assertTrue(Storage::exists("/baz/bar/foo_bfile.txt"));
        self::assertTrue(Storage::exists("/baz/bar/bar_bfile.txt"));

        self::assertEquals(
            "bar_a_original",
            Storage::get("/baz/bar/bar_bfile.txt")
        );
    }

    public function testMoveDirectoryFiles_returnsCopiedIfAnySourceNotDeleted()
    {
        Storage::shouldReceive("files")
            ->with("/foo/bar")
            ->andReturn(["/foo/bar/afile.txt", "/foo/bar/foo_afile.txt"]);

        Storage::shouldReceive("exists")
            ->with("/baz/bar/bfile.txt")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("/foo/bar/afile.txt", "/baz/bar/bfile.txt")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("/foo/bar/afile.txt")
            ->andReturn(true)
            ->once();

        Storage::shouldReceive("exists")
            ->with("/baz/bar/foo_bfile.txt")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("/foo/bar/foo_afile.txt", "/baz/bar/foo_bfile.txt")
            ->andReturn(true)
            ->once();
        // this one we fail to delete
        Storage::shouldReceive("delete")
            ->with("/foo/bar/foo_afile.txt")
            ->andReturn(false)
            ->once();

        $result = StorageHelper::moveDirectoryFiles(
            "/foo/bar",
            "/baz/bar",
            "afile",
            "bfile"
        );

        self::assertEquals(FileMoveResult::copied, $result);
    }

    public function testMoveDirectoryFiles_willTryToRollbackAndThrowExceptionIfFails()
    {
        Storage::shouldReceive("files")
            ->with("/foo/bar")
            ->andReturn(["/foo/bar/afile.txt", "/foo/bar/foo_afile.txt"]);

        Storage::shouldReceive("exists")
            ->with("/baz/bar/bfile.txt")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("/foo/bar/afile.txt", "/baz/bar/bfile.txt")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("/foo/bar/afile.txt")
            ->andReturn(true)
            ->once();

        Storage::shouldReceive("exists")
            ->with("/baz/bar/foo_bfile.txt")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("/foo/bar/foo_afile.txt", "/baz/bar/foo_bfile.txt")
            ->andReturn(false)
            ->once();
        Storage::shouldReceive("get")
            ->with("/foo/bar/foo_afile.txt")
            ->andReturn("filedata")
            ->once();
        Storage::shouldReceive("put")
            ->with("/baz/bar/foo_bfile.txt", "filedata")
            ->andReturn(false)
            ->once();

        // Will receive attempt to move back
        Storage::shouldReceive("exists")
            ->with("/foo/bar/afile.txt")
            ->andReturn(false)
            ->twice();
        Storage::shouldReceive("copy")
            ->with("/baz/bar/bfile.txt", "/foo/bar/afile.txt")
            ->andReturn(true)
            ->once();
        Storage::shouldReceive("delete")
            ->with("/baz/bar/bfile.txt")
            ->andReturn(true)
            ->once();

        $this->expectExceptionMessage(
            "Failed moving files from /foo/bar to /baz/bar"
        );
        StorageHelper::moveDirectoryFiles(
            "/foo/bar",
            "/baz/bar",
            "afile",
            "bfile"
        );
    }
}
