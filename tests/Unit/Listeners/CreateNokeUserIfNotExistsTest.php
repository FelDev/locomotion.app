<?php

namespace Tests\Unit\Listeners;

use App\Events\LoanCreatedEvent;
use App\Events\RegistrationApprovedEvent;
use App\Facades\Noke;
use App\Listeners\CreateNokeUserIfNotExists;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Pivots\CommunityUser;
use App\Models\User;
use Tests\TestCase;

class CreateNokeUserIfNotExistsTest extends TestCase
{
    public function testCreatesNokeUserOnApproval()
    {
        // Without events to avoid triggering noke user creation on save
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => true]))
                ->create()
        );

        $expectation = Noke::shouldReceive("createUser")
            ->with(\Mockery::on(fn($argument) => $user->is($argument)))
            ->once();

        $listener = app()->make(CreateNokeUserIfNotExists::class);
        $listener->handle(
            new RegistrationApprovedEvent($user->communities[0]->pivot)
        );

        $expectation->verify();
    }

    public function testDoesntCreatesNokeUserOnApprovalIfNotUsingNoke()
    {
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => false]))
                ->create()
        );

        $expectation = Noke::shouldReceive("createUser")
            ->with($user)
            ->never();

        $listener = app()->make(CreateNokeUserIfNotExists::class);
        $listener->handle(
            new RegistrationApprovedEvent($user->main_community->pivot)
        );

        $expectation->verify();
    }

    public function testCreatesNokeUserOnApproval_doesntPropagateException()
    {
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => true]))
                ->create()
        );

        $expectation = Noke::shouldReceive("createUser")
            ->with(\Mockery::on(fn($argument) => $user->is($argument)))
            ->andThrow(new \Exception())
            ->once();

        $listener = app()->make(CreateNokeUserIfNotExists::class);
        $listener->handle(
            new RegistrationApprovedEvent($user->main_community->pivot)
        );

        $expectation->verify();
    }

    public function testCreatesNokeUserOnLoanCreation()
    {
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => true]))
                ->create()
        );
        $loan = Loan::factory()->create([
            "loanable_id" => Loanable::factory()->withPadlock(),
        ]);

        $expectation = Noke::shouldReceive("createUser")
            ->with($user)
            ->once();
        $listener = app()->make(CreateNokeUserIfNotExists::class);
        $listener->handle(new LoanCreatedEvent($user, $loan));

        $expectation->verify();
    }

    public function testDoesntCreatesNokeUserOnLoanCreationWithoutPadlock()
    {
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => true]))
                ->create()
        );
        $loan = Loan::factory()->create([
            "loanable_id" => Loanable::factory(),
        ]);

        $expectation = Noke::shouldReceive("createUser")
            ->with($user)
            ->never();
        $listener = app()->make(CreateNokeUserIfNotExists::class);
        $listener->handle(new LoanCreatedEvent($user, $loan));

        $expectation->verify();
    }

    public function testCreatesNokeUserOnLoanCreation_propagateException()
    {
        $user = CommunityUser::withoutEvents(
            fn() => User::factory()
                ->withCommunity(Community::factory(["uses_noke" => true]))
                ->create()
        );
        $loan = Loan::factory()->create([
            "loanable_id" => Loanable::factory()->withPadlock(),
        ]);

        $expectation = Noke::shouldReceive("createUser")
            ->with($user)
            ->andThrow(new \Exception())
            ->once();
        $listener = app()->make(CreateNokeUserIfNotExists::class);

        $this->expectException(\Exception::class);
        $listener->handle(new LoanCreatedEvent($user, $loan));

        $expectation->verify();
    }
}
