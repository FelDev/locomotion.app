<?php

/**
 * tests\Unit\Mail\Registration\ReviewableTest
 *
 * Test the integrity of the notification sent to administrators when a user completes its registration
 */

namespace Tests\Unit\Mail\Registration;
use App\Events\RegistrationSubmittedEvent;
use App\Listeners\SendRegistrationSubmittedEmails;
use App\Mail\Registration\Reviewable as RegistrationReviewable;
use App\Models\Community;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use function PHPUnit\Framework\assertEquals;

class ReviewableTest extends TestCase
{
    public function testRegistrationAdminEmailContentIntegrity()
    {
        $user = User::factory()->create([
            "name" => "John",
            "last_name" => "Doe",
        ]);

        $community = Community::factory()->create([
            "name" => "Community_Name",
        ]);

        $mail = new RegistrationReviewable($user, $community);
        $mail_content = preg_replace("/\s+/", " ", $mail->render());

        // Search for the community's name.
        $this->assertStringContainsString("Community_Name", $mail_content);

        // Search for the member's name.
        $this->assertStringContainsString("John Doe", $mail_content);
    }

    public function testRegistrationAdminsEmailDelivery()
    {
        Mail::fake();

        // Prevent the event to send customer's email since this test is admin-only.
        $meta = [];
        $meta["sent_registration_submitted_email"] = true;

        // Fake User
        $user = User::factory()->create([
            "name" => "John",
            "last_name" => "Doe",
            "meta" => $meta,
        ]);
        $community = Community::factory()->create([
            "name" => "Community_Name",
        ]);
        $community->users()->attach($user);

        $now = Carbon::now();
        Carbon::setTestNow($now);

        // Fake global and local admins
        $global_admin = User::factory()->create(["role" => "admin"]);
        $community_admin = User::factory()
            ->adminOfCommunity($community)
            ->create();

        // This doesn't trigger Event but only the Listener.
        $event = new RegistrationSubmittedEvent($user->main_community->pivot);
        $listener = app()->make(SendRegistrationSubmittedEmails::class);
        $listener->handle($event);

        // Check mail delivery for global admin
        Mail::assertQueued(RegistrationReviewable::class, function ($mail) use (
            $global_admin
        ) {
            return $mail->hasTo($global_admin->email);
        });

        // Check mail delivery for local admin
        Mail::assertQueued(RegistrationReviewable::class, function ($mail) use (
            $community_admin
        ) {
            return $mail->hasTo($community_admin->email);
        });

        $user->refresh();

        assertEquals(
            $now->toISOString(),
            $user->meta["last_registration_reviewable_date"][$community->id]
        );
    }

    public function testRegistrationReviewableSentAtMostOnceAnHour()
    {
        Mail::fake();

        $community = Community::factory()->create();

        $meta = [];
        $meta["sent_registration_submitted_email"] = true;
        $meta["last_registration_reviewable_date"] = [
            $community->id => Carbon::now()->subMinutes(5),
        ];

        $user = User::factory()
            ->withCommunity($community)
            ->create([
                "meta" => $meta,
            ]);

        // Fake global and local admins
        $global_admin = User::factory()->create(["role" => "admin"]);
        $community_admin = User::factory()
            ->adminOfCommunity($user->main_community)
            ->create();

        // This doesn't trigger Event but only the Listener.
        $event = new RegistrationSubmittedEvent($user->main_community->pivot);
        $listener = app()->make(SendRegistrationSubmittedEmails::class);
        $listener->handle($event);

        Mail::assertNotQueued(RegistrationReviewable::class);

        Carbon::setTestNow(Carbon::now()->addHour());
        $listener->handle($event);
        Mail::assertQueued(RegistrationReviewable::class);
    }
}
