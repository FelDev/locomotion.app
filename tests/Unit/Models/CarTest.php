<?php

namespace Tests\Unit\Models;

use App\Models\Car;
use App\Models\Loanable;
use Tests\TestCase;

class CarTest extends TestCase
{
    public function testGetDailyPremiumFromValueCategoryAttribute()
    {
        $car = Loanable::factory()
            ->withCar(["value_category" => "lte50k"])
            ->create();
        $this->assertEquals(
            4,
            $car->details->daily_premium_from_value_category
        );

        $car = Loanable::factory()
            ->withCar(["value_category" => "lte70k"])
            ->create();
        $this->assertEquals(
            5,
            $car->details->daily_premium_from_value_category
        );

        $car = Loanable::factory()
            ->withCar(["value_category" => "lte100k"])
            ->create();
        $this->assertEquals(
            6,
            $car->details->daily_premium_from_value_category
        );
    }
}
