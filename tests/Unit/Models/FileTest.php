<?php

namespace Models;

use App\Enums\FileMoveResult;
use App\Models\File;
use App\Models\User;
use function PHPUnit\Framework\assertEquals;

class FileTest extends \Tests\TestCase
{
    public function testUpdateLocation()
    {
        $user = User::factory()->create();

        \Storage::fake();
        \Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.txt",
            "filedata"
        );
        $file = File::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.txt",
            "field" => "proof",
            "fileable_type" => "user",
            "fileable_id" => $user->id,
        ]);

        $file->updateFilePath();

        $expectedFilename = $file->generateUniqueName();
        self::assertEquals("files/user/$user->id/proof", $file->path);
        self::assertEquals($file->generateUniqueName(), $file->filename);

        self::assertTrue(
            \Storage::exists("files/user/$user->id/proof/$expectedFilename")
        );
        self::assertFalse(
            \Storage::exists(
                "storage/some/wrong/initial/path/wrongfilename.txt"
            )
        );

        assertEquals(FileMoveResult::unchanged, $file->updateFilePath());
    }

    public function testDelete_deletesFile()
    {
        $user = User::factory()->create();

        \Storage::fake();
        \Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.txt",
            "filedata"
        );
        $file = File::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.txt",
            "field" => "proof",
            "fileable_type" => "user",
            "fileable_id" => $user->id,
        ]);

        $expectedFilename = $file->generateUniqueName();
        $file->delete();

        self::assertFalse(
            \Storage::exists("files/user/$user->id/proof/$expectedFilename")
        );
        self::assertFalse(
            \Storage::exists(
                "storage/some/wrong/initial/path/wrongfilename.txt"
            )
        );
    }

    public function testDelete_logsIfFails()
    {
        $user = User::factory()->create();

        \Storage::fake();

        $file = File::factory()->create([
            "path" => "some/wrong/initial/path",
            "filename" => "wrongfilename.txt",
            "field" => "proof",
            "fileable_type" => "user",
            "fileable_id" => $user->id,
        ]);

        \Storage::shouldReceive("delete")->once();

        $assertion = \Log::shouldReceive("warning")
            ->with(
                \Mockery::on(
                    fn($log) => str_starts_with(
                        $log,
                        "[File.php] Failed to delete file"
                    )
                )
            )
            ->once();

        $file->delete();

        $assertion->verify();
    }
}
