<?php

namespace Models;

use App\Enums\FileMoveResult;
use App\Models\Image;
use App\Models\User;
use function PHPUnit\Framework\assertEquals;

class ImageTest extends \Tests\TestCase
{
    public function testUpdateLocation()
    {
        $user = User::factory()->create();

        \Storage::fake();
        \Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.jpg",
            "filedata"
        );
        \Storage::put(
            "storage/some/wrong/initial/path/thumbnail_wrongfilename.jpg",
            "filedata"
        );
        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.jpg",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
        ]);

        $image->updateFilePath();

        $uniqueDirName = $image->generateUniqueFolderName();
        self::assertEquals(
            "images/user/$user->id/avatar/$uniqueDirName",
            $image->path
        );
        self::assertEquals("avatar.jpg", $image->filename);

        self::assertTrue(
            \Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/avatar.jpg"
            )
        );
        self::assertTrue(
            \Storage::exists(
                "images/user/$user->id/avatar/$uniqueDirName/thumbnail_avatar.jpg"
            )
        );
        self::assertFalse(\Storage::exists("storage/some/wrong/initial/path"));

        assertEquals(FileMoveResult::unchanged, $image->updateFilePath());
    }

    public function testDelete_deletesWholeFolder()
    {
        $user = User::factory()->create();

        \Storage::fake();
        \Storage::put(
            "storage/some/wrong/initial/path/wrongfilename.jpg",
            "filedata"
        );
        \Storage::put(
            "storage/some/wrong/initial/path/thumbnail_wrongfilename.jpg",
            "filedata"
        );
        $image = Image::factory()->create([
            "path" => "storage/some/wrong/initial/path",
            "filename" => "wrongfilename.jpg",
            "field" => "avatar",
            "imageable_type" => "user",
            "imageable_id" => $user->id,
        ]);

        $uniqueDirName = $image->generateUniqueFolderName();
        $image->delete();

        self::assertFalse(
            \Storage::exists("images/user/$user->id/avatar/$uniqueDirName")
        );
        self::assertFalse(\Storage::exists("storage/some/wrong/initial/path"));
    }

    public function testDelete_logsIfFails()
    {
        $user = User::factory()->create();

        \Storage::fake();

        $image = Image::withoutEvents(
            fn() => Image::factory()->create([
                "path" => "some/wrong/initial/path",
                "filename" => "wrongfilename.jpg",
                "field" => "avatar",
                "imageable_type" => "user",
                "imageable_id" => $user->id,
            ])
        );

        \Storage::shouldReceive("deleteDirectory")->once();

        $assertion = \Log::shouldReceive("warning")
            ->with(
                \Mockery::on(
                    fn($log) => str_starts_with(
                        $log,
                        "[Image.php] Failed to delete directory"
                    )
                )
            )
            ->once();

        $image->delete();

        $assertion->verify();
    }
}
