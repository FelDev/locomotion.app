<?php

namespace Tests\Unit\Models;

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Community;
use App\Models\Extension;
use App\Models\Handover;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Payment;
use App\Models\User;
use Carbon\Carbon;
use Tests\TestCase;
use Carbon\CarbonImmutable;

class LoanTest extends TestCase
{
    // TODO: Move to a calendar helper (#1080).
    public function testGetCalendarDays()
    {
        // Loan starts before midnight
        $this->assertEquals(
            5,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-16 23:45:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Loan starts at midnight
        $this->assertEquals(
            4,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-17 00:00:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Loan starts after midnight
        $this->assertEquals(
            4,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-17 00:15:00"),
                new CarbonImmutable("2022-04-20 12:30:00")
            )
        );

        // Loan ends before midnight
        $this->assertEquals(
            5,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:30:00"),
                new CarbonImmutable("2022-04-20 23:45:00")
            )
        );

        // Loan ends at midnight
        $this->assertEquals(
            5,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:00:00"),
                new CarbonImmutable("2022-04-21 00:00:00")
            )
        );

        // Loan ends after midnight
        $this->assertEquals(
            6,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:15:00"),
                new CarbonImmutable("2022-04-21 00:15:00")
            )
        );

        // Loan spans some years.
        $this->assertEquals(
            4024,
            Loan::getCalendarDays(
                new CarbonImmutable("2014-04-16 12:15:00"),
                new CarbonImmutable("2025-04-21 22:15:00")
            )
        );

        // Loan starts and ends at the same time.
        $this->assertEquals(
            0,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-16 12:15:00"),
                new CarbonImmutable("2022-04-16 12:15:00")
            )
        );

        // Loan ends before it starts.
        $this->assertEquals(
            0,
            Loan::getCalendarDays(
                new CarbonImmutable("2022-04-20 12:30:00"),
                new CarbonImmutable("2022-04-16 23:45:00")
            )
        );
    }

    public function testCancel_Now()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->cancel();

        $this->assertTrue($loan->isCanceled());

        $this->assertNotNull($loan->canceled_at);
        $this->assertEquals("canceled", $loan->status);
    }

    public function testCancel_At()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->cancel(new CarbonImmutable("2022-04-16 12:34:56"));

        $this->assertTrue($loan->isCanceled());

        $this->assertNotNull($loan->canceled_at);
        $this->assertEquals("canceled", $loan->status);
        $this->assertEquals("2022-04-16 12:34:56", $loan->canceled_at);
    }

    public function testIsCanceled_Status()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->status = "canceled";

        $this->assertTrue($loan->isCanceled());
    }

    public function testIsCanceled_CanceledAt()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        $this->assertFalse($loan->isCanceled());

        $loan->canceled_at = new CarbonImmutable();

        $this->assertTrue($loan->isCanceled());
    }

    public function testIsContested_TakeoverContested()
    {
        $loan = Loan::factory()
            ->withContestedTakeover()
            ->create();

        $this->assertTrue($loan->is_contested);
    }

    public function testIsContested_HandoverContested()
    {
        $loan = Loan::factory()
            ->withContestedHandover()
            ->create();

        $this->assertTrue($loan->is_contested);
    }

    public function testIsNotContested_OtherActionsCanceled()
    {
        $loan = Loan::factory()
            ->withCanceledPrePayment()
            ->withCanceledExtension()
            ->create();

        $this->assertFalse($loan->is_contested);
    }

    public function testGetStatusFromActions_IntentionInProcess()
    {
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create();

        // Refresh loan from database
        $loan = $loan->fresh();

        // Assert that loan is in_process.
        $this->assertEquals("in_process", $loan->getStatusFromActions());
    }

    public function testGetStatusFromActions_PaymentInProcess()
    {
        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create();

        // Refresh loan from database
        $loan = $loan->fresh();

        // Assert that loan is in_process.
        $this->assertEquals("in_process", $loan->getStatusFromActions());
    }

    public function testGetStatusFromActions_PaymentCompleted()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create();

        // Refresh loan from database
        $loan = $loan->fresh();

        // Assert has payment completed.
        $this->assertEquals(
            "completed",
            $loan->payment ? $loan->payment->status : ""
        );

        // Assert that loan is completed.
        $this->assertEquals("completed", $loan->getStatusFromActions());
    }

    public function testLoanTimesAndDurations_IntentionInProcess()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Refresh loan from database
        $loan = $loan->fresh();

        $this->assertEquals("in_process", $loan->intention->status);

        // Validate loan return time.
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_IntentionCompleted()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        $this->assertEquals("completed", $loan->intention->status);

        // Validate loan return time.
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ExtensionInProcess()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Extension in process
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "in_process",
            ])
        );

        $loan->refresh();

        // Assert that the loan return time was not changed
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ExtensionsCompleted()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Completed extensions
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 150,
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(150)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(150, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ExtensionRejected()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Rejected extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "rejected",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts is not impacted by rejected extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ExtensionCanceled()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Canceled extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "canceled",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts is not impacted by canceled extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_EarlyReturn_PaymentInProcess()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Completed handover
        $loan->handover()->save(
            Handover::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(45),
            ])
        );

        // Payment in process
        $loan->payment()->save(
            Payment::factory()->make([
                "status" => "in_process",
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts is not impacted early
        // return (completed handover) when payment still in process.
        $this->assertTrue(
            $departureAt
                ->addMinutes(75)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(75, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_EarlyPayment()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Completed handover
        $loan->handover()->save(
            Handover::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(45),
            ])
        );

        // Completed payment
        $loan->payment()->save(
            Payment::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(60),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for early payment.
        $this->assertTrue(
            $departureAt
                ->addMinutes(60)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(60, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_EarlyPaymentWithAcceptedExtension()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 75,
            ]);

        // Accepted extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 120,
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        // Completed handover
        $loan->handover()->save(
            Handover::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(90),
            ])
        );

        // Completed payment
        $loan->payment()->save(
            Payment::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(105),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(105)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(105, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(1, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_ShortSpanOverTwoCalendarDays()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(23)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 60,
            ]);

        $loan->refresh();

        // Validate loan return time.
        $this->assertTrue(
            $departureAt
                ->addMinutes(60)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(60, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(2, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDays()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "departure_at" => $departureAt,
                // Arbitrarily more than 3 days. Loan then spans onto 4 calendar days.
                "duration_in_minutes" => 3 * 24 * 60 + 415,
            ]);

        $loan->refresh();

        // Validate loan return time.
        $this->assertTrue(
            $departureAt
                ->addMinutes(4735)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(4735, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(4, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDaysWithAcceptedExtension()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 24 * 60 + 345,
            ]);

        // Accepted extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 3 * 24 * 60 + 415,
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(4735)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(4735, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(4, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDaysAndEarlyPayment()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                // Arbitrarily more than 3 days. Loan then spans onto 4 calendar days.
                "duration_in_minutes" => 3 * 24 * 60 + 415,
            ]);

        // Completed handover
        $loan->handover()->save(
            Handover::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(90),
            ])
        );

        // Completed payment
        $loan->payment()->save(
            Payment::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(2 * 24 * 60 + 675),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(3555)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(3555, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(3, $loan->calendar_days);
    }

    public function testLoanTimesAndDurations_MultipleCalendarDaysEarlyPaymentWithAcceptedExtension()
    {
        // Milliseconds get truncated in the database, zero them out so as to
        // compare on seconds only.
        $departureAt = CarbonImmutable::now()
            ->setHours(4)
            ->setMinutes(30)
            ->setSeconds(0)
            ->setMilliseconds(0);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "departure_at" => $departureAt,
                "duration_in_minutes" => 24 * 60 + 345,
            ]);

        // Accepted extension
        $loan->extensions()->save(
            Extension::factory()->make([
                "new_duration" => 3 * 24 * 60 + 415,
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(15),
            ])
        );

        // Completed handover
        $loan->handover()->save(
            Handover::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(90),
            ])
        );

        // Completed payment
        $loan->payment()->save(
            Payment::factory()->make([
                "status" => "completed",
                "executed_at" => $departureAt->addMinutes(2 * 24 * 60 + 675),
            ])
        );

        $loan->refresh();

        // Assert that the loan return time accounts for early payment with extensions.
        $this->assertTrue(
            $departureAt
                ->addMinutes(3555)
                ->equalTo($loan->getActualReturnAtFromActions())
        );

        // Check the loan's actual duration.
        $this->assertEquals(3555, $loan->actual_duration_in_minutes);

        // Check the loan's number of calendar days.
        $this->assertEquals(3, $loan->calendar_days);
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        Loan::factory()->create();
        Loan::factory()->create();

        self::assertCount(2, Loanable::for("edit", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someLoan = Loan::factory()->create();
        Loan::factory()->create();

        $communityAdmin = User::factory()
            ->hasAttached($someLoan->community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        self::assertCount(1, Loanable::for("edit", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someLoan = Loan::factory()->create();
        $otherLoan = Loan::factory()->create();

        $regularUser = User::factory()
            ->hasAttached($someLoan->community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherLoan->community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, Loanable::for("edit", $regularUser)->get());
    }

    public function testActualDistance_includesContestedHandover()
    {
        $loan = Loan::factory()
            ->withCompletedHandover(1000, 1500)
            ->create([
                "estimated_distance" => 100,
            ]);

        self::assertEquals(500, $loan->actual_distance);

        $loan->handover->contest();
        $loan->handover->save();
        $loan->refresh();

        self::assertEquals(500, $loan->actual_distance);
    }
}
