<?php

namespace Tests\Unit\Models;

use App\Models\Bike;
use App\Models\Borrower;
use App\Models\Car;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\Trailer;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class LoanableTest extends TestCase
{
    public $borough;
    public $community;
    public $otherCommunity;
    public $memberOfCommunity;
    public $otherMemberOfCommunity;
    public $memberOfOtherCommunity;

    public $boroughLoanable;

    public function setUp(): void
    {
        parent::setUp();

        // Linking users and communities would trigger RegistrationApprovedEvent
        // which would then send email using an external service.
        // withoutEvents() makes the test robust to a non-existent or
        // incorrectly-configured email service.
        $this->withoutEvents();
        $this->community = Community::factory()->create();
        $this->otherCommunity = Community::factory()->create();

        $this->memberOfCommunity = User::factory()->create([
            "name" => "memberOfCommunity",
        ]);
        $this->community->users()->attach($this->memberOfCommunity, [
            "approved_at" => new \DateTime(),
        ]);

        $this->otherMemberOfCommunity = User::factory()->create([
            "name" => "otherMemberOfCommunity",
        ]);
        $this->community->users()->attach($this->otherMemberOfCommunity);

        $this->memberOfOtherCommunity = User::factory()->create([
            "name" => "memberOfOtherCommunity",
        ]);
        $this->otherCommunity->users()->attach($this->memberOfOtherCommunity, [
            "approved_at" => new \DateTime(),
        ]);

        foreach (
            [
                $this->memberOfCommunity,
                $this->otherMemberOfCommunity,
                $this->memberOfOtherCommunity,
            ]
            as $member
        ) {
            $member->owner = new Owner();
            $member->owner->user()->associate($member);
            $member->owner->save();
        }

        foreach (
            [
                $this->memberOfCommunity,
                $this->otherMemberOfCommunity,
                $this->memberOfOtherCommunity,
            ]
            as $member
        ) {
            Loanable::factory()
                ->withTrailer()
                ->create([
                    "name" => "$member->name trailer",
                    "owner_id" => $member->owner->id,
                ]);
        }
    }

    public function testLoanableNotAccessibleAccrossCommunitiesByDefault()
    {
        foreach (
            [
                $this->memberOfCommunity,
                $this->otherMemberOfCommunity,
                $this->memberOfOtherCommunity,
            ]
            as $member
        ) {
            $loanables = Loanable::accessibleBy($member)->pluck("name");
            $loanableIds = Loanable::accessibleBy($member)->pluck("id");
            $this->assertEquals(
                1,
                $loanables->count(),
                "too many loanables accessible to $member->name"
            );
            $this->assertEquals(
                $member->loanables()->first()->id,
                $loanableIds->first()
            );
        }
    }

    public function testLoanableBecomesAccessibleIfCommunityMembershipIsApproved()
    {
        $loanables = Loanable::accessibleBy($this->memberOfCommunity)->pluck(
            "id"
        );
        $this->assertEquals(1, $loanables->count());
        $this->assertEquals(
            $this->memberOfCommunity->loanables()->first()->id,
            $loanables->first()
        );

        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $loanables = Loanable::accessibleBy($this->memberOfCommunity)
            ->pluck("id")
            ->sort();
        $this->assertEquals(2, $loanables->count());
        $this->assertEquals(
            $this->memberOfCommunity->loanables()->first()->id,
            $loanables[0]
        );
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanables()->first()->id,
            $loanables[1]
        );
    }

    public function testCarBecomesAccessibleIfBorrowerIsApproved()
    {
        $car = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $this->memberOfCommunity->owner->id,
            ]);

        $loanables = Loanable::accessibleBy($this->memberOfCommunity)->pluck(
            "id"
        );
        $this->assertEquals(2, $loanables->count());

        $loanables = Loanable::accessibleBy(
            $this->otherMemberOfCommunity
        )->pluck("id");
        $this->assertEquals(1, $loanables->count());
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanables()->first()->id,
            $loanables[0]
        );

        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $loanables = Loanable::accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(2, $loanables->count());
        $firstId = $this->memberOfCommunity->loanables()->first()->id;
        $this->assertEquals(
            $firstId,
            $loanables[0],
            "$firstId not in " . implode(",", $loanables->toArray())
        );
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanables()->first()->id,
            $loanables[1]
        );

        $borrower = new Borrower();
        $borrower->user()->associate($this->otherMemberOfCommunity);
        $borrower->approved_at = new \DateTime();
        $borrower->save();

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $loanables = Loanable::accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(3, $loanables->count());
        $this->assertEquals(
            $this->memberOfCommunity->loanables()->first()->id,
            $loanables[0]
        );
        $this->assertEquals(
            $this->otherMemberOfCommunity->loanables()->first()->id,
            $loanables[1]
        );
        $this->assertEquals($car->id, $loanables[2]);
    }

    public function testLoanableAccessibleThroughInheritedClasses()
    {
        Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $this->memberOfCommunity->owner->id,
            ]);

        $bikes = Bike::accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $bikes->count());

        $trailers = Trailer::accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $trailers->count());

        $cars = Car::accessibleBy($this->memberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $cars->count());

        $cars = Car::accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $cars->count());

        $this->community
            ->users()
            ->updateExistingPivot($this->otherMemberOfCommunity->id, [
                "approved_at" => new \DateTime(),
            ]);

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $cars = Car::accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(0, $cars->count());

        $borrower = new Borrower();
        $borrower->user()->associate($this->otherMemberOfCommunity);
        $borrower->approved_at = new \DateTime();
        $borrower->save();

        $this->otherMemberOfCommunity = $this->otherMemberOfCommunity->fresh();

        $cars = Car::accessibleBy($this->otherMemberOfCommunity)
            ->orderBy("id")
            ->pluck("id");
        $this->assertEquals(1, $cars->count());
    }

    public function testIsAvailableEventIfLoanExistsWithIntentionInProcessOrCanceled()
    {
        $bike = Loanable::factory()->create([
            "owner_id" => $this->memberOfCommunity->owner->id,
        ]);

        $user = User::factory()
            ->withBorrower()
            ->create();
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "borrower_id" => $user->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-10 10:10:00",
                "duration_in_minutes" => 60,
            ]);

        $canceledLoan = Loan::factory()
            ->withInProcessPrePayment()
            ->create([
                "borrower_id" => $user->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-11 10:10:00",
                "duration_in_minutes" => 60,
                "canceled_at" => now(),
            ]);

        $canceledPrePaymentLoan = Loan::factory()
            ->withCanceledPrePayment()
            ->create([
                "borrower_id" => $user->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-11 10:10:00",
                "duration_in_minutes" => 60,
            ]);

        $confirmedLoan = Loan::factory()
            ->withInProcessPrePayment()
            ->create([
                "borrower_id" => $user->borrower->id,
                "loanable_id" => $bike->id,
                "community_id" => $this->community->id,
                "departure_at" => "3000-10-12 10:10:00",
                "duration_in_minutes" => 60,
            ])
            ->refresh();
        $confirmedLoan->intention->complete()->save();
        $confirmedLoan->refresh();

        $this->assertEquals(
            true,
            $bike->isAvailable("3000-10-10 10:20:00", 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable("3000-10-10 11:20:00", 60)
        );

        $this->assertEquals(
            true,
            $bike->isAvailable("3000-10-11 10:20:00", 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable("3000-10-11 11:20:00", 60)
        );

        $this->assertEquals(
            false,
            $bike->isAvailable("3000-10-12 10:20:00", 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable("3000-10-12 11:20:00", 60)
        );
    }

    public function testIsAvailableEarlyIfPaidBeforeDurationInMinutes()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "duration_in_minutes" => 60,
            ]);

        $bike = $loan->loanable;

        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(61, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(59, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(31, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(29, "minutes"), 60)
        );

        // The loan was completed earlier
        $payment = $loan->payment()->first();
        $payment->executed_at = Carbon::now()->add(30, "minutes");
        $payment->save();

        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(61, "minutes"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(59, "minutes"), 60)
        );
        $this->assertEquals(
            true,
            $bike->isAvailable(Carbon::now()->add(31, "minutes"), 60)
        );
        $this->assertEquals(
            false,
            $bike->isAvailable(Carbon::now()->add(29, "minutes"), 60)
        );
    }

    public function testLoanableAvailabilityRulesAreEmptyWhenInvalid()
    {
        $trailer = Loanable::factory()
            ->withTrailer()
            ->create([
                "availability_json" => "[{",
            ]);

        Log::shouldReceive("error")
            ->once()
            ->withArgs(function ($message) use ($trailer) {
                return strpos($message, "\"[{\"") !== false &&
                    strpos($message, (string) $trailer->id) !== false;
            });

        $rules = $trailer->getAvailabilityRules();

        $this->assertEquals([], $rules);
    }

    public function testLoanableAvailabilityRules()
    {
        $trailer = Loanable::factory()
            ->withTrailer()
            ->create([
                "availability_json" =>
                    '[{"available":true,"type":"weekdays","scope":["MO","TU","WE","TH","FR"],"period":"00:00-24:00"}]',
            ]);

        $rules = $trailer->getAvailabilityRules();

        $expected = [
            [
                "available" => true,
                "type" => "weekdays",
                "scope" => ["MO", "TU", "WE", "TH", "FR"],
                "period" => "00:00-24:00",
            ],
        ];

        $this->assertEquals($expected, $rules);
    }

    public function testAddCoowner()
    {
        $loanable = Loanable::factory()->create();
        $user = User::factory()->create();

        $initialCoownerCount = $loanable->coowners->count();

        $loanable->addCoowner($user->id);
        $loanable->refresh();

        self::assertCount($initialCoownerCount + 1, $loanable->coowners);
        self::assertContains(
            $user->id,
            $loanable->coowners->map(fn($coowner) => $coowner->user->id)
        );
    }

    public function testRemoveCoowner()
    {
        /** @var Loanable $loanable */
        $loanable = Loanable::factory()->create();
        $user = User::factory()->create();

        $initialCoownerCount = $loanable->coowners->count();

        $loanable->addCoowner($user->id);
        $loanable->refresh();
        $loanable->removeCoowner($user->id);
        $loanable->refresh();

        self::assertCount($initialCoownerCount, $loanable->coowners);
        self::assertNotContains(
            $user->id,
            $loanable->coowners->map(fn($coowner) => $coowner->user->id)
        );
    }

    public function testQueueMailToOwners()
    {
        \Mail::fake();

        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();

        $loanable->queueMailToOwners(
            fn(User $user) => (new Mailable())->to($user->email)
        );

        \Mail::assertQueued(
            Mailable::class,
            fn(Mailable $mailable) => $mailable->hasTo(
                $loanable->owner->user->email
            )
        );
        \Mail::assertQueued(
            Mailable::class,
            fn(Mailable $mailable) => $mailable->hasTo(
                $loanable->coowners[0]->user->email
            )
        );
    }

    public function testQueueMailToOwners_doesntSendToDeletedUsers()
    {
        \Mail::fake();

        $loanable = Loanable::factory()
            ->withCoowner()
            ->create();

        $loanable->owner->user->delete();
        $loanable->coowners[0]->user->delete();

        $loanable->queueMailToOwners(
            fn(User $user) => (new Mailable())->to($user->email)
        );

        \Mail::assertNotQueued(
            Mailable::class,
            fn(Mailable $mailable) => $mailable->hasTo(
                $loanable->owner->user->email
            )
        );
        \Mail::assertNotQueued(
            Mailable::class,
            fn(Mailable $mailable) => $mailable->hasTo(
                $loanable->coowners[0]->user->email
            )
        );
    }

    public function testForEdit_showsAllForGlobalAdmin()
    {
        $someLoanable = Loanable::factory()->create();
        $otherLoanable = Loanable::factory()->create();

        // 2 here + 3 in setup
        self::assertCount(5, Loanable::for("edit", $this->user)->get());
    }

    public function testForEdit_showsOnlyCommunityForCommunityAdmin()
    {
        $someLoanable = Loanable::factory()->create();
        $otherLoanable = Loanable::factory()->create();

        $communityAdmin = User::factory()
            ->hasAttached($someLoanable->owner->user->main_community, [
                "approved_at" => new \DateTime(),
                "role" => "admin",
            ])
            ->create();

        self::assertCount(1, Loanable::for("edit", $communityAdmin)->get());
    }

    public function testForEdit_showsNoneForNotAdmin()
    {
        $someLoanable = Loanable::factory()->create();
        $otherLoanable = Loanable::factory()->create();

        $regularUser = User::factory()
            ->hasAttached($someLoanable->owner->user->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->hasAttached($otherLoanable->owner->user->main_community, [
                "approved_at" => new \DateTime(),
            ])
            ->create();

        self::assertCount(0, Loanable::for("edit", $regularUser)->get());
    }

    public function testDelete_cancelsOngoingLoans()
    {
        $loanable = Loanable::factory()->create();

        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "loanable_id" => $loanable,
            ]);

        $loanable->delete();
        $loan->refresh();

        self::assertEquals("canceled", $loan->status);
    }
}
