<?php

namespace Tests\Unit\Models\Policies;

use App\Models\Borrower;
use App\Models\Car;
use App\Models\Community;
use App\Models\Loan;
use App\Models\Loanable;
use App\Models\Owner;
use App\Models\User;
use Carbon\Carbon;
use DateTime;
use Tests\TestCase;

class LoanPolicyTest extends TestCase
{
    private Community $community;
    private User $communityAdmin;
    private User $borrowerUser;
    private Borrower $borrower;
    private User $ownerUser;
    private User $coownerUser;
    private Loanable $loanable;
    private User $admin;
    private User $stranger;

    public function setUp(): void
    {
        parent::setUp();

        $this->withoutEvents();

        $this->community = Community::factory()
            ->withDefault10DollarsPricing()
            ->create();
        $this->communityAdmin = User::factory()->create();
        $this->communityAdmin->communities()->attach($this->community->id, [
            "approved_at" => new DateTime(),
            "role" => "admin",
        ]);

        $this->borrowerUser = User::factory()->create();
        $this->borrowerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);
        $this->borrower = Borrower::factory()->create([
            "user_id" => $this->borrowerUser->id,
        ]);

        $this->ownerUser = User::factory()->create();
        $this->ownerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);
        $owner = Owner::factory()->create([
            "user_id" => $this->ownerUser->id,
        ]);

        $this->coownerUser = User::factory()->create();
        $this->coownerUser
            ->communities()
            ->attach($this->community->id, ["approved_at" => new DateTime()]);

        $this->loanable = Loanable::factory()
            ->withCar()
            ->create([
                "owner_id" => $owner->id,
            ]);
        $this->loanable->addCoowner($this->coownerUser->id);

        $this->admin = User::factory()->create([
            "role" => "admin",
        ]);
        $this->stranger = User::factory()->create();
    }

    public function testCanView()
    {
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
                "borrower_id" => $this->borrower->id,
            ]);

        self::assertTrue($this->borrowerUser->can("view", $loan));
        self::assertTrue($this->ownerUser->can("view", $loan));
        self::assertTrue($this->coownerUser->can("view", $loan));
        self::assertTrue($this->admin->can("view", $loan));
        self::assertTrue($this->communityAdmin->can("view", $loan));

        self::assertFalse($this->stranger->can("view", $loan));
    }

    public function testCanViewInstructions()
    {
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
                "borrower_id" => $this->borrower->id,
            ]);

        self::assertTrue($this->ownerUser->can("viewInstructions", $loan));
        self::assertTrue($this->coownerUser->can("viewInstructions", $loan));
        self::assertTrue($this->admin->can("viewInstructions", $loan));
        self::assertTrue($this->communityAdmin->can("viewInstructions", $loan));

        self::assertFalse($this->borrowerUser->can("viewInstructions", $loan));
        self::assertFalse($this->stranger->can("viewInstructions", $loan));
    }

    public function testCanViewInstructionsIfBorrower_whenAccepted()
    {
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
                "borrower_id" => $this->borrower->id,
            ]);

        self::assertTrue($this->borrowerUser->can("viewInstructions", $loan));
    }

    public function testCanAccept()
    {
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->ownerUser->can("accept", $loan));
        self::assertTrue($this->coownerUser->can("accept", $loan));
        self::assertTrue($this->admin->can("accept", $loan));
        self::assertTrue($this->communityAdmin->can("accept", $loan));

        self::assertFalse($this->borrowerUser->can("accept", $loan));
        self::assertFalse($this->stranger->can("accept", $loan));
    }

    public function testCannotAcceptIfAccepted()
    {
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->ownerUser->can("accept", $loan));
        self::assertFalse($this->coownerUser->can("accept", $loan));
        self::assertFalse($this->admin->can("accept", $loan));
    }

    public function testCanReject()
    {
        $loan = Loan::factory()
            ->withInProcessIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->ownerUser->can("reject", $loan));
        self::assertTrue($this->coownerUser->can("reject", $loan));
        self::assertTrue($this->admin->can("reject", $loan));
        self::assertTrue($this->communityAdmin->can("reject", $loan));

        self::assertFalse($this->borrowerUser->can("reject", $loan));
        self::assertFalse($this->stranger->can("reject", $loan));
    }

    public function testCannotRejectIfAccepted()
    {
        $loan = Loan::factory()
            ->withCompletedIntention()
            ->create([
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->ownerUser->can("reject", $loan));
        self::assertFalse($this->coownerUser->can("reject", $loan));
        self::assertFalse($this->admin->can("reject", $loan));
    }

    public function testCannotCancelIfCanceled()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        $loan->cancel();

        self::assertFalse($this->admin->can("cancel", $loan));
    }

    public function testCannotCancelIfPaid()
    {
        $loan = Loan::factory()
            ->withCompletedPayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
                "final_price" => 10,
            ]);

        self::assertFalse($this->admin->can("cancel", $loan));
    }

    public function testCannotCancelIfOngoingAndNotFree()
    {
        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertFalse($this->borrowerUser->can("cancel", $loan));
    }

    public function testCannotCancelIfStranger()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create();

        self::assertFalse($this->stranger->can("cancel", $loan));
    }

    public function testCanCancelIfFree()
    {
        $this->community
            ->pricings()
            ->where("rule", "!=", 0)
            ->update(["rule" => 0]);

        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelInFuture()
    {
        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->addMinutes(30),
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelWhenNotTakenOver()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertTrue($this->borrowerUser->can("cancel", $loan));
        self::assertTrue($this->ownerUser->can("cancel", $loan));
    }

    public function testCanCancelIfAdmin()
    {
        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "departure_at" => Carbon::now()->subMinutes(30),
            ]);

        self::assertTrue($this->admin->can("cancel", $loan));
        self::assertTrue($this->communityAdmin->can("cancel", $loan));
    }

    public function testCanResumeIfAdmin()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();

        self::assertTrue($this->admin->can("resume", $loan));
        self::assertTrue($this->communityAdmin->can("resume", $loan));

        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfNotCancelled()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfOwnerArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();
        $loan->loanable->owner->user->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfBorrowerArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();
        $loan->borrower->user->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCannotResumeIfLoanableArchived()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();
        $loan->loanable->delete();

        self::assertFalse($this->admin->can("resume", $loan));
        self::assertFalse($this->communityAdmin->can("resume", $loan));
        self::assertFalse($this->ownerUser->can("resume", $loan));
        self::assertFalse($this->coownerUser->can("resume", $loan));
        self::assertFalse($this->borrowerUser->can("resume", $loan));
        self::assertFalse($this->stranger->can("resume", $loan));
    }

    public function testCanDeclareExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("declareExtension", $loan));
        self::assertTrue($this->communityAdmin->can("declareExtension", $loan));
        self::assertTrue($this->borrowerUser->can("declareExtension", $loan));

        self::assertFalse($this->ownerUser->can("declareExtension", $loan));
        self::assertFalse($this->coownerUser->can("declareExtension", $loan));
        self::assertFalse($this->stranger->can("declareExtension", $loan));
    }

    public function testCannotDeclareExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("declareExtension", $loan));
        self::assertFalse(
            $this->communityAdmin->can("declareExtension", $loan)
        );
        self::assertFalse($this->borrowerUser->can("declareExtension", $loan));
        self::assertFalse($this->ownerUser->can("declareExtension", $loan));
        self::assertFalse($this->coownerUser->can("declareExtension", $loan));
        self::assertFalse($this->stranger->can("declareExtension", $loan));
    }

    public function testCanAcceptExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("acceptExtension", $loan));
        self::assertTrue($this->ownerUser->can("acceptExtension", $loan));
        self::assertTrue($this->coownerUser->can("acceptExtension", $loan));
        self::assertTrue($this->communityAdmin->can("acceptExtension", $loan));

        self::assertFalse($this->borrowerUser->can("acceptExtension", $loan));
        self::assertFalse($this->stranger->can("acceptExtension", $loan));
    }

    public function testCannotAcceptExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("acceptExtension", $loan));
        self::assertFalse($this->communityAdmin->can("acceptExtension", $loan));
        self::assertFalse($this->borrowerUser->can("acceptExtension", $loan));
        self::assertFalse($this->ownerUser->can("acceptExtension", $loan));
        self::assertFalse($this->coownerUser->can("acceptExtension", $loan));
        self::assertFalse($this->stranger->can("acceptExtension", $loan));
    }

    public function testCanRejectExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("rejectExtension", $loan));
        self::assertTrue($this->ownerUser->can("rejectExtension", $loan));
        self::assertTrue($this->coownerUser->can("rejectExtension", $loan));
        self::assertTrue($this->communityAdmin->can("rejectExtension", $loan));

        self::assertFalse($this->borrowerUser->can("rejectExtension", $loan));
        self::assertFalse($this->stranger->can("rejectExtension", $loan));
    }

    public function testCannotRejectExtensionIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("rejectExtension", $loan));
        self::assertFalse($this->communityAdmin->can("rejectExtension", $loan));
        self::assertFalse($this->borrowerUser->can("rejectExtension", $loan));
        self::assertFalse($this->ownerUser->can("rejectExtension", $loan));
        self::assertFalse($this->coownerUser->can("rejectExtension", $loan));
        self::assertFalse($this->stranger->can("rejectExtension", $loan));
    }

    public function testCanCancelExtension()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("cancelExtension", $loan));
        self::assertTrue($this->communityAdmin->can("cancelExtension", $loan));
        self::assertTrue($this->borrowerUser->can("cancelExtension", $loan));

        self::assertFalse($this->ownerUser->can("cancelExtension", $loan));
        self::assertFalse($this->coownerUser->can("cancelExtension", $loan));
        self::assertFalse($this->stranger->can("cancelExtension", $loan));
    }

    public function testCanDeclareIncident()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("declareIncident", $loan));
        self::assertTrue($this->communityAdmin->can("declareIncident", $loan));
        self::assertTrue($this->borrowerUser->can("declareIncident", $loan));
        self::assertTrue($this->ownerUser->can("declareIncident", $loan));
        self::assertTrue($this->coownerUser->can("declareIncident", $loan));

        self::assertFalse($this->stranger->can("declareIncident", $loan));
    }

    public function testCanResolveIncidentIfAdmin()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertTrue($this->admin->can("resolveIncident", $loan));
        self::assertTrue($this->communityAdmin->can("resolveIncident", $loan));

        self::assertFalse($this->ownerUser->can("resolveIncident", $loan));
        self::assertFalse($this->coownerUser->can("resolveIncident", $loan));
        self::assertFalse($this->borrowerUser->can("resolveIncident", $loan));
        self::assertFalse($this->stranger->can("resolveIncident", $loan));
    }

    public function testCanChangeTakeoverInfo()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("changeTakeoverInfo", $loan));
        self::assertTrue(
            $this->communityAdmin->can("changeTakeoverInfo", $loan)
        );
        self::assertTrue($this->ownerUser->can("changeTakeoverInfo", $loan));
        self::assertTrue($this->coownerUser->can("changeTakeoverInfo", $loan));
        self::assertTrue($this->borrowerUser->can("changeTakeoverInfo", $loan));

        self::assertFalse($this->stranger->can("changeTakeoverInfo", $loan));
    }

    public function testCannotChangeTakeoverInfoIfTakeoverNotOngoingUnlessAdmin()
    {
        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("changeTakeoverInfo", $loan));
        self::assertTrue(
            $this->communityAdmin->can("changeTakeoverInfo", $loan)
        );

        self::assertFalse($this->ownerUser->can("changeTakeoverInfo", $loan));
        self::assertFalse($this->coownerUser->can("changeTakeoverInfo", $loan));
        self::assertFalse(
            $this->borrowerUser->can("changeTakeoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("changeTakeoverInfo", $loan));
    }

    public function testCannotChangeTakeoverInfoIfLoanNotOngoing()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("changeTakeoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("changeTakeoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("changeTakeoverInfo", $loan));
        self::assertFalse($this->coownerUser->can("changeTakeoverInfo", $loan));
        self::assertFalse(
            $this->borrowerUser->can("changeTakeoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("changeTakeoverInfo", $loan));
    }

    public function testCanContestTakeoverInfo()
    {
        $loan = Loan::factory()
            ->withCompletedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("contestTakeoverInfo", $loan));
        self::assertTrue($this->ownerUser->can("contestTakeoverInfo", $loan));
        self::assertTrue($this->coownerUser->can("contestTakeoverInfo", $loan));
        self::assertTrue(
            $this->borrowerUser->can("contestTakeoverInfo", $loan)
        );

        self::assertTrue(
            $this->communityAdmin->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestTakeoverInfo", $loan));
    }

    public function testCannotContestTakeoverInfoIfTakeoverNotCompleted()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("contestTakeoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("contestTakeoverInfo", $loan));
        self::assertFalse(
            $this->coownerUser->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestTakeoverInfo", $loan));
    }

    public function testCannotContestTakeoverInfoIfLoanNotOngoing()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("contestTakeoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("contestTakeoverInfo", $loan));
        self::assertFalse(
            $this->coownerUser->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("contestTakeoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestTakeoverInfo", $loan));
    }

    public function testCanChangeHandoverInfo()
    {
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("changeHandoverInfo", $loan));
        self::assertTrue(
            $this->communityAdmin->can("changeHandoverInfo", $loan)
        );
        self::assertTrue($this->ownerUser->can("changeHandoverInfo", $loan));
        self::assertTrue($this->coownerUser->can("changeHandoverInfo", $loan));
        self::assertTrue($this->borrowerUser->can("changeHandoverInfo", $loan));

        self::assertFalse($this->stranger->can("changeHandoverInfo", $loan));
    }

    public function testCannotChangeHandoverInfoIfHandoverNotOngoingUnlessAdmin()
    {
        $loan = Loan::factory()
            ->withCompletedHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("changeHandoverInfo", $loan));
        self::assertTrue(
            $this->communityAdmin->can("changeHandoverInfo", $loan)
        );

        self::assertFalse($this->ownerUser->can("changeHandoverInfo", $loan));
        self::assertFalse($this->coownerUser->can("changeHandoverInfo", $loan));
        self::assertFalse(
            $this->borrowerUser->can("changeHandoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("changeHandoverInfo", $loan));
    }

    public function testCannotChangeHandoverInfoIfLoanNotOngoing()
    {
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("changeHandoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("changeHandoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("changeHandoverInfo", $loan));
        self::assertFalse($this->coownerUser->can("changeHandoverInfo", $loan));
        self::assertFalse(
            $this->borrowerUser->can("changeHandoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("changeHandoverInfo", $loan));
    }

    public function testCanContestHandoverInfo()
    {
        $loan = Loan::factory()
            ->withCompletedHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("contestHandoverInfo", $loan));
        self::assertTrue($this->ownerUser->can("contestHandoverInfo", $loan));
        self::assertTrue($this->coownerUser->can("contestHandoverInfo", $loan));
        self::assertTrue(
            $this->borrowerUser->can("contestHandoverInfo", $loan)
        );

        self::assertTrue(
            $this->communityAdmin->can("contestHandoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestHandoverInfo", $loan));
    }

    public function testCannotContestHandoverInfoIfHandoverNotCompleted()
    {
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("contestHandoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("contestHandoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("contestHandoverInfo", $loan));
        self::assertFalse(
            $this->coownerUser->can("contestHandoverInfo", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("contestHandoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestHandoverInfo", $loan));
    }

    public function testCannotContestHandoverInfoIfLoanNotOngoing()
    {
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $loan->cancel();

        self::assertFalse($this->admin->can("contestHandoverInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("contestHandoverInfo", $loan)
        );
        self::assertFalse($this->ownerUser->can("contestHandoverInfo", $loan));
        self::assertFalse(
            $this->coownerUser->can("contestHandoverInfo", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("contestHandoverInfo", $loan)
        );
        self::assertFalse($this->stranger->can("contestHandoverInfo", $loan));
    }

    public function testCanValidateLoanInfo()
    {
        $loan = Loan::factory()
            ->withCompletedHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("validateLoanInfo", $loan));
        self::assertTrue($this->ownerUser->can("validateLoanInfo", $loan));
        self::assertTrue($this->coownerUser->can("validateLoanInfo", $loan));
        self::assertTrue($this->borrowerUser->can("validateLoanInfo", $loan));

        self::assertFalse(
            $this->communityAdmin->can("validateLoanInfo", $loan)
        );
        self::assertFalse($this->stranger->can("validateLoanInfo", $loan));
    }

    public function testCannotValidateLoanInfoIfHandoverNotCompleted()
    {
        $loan = Loan::factory()
            ->withInProcessHandover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("validateLoanInfo", $loan));
        self::assertFalse($this->ownerUser->can("validateLoanInfo", $loan));
        self::assertFalse($this->coownerUser->can("validateLoanInfo", $loan));
        self::assertFalse($this->borrowerUser->can("validateLoanInfo", $loan));
        self::assertFalse(
            $this->communityAdmin->can("validateLoanInfo", $loan)
        );
        self::assertFalse($this->stranger->can("validateLoanInfo", $loan));
    }

    public function testCanResolveContestedLoanIfAdmin()
    {
        $loan = Loan::factory()
            ->withContestedTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("resolveContestedLoan", $loan));
        self::assertTrue(
            $this->communityAdmin->can("resolveContestedLoan", $loan)
        );

        self::assertFalse($this->ownerUser->can("resolveContestedLoan", $loan));
        self::assertFalse(
            $this->coownerUser->can("resolveContestedLoan", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("resolveContestedLoan", $loan)
        );
        self::assertFalse($this->stranger->can("resolveContestedLoan", $loan));
    }

    public function testCannotResolveContestedIfNotContested()
    {
        $loan = Loan::factory()
            ->withInProcessTakeover()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("resolveContestedLoan", $loan));
        self::assertFalse(
            $this->communityAdmin->can("resolveContestedLoan", $loan)
        );
        self::assertFalse($this->ownerUser->can("resolveContestedLoan", $loan));
        self::assertFalse(
            $this->coownerUser->can("resolveContestedLoan", $loan)
        );
        self::assertFalse(
            $this->borrowerUser->can("resolveContestedLoan", $loan)
        );
        self::assertFalse($this->stranger->can("resolveContestedLoan", $loan));
    }

    public function testCanPrepay()
    {
        $loan = Loan::factory()
            ->withInProcessPrePayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertTrue($this->admin->can("prepay", $loan));
        self::assertTrue($this->communityAdmin->can("prepay", $loan));
        self::assertTrue($this->borrowerUser->can("prepay", $loan));

        self::assertFalse($this->ownerUser->can("prepay", $loan));
        self::assertFalse($this->coownerUser->can("prepay", $loan));
        self::assertFalse($this->stranger->can("prepay", $loan));
    }

    public function testCannotPrepayIfNotOngoing()
    {
        $loan = Loan::factory()
            ->withCompletedPrePayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        self::assertFalse($this->admin->can("prepay", $loan));
        self::assertFalse($this->communityAdmin->can("prepay", $loan));
        self::assertFalse($this->borrowerUser->can("prepay", $loan));
        self::assertFalse($this->ownerUser->can("prepay", $loan));
        self::assertFalse($this->coownerUser->can("prepay", $loan));
        self::assertFalse($this->stranger->can("prepay", $loan));
    }

    public function testCanPay()
    {
        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "owner_validated_at" => Carbon::now(),
                "borrower_validated_at" => Carbon::now(),
            ]);

        $this->borrowerUser->balance = 100000;
        $this->borrowerUser->save();

        self::assertTrue($this->admin->can("pay", $loan));
        self::assertTrue($this->communityAdmin->can("pay", $loan));
        self::assertTrue($this->borrowerUser->can("pay", $loan));
        self::assertTrue($this->ownerUser->can("pay", $loan));
        self::assertTrue($this->coownerUser->can("pay", $loan));

        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotOngoing()
    {
        $loan = Loan::factory()->create([
            "borrower_id" => $this->borrower->id,
            "loanable_id" => $this->loanable->id,
        ]);

        self::assertFalse($this->admin->can("pay", $loan));
        self::assertFalse($this->communityAdmin->can("pay", $loan));
        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotValidated()
    {
        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
            ]);

        $this->borrowerUser->balance = 100000;
        $this->borrowerUser->save();

        self::assertTrue($this->admin->can("pay", $loan));
        self::assertTrue($this->communityAdmin->can("pay", $loan));

        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }

    public function testCannotPayIfNotEnoughFunds()
    {
        $loan = Loan::factory()
            ->withInProcessPayment()
            ->create([
                "borrower_id" => $this->borrower->id,
                "loanable_id" => $this->loanable->id,
                "owner_validated_at" => Carbon::now(),
                "borrower_validated_at" => Carbon::now(),
            ]);

        $this->borrowerUser->balance = 0;
        $this->borrowerUser->save();

        self::assertFalse($this->admin->can("pay", $loan));
        self::assertFalse($this->communityAdmin->can("pay", $loan));
        self::assertFalse($this->borrowerUser->can("pay", $loan));
        self::assertFalse($this->ownerUser->can("pay", $loan));
        self::assertFalse($this->coownerUser->can("pay", $loan));
        self::assertFalse($this->stranger->can("pay", $loan));
    }
}
