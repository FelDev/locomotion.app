<?php

namespace Tests\Unit\Rules;

use App\Rules\PricingRule;

use Tests\TestCase;

class PricingRuleTest extends TestCase
{
    public function testPassesEmpty()
    {
        $rule = new PricingRule();

        $this->assertTrue($rule->passes("pricing_rule", null));
        $this->assertTrue($rule->passes("pricing_rule", ""));
    }

    public function testPasses_AllSymbols()
    {
        $rule = new PricingRule();

        $this->assertTrue(
            $rule->passes(
                "pricing_rule",
                // Just a cheat so as to write the test string over multiple lines.
                str_replace(
                    ["\r", "\n"],
                    "",
                    <<<RULE
  km + minutes
  + (loanable.engine == 'hybrid')
  + (loanable.pricing_category != 'small')
  + (loanable.year_of_circulation >= 2000)
  + (loanable.type == 'car')
  + loanable.daily_premium_from_value_category

  + loan.days

  + (loan.start.year - 1999)
  + loan.start.month
  + loan.start.day
  + loan.start.hour
  + loan.start.minute
  + loan.start.day_of_year
  + (loan.start.year_eight_months_ago - 2005)

  + (loan.end.year - 1999)
  + loan.end.month
  + loan.end.day
  + loan.end.hour
  + loan.end.minute
  + loan.end.day_of_year
  + (loan.end.year_eight_months_ago - 2005)
RULE
                )
            )
        );
    }

    public function testFails_UnknownSymbol()
    {
        $rule = new PricingRule();

        $this->assertFalse($rule->passes("pricing_rule", "unknownSymbol"));
    }

    public function testFails_UnmatchingSiAlors()
    {
        $rule = new PricingRule();

        $this->assertFalse(
            $rule->passes(
                "pricing_rule",
                // Just a cheat so as to write the test string over multiple lines.
                str_replace(
                    ["\r", "\n"],
                    "",
                    "SI (1 != 2) ALORS (SI (2 == 2) ALORS 1)"
                )
            )
        );
    }

    public function testMessage()
    {
        $rule = new PricingRule();

        $this->assertNotEmpty($rule->message());
    }
}
